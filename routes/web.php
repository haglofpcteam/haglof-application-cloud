<?php

use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ################### Start

Route::get('/subscription/new','WordpressController@new_subscription')->name('new_subscription');
Route::post('/sdfg6fd8g6d8f6g8df6g8df6g8df6g86dfg786as/subscription/send_email','WordpressController@new_order_send_mail');

Route::get('/subscription/connect/{order}', function (Request $request) {
    if (!$request->hasValidSignature()) {
        abort(401);
    }
    return view('app');  
    
})->name('subscription_connect');

Route::get('/verify', function (Request $request) {
    if($request->id) {
        if (!$request->hasValidSignature()) {
            abort(401);
        }
        else {
            $user=User::find($request->id);
            $user->email_verified_at = now();
            $user->save();
            return redirect('/organizations');
        }
    }
    return view('app');  
    
})->name('verificationapi.verify');


Route::get('/.well-known/assetlinks.json', function() {

    return response(Storage::get('assetlinks.json'), 200)->header('Content-Type', 'application/json');

});

Route::any('/hagloflink/auth/apple/callback', function (Request $request) {
    $uri = "hagloflink://auth/callback?code=" . $request->code;
    return view('authentication_complete')->with('back_to_link_url',$uri);
});

Route::get('/hagloflink/auth/{provider}/callback', function (Request $request) {
    return "";
    $qstring = $request->getQueryString();
    $uri = "hagloflink://auth/callback?" . $qstring;
    return redirect($uri);
});


Route::get('/mobileauth/{provider}', 'MobileAuthController@get_token');
Route::get('/mobilelogin/{provider}', 'MobileAuthController@login');

Route::middleware('signed')->get('/activate', function (Request $request) {
    return view('app');
})->name('activate');

Route::middleware('signed')->get('/reset', function (Request $request) {
    return view('app');
})->name('reset');

Route::any('{any}', function () {
    return view('app');
})->where('any', '.*')->name('app');
