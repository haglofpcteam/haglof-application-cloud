<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Model\Wordpress\Order;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

*/



Route::post('users', 'Api\UserController@store');
Route::post('users/{user}/activate', 'Api\UserController@activate');
Route::post('users/reset', 'Api\UserController@resetPassword');

Route::middleware(['verified:api','activated'])->group(function () {
    Route::apiResource('organizations', 'Api\OrganizationController');
    Route::post('organizations/{organization}/invite', 'Api\OrganizationController@invite');
    Route::apiResource('organizations.users', 'Api\OrganizationUserController');
    Route::apiResource('organizations.application_instances', 'Api\OrganizationApplicationInstanceController');
    Route::apiResource('application_instances', 'Api\ApplicationInstanceController')->except(['store']);
    Route::post('application_instances/{application_instance}/invite', 'Api\ApplicationInstanceController@invite');
    Route::delete('application_instances/{application_instance}/users/{user}', 'Api\ApplicationInstanceController@remove_user');
    Route::post('application_instances/{application_instance}/restore', 'Api\ApplicationInstanceController@restore');
    Route::apiResource('applications','Api\ApplicationController')->except(['store','update', 'destroy']);
    Route::apiResource('mdii_applications','Api\MDIIApplicationController')->except(['update','destroy','store']);
    Route::apiResource('mdii_folders','Api\MDIIFolderController');
    Route::apiResource('mdii_files','Api\MDIIFileController');
    Route::apiResource('application_instances.file_datas', 'Api\FileDataController');
    Route::get('reports/templates/{application_instance}/list/{key}','Api\ReportTemplateController@index');
    Route::get('reports/templates/list/','Api\ReportTemplateController@index');
    Route::get('reports/templates/{application_instance}/{reportTemplate}','Api\ReportTemplateController@show');
    Route::get('reports/templates/{reportTemplate}','Api\ReportTemplateController@show');
    Route::put('reports/templates/{reportTemplate}','Api\ReportTemplateController@update');
    Route::post('reports/templates/','Api\ReportTemplateController@store');
    Route::delete('reports/templates/{reportTemplate}','Api\ReportTemplateController@destroy');
    Route::apiResource('reports','Api\ReportController')->except(['index','update']);
    Route::apiResource('reports/templates.application_instances', 'Api\ReportTemplateApplicationInstanceController')->except(['index','update','show']);
    Route::apiResource('localizations/stimulsoft', 'Api\StimulsoftLocalizationController')->except(['update','destroy','store']);
    Route::get('subscription/connect/{order}/{organization}/{user}','WordpressController@connect_subscription')->name('connect_subscription');
    Route::get('order/{order}',function (Order $order) {
        return new App\Http\Resources\Order($order);
    });
});

Route::group(['middleware' => 'auth:api'], function () {
    Route::get('email/resend', 'Api\VerificationApiController@resend')->name('verificationapi.resend');
    Route::get('user','Api\UserController@current_user');
    Route::apiResource('users', 'Api\UserController')->except(['store']);
    Route::apiResource('users.social_providers','Api\SocialiteProviderController')->except(['update','show']);
});

Route::middleware(['verified:api','activated'])->group(function () {
    Route::apiResource('application_instances.pile_applications', 'Api\PileApplicationController')->except(['store','update']);
    Route::apiResource('application_instances.pile_applications.pile_application_sites','Api\PileApplicationSiteController');
    Route::apiResource('application_instances.pile_applications.pile_application_sites.pile_application_locations','Api\PileApplicationLocationController');
    Route::apiResource('application_instances.pile_applications.pile_application_sites.pile_application_locations.pile_application_measurements','Api\PileApplicationMeasurementController');
    Route::apiResource('application_instances.pile_applications.pile_application_sites.pile_application_locations.pile_application_measurements.pile_application_targets','Api\PileApplicationTargetController')->except(['store']);
    Route::get('application_instances/{application_instance}/pile_applications/{pile_application}/pile_application_sites/{pile_application_site}/pile_application_locations/{pile_application_location}/pile_application_measurements/{pile_application_measurement}/data','Api\PileApplicationMeasurementController@show_data');
    Route::get('application_instances/{application_instance}/pile_applications/{pile_application}/pile_application_sites/{pile_application_site}/pile_application_locations/{pile_application_location}/pile_application_measurements/{pile_application_measurement}/snapshot','Api\PileApplicationMeasurementController@show_snapshot');
    Route::post('application_instances/{application_instance}/pile_applications/{pile_application}/pile_application_sites/{pile_application_site}/pile_application_locations/{pile_application_location}/pile_application_measurements/{pile_application_measurement}/snapshot','Api\PileApplicationMeasurementController@update_snapshot');
});

Route::middleware(['verified:api','activated'])->group(function () {
    Route::get('application_instances/{application_instance}/dc_applications/{dc_application}/files/{file}/stands/{stand}/merge/{stand2}', 'Api\MDIIStandController@merge');
    Route::get('application_instances/{application_instance}/dc_applications/{dc_application}/files/{file}/stands/{stand}/full', 'Api\MDIIStandController@show_full');
    Route::apiResource('application_instances.dc_applications', 'Api\MDIIApplicationController')->except(['store']);
    Route::apiResource('application_instances.dc_applications.folders', 'Api\MDIIFolderController');
    Route::get('application_instances/{application_instance}/dc_applications/{dc_application}/folders/{folder}/stands', 'Api\MDIIFolderController@show_stands');
    Route::apiResource('application_instances.dc_applications.files', 'Api\MDIIFileController');
    Route::apiResource('application_instances.dc_applications.files.stands', 'Api\MDIIStandController');
    Route::apiResource('application_instances.dc_applications.files.stands.plots', 'Api\MDIIPlotController');
    Route::apiResource('application_instances.dc_applications.files.stands.trees', 'Api\MDIITreeController');
    Route::apiResource('application_instances.dc_applications.species', 'Api\MDIISpeciesController');
    Route::apiResource('application_instances.dc_applications.qualities', 'Api\MDIIQualityController');
    Route::get('application_instances/{application_instance}/dc_applications/{dc_application}/qualities/{quality1}/merge/{quality2}', 'Api\MDIIQualityController@merge');
    Route::get('application_instances/{application_instance}/dc_applications/{dc_application}/species/{species1}/merge/{species2}', 'Api\MDIISpeciesController@merge');
});

