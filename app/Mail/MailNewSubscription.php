<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;

use App\Model\Wordpress\Order;

class MailNewSubscription extends Mailable
{
    use SerializesModels;

    protected $order;
    protected $reminder;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Order $order,$reminder=false)
    {
        $this->order=$order;
        $this->reminder=$reminder;
    }

    public function logToFile($string) {
        if(config('app.debug')) {
            Storage::append('debug.log', $string . "\n");
        }       
    } 

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $order_id = $this->order->ID;
        $user_id = $this->order->userId;
        $message = __('Hi, you recently ordered a subscription from us. Please click on the button below to connect
        your subscription with your organization');
        $subject = 'New Subscription on Haglof.app';

        if($this->reminder) {
            $message = __('Hi, you recently ordered a subscription from us but it is still not connected with any organization. 
            Please click on the button below to connect your subscription with your application cloud account');
            $subject = 'Subscription not connected on Haglof.app';
        }

        return $this->view('emails.new_subscription')
            ->subject($subject)
            ->with('text',$message)
            ->with('order_id',$order_id)
            ->with('user_id',$user_id)
            ->with('signed_route',URL::signedRoute('subscription_connect',['order' => $this->order]));

    }

}
