<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Model\User;

class MailInvite extends Mailable
{
    use SerializesModels;

    protected $message;
    protected $usermessage;
    protected $invitee;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $invitee,$message,$usermessage)
    {
        $this->message=$message;
        $this->usermessage=$usermessage;
        $this->invitee=$invitee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->text('emails.invite_plain')
            ->subject('You have been invited to Haglof.app')
            ->view('emails.invite')
            ->with('info',$this->message)
            ->with('invitee',$this->invitee)
            ->with('usermessage',$this->usermessage);
    }
}
