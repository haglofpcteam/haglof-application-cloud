<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailNotification extends Mailable
{
    use SerializesModels;

    protected $text;
    protected $usermessage;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($text,$usermessage)
    {
        $this->text=$text;
        $this->usermessage=$usermessage;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->text('emails.notification_plain')
            ->view('emails.notification')
            ->subject('Notification from Haglof.app')
            ->with('text',$this->text)
            ->with('usermessage',$this->usermessage);
    }

}
