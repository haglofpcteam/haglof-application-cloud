<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class SocialiteProvider extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','provider', 'provider_id'
    ];

    public function identifiableName()
    {
        return "provider: " . $this->provider . " id: " . $this->provider_id;
    }

    public function user() {
        return $this->belongsTo(User::class);
    }
}
