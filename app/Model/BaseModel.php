<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

abstract class BaseModel extends Model
{

    //enable this if you want use methods that gets information about creating
    protected $revisionCreationsEnabled = true;


    public function logToFile($string) {
        if(config('app.debug')) {
            Storage::append('debug.log', $string . "\n");
        }       
    } 

}
