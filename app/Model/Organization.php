<?php

namespace App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

use Illuminate\Support\Facades\Auth;

class Organization extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    public function identifiableName()
    {
        return $this->name;
    }

    protected $fillable = [
        'name'
    ];

    public static function boot ()
    {
        parent::boot();

        self::deleting(function (Organization $organization) {
             $application_instances = $organization->applicationInstances()->get();

             foreach($application_instances as $application_instance) {
                 $application_instance->delete();
             }
        });

    }

    public function createApplicationInstances() {
        
        $organization = $this;

        $applications = Application::where('public',1)->get();

        foreach($applications as $application) {

            $applicationInstance = ApplicationInstance::create([
                'organization_id' => $organization->id,
                'application_id' => $application->id,
                'number_of_users' => 0,
                'paid_until' => null
            ]);

            $user = $organization->users()->first();

            $user->applicationInstances()->save($applicationInstance, ['role' => 'admin']);
        }
    }

    public function users() {
        return $this->belongsToMany(User::class);
    }


    public function applicationInstances() {
        return $this->hasMany(ApplicationInstance::class);
    }

    public function getAdminAttribute() {
        return $this->users()->where('id',Auth::id())->first() ? true : false;
    }

    public function getUserAttribute() {
        if($this->admin) {
            return true;
        }
        $applicationInstanceIndexes = auth()->user()->applicationInstances()->select('id')->get();

        if($this->applicationInstances()->whereIn('id',$applicationInstanceIndexes)->first()) {
            return true;
        }

        return false;
    }
}
