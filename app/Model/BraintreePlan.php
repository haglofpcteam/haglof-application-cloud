<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BraintreePlan extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['application_id', 'number_of_users', 'braintree_id', 'merchantId', 'billingDayOfMonth','billingFrequency', 'currencyIsoCode', 'description', 'name'
    , 'numberOfBillingCycles', 'price', 'trialDuration', 'trialDurationUnit', 'trialPeriod'];

    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    public function getFrequencyAttribute() {
        $years = (int)$this->billingFrequency/12;
        $months = (int)$this->billingFrequency%12;

        $str = "";

        switch($years) {
            case 0:
                $str = "";
                break;
            case 1:
                $str = __("1 year ");
                break;
            default:
                $str = $years . __("years ");
                break;
        }

        switch($months) {
            case 0:
                break;
            case 1:
                $str = $str . __("1 month ");
                break;
            default:
                $str = $months . __("months ");
                break;
        }

        return $str;
    }


}
