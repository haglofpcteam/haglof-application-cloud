<?php

namespace App\Model;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Laravel\Passport\HasApiTokens;
use Schedula\Laravel\PassportSocialite\User\UserSocialAccount;
use App\Notifications\VerifyApiEmail;
use App\Notifications\ActivateAccountEmail;
use App\Notifications\ResetPasswordEmail;


class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;
    use HasApiTokens;

    protected $dates = ['deleted_at'];

    public static function boot ()
    {
        parent::boot();

        self::created(function (User $user) {

                $organization = Organization::create([
                    'name' => 'My Organization'
                ]);

                $user->organizations()->save($organization, ['role' => 'admin']);

                $organization->createApplicationInstances();
            }
        );
    }

    public function identifiableName()
    {
        if(!$this->name) {
            return "<" . $this->email . ">";
        }
        return $this->name;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'email_verified_at',
        'activated',
        'activation_token',
        'role',
        'timezone',
        'measurement_system',
        'date_format',
        'time_format',
        'settings',

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token','activation_token'
    ];

    public function organizations() {
        return $this->belongsToMany(Organization::class);
    }

    public function applicationInstances() {
        return $this->belongsToMany(ApplicationInstance::class)->withPivot('first_run');
    }

    public function revisions()
    {
        return $this->hasMany(\Venturecraft\Revisionable\Revision::class);
    }

    public function socialiteProviders()
    {
        return $this->hasMany(SocialiteProvider::class);
    }

    public function getSetting($key,$default) {

        $settings=json_decode($this->settings);
        if(isset($settings[$key])) {
            return $settings[$key];
        }
        return $default;
    }

    public function saveSetting($key,$value) {
        $settings=json_decode($this->settings);
        $settings[$key]=$value;
        $this->settings=json_encode($settings);
        $this->save();
    }

    public function sendApiEmailVerificationNotification()
    {
        $this->notify(new VerifyApiEmail);
    }

    public function sendResetPassowordNotification()
    {
        $this->notify(new ResetPasswordEmail);
    }

    public function sendAccountActivationNotification($message)
    {
        $activationEmail = new ActivateAccountEmail;
        $activationEmail->usermessage=$message;
        $this->notify($activationEmail);
    }
}

