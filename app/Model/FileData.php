<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class FileData extends BaseModel
{
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'file_datas';

    protected $dates = ['deleted_at'];

    protected $fillable = ['mime_type', 'data', 'application_instance_id', 'tag','user_id','name'];
}
