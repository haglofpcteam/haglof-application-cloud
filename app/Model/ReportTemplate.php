<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportTemplate extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'key',
        'standard',
        'premium', 
        'test_data', 
        'template',  
        'data_type',
        'javascript'
    ];

    protected $dates = ['deleted_at'];

    protected $guarded = ['id'];

    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    public function applicationInstances() {
        return $this->belongsToMany(ApplicationInstance::class);
    }

    public function parameters() {
        return $this->hasMany(ReportTemplateParameter::class);
    }
}
