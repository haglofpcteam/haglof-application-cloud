<?php

namespace App\Model\Wordpress;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $connection = 'wordpress';
    protected $table = 'wpuu_posts';
    protected $primaryKey = 'ID';

    public function children() {
        return $this->hasMany(Post::class,'post_parent');
    }

    public function meta() {
        return $this->hasMany(PostMeta::class,'post_id');
    }

    public function parent() {
        return $this->hasOne(Post::class,'post_parent');
    } 

    public function subscriptions() {
        return $this->hasMany(Subscription::class,'post_parent')->where('post_type','shop_subscription');
    }

    public function orderItems() {
        return $this->hasMany(OrderItem::class,'order_id');
    }

    public function getEmailAttribute() {
        return $this->meta()->where('meta_key','_billing_email')->first()->meta_value;
    }

    public function getUserIdAttribute() {
        return $this->meta()->where('meta_key','_customer_user')->first()->meta_value;
    }
    
}