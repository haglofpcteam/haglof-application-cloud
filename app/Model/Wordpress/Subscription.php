<?php

namespace App\Model\Wordpress;

use App\Model\ApplicationInstance;
use App\Model\Application;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Subscription extends Model
{
    protected $connection = 'wordpress';
    protected $table = 'wpuu_posts';
    protected $primaryKey = 'ID';

    public function children() {
        return $this->hasMany(Post::class,'post_parent');
    }

    public function meta() {
        return $this->hasMany(PostMeta::class,'post_id');
    }

    public function parent() {
        return $this->hasOne(Post::class,'post_parent');
    } 

    public function order() {
        return $this->hasOne(Order::class,'post_parent');
    } 

    public function orderItems() {
        return $this->hasMany(OrderItem::class,'order_id');
    }

    public function getQtyAttribute() {
        return $this->orderItems()->where('order_item_type','line_item')->first()->meta()->where('meta_key','_qty')->first()->meta_value;
    }

    public function getProductIdAttribute() {
        $orderItem = OrderItem::where('order_id',$this->ID)->where('order_item_type','line_item')->first();

        if($orderItem!=null) {
            $meta=$orderItem->meta()->where('meta_key','_product_id')->first();
            if($meta) {
                return $meta->meta_value;
            }
        }

        return null;
    }

    public function getApplicationAttribute() {
        return Application::where('product_id',$this->productId)->first();
    }

    public function getIsActiveAttribute() {
        return $this->post_status=='wc-active';
    }

    public function getUserIdAttribute() {
        return $this->meta()->where('meta_key','_customer_user')->meta_value;
    }

    public function getApplicationInstanceAttribute() {
        $temp = DB::table('application_instances_subscription')->where('subscription_id',$this->ID)->first();

        if($temp) {
            $id=$temp->application_instance_id;
            return ApplicationInstance::where('id',$id)->first();
        }
        return null;
        
    }
}