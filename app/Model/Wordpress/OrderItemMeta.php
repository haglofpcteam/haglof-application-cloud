<?php

namespace App\Model\Wordpress;

use Illuminate\Database\Eloquent\Model;

class OrderItemMeta extends Model
{
    protected $connection = 'wordpress';
    protected $table = 'wpuu_woocommerce_order_itemmeta';
    protected $primaryKey = 'meta_id';

    public function orderItem() {
        return $this->hasOne(OrderItem::class,'order_item_id');
    }    
}