<?php

namespace App\Model\Wordpress;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
    protected $connection = 'wordpress';
    protected $table = 'wpuu_postmeta';
    protected $primaryKey = 'ID';

    public function post() {
        return $this->hasOne(Post::class,'post_id');
    }    
}