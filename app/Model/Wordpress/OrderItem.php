<?php

namespace App\Model\Wordpress;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $connection = 'wordpress';
    protected $table = 'wpuu_woocommerce_order_items';
    protected $primaryKey = 'order_item_id';

    public function order() {
        return $this->hasOne(Order::class,'order_id');
    }  
    
    public function meta() {
        return $this->hasMany(OrderItemMeta::class,'order_item_id');
    }
}