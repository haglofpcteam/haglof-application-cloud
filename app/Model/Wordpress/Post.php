<?php

namespace App\Model\Wordpress;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $connection = 'wordpress';
    protected $table = 'wpuu_posts';
    protected $primaryKey = 'ID';

    public function children() {
        return $this->hasMany(Post::class,'post_parent');
    }

    public function meta() {
        return $this->hasMany(PostMeta::class,'post_id');
    }

    public function parent() {
        return $this->hasOne(Post::class,'post_parent');
    } 
    
}