<?php

namespace App\Model\MDII;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FormatsDate;

class MDIIPlot extends BaseModel
{
    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'm_d_i_i_stand_id','name','number'
    ];

    public function trees() {
        return $this->hasMany(MDIITree::class);
    }

    public function stand()
    {
        return $this->belongsTo(MDIIStand::class,'m_d_i_i_stand_id');
    }

    public function getAdminAttribute() {
        return $this->stand()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->stand()->first()->user;
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }

        if($fromTime!=null) {
            $trees=$this->trees()->withTrashed()->where('deleted_at','>',$fromTime)->get();
        }
        else {
            $trees=$this->trees()->withTrashed()->get();
        }

        foreach($trees as $tree) {
            $tree->restoreRecursive($fromTime);
        }
    }
}
