<?php

namespace App\Model\MDII;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FormatsDate;

class MDIIStand extends BaseModel
{
    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'm_d_i_i_file_id','name','number','log_list','settings'
    ];

    public static function boot ()
    {
        parent::boot();

        self::created(function (MDIIStand $stand) {

            $file = $stand->file()->first();
            $application = null;

            if($file) {
                $application = $file->mdii_application()->first();
            }

            if($application) {
                $stand->settings=$application->stand_default_settings;
                $stand->save();
            }

        });
    }

    public function plots() {
        return $this->hasMany(MDIIPlot::class);
    }

    public function trees() {
        return $this->hasManyThrough(MDIITree::class,MDIIPlot::class);
    }

    public function species() {
        return MDIISpecie::whereIn('id',$this->trees()->pluck('m_d_i_i_specie_id')->toArray());
    }

    public function qualities() {
        return MDIIQuality::whereIn('id',$this->trees()->pluck('m_d_i_i_quality_id')->toArray());
    }

    public function file()
    {
        return $this->belongsTo(MDIIFile::class,'m_d_i_i_file_id');
    }

    public function getSetting($key,$default) {

        $settings=json_decode($this->settings,true);
        if(isset($settings[$key])) {
            return $settings[$key];
        }
        return $default;
    }

    public function saveSetting($key,$value) {
        $settings=json_decode($this->settings,true);
        if($value=="true") {
            $settings[$key]=true;
        }
        elseif($value=="false") {
            $settings[$key]=false;
        }
        else {
            $settings[$key]=$value;
        }
        
        $this->settings=json_encode($settings);
        $this->save();
    }

    public function getAdminAttribute() {
        return $this->file()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->file()->first()->user;
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }

        if($fromTime!=null) {
            $plots=$this->plots()->withTrashed()->where('deleted_at','>',$fromTime)->get();
            $trees=$this->trees()->withTrashed()->where('deleted_at','>',$fromTime)->get();
            $species=$this->species()->withTrashed()->where('deleted_at','>',$fromTime)->get();
            $qualities=$this->qualities()->withTrashed()->where('deleted_at','>',$fromTime)->get();
        }
        else {
            $plots=$this->plots()->withTrashed()->get();
            $trees=$this->trees()->withTrashed()->get();
            $species=$this->species()->withTrashed()->get();
            $qualities=$this->qualities()->withTrashed()->get();
        }

        foreach($plots as $plot) {
            $plot->restoreRecursive($fromTime);
        }

        foreach($trees as $tree) {
            $tree->restoreRecursive($fromTime);
        }

        foreach($species as $s) {
            $s->restore();
        }

        foreach($qualities as $quality) {
            $quality->restore();
        }

    }
}
