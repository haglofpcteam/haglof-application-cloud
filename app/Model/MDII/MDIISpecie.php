<?php

namespace App\Model\MDII;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FormatsDate;

class MDIISpecie extends BaseModel
{
    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'm_d_i_i_application_id','specie_text','specie_nr','form_height','form_factor'
    ];

    public function trees() {
        return $this->hasMany(MDIITree::class,'m_d_i_i_specie_id');
    }

    public function mdii_application()
    {
        return $this->belongsTo(MDIIApplication::class,'m_d_i_i_application_id');
    }

    public function getAdminAttribute() {
        return $this->trees()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->trees()->first()->user;
    }
}
