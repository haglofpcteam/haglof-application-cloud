<?php

namespace App\Model\MDII;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FormatsDate;

class MDIIFolder extends BaseModel
{
    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'm_d_i_i_application_id','m_d_i_i_folder_id','name','root'
    ];

    public static function boot ()
    {
        parent::boot();

        self::deleting(function ($mdiiapplication) {
            $mdiiapplication->folders()->delete();
            $mdiiapplication->files()->delete();
        });

    }

    public function folders() {
        return $this->hasMany(MDIIFolder::class);
    }

    public function files() {
        return $this->hasMany(MDIIFile::class);
    }

    public function stands() {
        return $this->hasManyThrough(MDIIStand::class, MDIIFile::class);
    }

    public function folder()
    {
        return $this->belongsTo(MDIIFolder::class,'m_d_i_i_folder_id');
    }

    public function mdii_application()
    {
        return $this->belongsTo(MDIIApplication::class,'m_d_i_i_application_id');
    }

    public function getAdminAttribute() {
        return $this->mdii_application()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->mdii_application()->first()->user;
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }

        if($fromTime==null) {
            $files=$this->files()->withTrashed()->get();
            $folders=$this->folders()->withTrashed()->get();
        }
        else {
            $folders=$this->folders()->withTrashed()->where('deleted_at','>',$fromTime)->get();
            $files=$this->files()->withTrashed()->where('deleted_at','>',$fromTime)->get();
        }

        foreach($folders as $folder) {
            $folder->restoreRecursive($fromTime);
        }

        foreach($files as $file) {
            $file->restoreRecursive($fromTime);
        }

    }


}
