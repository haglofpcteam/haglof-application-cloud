<?php

namespace App\Model\MDII;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FormatsDate;

class MDIIFile extends BaseModel
{
    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'm_d_i_i_application_id','m_d_i_i_folder_id','file_datetime','name','user_id'
    ];

    public function identifiableName()
    {
        if(!$this->name) {
            return $this->file_datetime;
        }
        return $this->name;
    }

    public function folder()
    {
        return $this->belongsTo(MDIIFolder::class);
    }

    public function stands() {
        return $this->hasMany(MDIIStand::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function mdii_application()
    {
        return $this->belongsTo(MDIIApplication::class,'m_d_i_i_application_id');
    }

    public function getAdminAttribute() {
        return $this->mdii_application()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->mdii_application()->first()->user;
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }

        if($fromTime==null) {
            $stands=$this->stands()->withTrashed()->get();
        }
        else {
            $stands=$this->stands()->withTrashed()->where('deleted_at','>',$fromTime)->get();
        }

        foreach($stands as $stand) {
            $stand->restoreRecursive($fromTime);
        }

    }
}
