<?php

namespace App\Model\MDII;

use App\Model\BaseModel;
use App\Model\ApplicationInstance;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FormatsDate;

class MDIIApplication extends BaseModel
{
    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'application_instance_id',
        'stand_default_settings'
    ];

    public static function boot ()
    {
        parent::boot();

        self::deleting(function ($mdiiapplication) {
            $mdiiapplication->rootfolder()->delete();
        });

        self::created(function (MDIIApplication $mdiiapplication) {

            MDIIFolder::create([
                'm_d_i_i_application_id' => $mdiiapplication->id,
                'name' => "/",
                'root' => true
            ]);

            $mdiiapplication->save();

        });
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }
        if($fromTime==null) {
            $folders=$this->folders()->withTrashed()->get();
            $files=$this->files()->withTrashed()->get();
            $species=$this->species()->withTrashed()->get();
            $qualities=$this->qualities()->withTrashed()->get();
        }
        else {
            $folders=$this->folders()->withTrashed()->where('deleted_at','>',$fromTime)->get();
            $files=null;
            
            foreach($folders as $folder) {
                $folderFiles=$folder->files()->withTrashed()->where('deleted_at','>',$fromTime)->get();
                if(!$files) {
                    $files = $folderFiles;
                }
                else {
                    $files->merge($folderFiles);
                }
            }

            $species=$this->species()->withTrashed()->where('deleted_at','>',$fromTime)->get();
            $qualities=$this->qualities()->withTrashed()->where('deleted_at','>',$fromTime)->get();
        }

        
        foreach($folders as $folder) {
            $folder->restoreRecursive($fromTime);
        }

        foreach($files as $file) {
            $file->restoreRecursive($fromTime);
        }

        foreach($species as $s) {
            $s->restoreRecursive($fromTime);
        }

        foreach($qualities as $quality) {
            $quality->restoreRecursive($fromTime);
        }
        
    }

    public function folders() {
        return $this->hasMany(MDIIFolder::class);
    }

    public function files() {
        return $this->hasManyThrough(MDIIFile::class,MDIIFolder::class);
    }

    public function rootfolder() {
        return $this->folders()->where("root",true);
    }

    public function species() {
        return $this->hasMany(MDIISpecie::class,'m_d_i_i_application_id');
    }

    public function qualities() {
        return $this->hasMany(MDIIQuality::class,'m_d_i_i_application_id');
    }

    public function applicationInstance()
    {
        return $this->belongsTo(ApplicationInstance::class,'application_instance_id');
    }

    public function getAdminAttribute() {
        return $this->applicationInstance()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->applicationInstance()->first()->user;
    }
}
