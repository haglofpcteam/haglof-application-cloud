<?php

namespace App\Model\MDII;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class MDIITree extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
            'm_d_i_i_plot_id',
            'm_d_i_i_specie_id',
            'm_d_i_i_quality_id',
            'number',
            'diameter',
            'height1',
            'height2',
            'height3',
            'item_id',
            'unique_id',
            'is_log',
            'on_bark',
            'metric'
    ];

    public function plot()
    {
        return $this->belongsTo(MDIIPlot::class,'m_d_i_i_plot_id');
    }

    public function quality()
    {
        return $this->belongsTo(MDIIQuality::class,'m_d_i_i_quality_id');
    }

    public function specie()
    {
        return $this->belongsTo(MDIISpecie::class,'m_d_i_i_specie_id');
    }

    public function getAdminAttribute() {
        return $this->plot()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->plot()->first()->user;
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }

        $quality=$this->quality()->first();

        if($quality) {
            $quality->restore();
        }

        $specie=$this->specie()->first();

        if($specie) {
            $specie->restore();
        }

    }
}
