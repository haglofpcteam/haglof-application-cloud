<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Report extends BaseModel
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'data',
        'report',
        'application_instance_id',
        'user_id',
        'report_template_id'
    ];

    protected $dates = ['deleted_at'];

    public function applicationInstance()
    {
        return $this->belongsTo(ApplicationInstance::class);
    }

    public function reportTemplate()
    {
        return $this->belongsTo(ReportTemplate::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
