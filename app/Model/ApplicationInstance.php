<?php

namespace App\Model;

use App\Model\Pile\PileApplication;
use App\Model\MDII\MDIIApplication;
use App\Model\Wordpress\Subscription;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Billable;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ApplicationInstance extends BaseModel
{

    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'application_id', 'organization_id'
    ];

    public static function boot ()
    {
        parent::boot();

        self::deleting(function ($applicationInstance) {
            
            $pileApplication = $applicationInstance->pileApplication()->first();
            if($pileApplication!=null) {
                $pileApplication->delete();
            }

            $mdiiApplication = $applicationInstance->mdiiApplication()->first();
            if($mdiiApplication!=null) {
                $mdiiApplication->delete();
            }

        });

        self::created(function (ApplicationInstance $applicationInstance) {
            switch($applicationInstance->application()->first()->type) {
                case "PILE":
                    PileApplication::create([
                        'application_instance_id' => $applicationInstance->id
                    ]);
                    break;
                case "MDIICOM":
                    $default_settings=json_encode([
                        'plot_areal' => 314.16,
                        'plot_diameter' => 20,
                        'total_areal' => 0,
                        'measurement_system' => 'metric',
                        'inventory_type' => 'total',
                        'volume_calculation' => 'form_factor'
                    ]);
                    MDIIApplication::create([
                        'application_instance_id' => $applicationInstance->id,
                        'stand_default_settings' => $default_settings
                    ]);
                    break;
            }
        });
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function application()
    {
        return $this->belongsTo(Application::class);
    }

    public function users() {
        return $this->belongsToMany(User::class)->withPivot('first_run');
    }

    public function file_datas() {
        return $this->belongsToMany(FileData::class);
    }

    public function reports() {
        return $this->hasMany(Report::class);
    }

    public function attachedReportTemplates() {
        return $this->belongsToMany(ReportTemplate::class);
    }

    public function reportTemplates() {

        if($this->paid) {
            return ReportTemplate::where('premium',true)->orWhere('standard',true)->orWhereIn('id',$this->attachedReportTemplates()->select('id')->get());
        }
        else {
            return ReportTemplate::where('standard',true)->orWhereIn('id',$this->attachedReportTemplates()->select('id')->get());
        }

        
    }

    public function getPaidNumberOfUsersAttribute() {
        if($this->application()->first()->freeware==true) {
            return 1000;
        }

        $subscriptions = $this->subscriptions;

        $users=0;

        if($this->paid_until!=null) {
            if($this->paid_until>Carbon::now()) {
                $users = $users+$this->number_of_users;
            }
        }
        
        foreach($subscriptions as $subscription) {
            if($subscription->isActive) {
                $users = $users + $subscription->qty;
            }
            else {
                $this->logToFile("Subscription is not active");
            }
        }

        return $users;
    }

    public function getPaidAttribute() {

        if($this->users()->count()<=$this->paidNumberOfUsers) {
            return true;
        }

        return false;
    }

    public function getTrialAttribute() {
        if($this->subscriptions->isEmpty()&&$this->paid_until==null) {
            return true;
        }
        return false;
    }

    public function getCanAddUserAttribute() {
        $usercount=$this->users()->count();
        
        
        if($this->paidNumberOfUsers>$usercount) {
            return true;
        }
        return false;
    }

    public function getNameAttribute() {
        return $this->application()->first()->name;
    }

    public function getAdminAttribute() {
        if($this->organization()->first()->admin) {
            return true;
        }
        elseif($this->users()->where('role','admin')->where('id',Auth::id())) {
            return true;
        }
        return false;
    }

    public function getUserAttribute() {
        if($this->admin) {
            return true;
        }
        elseif($this->users()->where('id',Auth::id())) {
            return true;
        }
        return false;
    }

    public function getFirstRunAttribute() {
        if(Auth::user()->applicationInstances()->find($this->id)->pivot->first_run==null) {
            Auth::user()->applicationInstances()->updateExistingPivot($this->id,['first_run' => Carbon::now()]);
            return true;
        }
        return false;
    }

    public function getSubscriptionsAttribute() {
        $id = DB::table('application_instances_subscription')->where('application_instance_id',$this->id)->pluck('subscription_id');
        return Subscription::whereIn('id',$id)->get();
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }

        Log::debug($fromTime);

        switch($this->application()->first()->type) {
            case "PILE":
                $this->pileApplication()->withTrashed()->first()->restoreRecursive($fromTime);
                break;
            case "MDIICOM":
                $this->mdiiApplication()->withTrashed()->first()->restoreRecursive($fromTime);
                break;
        }

    }

    /**
     * Pile data relationships
     */

    public function pileApplication() {
        return $this->hasOne(PileApplication::class);
    }

    /**
     * MDII Com data relationships
     */

    public function mdiiApplication() {
        return $this->hasOne(MDIIApplication::class);
    }

}
