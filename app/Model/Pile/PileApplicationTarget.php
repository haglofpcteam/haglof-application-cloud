<?php

namespace App\Model\Pile;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FormatsDate;

class PileApplicationTarget extends BaseModel
{
    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at','time'];

    protected $fillable = [
        'pile_application_measurement_id','type','latitude','ns','longitude','ew','altitude','hdop','time','seq','sd','hd','h','pitch','az','x','y','z','utm_zone','created_at','modified_at'
    ];

    public function measurement()
    {
        return $this->belongsTo(PileApplicationMeasurement::class,'pile_application_measurement_id');
    }

    public function getAdminAttribute() {
        return $this->measurement()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->measurement()->first()->user;
    }
}
