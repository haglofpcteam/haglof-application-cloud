<?php

namespace App\Model\Pile;

use App\Model\ApplicationInstance;
use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FormatsDate;

class PileApplicationSite extends BaseModel
{

    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name','pile_application_id','description'
    ];

    public static function boot ()
    {
        parent::boot();

        self::deleting(function (PileApplicationSite $pileApplicationSite) {
            $locations=$pileApplicationSite->locations()->get();
            foreach($locations as $location) {
                $location->delete();
            }
        });
    }

    public function pileApplication()
    {
        return $this->belongsTo(PileApplication::class,'pile_application_id');
    }

    public function locations() {
        return $this->hasMany(PileApplicationLocation::class);
     }

     public function getAdminAttribute() {
        return $this->pileApplication()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->pileApplication()->first()->user;
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }
        if($fromTime==null) {
            $locations=$this->locations()->withTrashed()->get();
        }
        else {
            $locations=$this->locations()->withTrashed()->where('deleted_at','>',$fromTime)->get();
        }

        foreach($locations as $location) {
            $location->restoreRecursive($fromTime);
        }

    }
}
