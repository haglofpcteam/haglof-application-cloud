<?php

namespace App\Model\Pile;

use Illuminate\Database\Eloquent\Model;
use App\Model\ApplicationInstance;
use App\Traits\FormatsDate;

use Illuminate\Database\Eloquent\SoftDeletes;

class PileApplication extends Model
{
    use SoftDeletes, FormatsDate;

    protected $fillable = [
        'application_instance_id'
    ];

    protected $dates = ['deleted_at'];

    public static function boot ()
    {
        parent::boot();

        self::deleting(function (PileApplication $pileApplication) {
            $sites=$pileApplication->sites()->get();
            
            foreach($sites as $site) {
                $site->delete();
            } 
        });
    }


    public function sites() {
        return $this->hasMany(PileApplicationSite::class);
    }

    public function applicationInstance()
    {
        return $this->belongsTo(ApplicationInstance::class,'application_instance_id');
    }

    public function getAdminAttribute() {
        return $this->applicationInstance()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->applicationInstance()->first()->user;
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }
        if($fromTime==null) {
            $sites=$this->sites()->withTrashed()->get();
        }
        else {
            $sites=$this->sites()->withTrashed()->where('deleted_at','>',$fromTime)->get();
        }

        foreach($sites as $site) {
            $site->restoreRecursive($fromTime);
        }

    }

}
