<?php

namespace App\Model\Pile;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Traits\FormatsDate;

class PileApplicationLocation extends BaseModel
{
    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name','pile_application_site_id','volume_adjustment_factor','description','measurement_system','density'
    ];

    public static function boot ()
    {
        parent::boot();

        self::deleting(function (PileApplicationLocation $pileApplicationLocation) {
            $measurements=$pileApplicationLocation->measurements()->get(); 
            foreach($measurements as $measurement) {
                $measurement->delete();
            }
        });
    }

    public function site()
    {
        return $this->belongsTo(PileApplicationSite::class,'pile_application_site_id');
    }

    public function measurements() {
        return $this->hasMany(PileApplicationMeasurement::class);
     }

     public function getAdminAttribute() {
        return $this->site()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->site()->first()->user;
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }
        if($fromTime==null) {
            $measurements=$this->measurements()->withTrashed()->get();
        }
        else {
            $measurements=$this->measurements()->withTrashed()->where('deleted_at','>',$fromTime)->get();
        }

        foreach($measurements as $measurement) {
            $measurement->restoreRecursive($fromTime);
        }

    }
}
