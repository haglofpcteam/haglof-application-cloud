<?php

namespace App\Model\Pile;

use App\Model\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Model\FileData;
use App\Model\User;
use App\Traits\FormatsDate;

class PileApplicationMeasurement extends BaseModel
{
    use SoftDeletes,FormatsDate;

    protected $dates = ['deleted_at','measurement_time'];

    protected $fillable = [
        'user_id',
        'measurement_time',
        'pile_application_location_id',
        'description',
        'file_data_id',
        'density',
        'eye_height',
        'file_id',
        'magnetic_declination',
        'pivot_offset',
        'settings',
        'transponder_height',
        'type',
        'unit_production_id',
        'unit_serial_number',
        'unit_version_id',
        'volume_adjustment_factor'
    ];

    public static function boot ()
    {
        parent::boot();

        self::deleting(function (PileApplicationMeasurement $pileApplicationMeasurement) {
            $pileApplicationMeasurement->targets()->delete();
        });
    }


    public function location()
    {
        return $this->belongsTo(PileApplicationLocation::class,'pile_application_location_id');
    }

    public function targets() {
        return $this->hasMany(PileApplicationTarget::class);
    }

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function image() {
        return $this->belongsTo(FileData::class,'file_data_id');
    }

    public function getAdminAttribute() {
        return $this->location()->first()->admin;
    }

    public function getUserAttribute() {
        return $this->location()->first()->user;
    }

    public function getSetting($key,$default) {

        $settings=json_decode($this->settings,true);
        if(isset($settings[$key])) {
            return $settings[$key];
        }
        return $default;
    }

    public function saveSetting($key,$value) {
        $settings=json_decode($this->settings,true);
        if($value=="true") {
            $settings[$key]=true;
        }
        elseif($value=="false") {
            $settings[$key]=false;
        }
        else {
            $settings[$key]=$value;
        }
        
        $this->settings=json_encode($settings);
        $this->save();
    }

    public function restoreRecursive($fromTime=null) {
        if($this->trashed()) {
            $this->restore();
        }
        if($fromTime==null) {
            $targets=$this->targets()->withTrashed()->get();
        }
        else {
            $targets=$this->targets()->withTrashed()->where('deleted_at','>',$fromTime)->get();
        }

        $image = $this->image()->withTrashed()->first();

        if($image) {
            $image->restore();
        }

        foreach($targets as $target) {
            $target->restore();
        }

    }
}
