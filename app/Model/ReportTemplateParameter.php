<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportTemplateParameter extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'report_template_id',
        'data_type',
        'default_value'
    ];

    public function reportTemplate() {
        return $this->belongsTo(ReportTemplate::class);
    }
}
