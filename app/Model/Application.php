<?php

namespace App\Model;

use Illuminate\Database\Eloquent\SoftDeletes;

class Application extends BaseModel
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['type', 'name', 'route', 'price_per_year', 'currency'];

    public function identifiableName()
    {
        return $this->name;
    }

    public function applicationInstances() {
        return $this->hasMany(ApplicationInstance::class);
    }

    public function plans() {
        return $this->hasMany(BraintreePlan::class);
    }

    public function reportTemplates() {
        return $this->hasMany(ReportTemplate::class);
    }

}
