<?php

namespace App\Composers;

use Illuminate\View\View;
use App\Model\AdminNotice;

class LayoutComposer
{

    protected $notices;

    /**
     * Create a new layout composer.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $jumbotrons = array(
            "4k-wallpaper-architecture-background-1308624.jpg",
            "4k-wallpaper-audi-audi-r8-1402787.jpg",
            "4k-wallpaper-background-beautiful-853199.jpg",
            "4k-wallpaper-blur-bokeh-1213447.jpg",
            "4k-wallpaper-calm-waters-dark-1252869.jpg",
            "android-wallpaper-city-dark-248159.jpg"
        );

        $jumbotrons = array(
            "slides.jpg"
        );

        $view->with('notices_count',\Cache::remember('posts', 1, function() {
            return AdminNotice::where('read_at',null)->count();
        }))
        ->with('random_jumbotron',$jumbotrons[array_rand($jumbotrons)]);
    }
}
