<?php

namespace App\Logic;

use App\Model\User;
use Illuminate\Support\Facades\Auth;
use App\Model\ApplicationInstance;
use App\Model\MDII\MDIIApplication;
use App\Model\MDII\MDIIFolder;
use App\Model\MDII\MDIIFile;
use App\Model\MDII\MDIIStand;
use App\Model\MDII\MDIITree;
use App\Model\MDII\MDIISpecie;
use App\Model\MDII\MDIIQuality;

use Illuminate\Support\Facades\Storage;

use DomDocument;
use XMLWriter;

class MDIIFileParser
{
    public function parse_mdii_file(MDIIFolder $folder,$file,$filename) {
        
        try {

            $mdii_application = $folder->mdii_application()->first();

            $mdii_application = MDIIApplication::find($folder->m_d_i_i_application_id);

            $species = $mdii_application->species()->get();

            $qualities = $mdii_application->qualities()->get();

            $mdiifile = $folder->files()->create([
                'user_id' => Auth::user()->id,
                'name' => $filename,
                'm_d_i_i_application_id' => $folder->m_d_i_i_application_id
            ]);

            $stand = $mdiifile->stands()->create([
                'number' => 1
            ]);

            $plot = $stand->plots()->create([
                'number' => 1
            ]);

            $treecounter=0;

            $zerodiameter=0;

            if ($file->isValid()) {
                $filecontent=$file->get();

                $xml_document = new DomDocument;
                $xml_document->loadXML($filecontent);
        
                if(isset($xml_document->childNodes[0])&&$xml_document->childNodes[0]->tagName=="MDIIData") {
                    foreach ($xml_document->childNodes[0]->childNodes as $node) {
                        if(isset($node->tagName)) {
                            if($node->tagName=="Header") {
                                foreach($node->childNodes as $childnode) {
                                    if(isset($childnode->tagName)&&isset($childnode->nodeValue)) {
                                        switch($childnode->tagName) {
                                            case "Name":
                                                break;
                                            case "Date":
                                                $mdiifile->file_datetime=$childnode->nodeValue;
                                                break;
                                        }
                                    }
                                }
                            } 
                            elseif($node->tagName=="Data") {
                                foreach($node->childNodes as $childnode) {
                                    if(isset($childnode->tagName)&&$childnode->tagName=="Tree") {
                                        $treedata=$this->parse_tree($childnode);
                                        if($treedata==null) {
                                            $zerodiameter++;
                                        }
                                        else {
                                            if($zerodiameter==2) {
                                                $plot = $stand->plots()->create([
                                                    'number' => $plot->number+1
                                                ]);
                                                $treecounter=0;
                                                $zerodiameter=0;
                                            }
                                            elseif($zerodiameter==3) {
                                                $stand = $mdiifile->stands()->create([
                                                    'number' => $stand->number+1
                                                ]);
                                    
                                                $plot = $stand->plots()->create([
                                                    'number' => 1
                                                ]);
                                                $treecounter=0;
                                                $zerodiameter=0;
                                            }
                                            $treedata['number']=++$treecounter;

                                            $specie = null;

                                            foreach($species as $s) {
                                                if($s->specie_text==$treedata['species_text']) {
                                                    $specie=$s;
                                                }
                                            }

                                            if($specie==null) {
                                                $specie=MDIISpecie::create([
                                                    'specie_nr' => $treedata['species'],
                                                    'specie_text' => $treedata['species_text']
                                                ]);

                                                $species->push($specie);
                                            }

                                            $treedata['m_d_i_i_specie_id']=$specie->id;

                                            if(isset($treedata['quality_text'])) {
                                                $quality = null;

                                                foreach($qualities as $q) {
                                                    if($q->quality_text==$treedata['quality_text']) {
                                                        $quality=$q;
                                                    }
                                                }
    
                                                if($quality==null) {
                                                    $quality=MDIIQuality::create([
                                                        'quality' => $treedata['quality'],
                                                        'quality_text' => $treedata['quality_text']
                                                    ]);
    
                                                    $qualities->push($quality);
                                                }
    
                                                $treedata['m_d_i_i_quality_id']=$quality->id;
                                            }


                                            $plot->trees()->create($treedata);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $mdiifile->save();

                return response()->json(['code' => 'success', 'message' => __('Uploaded MDII-file successfully')]);

            }

            return response()->json(['code' => 'danger', 'message' => __('There seem to be a problem with the uploaded MDII-file')]);

        } catch (\Exception $e) {
            return response()->json(['code' => 'danger', 'message' => $e->getMessage()]);
            return response()->json(['code' => 'danger', 'message' => __('There seem to be a problem with the uploaded MDII-file')]);
        }
        
    }

    private function parse_tree($treenode) {
        $treearray = array();
        foreach($treenode->childNodes as $node) {
            if(isset($node->tagName)&&isset($node->nodeValue)&&$node->nodeValue!=null) {
                switch($node->tagName) {
                    case "Species":
                        $treearray['species']=$node->nodeValue;
                        break;
                    case "SpeciesText":
                        $treearray['species_text']=$node->nodeValue;
                        break;
                    case "Diameter":
                        $treearray['diameter']=$node->nodeValue;
                        break;
                    case "Height1":
                        $treearray['height1']=$node->nodeValue;
                        break;
                    case "Height2":
                        $treearray['height2']=$node->nodeValue;
                        break;
                    case "Height3":
                        $treearray['height3']=$node->nodeValue;
                        break;
                    case "Quality":
                        $treearray['quality']=$node->nodeValue;
                        break;
                    case "QualityText":
                        $treearray['quality_text']=$node->nodeValue;
                        break;
                    case "Id":
                        $treearray['item_id']=$node->nodeValue;
                        break;
                    case "UniqueId":
                        $treearray['unique_id']=$node->nodeValue;
                        break;
                    case "IsLog":
                        $treearray['is_log']=$node->nodeValue;
                        break;
                    case "OnBark":
                        $treearray['on_bark']=$node->nodeValue;
                        break;
                    case "Metric":
                        $treearray['metric']=$node->nodeValue;
                        break;
                }
            }
        }
        if(!isset($treearray['diameter'])||$treearray['diameter']==0) {
            return null;
        }
        return $treearray;
    }

    public function create_mdii_file(MDIIFolder $folder,$data,$name) {

        try {
            $data = json_decode($data);

            if(isset($data->MDIIData)) {
                $mdii_application = $folder->mdii_application()->first();
    
                $mdii_application = MDIIApplication::find($folder->m_d_i_i_application_id);
        
                $species = $mdii_application->species()->get();
        
                $qualities = $mdii_application->qualities()->get();
        
                $mdiifile = $folder->files()->create([
                    'user_id' => Auth::user()->id,
                    'name' => $name,
                    'm_d_i_i_application_id' => $folder->m_d_i_i_application_id
                ]);
        
                $stand = $mdiifile->stands()->create([
                    'number' => 1
                ]);
        
                $plot = $stand->plots()->create([
                    'number' => 1
                ]);

                $mdiifile->file_datetime=now();

                $default_measurement_system=1;

                if(isset($data->MDIIData->Header)&&isset($data->MDIIData->Header->Metric)) {
                    $default_measurement_system=$data->MDIIData->Header->Metric;
                }

    
                if(isset($data->MDIIData->Header)&&isset($data->MDIIData->Header->Date)&&$data->MDIIData->Header->Date!=null) {
                    $mdiifile->file_datetime=$data->MDIIData->Header->Date;
                }
    
                $zerodiameter=0;
                $treecounter=0;

                $trees = array();
    
                if(isset($data->MDIIData->Data)) {
                    foreach($data->MDIIData->Data->Tree as $tree) {
                        
                        $tree->Diameter=$this->getAsInteger($tree->Diameter);

                        if($tree->Diameter==0) {
                            $zerodiameter++;
                        }
                        else {

                            $treedata = [
                                'diameter' => $tree->Diameter,
                                'height1' => isset($tree->Height1)?$this->getAsInteger($tree->Height1):null,
                                'height2' => isset($tree->Height2)?$this->getAsInteger($tree->Height2):null,
                                'height3' => isset($tree->Height3)?$this->getAsInteger($tree->Height3):null,
                                'item_id' => isset($tree->Id)?$tree->Id:null,
                                'unique_id' => isset($tree->UniqueId)?$tree->UniqueId:null,
                                'is_log' => isset($tree->IsLog)?$tree->IsLog:0,
                                'on_bark' => isset($tree->OnBark)?$tree->OnBark:1,
                                'metric' => isset($tree->Metric)?$tree->Metric:$default_measurement_system,
                                'm_d_i_i_quality_id' => null,
                             ];  

                            if($zerodiameter==2) {
                                $plot = $stand->plots()->create([
                                    'number' => $plot->number+1
                                ]);
                                $treecounter=0;
                                $zerodiameter=0;
                            }
                            elseif($zerodiameter>2) {
                                $stand = $mdiifile->stands()->create([
                                    'number' => $stand->number+1
                                ]);
                    
                                $plot = $stand->plots()->create([
                                    'number' => 1
                                ]);
                                $treecounter=0;
                                $zerodiameter=0;
                            }
                            $treedata['number']=++$treecounter;

                            $specie = null;

                            foreach($species as $s) {
                                if($s->specie_text==$tree->SpeciesText) {
                                    $specie=$s;
                                }
                            }

                            if($specie==null) {
                                $specie=MDIISpecie::create([
                                    'specie_nr' => $tree->Species,
                                    'specie_text' => $tree->SpeciesText
                                ]);

                                $species->push($specie);
                            }

                            $treedata['m_d_i_i_specie_id']=$specie->id;

                            if(isset($tree->QualityText)) {
                                $quality = null;

                                foreach($qualities as $q) {
                                    if($q->quality_text==$tree->QualityText) {
                                        $quality=$q;
                                    }
                                }

                                if($quality==null) {
                                    $quality=MDIIQuality::create([
                                        'quality' => $tree->Quality,
                                        'quality_text' => $tree->QualityText
                                    ]);

                                    $qualities->push($quality);
                                }

                                $treedata['m_d_i_i_quality_id']=$quality->id;
                            }

                            $treedata['m_d_i_i_plot_id']=$plot->id;

                            array_push($trees,$treedata); 
                        }

                        if(count($trees)>500) {
                            MDIITree::insert($trees);
                            $trees = array();
                        }

                    }

                    if(count($trees)>0) {
                        MDIITree::insert($trees);
                    }
                }

                $mdiifile->save();

                return $mdiifile;
            }

        } 
        catch (\Exception $e) {
            $this->logToFile($e->getLine() . " : " . $e->getMessage());

            return null;
            
        }
    }

    private function getAsInteger($val) {
        $type=gettype($val);
        if($type=='object') {
            return intval($val->text);
        }

        return intval($val);
    }

    public function logToFile($string) {
        if(config('app.debug')) {
            Storage::append('debug.log', $string . "\n");
        }       
    } 

}                 
    