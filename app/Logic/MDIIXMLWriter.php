<?php

namespace App\Logic;

use App\Model\MDII\MDIIStand;
use App\Model\MDII\MDIITree;
use Illuminate\Http\Resources\Json\JsonResource;

use DomDocument;
use XMLWriter;
use xsltProcessor;
use File;

class MDIIXMLWriter
{

    public function generateXML($stands) {

        $xml = new XMLWriter();
        $xml->openMemory();
        $xml->setIndent(true);
        $xml->startDocument();

        $xml->startElement('Stands');

        foreach($stands as $s) {

            $xml->writeElement("StandJSON", json_encode(generateStandData($s)));
        }

        $xml->endElement();
        $xml->endDocument();

        $xml_out = $xml->outputMemory();

        return $xml_out;
    }

    public function generateData($stands) {
        $data = [];

        foreach($stands as $s) {
            $data[]=$this->generateStandData($s);
        }

        return $data;
    }

    private function generateStandData(MDIIStand $stand) {

        $tree_columns=[
            'm_d_i_i_trees.id',
            "m_d_i_i_trees.m_d_i_i_plot_id",
            "m_d_i_i_trees.m_d_i_i_specie_id",
            "m_d_i_i_trees.m_d_i_i_quality_id",
            "m_d_i_i_trees.number",
            "m_d_i_i_trees.diameter",
            "m_d_i_i_trees.height1",
            "m_d_i_i_trees.height2",
            "m_d_i_i_trees.height3",
            "m_d_i_i_trees.item_id",
            "m_d_i_i_trees.unique_id",
            "m_d_i_i_trees.is_log",
            "m_d_i_i_trees.on_bark",
            "m_d_i_i_trees.metric",
        ];

        $stand->settings=json_decode($stand->settings);

        $trees=$stand->trees()->get($tree_columns);

        foreach($trees as $tree) {
            $tree->diameter=$tree->diameter!=null ? intval($tree->diameter) : null;
            $tree->height1=$tree->height1!=null ? intval($tree->height1) : null;
            $tree->height2=$tree->height2!=null ? intval($tree->height2) : null;
            $tree->height3=$tree->height3!=null ? intval($tree->height3) : null;
        }

        //$trees=$this->calculateHeight($trees);

        $data = [
            'stand' => $stand,
            'species' => $stand->species()->get(),
            'qualities' => $stand->qualities()->get(),
            'trees' => $trees
        ];

        return $data;
    }

    private function generateStandXML(MDIIStand $stand,$xml) {

        $xml->startElement('Stand');
        $species = $stand->species()->get();
        $specie_names=$species->pluck('specie_text');
        $trees = $stand->trees()->with('quality')->with('specie')->get();
        $sample_trees = array();

        $diameter_classes = array();
        $circumference_classes = array();
        $largest_diameter_class = 0;
        $largest_circumference_class = 0;

        $total_form_height_volume=0;
        $total_form_factor_volume=0;
        $total_basel_area=0;

        $total_dia=0;
        $total_dia2=0;
        $total_dia3=0;
        $total_height=0;
        $total_heightcount=0;

        $plots=array();
        $plot_areal = $stand->getSetting("plot_areal",0);

        $trees=$this->calculateHeight($trees);

        //######################## Species
        $xml->startElement('Species');
        foreach($species as $specie) {
            //######################## Specie
            $xml->startElement('Specie');
            $xml->writeAttribute("name",$specie->specie_text);

            $dia=0;
            $dia2=0;
            $dia3=0;
            $heightsum=0;
            $heightcount=0;
            $form_height_volume=0;
            $form_height=$specie->form_height;
            $form_factor_volume=0;
            $form_factor=$specie->form_factor;
            $basel_area=0;

            $xml->startElement('Trees');

            foreach($trees as $tree) {
                if($tree->m_d_i_i_specie_id!=$specie->id) {
                    continue;
                }

                $tree['basel_area']=0.0001*pi()*pow($tree->diameter/20,2);

                $plot_id=$tree->m_d_i_i_plot_id;
                if(!isset($plots[$plot_id])) {
                    $plots[$plot_id]=array();
                    $plots[$plot_id]['basel_area']=0;
                    $plots[$plot_id]['tree_count']=0;
                    $plots[$plot_id]['number']=count($plots);
                    $plots[$plot_id]['PlotAreal']=$plot_areal;
                }
                $plots[$plot_id]['basel_area']+=$tree['basel_area'];
                $plots[$plot_id]['tree_count']++;

                $tree['plot']=$plots[$plot_id];

                if($tree->height1!=null&&$tree->height1>0&&!$tree->is_log) {
                    array_push($sample_trees,$tree);
                    $heightsum+=$tree->height1;
                    $heightcount++;
                }

                $height=$tree->height1;
                if($height==null||$height==0) {
                    $height=$tree->calculated_height;
                }
                
                $basel_area+=$tree['basel_area'];
                $total_basel_area+=$tree['basel_area'];

                if($height>13) {
                    $tree['form_factor_volume']=pi()*0.0000001*$tree->diameter*$tree->diameter*$form_factor*$height/4.0;
                    $tree['form_factor_volume']=floor(1000*$tree['form_factor_volume']+0.5)/1000;
                    $form_factor_volume+=$tree['form_factor_volume'];
                    $total_form_factor_volume+=$tree['form_factor_volume'];
                }
                else {
                    $tree['form_factor_volume']=0;
                }

                $tree['form_height_volume']=(pi()/4)*pow($tree->diameter/1000,2)*$form_height;
                $form_height_volume+=$tree['form_height_volume'];
                $total_form_height_volume+=$tree['form_height_volume'];

                $dia+=$tree->diameter;
                $dia2+=pow($tree->diameter,2);
                $dia3+=pow($tree->diameter,3);

                $class_key = floor($tree->diameter/5);

                $largest_diameter_class = max($class_key,$largest_diameter_class);

                if(!isset($diameter_classes[$class_key])) {
                    $diameter_classes[$class_key]=array();

                    foreach($specie_names as $specie_name) {
                        $diameter_classes[$class_key][$specie_name]=0;
                    }
                }

                $diameter_classes[$class_key][$specie->specie_text]++;

                $class_key = floor($this->treeCircumference($tree)/50);

                $largest_circumference_class = max($class_key,$largest_circumference_class);

                if(!isset($circumference_classes[$class_key])) {
                    $circumference_classes[$class_key]=array();

                    foreach($specie_names as $specie_name) {
                        $circumference_classes[$class_key][$specie_name]=0;
                    }
                }

                $circumference_classes[$class_key][$specie->specie_text]++;

                $xml=$this->writeTreeToXml($tree,$xml);
            }

            $xml->endElement();

            $total_dia+=$dia;
            $total_dia2+=$dia2;
            $total_dia3+=$dia3;
            $total_height+=$heightsum;
            $total_heightcount+=$heightcount;

            $count=count($specie->trees);
            $dgv=$dia2!=0?$dia3/$dia2:0;
            $dg=sqrt($dia2/$count);
            $average_diameter = $dia/$count;
            $average_height = $heightcount==0?0:$heightsum/$heightcount;

            $xml->startElement('Values');
            $xml->writeAttribute("dgv",round($dgv));
            $xml->writeAttribute("dg",round($dg));
            $xml->writeAttribute("average_diameter",round($average_diameter));
            $xml->writeAttribute("average_height",round($average_height));
            $xml->writeAttribute("form_height",$form_height);
            $xml->writeAttribute("form_height_volume",$form_height_volume);
            $xml->writeAttribute("form_factor",$specie->form_factor);
            $xml->writeAttribute("form_factor_volume",$form_factor_volume);
            $xml->writeAttribute("basel_area",$basel_area);

            $xml->endElement();

            $xml->endElement();
            //######################## End Specie
        }
        $xml->endElement();
        //######################## End Species

        ksort($diameter_classes);

        //######################## Classes

        $xml->startElement('Classes');

        //### Class 0.5cm, Start 0
        $xml->startElement('Class');
        $xml->writeAttribute("Size","0.5cm");
        foreach(array_keys($diameter_classes) as $key) {
            $xml->startElement('Tree-class');
            $xml->writeAttribute("Name",($key*5)/10);
            foreach(array_keys($diameter_classes[$key]) as $species_key) {
                $xml->startElement('Species');
                $xml->writeAttribute("Name",$species_key);
                $xml->writeAttribute("Count",$diameter_classes[$key][$species_key]);
                $xml->endElement();
            }         
            $xml->endElement();
        }     
        $xml->endElement();
        //### End Class

         //### Class 1cm, Start 0
         $xml->startElement('Class');
         $xml->writeAttribute("Size","1cm");
         for($i=0;$i<$largest_diameter_class+1;$i=$i+2) {
            $species_keys=null;
            for($j=$i;$j<($i+2);$j++) {
                if(isset($diameter_classes[$j])) {
                    $species_keys=array_keys($diameter_classes[$j]);
                }
            }

            if($species_keys==null) {
                continue;
            }

            $xml->startElement('Tree-class');
            $xml->writeAttribute("Name",(($i*5)/10) . "-" . ((($i+2)*5)/10));
             foreach($species_keys as $species_key) {
                $count=0;
                for($j=$i;$j<($i+2);$j++) {
                    if(isset($diameter_classes[$j])) {
                        $count=$count+$diameter_classes[$j][$species_key];
                    }
                }

                $xml->startElement('Species');
                $xml->writeAttribute("Name",$species_key);
                $xml->writeAttribute("Count",$count);
                $xml->endElement();
                
            }
            $xml->endElement();
         }     
         $xml->endElement();
         //### End Class

        //### Class 2cm, Start 0
        $xml->startElement('Class');
        $xml->writeAttribute("Size","2cm");
        for($i=0;$i<$largest_diameter_class+1;$i=$i+4) {
            $species_keys=null;
            for($j=$i;$j<($i+4);$j++) {
                if(isset($diameter_classes[$j])) {
                    $species_keys=array_keys($diameter_classes[$j]);
                }
            }

            if($species_keys==null) {
                continue;
            }

            $xml->startElement('Tree-class');
            $xml->writeAttribute("Name",(($i*5)/10) . "-" . ((($i+4)*5)/10));
             foreach($species_keys as $species_key) {
                $count=0;
                for($j=$i;$j<($i+4);$j++) {
                    if(isset($diameter_classes[$j])) {
                        $count=$count+$diameter_classes[$j][$species_key];
                    }
                }

                $xml->startElement('Species');
                $xml->writeAttribute("Name",$species_key);
                $xml->writeAttribute("Count",$count);
                $xml->endElement();
                
            }
            $xml->endElement();
        }     
        $xml->endElement();
        //### End Class

        //### Class 5cm, Start 0
        $xml->startElement('Class');
        $xml->writeAttribute("Size","5cm");
        for($i=0;$i<$largest_diameter_class+1;$i=$i+10) {
            $species_keys=null;
            for($j=$i;$j<($i+10);$j++) {
                if(isset($diameter_classes[$j])) {
                    $species_keys=array_keys($diameter_classes[$j]);
                }
            }

            if($species_keys==null) {
                continue;
            }

            $xml->startElement('Tree-class');
            $xml->writeAttribute("Name",($i*5)/10);
             foreach($species_keys as $species_key) {
                $count=0;
                for($j=$i;$j<($i+10);$j++) {
                    if(isset($diameter_classes[$j])) {
                        $count=$count+$diameter_classes[$j][$species_key];
                    }
                }

                $xml->startElement('Species');
                $xml->writeAttribute("Name",$species_key);
                $xml->writeAttribute("Count",$count);
                $xml->endElement();
                
            }
            $xml->endElement();
        }     
        $xml->endElement();
        //### End Class

        //### Class 2.5cm, Start 2.5cm
        $xml->startElement('Class');
        $xml->writeAttribute("Size","5cmStart2.5cm");
        for($i=5;$i<$largest_diameter_class+1;$i=$i+5) {
            $species_keys=null;
            for($j=$i;$j<($i+5);$j++) {
                if(isset($diameter_classes[$j])) {
                    $species_keys=array_keys($diameter_classes[$j]);
                }
            }

            if($species_keys==null) {
                continue;
            }

            $xml->startElement('Tree-class');
            if($i%10==0) {
                $xml->writeAttribute("Name",(($i*5)/10) . "B");
                
            }
            else {
                $xml->writeAttribute("Name",((($i+5)*5)/10) . "A");
    
            }
             foreach($species_keys as $species_key) {
                $count=0;
                for($j=$i;$j<($i+5);$j++) {
                    if(isset($diameter_classes[$j])) {
                        $count=$count+$diameter_classes[$j][$species_key];
                    }
                }

                $xml->startElement('Species');
                $xml->writeAttribute("Name",$species_key);
                $xml->writeAttribute("Count",$count);
                $xml->endElement();
                
            }
            $xml->endElement();
        }     
        $xml->endElement();
        //### End Class

        //### Circumference Class 5cm, Start 0
        $xml->startElement('CircumferenceClass');
        $xml->writeAttribute("Size","5cm");
        for($i=0;$i<$largest_circumference_class+1;$i++) {
            if(isset($circumference_classes[$i])) {
                $xml->startElement('Tree-class');
                $xml->writeAttribute("Name",($i*5));

                foreach($species_keys as $species_key) {
                    $xml->startElement('Species');
                    $xml->writeAttribute("Name",$species_key);
                    $xml->writeAttribute("Count",$circumference_classes[$i][$species_key]);
                    $xml->endElement();
                }

                $xml->endElement();
            }
        }     
        $xml->endElement();
        //### End Class

        //### Circumference Class 10cm, Start 0
        $xml->startElement('CircumferenceClass');
        $xml->writeAttribute("Size","10cm");
        for($i=0;$i<$largest_circumference_class+1;$i=$i+2) {
            $xml->startElement('Tree-class');
            $xml->writeAttribute("Name",($i*5));
            foreach($species_keys as $species_key) {
                $count=0;
                if(isset($circumference_classes[$i])) {
                    $count=$count+$circumference_classes[$i][$species_key];
                }
                if(isset($circumference_classes[$i+1])) {
                    $count=$count+$circumference_classes[$i+1][$species_key];
                }

                $xml->startElement('Species');
                $xml->writeAttribute("Name",$species_key);
                $xml->writeAttribute("Count",$count);
                $xml->endElement();

            }
            $xml->endElement();
        }     
        $xml->endElement();
        //### End Class

        $xml->endElement();
        //######################## End Classes

        //######################## Sample Trees
        $xml->startElement('SampleTrees');
        foreach($sample_trees as $sample_tree) {
            $xml=$this->writeTreeToXml($sample_tree,$xml);
        }
        $xml->endElement();
        //######################## End Sample Trees

        //######################## Plots
        $xml->startElement('Plots');
        foreach(array_keys($plots) as $key) {
            $plot=$plots[$key];
            $xml->startElement('Plot');
            $xml->writeAttribute("Id",$key);
            $xml->writeAttribute("Number",$plot['number']);
            $xml->writeAttribute("BaselArea",$plot['basel_area']);
            $xml->writeAttribute("TreeCount",$plot['tree_count']);
            $xml->endElement();
        }
        $xml->endElement();
        //######################## End Sample Trees

        //######################## Stand values

        $xml->startElement('Values');
            $xml->writeAttribute("Name",$stand->name);
            $xml->writeAttribute("BaselArea",$total_basel_area);
            $xml->writeAttribute("FormHeightVolume",$total_form_height_volume);
            $xml->writeAttribute("FormFactorVolume",$total_form_factor_volume);
            
            $areal_factor=0;

            if($plot_areal) {
                $areal_factor = 10000.0/($plot_areal*count($plots));
                $xml->writeAttribute("PlotAreal",$plot_areal);
            }

            $xml->writeAttribute('ArealFactor',$areal_factor);

            $plot_diameter = $stand->getSetting("plot_diameter",0);

            if($plot_diameter) {
                $xml->writeAttribute("PlotDiameter",$plot_diameter);
            }

            $count=$trees->count();

            $xml->writeAttribute("TreeCount",$count);

            
            $dgv=$total_dia2!=0?$total_dia3/$total_dia2:0;
            $dg=sqrt($total_dia2/$count);
            $average_diameter = $total_dia/$count;
            $average_height = $total_heightcount==0?0:$total_height/$total_heightcount;

            $xml->writeAttribute("dgv",round($dgv));
            $xml->writeAttribute("dg",round($dg));
            $xml->writeAttribute("average_diameter",round($average_diameter));
            $xml->writeAttribute("average_height",round($average_height));


        $xml->endElement();
        //######################## End Stand values
        

        $xml->endElement();
        //######################## End Stand


        return $xml;

    }

    private function treeCircumference($tree) {
        return round($tree->diameter * pi());
    }

    private function calculateHeight($trees) {
        $v2=array();
        $v2[0]=array();

        $v2[0]['a']=0;
        $v2[0]['b']=0;
        $v2[0]['c']=0;
        $v2[0]['d']=0;
        $v2[0]['e']=0;
        $v2[0]['k']=0;
        $v2[0]['m']=0;

        $heightfound=false;

        foreach($trees as $tree) {
            if($tree->is_log!=1) {
                $id=$tree->m_d_i_i_specie_id;
                $height=$tree->height1;
                $diameter=$tree->diameter;

                if(!isset($v2[$id])) {
                    $v2[$id]=array();

                    $v2[$id]['a']=0;
                    $v2[$id]['b']=0;
                    $v2[$id]['c']=0;
                    $v2[$id]['d']=0;
                    $v2[$id]['e']=0;
                    $v2[$id]['k']=0;
                    $v2[$id]['m']=0;
                }

                if($height==null) {
                    continue;
                }

                if($height>10||$diameter>10) {
                    $heightfound=true;

                    $x=log($diameter);
                    

                    $v2[0]['a']++;
                    $v2[$id]['a']++;
                    $v2[0]['b']+=$x;
                    $v2[$id]['b']+=$x;
                    $v2[0]['c']+=$height;
                    $v2[$id]['c']+=$height;
                    $v2[0]['d']+=($x*$x);
                    $v2[$id]['d']+=($x*$x);
                    $v2[0]['e']+=($x*$height);
                    $v2[$id]['e']+=($x*$height);
                }
            }
        }

        if($heightfound) {

            foreach(array_keys($v2) as $key) {
                if($v2[$key]['a']>1) {
                    $div=($v2[$key]['b']*$v2[$key]['b']) - ($v2[$key]['a'] * $v2[$key]['d']);
                    if($div!=0) {
                        $v2[$key]['k']=(($v2[$key]['b']*$v2[$key]['c'])-($v2[$key]['a']*$v2[$key]['e']))/$div;
                        $v2[$key]['m']=(($v2[$key]['e']*$v2[$key]['b'])-($v2[$key]['c']*$v2[$key]['d']))/$div;
                    }
                }
                else {
                    $v2[$key]['k']=$v2[0]['k'];
                    $v2[$key]['m']=$v2[0]['m'];
                }
            }
    
            foreach($trees as $tree) {
                if($tree->is_log!=1) {
                    if(!($tree->height1>0||$tree->height1!=null)) {
                        $k=$v2[$tree->m_d_i_i_specie_id]['k'];
                        $m=$v2[$tree->m_d_i_i_specie_id]['m'];
                        $tree['calculated_height']=($k*log($tree->diameter))+$m;
                    }
                }
            }     

        }
        else {
            foreach($trees as $tree) {
                if($tree->is_log!=1) {
                    $tree['calculated_height']=0;
                }
            }  
        }

        return $trees;

    }

    private function writeTreeToXml($tree,$xml) {
        $xml->startElement('Tree');
        $xml->writeAttribute("Diameter",$tree->diameter);
        $xml->writeAttribute("circumference",$this->treeCircumference($tree));
        $xml->writeAttribute("Species",$tree->specie->specie_text);
        $xml->writeAttribute("Plot",$tree->m_d_i_i_plot_id);
        $xml->writeAttribute("PlotNumber",$tree['plot']['number']);
        $xml->writeAttribute("PlotAreal",$tree['plot']['PlotAreal']);

        if($tree->quality!=null) {
            $xml->writeAttribute("Quality",$tree->quality->quality_text);
        }

        if($tree->is_log==1) {
            $xml->writeAttribute("Id",$tree->item_id);
            $xml->writeAttribute("UniqueId",$tree->unique_id);
            $xml->writeAttribute("OnBark",$tree->on_bark);
            $xml->writeAttribute("Length",$tree->height1);
        }
        else {
            $xml->writeAttribute("Height1",$tree->height1);
            $xml->writeAttribute("Height2",$tree->height2);
            $xml->writeAttribute("height3",$tree->height3);
            if(isset($tree->calculated_height)) {
                $xml->writeAttribute("CalculatedHeight",$tree->calculated_height);
            }
            $xml->writeAttribute("FormFactorVolume",$tree['form_factor_volume']);
            $xml->writeAttribute("FormHeightVolume",$tree['form_height_volume']);
            $xml->writeAttribute("BaselArea",$tree['basel_area']);
        }

        $xml->writeAttribute("IsLog",$tree->is_log);

        $xml->writeAttribute("Metric",$tree->metric);
        $xml->endElement();
        return $xml;
    }

}
