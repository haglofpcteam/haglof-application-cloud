<?php

namespace App\Auth\Grants;

use RuntimeException;
use Illuminate\Http\Request;

use Laravel\Passport\Bridge\User as PassportUser;
use App\Model\User;
use League\OAuth2\Server\RequestEvent;
use App\Auth\Grants\OtpVerifierFactory;
use Psr\Http\Message\ServerRequestInterface;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;


class ActivationAuthorizationGrant extends AbstractGrant
{
    /**
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     */
    public function __construct(RefreshTokenRepositoryInterface $refreshTokenRepository) {
        $this->setRefreshTokenRepository($refreshTokenRepository);

        $this->refreshTokenTTL = new \DateInterval('P1M');
    }

    /**
     * {@inheritdoc}
     */
    public function respondToAccessTokenRequest(ServerRequestInterface $request,ResponseTypeInterface $responseType,\DateInterval $accessTokenTTL) {
        // Validate request
        $client = $this->validateClient($request);
        $scopes = $this->validateScopes($this->getRequestParameter('scope', $request));
        $user = $this->validateUser($request, $client);

        // Finalize the requested scopes
        $scopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getIdentifier());

        // Issue and persist new tokens
        $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $scopes);
        $refreshToken = $this->issueRefreshToken($accessToken);

        // Inject tokens into response
        $responseType->setAccessToken($accessToken);
        $responseType->setRefreshToken($refreshToken);

        return $responseType;
    }

    protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client)
    {
        $url=$this->getRequestParameter('url', $request);

        $newRequest=Request::create($url);

        if($newRequest->hasValidSignature()) {
            $user=User::find($newRequest->id);
            if($user) {
                if(!$user->activated) {
                    $user->email_verified_at=now();
                    $user->save();
                    return new PassportUser($user->getAuthIdentifier());
                }
                abort(401,'User already activated');
            }
            abort(401,'User not found');
        }
        abort(401,'Invalid url');
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
        return 'activation_authorization';
    }
}