<?php

namespace App\Auth\Grants;

use RuntimeException;
use Illuminate\Http\Request;

use Laravel\Passport\Bridge\User as PassportUser;
use App\Model\User;
use League\OAuth2\Server\RequestEvent;
use App\Auth\Grants\OtpVerifierFactory;
use Psr\Http\Message\ServerRequestInterface;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Illuminate\Support\Facades\Log;

use Socialite;
use App\Model\SocialiteProvider;

class SocialAuthorizationGrant extends AbstractGrant
{
    /**
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     */
    public function __construct(RefreshTokenRepositoryInterface $refreshTokenRepository) {
        $this->setRefreshTokenRepository($refreshTokenRepository);

        $this->refreshTokenTTL = new \DateInterval('P1M');
    }

    /**
     * {@inheritdoc}
     */
    public function respondToAccessTokenRequest(ServerRequestInterface $request,ResponseTypeInterface $responseType,\DateInterval $accessTokenTTL) {
        // Validate request
        $client = $this->validateClient($request);

        $scopes = $this->validateScopes($this->getRequestParameter('scope', $request));
        $user = $this->validateUser($request, $client);

        // Finalize the requested scopes
        $scopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getIdentifier());

        // Issue and persist new tokens
        $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $scopes);
        $refreshToken = $this->issueRefreshToken($accessToken);

        // Inject tokens into response
        $responseType->setAccessToken($accessToken);
        $responseType->setRefreshToken($refreshToken);

        return $responseType;
    }

    protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client)
    {
        $provider = $this->getRequestParameter('provider', $request);

        $access_token = $this->getRequestParameter('access_token', $request);

        if($access_token) {
            $userData = Socialite::driver($provider)->userFromToken($access_token)->user;
            if($provider=="google"|$provider=="apple") {
                $user_id = $userData['sub'];
            }
            else {
                $user_id = $userData['id'];
            }

        }
        else if($provider=='apple') {
            $url = 'https://appleid.apple.com/auth/token';

            $mobileauth = $this->getRequestParameter('mobileauth', $request);

            if($mobileauth) {
                // for app login (generated with client_secret.rb)
                $client_id = 'com.haglofsweden.link';
                $client_secret = 'eyJraWQiOiJUM1hZR1BLVlhIIiwiYWxnIjoiRVMyNTYifQ.eyJpc3MiOiI3U0RVREgyNzJCIiwiaWF0IjoxNjY1NzE5ODc5LCJleHAiOjE2ODEyNzE4NzksImF1ZCI6Imh0dHBzOi8vYXBwbGVpZC5hcHBsZS5jb20iLCJzdWIiOiJjb20uaGFnbG9mc3dlZGVuLmxpbmsifQ.kf6VcJ6cQm0O9v898f13m6OQsBzGK7V6CEm8fxK5_PDfS7ZNyuxQkl-kRfzrbDtlwGJciBfiF-TTuXGVlm2kVw';
            }
            else
            {
                // for web login (generated with client_secret.rb)
                $client_id = 'auth.app.haglof.applications';
                $client_secret = 'eyJraWQiOiJUM1hZR1BLVlhIIiwiYWxnIjoiRVMyNTYifQ.eyJpc3MiOiI3U0RVREgyNzJCIiwiaWF0IjoxNjY0ODg1NTIwLCJleHAiOjE2ODA0Mzc1MjAsImF1ZCI6Imh0dHBzOi8vYXBwbGVpZC5hcHBsZS5jb20iLCJzdWIiOiJhdXRoLmFwcC5oYWdsb2YuYXBwbGljYXRpb25zIn0.iYlFGmZ0IPEADitB2DSzV5uakEqM9EKLCeqlhywA9Oc3dvztiQlL_ZL-fpZj1UFdjUG08qqPgLrOp77rSDGECQ';
            }




            $form_params = [
                'grant_type' => 'authorization_code',
                'code' => $this->getRequestParameter('code', $request),
                'redirect_uri' => $this->getRequestParameter('callback', $request),
                'client_id' => $client_id,
                'client_secret' => $client_secret,
            ];

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            if($form_params) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($form_params));
            }

            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Accept: application/json',
                'User-Agent: curl',
            ]);

            $r = curl_exec($ch);

            Log::debug($r);

            $r = json_decode($r);

            $userData = Socialite::driver($provider)->userFromToken($r->id_token)->user;

            $user_id = $userData['sub'];
        }
        else {
            $userData = Socialite::driver($provider)->stateless()->user();
            $user_id = $userData->id;
        }

        $socialite_provider = SocialiteProvider::where('provider_id',$user_id)->where('provider',$provider)->first();

        $user=null;

        if($socialite_provider) {
            $user = $socialite_provider->user()->first();
        }
        else if($userData){

            Log::debug(json_encode($userData));

            if(is_array($userData)) {
                $userData=(object)$userData;
            }

            if($provider=='microsoft'||$provider=='live') {
                $email=$userData->userPrincipalName;
                $name=$userData->displayName;
            }
            else if(isset($userData->email)&&isset($userData->name)) {
                $email=$userData->email;
                $name=$userData->name;
            }
            else {
                abort(403, 'This socialprovider account is not connected to any account');
            }

            if(User::where('email',$email)->first()) {
                abort(403, 'E-mail already in use but not connected with this Social account');
            }
            else {
                $email_verified_at = null;

                if($userData->email!=null) {
                    $email_verified_at=now();
                }

                $user = User::create([
                    'name' => $name,
                    'email' => $email,
                    'email_verified_at' => $email_verified_at
                ]);

                $user->save();

                if($email_verified_at==null) {
                    $user->sendApiEmailVerificationNotification();
                }

                $socialite_provider = new SocialiteProvider();
                $socialite_provider->provider = $provider;
                $socialite_provider->provider_id = $userData->id;
                $socialite_provider->user_id = $user->id;

                $socialite_provider->save();
            }
        }

        return new PassportUser($user->getAuthIdentifier());
    }

    /**
     * {@inheritdoc}
     */
    public function getIdentifier()
    {
        return 'social_authorization';
    }
}