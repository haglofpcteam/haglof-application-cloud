<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Laravel\Cashier\Cashier;
use App\Resolvers\SocialUserResolver;
use Illuminate\Support\Facades\Schema;
use Coderello\SocialGrant\Resolvers\SocialUserResolverInterface;

class AppServiceProvider extends ServiceProvider
{

    /**
     * All of the container bindings that should be registered.
     *
     * @var array
     */
    public $bindings = [
        
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

        \URL::forceScheme('https');

        Schema::defaultStringLength(191);

        \App\Http\Resources\Organization::withoutWrapping();
        \App\Http\Resources\OrganizationCollection::withoutWrapping();
        \App\Http\Resources\ApplicationInstance::withoutWrapping();
        \App\Http\Resources\ApplicationInstanceCollection::withoutWrapping();
        \App\Http\Resources\PileApplication::withoutWrapping();
        \App\Http\Resources\PileApplicationCollection::withoutWrapping();
        \App\Http\Resources\PileApplicationSite::withoutWrapping();
        \App\Http\Resources\PileApplicationSiteCollection::withoutWrapping();
        \App\Http\Resources\PileApplicationLocation::withoutWrapping();
        \App\Http\Resources\PileApplicationLocationCollection::withoutWrapping();
        \App\Http\Resources\PileApplicationMeasurement::withoutWrapping();
        \App\Http\Resources\PileApplicationMeasurementCollection::withoutWrapping();
        \App\Http\Resources\PileApplicationTarget::withoutWrapping();
        \App\Http\Resources\PileApplicationTargetCollection::withoutWrapping();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
