<?php

namespace App\Providers;

use App\Auth\Grants\SocialAuthorizationGrant;
use App\Auth\Grants\ActivationAuthorizationGrant;
use Laravel\Passport\Passport;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Bridge\RefreshTokenRepository;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use League\OAuth2\Server\AuthorizationServer;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        app(AuthorizationServer::class)->enableGrantType(
            $this->makeSocialAuthorizationGrant(), Passport::tokensExpireIn()
        );

        app(AuthorizationServer::class)->enableGrantType(
            $this->makeActivationAuthorizationGrant(), Passport::tokensExpireIn()
        );

        Passport::routes();
    }

    protected function makeSocialAuthorizationGrant() 
    {
        $grant = new SocialAuthorizationGrant(
            $this->app->make(RefreshTokenRepository::class)
        );

        $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        return $grant;
    }

    protected function makeActivationAuthorizationGrant() 
    {
        $grant = new ActivationAuthorizationGrant(
            $this->app->make(RefreshTokenRepository::class)
        );

        $grant->setRefreshTokenTTL(Passport::refreshTokensExpireIn());

        return $grant;
    }
}
