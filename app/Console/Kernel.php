<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Http\Controllers\Scheduled\CleanPileDataController;
use App\Http\Controllers\Scheduled\CleanDigitechCloudDataController;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\SendReminders'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->exec('/usr/local/bin/php /home/haglofap/haglof-application-cloud/artisan queue:work --stop-when-empty --tries=3')
        ->everyMinute()
        ->thenPing('https://cronhub.io/ping/a4a97b60-7bb9-11e9-9abe-e78f48e77c4d');

        $schedule->call(new CleanPileDataController)
        ->everyMinute();

        $schedule->call(new CleanDigitechCloudDataController)
        ->everyMinute();

        $schedule->command('quicksand:run')
        ->daily();

        $schedule->command('subscriptions:remind')
        ->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
