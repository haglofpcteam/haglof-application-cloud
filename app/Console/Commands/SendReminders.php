<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\WordpressController;

class SendReminders extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriptions:remind';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send reminders to unconnected subscriptions';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $wordpresscontroller = new WordpressController();
        $wordpresscontroller->remind_unconnected_subscriptions();
    }
}
