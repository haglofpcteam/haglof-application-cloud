<?php

namespace App\Libraries;

use Exception;

class VolumeCalculator {  
    public $volume;
    public $area;
    public $average_height;
    public $newMeshIndex;

    function __construct($meshIndex,$targets) {
        $this->volume=0;
        $this->area=0;

        $count = count($meshIndex);

        $this->newMeshIndex=array();

        for($i=0;$i<$count;$i+=3) {
            $a = $targets[$meshIndex[$i]]->point;
            $b = $targets[$meshIndex[$i+1]]->point;
            $c = $targets[$meshIndex[$i+2]]->point;
            
            if(!($a->y==0&&$b->y==0&&$c->y==0)) {
                $triangle_area = self::AreaOfTriangle($a,$b,$c);
                $triangle_height = self::HeightOfTriangle($a,$b,$c);
                $triangle_volume = $triangle_area*$triangle_height;

                array_push($this->newMeshIndex,$meshIndex[$i]);
                array_push($this->newMeshIndex,$meshIndex[$i+1]);
                array_push($this->newMeshIndex,$meshIndex[$i+2]);
                
                $this->volume+=abs($triangle_volume);
                $this->area+=$triangle_area;
            } 
            
            
        }

        try {
            $this->average_height=$this->volume/$this->area;
        }
        catch(Exception $e) {
            $this->average_height=0;
        }
        
    }

    private static function AreaOfTriangle($a,$b,$c) {
        return abs(0.5*(($a->x*($b->z-$c->z))+($b->x*($c->z-$a->z))+($c->x*($a->z-$b->z))));
    }

    private static function HeightOfTriangle($a,$b,$c) {
        return ($a->y+$b->y+$c->y)/3;
    }

}

?>