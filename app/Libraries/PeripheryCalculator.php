<?php

namespace App\Libraries;

class PeripheryCalculator {
    public $periphery;

    function __construct($targets,$containsBasePoints) {

        $hull = array();
        $periphery = 0;

        foreach($targets as $t) {
            if($t->type=="BASE") {
                array_push($hull,$t);
            }
        }

        if($containsBasePoints) {
            usort($hull, function($a, $b)
            {
                return $a->seq<$b->seq;
            });
        }
        else {
            usort($hull, function($a, $b)
            {
                return $this->calculateAngle($a)>$this->calculateAngle($b);
            });
        }
        

        $hullSize=count($hull);

        for($i=1;$i<$hullSize;$i++) {
            $periphery+=$this->dist($hull[$i-1],$hull[$i]);
        }

        $this->periphery=$periphery;

    }

    
    private function calculateAngle($target) {

        if($target->point->x==0) {
            $rad = atan($target->point->z);
        }
        elseif($target->point->y==0) {
            $rad = atan($target->point->x);
        }
        elseif($target->point->y==0&&$target->point->x==0) {
            $rad = atan(0);
        }
        else {
            $rad = atan($target->point->z/$target->point->x);
        }
        
        if($target->point->x<0&&$target->point->z>0) {
            $rad+=pi();
        }
        else if($target->point->x<0&&$target->point->z<0) {
            $rad = abs($rad) + pi();
        }
        else if($target->point->x>0&&$target->point->z<0) {
            $rad+=(2*pi());
        }
        
        return $rad;
        
    }


    private function dist($p,$q) {
        $dx = ($q->point->x-$p->point->x);
        $dz = ($q->point->z-$p->point->z);

        return sqrt($dx * $dx + $dz * $dz);
    }
}

?>