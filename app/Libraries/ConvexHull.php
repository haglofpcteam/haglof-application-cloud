<?php

namespace App\Libraries;

    class ConvexHull {
        public $hull;

        function __construct($targets) {
            if($targets == null)        
                return null;

            if(count($targets)<3)
                return null;

            $hull = array();

            $pointOnHull=$targets[0];

            foreach($targets as $t) {
                if($t->point->x<$pointOnHull->point->x)
                    $pointOnHull=$t;
            }        

            do {
                array_push($hull,$pointOnHull);
                $endpoint = $targets[0];

                foreach($targets as $t) {
                    $turn = $this->findTurn($pointOnHull,$endpoint,$t);
                    if($endpoint==$pointOnHull || $turn < 0 || ($turn==0 && $this->dist($pointOnHull,$t) > $this->dist($endpoint,$pointOnHull))) {
                        $endpoint=$t;
                    }
                }  
                $pointOnHull=$endpoint;
            } while ($endpoint!=$hull[0]);

            $this->hull=$hull;
        }

        private function dist($p,$q) {
            $dx = ($q->point->x-$p->point->x);
            $dz = ($q->point->z-$p->point->z);

            return $dx * $dx + $dz * $dz;
        }

        private function findTurn($p,$q,$r) {
            $x1 = ($q->point->x - $p->point->x) * ($r->point->z - $p->point->z);
            $x2 = ($r->point->x - $p->point->x) * ($q->point->z - $p->point->z);
            return $x1 - $x2;
        }
    }

?>