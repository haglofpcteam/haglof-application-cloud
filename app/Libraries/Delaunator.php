<?php

namespace App\Libraries;

use Exception;

class Delaunator {    
    public $epsilon;
    public $edge_stack;
    public $coords;
    public $triangles;
    public $halfedges;
    public $_hashSize;
    public $hullPrev;
    public $hullNext;
    public $hullTri;
    public $_cx;
    public $_cy;
    public $trianglesLen;

    public static function from($points) {
        $n = count($points);
        $coords = array_fill(0, $n * 2,0);

        for($i = 0; $i < $n; $i++) {
            $p = $points[$i];
            $coords[2 * $i] = $p->point->x;
            $coords[2 * $i + 1] = $p->point->z;
        }

        return new Delaunator($coords);
    }

    function __construct($c) {

        $this->edge_stack = array_fill(0,512,0);
        $this->epsilon = 0.5 ** 52;

        $n = count($c) >> 1;
        if ($n == 0) {
            throw new Exception('coords is empty');
        }
        if (!(gettype($c[0]) == "integer" || gettype($c[0]) == "double")) {
            throw new Exception('Expected coords to contain numbers.');
        } 

        $this->coords = $c;

        // arrays that will store the triangulation graph
        $maxTriangles = 2 * $n - 5;
        $this->triangles = array_fill(0,$maxTriangles * 3,0);
        $this->halfedges = array_fill(0,$maxTriangles * 3,0);

        // temporary arrays for tracking the edges of the advancing convex hull
        $this->_hashSize = ceil(sqrt($n));
        $this->hullPrev = $hullprev = array_fill(0,$n,0); // edge to prev edge
        $this->hullNext = $hullNext = array_fill(0,$n,0); // edge to next edge
        $this->hullTri = $hullTri = array_fill(0,$n,0); // edge to adjacent triangle
        $hullHash = array_fill(0,$this->_hashSize,-1); // angular edge hash

        // populate an array of point indices; calculate input data bbox
        $ids = array();
        $minX = INF;
        $minY = INF;
        $maxX = 0-INF;
        $maxY = 0-INF;

        for ($i = 0; $i < $n; $i++) {
            $x = $this->coords[2 * $i];
            $y = $this->coords[2 * $i + 1];
            $minX=min($x,$minX);
            $minY=min($y,$minY);
            $maxX=max($x,$maxX);
            $maxY=max($y,$maxY);
            array_push($ids,$i);
        }
        $cx = ($minX + $maxX) / 2;
        $cy = ($minY + $maxY) / 2;

        $minDist = INF;
        $i0 = 0;
        $i1 = 0;
        $i2 = 0;

        // pick a seed point close to the center
        for ($i = 0; $i < $n; $i++) {
            $d = $this::dist($cx, $cy, $this->coords[2 * $i], $this->coords[2 * $i + 1]);
            if ($d < $minDist) {
                $i0 = $i;
                $minDist = $d;
            }
        }
        $i0x = $this->coords[2 * $i0];
        $i0y = $this->coords[2 * $i0 + 1];

        $minDist = INF;

        // find the point closest to the seed
        for ($i = 0; $i < $n; $i++) {
            if ($i == $i0) continue;
            $d = $this::dist($i0x, $i0y, $this->coords[2 * $i], $this->coords[2 * $i + 1]);
            if ($d < $minDist && $d > 0) {
                $i1 = $i;
                $minDist = $d;
            }
        }
        $i1x = $this->coords[2 * $i1];
        $i1y = $this->coords[2 * $i1 + 1];

        $minRadius = INF;

        // find the third point which forms the smallest circumcircle with the first two
        for ($i = 0; $i < $n; $i++) {
            if ($i == $i0 || $i == $i1) continue;
            $r = $this::circumradius($i0x, $i0y, $i1x, $i1y, $this->coords[2 * $i], $this->coords[2 * $i + 1]);
            if ($r < $minRadius) {
                $i2 = $i;
                $minRadius = $r;
            }
        }

        $i2x = $this->coords[2 * $i2];
        $i2y = $this->coords[2 * $i2 + 1];

        if ($minRadius == INF) {
            throw new Exception('No Delaunay triangulation exists for this input.');
        }

        // swap the order of the seed points for counter-clockwise orientation
        if ($this::orient($i0x, $i0y, $i1x, $i1y, $i2x, $i2y)) {
            $i = $i1;
            $x = $i1x;
            $y = $i1y;
            $i1 = $i2;
            $i1x = $i2x;
            $i1y = $i2y;
            $i2 = $i;
            $i2x = $x;
            $i2y = $y;
        }

        $center = $this::circumcenter($i0x, $i0y, $i1x, $i1y, $i2x, $i2y);
        $this->_cx = $center->x;
        $this->_cy = $center->y;

        $dists = array_fill(0,$n,0);;
        for ($i = 0; $i < $n; $i++) {
            $dists[$i] = $this::dist($this->coords[2 * $i], $this->coords[2 * $i + 1], $center->x, $center->y);
        }

        // sort the points by distance from the seed triangle circumcenter
        $this::quicksort($ids, $dists, 0, $n - 1);

        // set up the seed triangle as the starting hull
        $this->hullStart = $i0;
        $hullSize = 3;

        $this->hullNext[$i0] = $this->hullPrev[$i2] = $i1;
        $this->hullNext[$i1] = $this->hullPrev[$i0] = $i2;
        $this->hullNext[$i2] = $this->hullPrev[$i1] = $i0;

        $this->hullTri[$i0] = 0;
        $this->hullTri[$i1] = 1;
        $this->hullTri[$i2] = 2;

        $hullHash[$this->_hashKey($i0x, $i0y)] = $i0;
        $hullHash[$this->_hashKey($i1x, $i1y)] = $i1;
        $hullHash[$this->_hashKey($i2x, $i2y)] = $i2;

        $this->trianglesLen = 0;
        $this->_addTriangle($i0, $i1, $i2, -1, -1, -1);

        $xp = 0;
        $yp = 0;
        for ($k = 0; $k < count($ids); $k++) {
            $i = $ids[$k];
            $x = $this->coords[2 * $i];
            $y = $this->coords[2 * $i + 1];

            // skip near-duplicate points
            if ($k > 0 && abs($x - $xp) <= $this->epsilon && abs($y - $yp) <= $this->epsilon) continue;
            $xp = $x;
            $yp = $y;

            // skip seed triangle points
            if ($i == $i0 || $i == $i1 || $i == $i2) continue;

            // find a visible edge on the convex hull using edge hash
            $start = 0;
            $key = $this->_hashKey($x, $y);
            for ($j = 0; $j < $this->_hashSize; $j++) {
                $start = $hullHash[($key + $j) % $this->_hashSize];
                if ($start !== -1 && $start !== $this->hullNext[$start]) {
                    break;
                }
            }

            $start = $this->hullPrev[$start];
            $e = $start;
            $q = $this->hullNext[$e];
            while (!$this::orient($x, $y, $this->coords[2 * $e], $this->coords[2 * $e + 1], $this->coords[2 * $q], $this->coords[2 * $q + 1])) {
                $e = $q;
                if ($e == $start) {
                    $e = -1;
                    break;
                }
                $q = $this->hullNext[$e];
            }
            if ($e == -1) continue; // likely a near-duplicate point; skip it

            // add the first triangle from the point
            $t = $this->_addTriangle($e, $i, $this->hullNext[$e], -1, -1, $this->hullTri[$e]);

            // recursively flip triangles from the point until they satisfy the Delaunay condition
            $this->hullTri[$i] = $this->_legalize($t + 2);
            $this->hullTri[$e] = $t; // keep track of boundary triangles on the hull
            $hullSize++;

            // walk forward through the hull, adding more triangles and flipping recursively
            $n = $this->hullNext[$e];
            $q = $this->hullNext[$n];
            while ($this::orient($x, $y, $this->coords[2 * $n], $this->coords[2 * $n + 1], $this->coords[2 * $q], $this->coords[2 * $q + 1])) {
                $t = $this->_addTriangle($n, $i, $q, $this->hullTri[$i], -1, $this->hullTri[$n]);
                $this->hullTri[$i] = $this->_legalize($t + 2);
                $this->hullNext[$n] = $n; // mark as removed
                $hullSize--;
                $n = $q;
                $q = $this->hullNext[$n];
            }

            // walk backward from the other side, adding more triangles and flipping
            if ($e == $start) {
                $q = $this->hullPrev[$e];
                while ($this::orient($x, $y, $this->coords[2 * $q], $this->coords[2 * $q + 1], $this->coords[2 * $e], $this->coords[2 * $e + 1])) {
                    $t = $this->_addTriangle($q, $i, $e, -1, $this->hullTri[$e], $this->hullTri[$q]);
                    $this->_legalize($t + 2);
                    $this->hullTri[$q] = $t;
                    $this->hullNext[$e] = $e; // mark as removed
                    $hullSize--;
                    $e = $q;
                    $q = $this->hullPrev[$e];
                }
            }

            // update the hull indices
            $this->hullStart = $this->hullPrev[$i] = $e;
            $this->hullNext[$e] = $this->hullPrev[$n] = $i;
            $this->hullNext[$i] = $n;

            // save the two new edges in the hash table
            $hullHash[$this->_hashKey($x, $y)] = $i;
            $hullHash[$this->_hashKey($this->coords[2 * $e], $this->coords[2 * $e + 1])] = $e;
        }

        $this->hull = array_fill(0,$hullSize,0);
        $e = $this->hullStart;
        for ($i = 0; $i < $hullSize; $i++) {
            $this->hull[$i] = $e;
            $e = $this->hullNext[$e];
        }
        $this->hullPrev = $this->hullNext = $this->hullTri = null; // get rid of temporary arrays

        // trim typed triangle mesh arrays
        $this->triangles = array_slice($this->triangles,0, $this->trianglesLen);
        $this->halfedges = array_slice($this->halfedges,0, $this->trianglesLen);
    }

    

    function _hashKey($x, $y) {
        return floor(self::pseudoAngle($x - $this->_cx, $y - $this->_cy) * $this->_hashSize) % $this->_hashSize;
    }

    function _legalize($a) {
        $i = 0;
        $ar = 0;

        // recursion eliminated with a fixed-size stack
        while (true) {
            $b = $this->halfedges[$a];

            /* if the pair of triangles doesn't satisfy the Delaunay condition
             * (p1 is inside the circumcircle of [p0, pl, pr]), flip them,
             * then do the same check/flip recursively for the new pair of triangles
             *
             *           pl                    pl
             *          /||\                  /  \
             *       al/ || \bl            al/    \a
             *        /  ||  \              /      \
             *       /  a||b  \    flip    /___ar___\
             *     p0\   ||   /p1   =>   p0\---bl---/p1
             *        \  ||  /              \      /
             *       ar\ || /br             b\    /br
             *          \||/                  \  /
             *           pr                    pr
             */
            
            $a0 = $a - $a % 3;
            $ar = $a0 + ($a + 2) % 3;

            if ($b == -1) { // convex hull edge
                if ($i == 0) {
                    break;
                } 
                $a = $this->edge_stack[--$i];
                continue;
            }

            $b0 = $b - $b % 3;
            $al = $a0 + ($a + 1) % 3;
            $bl = $b0 + ($b + 2) % 3;

            $p0 = $this->triangles[$ar];
            $pr = $this->triangles[$a];
            $pl = $this->triangles[$al];
            $p1 = $this->triangles[$bl];

            $illegal = self::inCircle(
                $this->coords[2 * $p0], $this->coords[2 * $p0 + 1],
                $this->coords[2 * $pr], $this->coords[2 * $pr + 1],
                $this->coords[2 * $pl], $this->coords[2 * $pl + 1],
                $this->coords[2 * $p1], $this->coords[2 * $p1 + 1]);

            if ($illegal) {
                $this->triangles[$a] = $p1;
                $this->triangles[$b] = $p0;

                $hbl = $this->halfedges[$bl];

                // edge swapped on the other side of the hull (rare); fix the halfedge reference
                if ($hbl == -1) {
                    $e = $this->hullStart;
                    do {
                        if ($this->hullTri[$e] == $bl) {
                            $this->hullTri[$e] = $a;
                            break;
                        }
                        $e = $this->hullNext[$e];
                    } while ($e != $this->hullStart);
                }
                $this->_link($a, $hbl);
                $this->_link($b, $this->halfedges[$ar]);
                $this->_link($ar, $bl);

                $br = $b0 + ($b + 1) % 3;

                // don't worry about hitting the cap: it can only happen on extremely degenerate input
                if ($i < count($this->edge_stack)) {
                    $this->edge_stack[$i++] = $br;
                }
                } else {
                    if ($i == 0) break;
                    $a = $this->edge_stack[--$i];
                }
        }

        return $ar;
    }

    

    function _link($a, $b) {
        $this->halfedges[$a] = $b;
        if ($b != -1) $this->halfedges[$b] = $a;
    }

    // add a new triangle given vertex indices and adjacent half-edge ids
    function _addTriangle($i0, $i1, $i2, $a, $b, $c) {
        $t = $this->trianglesLen;

        $this->triangles[$t] = $i0;
        $this->triangles[$t + 1] = $i1;
        $this->triangles[$t + 2] = $i2;

        $this->_link($t, $a);
        $this->_link($t + 1, $b);
        $this->_link($t + 2, $c);

        $this->trianglesLen += 3;

        return $t;
    }

    // monotonically increases with real angle, but doesn't need expensive trigonometry
    private static function pseudoAngle($dx, $dy) {
        $p = $dx / (abs($dx) + abs($dy));
        return (($dy > 0) ? (3 - $p) : (1 + $p) / 4); // [0..1]
    }

    private static function dist($ax, $ay, $bx, $by) {
        $dx = $ax - $bx;
        $dy = $ay - $by;
        return (($dx * $dx) + ($dy * $dy));
    }

    private static function orient($px, $py, $qx, $qy, $rx, $ry) {
        return ((($qy - $py) * ($rx - $qx)) - (($qx - $px) * ($ry - $qy))) < 0;
    }


    private static function inCircle($ax, $ay, $bx, $by, $cx, $cy, $px, $py) {
        $dx = $ax - $px;
        $dy = $ay - $py;
        $ex = $bx - $px;
        $ey = $by - $py;
        $fx = $cx - $px;
        $fy = $cy - $py;

        $ap = (($dx * $dx) + ($dy * $dy));
        $bp = (($ex * $ex) + ($ey * $ey));
        $cp = (($fx * $fx) + ($fy * $fy));

        return $dx * ($ey * $cp - $bp * $fy) -
            $dy * ($ex * $cp - $bp * $fx) +
            $ap * ($ex * $fy - $ey * $fx) < 0;
    }

    private static function circumradius($ax, $ay, $bx, $by, $cx, $cy) {
        $dx = $bx - $ax;
        $dy = $by - $ay;
        $ex = $cx - $ax;
        $ey = $cy - $ay;

        $bl = $dx * $dx + $dy * $dy;
        $cl = $ex * $ex + $ey * $ey;
        
        $temp = ($dx * $ey - $dy * $ex);
        if($temp==0) {
            $temp=0.00001;
        }

        $d = 0.5 / $temp;

        $x = ($ey * $bl - $dy * $cl) * $d;
        $y = ($dx * $cl - $ex * $bl) * $d;

        return $x * $x + $y * $y;
    }



    private static function circumcenter($ax, $ay, $bx, $by, $cx, $cy) {
        try {
            $dx = $bx - $ax;
            $dy = $by - $ay;
            $ex = $cx - $ax;
            $ey = $cy - $ay;
    
            $bl = $dx * $dx + $dy * $dy;
            $cl = $ex * $ex + $ey * $ey;
            $d = 0.5 / ($dx * $ey - $dy * $ex);
    
            $x = $ax + ($ey * $bl - $dy * $cl) * $d;
            $y = $ay + ($dx * $cl - $ex * $bl) * $d;
    
    
            $object = (object) [
                'x' => $x,
                'y' => $y,
              ];
    
            return $object;
        }
        catch (Exception $e) {
            return self::circumcenter($ax+(rand(0,1000)/100000), 
            $ay+(rand(0,1000)/100000), 
            $bx+(rand(0,1000)/100000), 
            $by+(rand(0,1000)/100000), 
            $cx+(rand(0,1000)/100000), 
            $cy+(rand(0,1000)/100000));
        }

    }


    private static function quicksort(&$ids, &$dists, $left, $right) {
        if ($right - $left <= 20) {
            for ($i = $left + 1; $i <= $right; $i++) {
                $temp = $ids[$i];
                $tempDist = $dists[$temp];
                $j = $i - 1;
                while ($j >= $left && $dists[$ids[$j]] > $tempDist) {
                    $ids[$j + 1] = $ids[$j--];
                }
                $ids[$j + 1] = $temp;
            }
        } else {
            $median = ($left + $right) >> 1;
            $i = $left + 1;
            $j = $right;
            self::swap($ids, $median, $i);
            if ($dists[$ids[$left]] > $dists[$ids[$right]]) self::swap($ids, $left, $right);
            if ($dists[$ids[$i]] > $dists[$ids[$right]]) self::swap($ids, $i, $right);
            if ($dists[$ids[$left]] > $dists[$ids[$i]]) self::swap($ids, $left, $i);

            $temp = $ids[$i];
            $tempDist = $dists[$temp];
            while (true) {
                    $IdsCount=count($ids);
                    do {
                        $i++;
                        if(!($i<$IdsCount)) {
                            break;
                        }
                    }while ($dists[$ids[$i]] < $tempDist);
                    do $j--; while ($dists[$ids[$j]] > $tempDist);

                if ($j < $i) break;
                self::swap($ids, $i, $j);
            }
            $ids[$left + 1] = $ids[$j];
            $ids[$j] = $temp;

            if ($right - $i + 1 >= $j - $left) {
                self::quicksort($ids, $dists, $i, $right);
                self::quicksort($ids, $dists, $left, $j - 1);
            } else {
                self::quicksort($ids, $dists, $left, $j - 1);
                self::quicksort($ids, $dists, $i, $right);
            }
        }
    }



    private static function swap(&$arr, $i, $j) {
        $tmp = $arr[$i];
        $arr[$i] = $arr[$j];
        $arr[$j] = $tmp;
    }
}

?>