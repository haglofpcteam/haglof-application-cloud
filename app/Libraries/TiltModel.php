<?php

namespace App\Libraries;

class TiltModel {  
    public $tilt;
    public $tiltpercent;
    public $transformedTargets;

    function __construct($targets) {
        $xb = array();
        $yb = array();
        $zb = array();

        $this->transformedTargets = array();

        foreach($targets as $t) {
            array_push($this->transformedTargets,clone $t);
        }

        $coeff = array_fill(0,4,0);
        $dist = array_fill(0,6,0);

        foreach($this->transformedTargets as $t) {
            if($t->type=="BASE") {
                array_push($xb,$t->point->x);
                array_push($yb,$t->point->z);
                array_push($zb,$t->point->y);
            }
        }

        self::OrteReg(count($xb),$xb,$yb,$zb,$coeff,$dist);

        $A = $coeff[0]; 
        $B = $coeff[1]; 
        $C = $coeff[2]; 
        $D = $coeff[3];

        $TMAT = array_fill(0,3,array_fill(0,3,0));

        $IFEL=0;

        self::TR3PLAN($A, $B, $C, $TMAT, $IFEL);
        if ($C != 0.0)
        {
            $this->tilt = atan(sqrt($A * $A + $B * $B) / $C) * 180.0 / M_PI;
            $this->tiltpercent = sqrt($A * $A + $B * $B) / $C * 100.0;
        }
        else
            $this->tiltpercent = $this->tilt = 0.0;



        foreach($this->transformedTargets as $t)
        {
            $t->point->x = $TMAT[0][0] * $t->point->x + $TMAT[0][1] * $t->point->z + $TMAT[0][2] * $t->point->y;
            $t->point->z = $TMAT[1][0] * $t->point->x + $TMAT[1][1] * $t->point->z + $TMAT[1][2] * $t->point->y;
            $t->point->y = $TMAT[2][0] * $t->point->x + $TMAT[2][1] * $t->point->z + $TMAT[2][2] * $t->point->y + $D;

            if($t->type=="BASE") {
                $t->point->y = 0.0;
            }
        }
    }

    /*
    C       TAR FRAM KOEFFICIENTERNA A,B,C,D = COEFF(1),...,COEFF(4) 
    C       MED A*A+B*B+C*C=1 (NORMALFORM) SOM GER MINSTA SUMMA
    C       KVADRATISKA AVSTND TILL PUNKTERNA (Xi,Yi,Zi),i=1,...,N
    C       DIST ŽR MINSTA SUMMAN (AV KVAD AVST)
    C
    C       PUNKTERNA INPUT P FIL INFIL, MED
    C       REC1 = N
    C       REC 2, 3, ..., N+1 = X Y Z
    C       ANTAGES VARA INLŽST I HUVUDPROGRAM
    */
    private static function OrteReg($N, &$X, &$Y, &$Z, &$COEFF, &$DIST)
    {
        $K=0;
        $T=1.0;
        $AT=0;
        $P=0;

        $S = array();
        $V = array_fill(0,3,0); 
        $EG = array_fill(0,3,0); 
        $C = array_fill(0,3,array_fill(0,3,0.0));
        $EGV = array_fill(0,3,array_fill(0,3,0.0));

        array_push($S,$X[0]);
        array_push($S,$Y[0]);
        array_push($S,$Z[0]);


        for ($L = 1; $L < $N; $L++)
        {
            $V[0] = $X[$L];
            $V[1] = $Y[$L];
            $V[2] = $Z[$L];
            $T += 1.0;
            $AT = ($T - 1.0) / $T;
            for ($I = 0; $I < 3; $I++)
            {
                $P = $AT * ($V[$I] - $S[$I]);
                for ($J = $I; $J < 3; $J++)                    
                {
                    $C[$J][$I] = $C[$J][$I] + $P * ($V[$J] - $S[$J]);
                }
            }
            for ($I = 0; $I < 3; $I++)
                $S[$I] = $S[$I] + ($V[$I] - $S[$I]) / $T;
        }

        for ($I = 0; $I < 3; $I++)
            for ($J = 0; $J < 3; $J++)
                $C[$I][$J] = $C[$J][$I];

        self::EIGJAC($C, $EG, $EGV, 3, 3);

        /*
        C       VI SKA VŽLJA VEKTORN MED MINSTA EGENVŽRDET
        */
        if ($EG[0] <= $EG[1])
            if ($EG[0] <= $EG[2])
                $K = 0;
            else
                $K = 2;
        else
            if ($EG[1] <= $EG[2])
                $K = 1;
            else
                $K = 2;

        $DIST[0] = $EG[$K];

        $T = 0.0;
        for ($I = 0; $I < 3; $I++)
        {

            $COEFF[$I] = $EGV[$I][$K];
            $T = $T + $COEFF[$I] * $S[$I];
        }
        $COEFF[3] = -$T;


        return 1;
    }

        /*       EGENVŽRDEN, EGENVEKTORER SYMM MATRIS, JACOBIS METOD, FR™BERG AVSNITT 6.2.
        C       KONVERGENS I ALLMŽNHET INTE SPECIELLT SNABB
        C       RŽKNA MED (F™R EPS=1.D-6) 15-20 ITER OM N=4, 60-TALET OM N=7,
        C       KRING 130 F™R N=10,  DVS SER UT SOM CIRKA 1.5*N*(N-1).
        C       MAX DIM = 15
        C       A = INPUT SYMM MATRIS, F™RST™RS EJ
        C       MD= DIM A,V I HP   N=AKTUELL DIM
        C       V = VEKTOR MED EGENVŽRDEN (OM IFEL>0 S NOLL)
        C       P'S KOLONNER ŽR MOTSVARANDE EGENVEKTORER (P MATRIS)
        C       IFEL=1 OM DIM-FEL
        C           =2 OM ITERATIONSFEL, 0 ANNARS
        C       MAXIT=ANTAL TILLTNA ITERATIONER
        C       EPS=AVBROTTS-EPS IN  OCH (OBS!), OM IFEL=2 S TESTVARIABELNS VŽRDE. 
        */
        private static function EIGJAC(&$A, &$V, &$P, $N, $MD)
        {
            $ITER=0; 
            $IFEL=0;
            $EPS=10**-6; 
            $MAXIT=20;
            $MAXD=15;
            $I=0;
            $J=0;
            $N1=0;
            $I1=0;
            $IM=0;
            $JM=0;
            $K=0;

            $PI=atan(1.0);
            $T=0; 
            $T1=0; 
            $AA=0; 
            $BB=0; 
            $AB=0; 
            $R=0; 
            $S=0; 
            $F=0; 
            $C1=0; 
            $C2=0;

            $B = array_fill(0,15,array_fill(0,15,0.0));

            if ($N > $MD || $MD > 15)
            {
                $IFEL = 1;
                return 0;
            }

            for ($I = 0; $I < $N; $I++)
                for ($J = 0; $J < $N; $J++)
                {
                    $P[$I][$J] = 0.0;
                    if ($J == $I)
                        $P[$I][$J] = 1.0;
                    $B[$I][$J] = $A[$I][$J];
                }
            $ITER = 0;
            $N1 = $N - 1;
            /*
            C       NYTT ITERATIONSSTEG.
            C       F™RST MAX(ABS(B)) UTANF™R DIAGONALEN
            */
            do
            {

                $ITER = $ITER + 1;
                $T = 0.0;
                for ($I = 0; $I < $N1; $I++)
                {
                    $I1 = $I + 1;
                    for ($J = $I1; $J < $N; $J++)
                    {
                        $T1 = abs($B[$I][$J]);
                        if ($T1 > $T)
                        {
                            $IM = $I;
                            $JM = $J;
                            $T = $T1;
                        }

                    }
                }
                if ($T == 0)
                {
                    for ($I = 0; $I < $N; $I++)
                        $V[$I] = $B[$I][$I];
                }
                /*
                C        BESTŽMNING AV VRIDNINGSVINKEL MM
                */
                $AA = $B[$IM][$IM];
                $BB = $B[$JM][$JM];
                $AB = $B[$IM][$JM];
                $T = $AA - $BB;
                $R = sqrt($T * $T + 4.0 * $AB * $AB);
                $S = 1.0;
                if ($AA < $BB)
                    $S = -$S;
                if (abs($T) > (10**-10))
                {
                    $F = 2.0 * $AB / $T;
                    $F = atan($F) / 2.0;
                }
                else
                    if ($AB > 0.0)
                        $F = $PI;
                    else
                        $F = $PI;
                $C1 = cos($F);
                $C2 = sin($F);
                /*
                C       NY P-MATRIS
                */
                for ($K = 0; $K < $N; $K++)
                {
                    $T = $P[$K][$IM];
                    $P[$K][$IM] = $T * $C1 + $P[$K][$JM] * $C2;
                    $P[$K][$JM] = -$T * $C2 + $P[$K][$JM] * $C1;

                }

                /*
                C       NY B-MATRIS
                C       G™RS I TV STEG
                */
                for ($K = 0; $K < $N; $K++)
                {
                    $T = $B[$K][$IM];
                    $B[$K][$IM] = $T * $C1 + $B[$K][$JM] * $C2;
                    $B[$K][$JM] = -$T * $C2 + $B[$K][$JM] * $C1;
                }
                for ($K = 0;  $K < $N; $K++)
                {
                    $T = $B[$IM][$K];
                    $B[$IM][$K] = $C1 * $T + $C2 * $B[$JM][$K];
                    $B[$JM][$K] = -$C2 * $T + $C1 * $B[$JM][$K];
                }

                /*
                C       GERSHGORIN-TEST
                */
                $T = 0.0;
                for ($I = 0; $I < $N; $I++)
                {
                    $T1 = 0.0;
                    for ($J = 0; $J < $N; $J++)
                    {
                        if ($J != $I)//IF(J.EQ.I) GOTO 29
                            $T1 = $T1 + abs($B[$I][$J]);
                    }
                    if ($T1 > $T)//IF(T1.GT.T) T=T1
                        $T = $T1;
                }
                if ($T < $EPS)///IF(T.LT.EPS) THEN
                {
                    for ($I = 0; $I < $N; $I++)// DO 40 I=1,N
                        $V[$I] = $B[$I][$I];
                    return 1;
                }
            } while ($ITER < $MAXIT);
            $IFEL = 2;
            $EPS = $T;


            return 1;
        }

        /*
        C       G™R EN ON-KOORDINATTRANSFORMATION I R3 S ATT PLANET
        C       AX+BY+CZ+D=0 KOMMER ATT BLI Zprim=D (ALLTS "NYTT" XY-PLAN.
        C       TRANSFORMATIONEN ŽR (X,Y,Z)(T)=TMAT*(Xp,Yp,Zp).
        C       PLANET SA VARA GIVET I NORMALFORM (A*A+B*B+C*C=1).
        C
        C       IFEL=2 OM EJ NORMALFORM, =1 OM OM™JLIG TRANSF, =0 ANNARS
        */

        private function TR3PLAN($A, $B, $C, &$TMAT, &$IFEL)
        {
            $IFEL = 0;

            /*
            C       TESTA F™RST ATT PLANET GIVET I NORMALFORM
            */
            $T1 = $A * $A + $B * $B + $C * $C;
            if (abs($T1 - 1.0) > (10**-12))
            {
                $IFEL = 2;
                return 1;
            }

            /*
            C       DET GIVNA PLANET GER TREDJE KOLUMNEN I TMAT (Z)
            */
            $TMAT[2][0] = $A;
            $TMAT[2][1] = $B;
            $TMAT[2][2] = $C;

            /*
            C       TAG EN VEKTOR (NŽSTAN) VILKEN SOM HELST OCH
            C       ORTOGONALISERA. GER F™RST KOLUMNEN I TMAT (X).
            */
            //do

            $T1 = 1.0;
            $T2 = 0.0;
            $T3 = 0.0;
            $TA = $A * $T1 + $B * $T2 + $C * $T3;
            //C        IF(TA.EQ.0.D0) THEN
            //C          IFEL=1
            //C          RETURN
            //C        ENDIF
            $XLAM = $TA;
            $T1 = $T1 - $XLAM * $A;
            $T2 = $T2 - $XLAM * $B;
            $T3 = $T3 - $XLAM * $C;
            $DD = sqrt($T1 * $T1 + $T2 * $T2 + $T3 * $T3);
            $TMAT[0][0] = $T1 / $DD;
            $TMAT[0][1] = $T2 / $DD;
            $TMAT[0][2] = $T3 / $DD;
            /*
            C       TAG NY VEKTOR P SAMMA SŽTT
            */

            //do

            $T1 = 0.0;
            $T2 = 1.0;
            $T3 = 0.0;
            $TA = $A * $T1 + $B * $T2 + $C * $T3;
            $TA2 = $T1 * $TMAT[0][0] + $T2 * $TMAT[0][1] + $T3 * $TMAT[0][2];
            //C        IF(TA.EQ.0.OR.TA2.EQ.0.D0) THEN
            //C          IFEL=1
            //C         RETURN
            //C       ENDIF
            $XLAM = $TA;
            $XLAM2 = $TA2;
            $T1 = $T1 - $XLAM * $TMAT[2][0] - $XLAM2 * $TMAT[0][0];
            $T2 = $T2 - $XLAM * $TMAT[2][1] - $XLAM2 * $TMAT[0][1];
            $T3 = $T3 - $XLAM * $TMAT[2][2] - $XLAM2 * $TMAT[0][2];
            $DD = sqrt($T1 * $T1 + $T2 * $T2 + $T3 * $T3);
            $TMAT[1][0] = $T1 / $DD;
            $TMAT[1][1] = $T2 / $DD;
            $TMAT[1][2] = $T3 / $DD;

            /*
            C       KOLLA OM POSITIVT ORIENTERAT - VIA DETEMINANTEN
            */
            $DET = $TMAT[0][0] * $TMAT[1][1] * $TMAT[2][2] +
           $TMAT[0][1] * $TMAT[1][2] * $TMAT[2][0] +
           $TMAT[0][2] * $TMAT[1][0] * $TMAT[2][1] - $TMAT[2][0] * $TMAT[1][1] * $TMAT[2][0] -
           $TMAT[1][2] * $TMAT[2][1] * $TMAT[0][0] - $TMAT[2][2] * $TMAT[1][0] * $TMAT[0][1];

            if ($DET < 0.0)
                for ($I = 0; $I < 3; $I++)
                    $TMAT[2][$I] = -$TMAT[2][$I];

            return 1;
        }

}


?>