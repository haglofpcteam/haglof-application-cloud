<?php

namespace App\Libraries;

use App\Libraries\ConvexHull;
use App\Libraries\Delaunator;
use App\Libraries\TiltModel;
use App\Libraries\VolumeCalculator;
use DateTime;
use DateTimeZone;
use Carbon\Carbon;

class PileModel {
    public $targets;
    private $transformedTargets;
    public $meshIndex;
    public $center_x;
    public $center_y;
    public $center_z;
    public $size_x;
    public $size_y;
    public $size_z;
    public $volume;
    public $area;
    public $generatedBasePoints=false;
    public $average_height;
    public $tilt;
    public $tiltpercent;
    public $periphery;
    public $center_latitude;
    public $center_longitude;
    public $type;
    public $volume_adjustment_factor;
    public $density;
    public $weight;
    public $file_id;
    public $user;
    public $measurement_time;
    private $from;
    private $to;
    public $measurement_system;


    public static function fromArray($targets) {
        $instance = new self();
        $instance->targets = $targets;
        $instance->calculate();
        return $instance;
    }

    function __construct($volume_adjustment_factor,$density) {
        $this->volume_adjustment_factor=$volume_adjustment_factor;
        $this->density=$density;
        $this->targets = array();

        $this->from=Carbon::maxValue();
        $this->to=Carbon::minValue();
    }

    public function addTarget($target) {
        $t = new target;

        $t->id=$target->id;
        $t->type=$target->type;
        $t->latitude=$target->latitude;
        $t->ns=$target->ns;
        $t->longitude=$target->longitude;
        $t->ew=$target->ew;
        $t->altitude=$target->altitude;
        $t->hdop=$target->hdop;
        $t->timestamp=$target->toLocalTime('time')["date_time_object"];
        $t->seq=$target->seq;
        $t->sd=$target->sd;
        $t->hd=$target->hd;
        $t->h=$target->h;
        $t->pitch=$target->pitch;
        $t->az=$target->az;
        $t->x=$target->x;
        $t->y=$target->y;
        $t->z=$target->z;
        $t->utm_zone=$target->utm_zone;

        $this->from=$this->from->min($t->timestamp);
        $this->to=$this->to->max($t->timestamp);

        array_push($this->targets,$t);
    }

    public function calculate() {

        $containsBasePoints=false;

        $max_x=-INF;
        $min_x=INF;
        $max_y=-INF;
        $min_y=INF;
        $max_z=-INF;
        $min_z=INF;

        $max_latitude=-INF;
        $max_longitude=-INF;
        $min_latitude=INF;
        $min_longitude=INF;

        foreach($this->targets as $t) {
            if($t->type=="BASE") {
                $containsBasePoints=true;
            }

            $max_x=max($max_x,$t->x);
            $max_z=max($max_z,$t->y);
            $max_y=max($max_y,$t->z);
            $min_x=min($min_x,$t->x);
            $min_z=min($min_z,$t->y);
            $min_y=min($min_y,$t->z);

            $max_latitude=max($max_latitude,$t->latitude);
            $max_longitude=max($max_longitude,$t->longitude);
            $min_latitude=min($min_latitude,$t->latitude);
            $min_longitude=min($min_longitude,$t->longitude);

        }

        $this->measurement_time=$this->to->diffInSeconds($this->from);

        $this->center_latitude=$min_latitude+(($max_latitude-$min_latitude)/2);
        $this->center_longitude=$min_longitude+(($max_longitude-$min_longitude)/2);

        $this->size_x = ($max_x-$min_x);
        $this->size_y = ($max_y-$min_y);
        $this->size_z = ($max_z-$min_z);

        $this->center_x = $min_x + ($this->size_x/2);
        $this->center_y = $min_y + ($this->size_y/2);
        $this->center_z = $min_z + ($this->size_z/2);

        foreach($this->targets as $t) {
            $point = new point;
            $point->x = $t->x-$this->center_x;
            $point->y = $t->z-$this->center_y;
            $point->z = $t->y-$this->center_z;

            $t->point=$point;
        }

        if(!$containsBasePoints) {
            $convexHull = new ConvexHull($this->targets);

            $this->generatedBasePoints=true;

            //Mark hull coordinates
            foreach($this->targets as $t) {
                foreach($convexHull->hull as $h) {
                    if($t==$h) {
                        $t->type="BASE";
                    }
                }
            }
        }

        $TiltModel= new TiltModel($this->targets);

        $this->transformedTargets = $TiltModel->transformedTargets;
        $this->tilt = $TiltModel->tilt;
        $this->tiltpercent = $TiltModel->tiltpercent;

        $indexDelaunator = Delaunator::from($this->targets);

        $this->meshIndex = $indexDelaunator->triangles;

        $volumeCalculator = new VolumeCalculator($this->meshIndex,$this->transformedTargets);
        $this->meshIndex = $volumeCalculator->newMeshIndex;

        $peripheryCalculator = new PeripheryCalculator($this->targets,$containsBasePoints);
        $this->periphery = $peripheryCalculator->periphery;

        $this->volume=$volumeCalculator->volume*$this->volume_adjustment_factor;

        $this->weight=$this->volume*$this->density;

        $this->area=$volumeCalculator->area;
        $this->average_height=$volumeCalculator->average_height;

    }
}

class point {
    public $x;
    public $y;
    public $z;
}

class target {
    public $id;
    public $type;
    public $latitude;
    public $ns;
    public $longitude;
    public $ew;
    public $altitude;
    public $hdop;
    public $timestamp;
    public $seq;
    public $sd;
    public $hd;
    public $h;
    public $pitch;
    public $az;
    public $x;
    public $y;
    public $z;
    public $utm_zone;
    public $point;

    function __clone()
    {
        $this->point = clone $this->point;
    }
}

?>
