<?php

function getClassName($h) {
    $stringarray = explode("\\",$h->revisionable_type);

    $model = $h->revisionable_type::find($h->revisionable_id);

    if($model) {
        $new_string = $stringarray[count($stringarray)-1] . "(" . $model->identifiableName() . ")";
    }
    else {
        $new_string = $stringarray[count($stringarray)-1] . "(" . $h->revisionable_id . ")";
    }

    return $new_string;
}

function getCurrentDelay() {

    $delay=10;

    $baseDelay = time();

    $getDelay = cache('jobs.delay', $baseDelay);

    if($baseDelay>$getDelay) {
        $getDelay=$baseDelay;
    }

    $setDelay = $getDelay+$delay;

    cache([
        'jobs.delay' => $setDelay
    ], $setDelay-$baseDelay);

    return $setDelay-$baseDelay;
}


