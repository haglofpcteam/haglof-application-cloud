<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use Illuminate\Http\Client\Response;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;


class MobileAuthController extends Controller
{
    private $provider_login_url = [
      'APPLE' => 'https://appleid.apple.com/auth/authorize',
      'FACEBOOK' => 'https://m.facebook.com/dialog/oauth',
      'GOOGLE' => 'https://accounts.google.com/o/oauth2/v2/auth',
      // 'LINKEDIN' => 'https://www.linkedin.com/oauth/v2/authorization',
      'MICROSOFT' => 'https://login.live.com/oauth20_authorize.srf',
    ];
    /* Code callback, redirects to token callback */
    private $provider_token_url = [
      'APPLE' => 'https://appleid.apple.com/auth/token',
      'FACEBOOK' => 'https://graph.facebook.com/v3.3/oauth/access_token',
      'GOOGLE' => 'https://www.googleapis.com/oauth2/v4/token',
      // 'LINKEDIN' => 'https://www.linkedin.com/oauth/v2/accessToken', // Not used
      'MICROSOFT' => 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
    ];
    private $provider_scopes = [
      'APPLE' => "name email",
      'FACEBOOK' => null,
      'GOOGLE' => "openid profile email",
      // 'LINKEDIN' => "r_liteprofile r_emailaddress", // Not used
      'MICROSOFT' => "User.Read",
    ];

    private function get_redirect_uri($provider) {
        return sprintf('%s/mobileauth/%s', config('mobileauth.APP_URL'), $provider);
    }

    public function login(Request $request, $_provider) {
      $provider = strtoupper($_provider);
      $url = $this->provider_token_url[$provider];

      $params = [
        'client_id' => config(sprintf("mobileauth.%s_KEY", $provider)),
        'redirect_uri' => $this->get_redirect_uri($_provider),
        'response_type' => 'code',
      ];

      if ($this->provider_scopes[$provider]) {
        $params['scope'] = $this->provider_scopes[$provider];
      }

      $query_string = http_build_query($params);
      $redirect_uri = sprintf("%s?%s", $this->provider_login_url[$provider], $query_string);

      return redirect($redirect_uri);
    }

    public function get_token(Request $request, $_provider) {
        $app_url = "hagloflink://auth/callback?";
        $provider = strtoupper($_provider);
        $url = $this->provider_token_url[$provider];

        $params = [
            'client_id' => config(sprintf("mobileauth.%s_KEY", $provider)),
            'client_secret' => config(sprintf("mobileauth.%s_SECRET", $provider)),
            'grant_type' => 'authorization_code',
            'scope' => $this->provider_scopes[$provider],
            'redirect_uri' => $this->get_redirect_uri($_provider),
            'code' => $request->code,
        ];

        if ($provider === 'FACEBOOK') {
            $response = Http::get(sprintf('%s?%s', $url, http_build_query($params)));
        } else {
            $response = Http::asForm()->post($url, $params);
        }

        $query_string = http_build_query(json_decode($response));
        $redirect_uri = sprintf("%s%s", $app_url, $query_string);

        //return $response;
        return redirect($redirect_uri);
    }

}