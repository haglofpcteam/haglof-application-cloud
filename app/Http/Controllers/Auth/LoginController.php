<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\Model\User;
use App\Model\SocialiteProvider;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except(['logout','redirectToProvider','handleProviderCallback','removeProvider']);
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function removeProvider($provider)
    {
        $user=Auth::user();

        if(!$user) {
            return redirect()->back();
        }

        $count=$user->socialiteProviders()->count();

        $p = $user->socialiteProviders()->where('provider',$provider)->first();

        if($p&&($count>1||$user->password!=null)) {
            $p->delete();
            return redirect()->back();
        }

        return redirect()->back()->with(['code' => 'danger', 'message' => 'You have to have a password before erasing the last Social connection']);

    }

    public function handleProviderCallback($provider)
    {
        $redirectTo = route('organizations');

        if($provider=="twitter") {
            $userData = Socialite::driver($provider)->user();
        }
        else {
            $userData = Socialite::driver($provider)->stateless()->user();
        }

        $socialite_provider = SocialiteProvider::where('provider_id',$userData->id)->where('provider',$provider)->first();

        $user=null;

        if($socialite_provider) {
            $user = $socialite_provider->user()->first();
        }

        if (!$user) {

            $user=Auth::user();

            if(!$user) {

                if(User::where('email',$userData->email)->first()) {
                    return redirect()->route('login')->with(['code' => 'danger', 'message' => 'A user with that email already exists']);
                }

                $email_verified_at = null;

                if($userData->email!=null) {
                    $email_verified_at=now();
                }

                $user = User::create([
                    'name'     => $userData->name,
                    'email'    => $userData->email,
                    'email_verified_at' => $email_verified_at
                ]);
            }
            else {
                $redirectTo = route('edit_user',$user);
            }

            $user->activated=true;

            $socialite_provider = new SocialiteProvider();
            $socialite_provider->provider = $provider;
            $socialite_provider->provider_id = $userData->id;
            $socialite_provider->user_id = $user->id;

            $socialite_provider->save();
            $user->save();
        }

        Auth::login($user, true);

        return redirect(request()->session()->pull('url.intended', $redirectTo));
    }

}
