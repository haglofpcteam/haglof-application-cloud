<?php

namespace App\Http\Controllers;

use App\Model\Wordpress\Order;
use App\Model\Wordpress\OrderItem;
use App\Model\Wordpress\Subscription;
use App\Model\Application;
use App\Model\ApplicationInstance;
use App\Model\Organization;
use App\Model\User;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App\Mail\MailNewSubscription;
use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\Log;

class WordpressController extends Controller
{
    public function index()
    {
        $subscriptions=Subscription::where('post_type','shop_subscription')->get();

        foreach($subscriptions as $subscription) {
            $applicationInstanceId=DB::table('application_instances_subscription')->where('subscription_id',$subscription->ID)->pluck('application_instance_id');
            $order=Order::find($subscription->post_parent);
            $subscription['email']=$order!=null?$order->email:null;
            $subscription['application']=Application::where('product_id',$subscription->productId)->first();
            $subscription['application_instance']=ApplicationInstance::find($applicationInstanceId);
        }

        return view('admin_subscriptions')->with("subscriptions",$subscriptions);
    }

    public function remind_unconnected_subscriptions() {
        $subscriptions=Subscription::where('post_type','shop_subscription')->where('post_status','wc-active')->get();

        $order_ids = array();

        foreach($subscriptions as $subscription) {
            if(!$subscription->applicationInstance) {
                $product_id = $subscription->productId;
                if($product_id) {
                    if(Application::where('product_id',$product_id)->count()>0) {
                        $order_id=$subscription->post_parent;
                        if($order_id) {
                            array_push($order_ids,$order_id);
                        }       
                    }
                }
            }
        }

        $orders = Order::whereIn('ID',$order_ids)->get();

        foreach($orders as $order) {
            $email=$order->email;
            $this->mail_new_subscription($order,true);
        }
    }

    public function new_order_send_mail(Request $request) {
        $data = $request->json()->all();

        if(!isset($data['parent_id'])) {
            return;
        }

        $order = Order::find($data['parent_id']);

        $product_ids = array();

        if($order) {
            $subscriptions = $order->subscriptions()->get();
            if($subscriptions) {
                foreach($subscriptions as $subscription) {
                    $orderItem=OrderItem::where('order_id',$subscription->ID)->where('order_item_type','line_item')->first();

                    if($orderItem!=null) {
                        $meta=$orderItem->meta()->where('meta_key','_product_id')->first();
                        if($meta) {
                            array_push($product_ids,$meta->meta_value);
                        }             
                    }     
                }

                $applications = Application::whereIn('product_id',$product_ids)->get();

                if($applications) {
                    $email=$order->email;
                    $user=User::where('email',$email)->first();

                    if($user) {
                        $organizations = $user->organizations()->get();
                        if(count($organizations)==1) {
                            $organization=$organizations->first();
                            $this->connect_subscription($order,$organization,$user);
                            return;
                        }
                    }
                    $this->mail_new_subscription($order);
                }
            }       
        }
        return;
    }

    private function mail_new_subscription($order,$reminder=false) {
        Log::debug("Sending email to " . $order->email);
        $mail_message=new MailNewSubscription($order,$reminder);
        Mail::to($order->email)->later(getCurrentDelay(),$mail_message);
    }

    public function connect_subscription(Order $order, Organization $organization, User $user) {

        $subscriptions = $order->subscriptions()->get();

        foreach($subscriptions as $subscription) {
            if($subscription->applicationInstance==null) {
                $product_id=$subscription->productId;

                if($product_id!=null) {

                    $application=Application::where('product_id',$product_id)->first();
                    
                    if($application!=null) {

                        $applicationInstance=$organization->applicationInstances()->where('application_id',$application->id)->first();

                        if($applicationInstance==null) {
                            $applicationInstance = ApplicationInstance::create([
                                'organization_id' => $organization->id,
                                'application_id' => $application->id,
                                'number_of_users' => 0,
                                'paid_until' => null
                            ]);
                
                            $user->applicationInstances()->save($applicationInstance, ['role' => 'admin']);
                        }

                        DB::table('application_instances_subscription')->insert([
                            'subscription_id' => $subscription->ID,
                            'application_instance_id' => $applicationInstance->id,
                        ]);
                    }
                }
            }
        }

        return response('ok',200);
    }

}