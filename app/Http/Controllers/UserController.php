<?php

namespace App\Http\Controllers;


use App\Model\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $current_user=auth()->user();
        if(!($current_user==$user||$current_user->role>$user->role)) {
            return redirect()->back()->with(['code' => 'danger', 'message' => 'You are not authorized to do  that']);;
        }

        $route = redirect()->back();

        if($current_user==$user) {

            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'same:email-confirm', Rule::unique('users')->ignore($user->id)],
                'password' => ['nullable', 'string', 'min:6', 'confirmed'],
                'timezone' => ['required', 'string', 'max:50'],
                'measurement_system' => ['required', 'string', 'max:50'],
                'time_format' => ['required', 'string', 'max:50'],
                'date_format' => ['required', 'string', 'max:50'],
            ]);

            $user->name=$request['name'];
            $user->timezone=$request['timezone'];
            $user->measurement_system=$request['measurement_system'];
            $user->time_format=$request['time_format'];
            $user->date_format=$request['date_format'];

            if($user->email!=$request['email']) {
                $user->email=$request['email'];
                $user->email_verified_at=null;
                $route=redirect()->to('/email/resend');
            }


            if($request['password']!="") {
                if(Hash::check($request['old_password'],$user->password)||$user->password=="") {
                    $user->password=Hash::make($request['password']);
                    $user->activated=true;
                }
                else {
                    $user->save();
                    return $route->with(['code' => 'danger', 'message' => 'Wrong password, did not update password']);;
                }
            }

            $user->save();
        }
        elseif($current_user->role>1) {

            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', Rule::unique('users')->ignore($user->id)],
                'role' => ['required', 'integer', 'min:0', 'max:' . ((Auth::user()->role) - 1)],
                'timezone' => ['required', 'string', 'max:50'],
            ]);

            $user->name=$request['name'];
            $user->timezone=$request['timezone'];

            if($user->email!=$request['email']) {
                $user->email=$request['email'];
                if(!(strtotime($request['email_verified_at'])<strtotime(date("Y-m-d")))&&$user->role>0) {
                    $user->email_verified_at=now();
                }
                else {
                    $user->email_verified_at=null;
                    $route=redirect()->to('/email/resend');
                }
            }

            if(isset($request['role'])) {
                if($request['role']<Auth::user()->role) {
                    $user->role=$request['role'];
                }
            }

            $user->save();
        }


        return $route->with(['code' => 'success', 'message' => 'Updated succesfully']);;
    }

    public function edit(User $user) {
        if($user==Auth::user()) {
            return view('edit_user')->with("user",$user);
        }
        return redirect()->back()->with(['code' => 'danger', 'message' => 'You are not authorized to do  that']);;
    }
}
