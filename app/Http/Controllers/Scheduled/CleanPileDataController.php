<?php

namespace App\Http\Controllers\Scheduled;

use Illuminate\Http\Request;
use App\Model\Pile\PileApplicationMeasurement;
use App\Model\Pile\PileApplication;
use App\Model\Application;
use App\Http\Controllers\Controller;

use Carbon\Carbon;

class CleanPileDataController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $instances = Application::where("type","PILE")->first()->applicationInstances()->get();

        foreach($instances as $instance) {
            if($instance->trial) {
                $pileApplicationIntance=PileApplication::where('application_instance_id',$instance->id)->first();
                if($pileApplicationIntance) {
                    $sites = $pileApplicationIntance->sites()->get();
                    foreach($sites as $site) {
                        $locations=$site->locations()->get();
                        foreach($locations as $location) {
                            $location->measurements()
                            ->where('created_at', '<=', Carbon::now()->subHours(1)->toDateTimeString())
                            ->delete();
                        }
                    }
                }
            }
        }
    }
}
