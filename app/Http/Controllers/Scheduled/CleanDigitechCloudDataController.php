<?php

namespace App\Http\Controllers\Scheduled;

use Illuminate\Http\Request;
use App\Model\Application;
use App\Http\Controllers\Controller;

use App\Model\MDII\MDIIApplication;

use Carbon\Carbon;

class CleanDigitechCloudDataController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $instances = Application::where("type","MDIICOM")->first()->applicationInstances()->get();

        foreach($instances as $instance) {
            if($instance->trial) {
                $dc_application=MDIIApplication::where('application_instance_id',$instance->id)->first();
                if($dc_application) {
                    $files = $dc_application->files()->get();
                    foreach($files as $file) {
                        $file->stands()
                            ->where('created_at', '<=', Carbon::now()->subHours(1)->toDateTimeString())
                            ->delete();
                    }
                }
            }
        }
    }
}
