<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\MDII\MDIITree;
use App\Model\MDII\MDIIFile;
use App\Model\MDII\MDIISpecie;
use App\Model\MDII\MDIIApplication;
use App\Model\ApplicationInstance;
use App\Model\MDII\MDIIStand;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Validator;

use App\Http\Resources\MDIITreeCollection;
use App\Http\Resources\MDIITree as MDIITreeResource;

class MDIITreeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new MDIITreeCollection($stand->trees()->paginate());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand,Request $request)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'm_d_i_i_plot_id' => 'required|integer',
            'm_d_i_i_specie_id' => 'nullable|integer',
            'm_d_i_i_quality_id' => 'nullable|integer',
            'diameter' => 'required|integer',
            'height1' => 'nullable|numeric',
            'is_log' => 'boolean|required',
            'on_bark' => 'boolean',
            'metric' => 'boolean|required',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        $tree = MDIITree::create($request->all());
        
        return new MDIITreeResource($tree);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MDII\MDIITree  $mDIITree
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand,MDIITree $tree)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MDII\MDIITree  $mDIITree
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand,MDIITree $tree,Request $request)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand,$tree)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $tree->update($request->all());
        $tree->save();

        return new MDIITreeResource($tree);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MDII\MDIITree  $mDIITree
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand,MDIITree $tree)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand,$tree)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $tree->delete();

        return response('Destroyed.', 200);
    }

    /**
     * Check the paths relationships
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\MDII\MDIIApplication $mdii_application
     * @param  \App\Model\MDII\MDIIFile $file
     * @param  \App\Model\MDII\MDIIStand $stand
     * @return Boolean
     */
    private function checkPath(ApplicationInstance $application_instance,MDIIApplication $mdii_application,MDIIFile $file,MDIIStand $stand,MDIITree $tree=null) {
        if($application_instance->id==$mdii_application->application_instance_id
            &&$file->m_d_i_i_application_id==$mdii_application->id
            &&$stand->m_d_i_i_file_id==$file->id) {
            if($tree!=null) {
                return $tree->plot()->first()->m_d_i_i_stand_id==$stand->id;
            }
            return true;
        }
        return false;
    }
}
