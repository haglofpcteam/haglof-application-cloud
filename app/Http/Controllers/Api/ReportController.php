<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Report;
use App\Model\ReportTemplate;
use App\Model\ApplicationInstance;
use Illuminate\Http\Request;

use App\Http\Resources\ReportCollection;
use App\Http\Resources\Report as ReportResource;

use Illuminate\Support\Facades\Log;

class ReportController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
	 try {
            $applicationInstance = ApplicationInstance::find($request->application_instance);
        
            if(!$applicationInstance->user) {
                return response('Unauthorized.', 401);
            }
    
            $template = ReportTemplate::find($request->template_id);
    
            if(!$applicationInstance->reportTemplates()->where('id',$request->template_id)->count()) {
                if(auth()->user()->role==0) {
                    return response('Unauthorized.', 401);
                }
            }
    
            $report = Report::create([
                'data' => json_encode($request->data),
                'report' => $template->template,
                'report_template_id' => $template->id,
                'application_instance_id' => $applicationInstance->id,
                'name' => $request->name,
                'user_id' => auth()->user()->id
            ]);
    
            return $report->id;
        }
        catch(Exception $e) {
            Log::channel('debug')->error($e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function show(Report $report)
    {
        if(!$report->applicationInstance()->first()->user&&auth()->user()->role==0) {
            return response('Unauthorized.', 401);
        }
        return new ReportResource($report);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Report  $report
     * @return \Illuminate\Http\Response
     */
    public function destroy(Report $report)
    {
        if(!$report->applicationInstance()->first()->user) {
            return response('Unauthorized.', 401);
        }

        $report->delete();

        return response('Destroyed.', 200);
    }
}
