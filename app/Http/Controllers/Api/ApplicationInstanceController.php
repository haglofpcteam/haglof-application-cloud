<?php

namespace App\Http\Controllers\Api;

use App\Model\ApplicationInstance;
use App\Model\User;
use App\Model\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

use Illuminate\Support\Facades\Auth;

use App\Http\Resources\ApplicationInstanceCollection;
use App\Http\Resources\ApplicationInstance as ApplicationInstanceResource;

use Illuminate\Support\Carbon;

class ApplicationInstanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(isset($request['all'])&&Auth::user()->role>0) {
            return new ApplicationInstanceCollection(ApplicationInstance::all());
        }
        return new ApplicationInstanceCollection(auth()->user()->applicationInstances()->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ApplicationInstance  $applicationInstance
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $applicationInstance)
    {
        if(!$applicationInstance->user) {
            return response('Unauthorized.', 401);
        }

        return new ApplicationInstanceResource($applicationInstance);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ApplicationInstance  $applicationInstance
     * @return \Illuminate\Http\Response
     */
    public function restore($application_instance,Request $request)
    {
        if(auth()->user()->role==0) {
            return response('Unauthorized.', 401);
        }

        $request->validate([
            'from_time' => ['nullable','date'],
        ]);

        $t = new Carbon($request->from_time);

        $application_instance = ApplicationInstance::withTrashed()->where('id', $application_instance)->first();

        $application_instance->restoreRecursive($t);

        return new ApplicationInstanceResource($application_instance);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ApplicationInstance  $applicationInstance
     * @return \Illuminate\Http\Response
     */
    public function destroy($application_instance)
    {
        $application_instance = ApplicationInstance::withTrashed()->where('id', $application_instance)->first();

        if(!$application_instance->admin&&auth()->user()->role==0) {
            return response('Unauthorized.', 401);
        }

        if($application_instance->trashed()) {
            $application_instance->forceDelete();
        }
        else {
            $application_instance->delete();
        }
        

        return response('Destroyed.', 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance, Request $request)
    {
        if(!$application_instance->admin) {
            return response('Unauthorized.', 401);
        }

        if($application_instance->paid_until!=$request->paid_until||$application_instance->paid_number_of_users!=$request->paid_number_of_users) {
            if(auth()->user()->role==0) {
                return response('Unauthorized.', 401);
            }
            $application_instance->paid_until=$request->paid_until;
            $application_instance->number_of_users=$request->paid_number_of_users;
            $application_instance->save();
        }



        return new ApplicationInstanceResource($application_instance);
    }

    public function remove_user(ApplicationInstance $application_instance,User $user)
    {
        if(!$application_instance->admin&&auth()->user()->role==0) {
            return response('Unauthorized.', 401);
        }

        $user->applicationInstances()->detach($application_instance);
        
        return response('Destroyed.', 200);
    }

    public function invite(Request $request,ApplicationInstance $application_instance) {

        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'usermessage' => ['nullable','string', 'max:1000'],
        ]);

        $email  = $request['email'];

        $message = $request['usermessage'];


        if($application_instance->admin||Auth::user()->role>1) {

            $inviteduser = User::where('email', $email)->first();

            if (!$inviteduser) {

                $inviteduser=User::create([
                    'email' => $email,
                    'activated' => false,
                ]);

                $inviteduser->sendAccountActivationNotification($message);

            }

            if($application_instance->users()->where('id',$inviteduser->id)->exists()){
                return response()->json('User is already added to this application instance', 422);
            }

            $inviteduser->applicationInstances()->save($application_instance, ['role' => 'user']);
            
            return response()->json('User added to the application instance');
            
        }

        return response('Unauthorized.', 401);
     }
}
