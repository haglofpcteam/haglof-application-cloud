<?php

namespace App\Http\Controllers\Api;

use App\Model\Pile\PileApplicationTarget;
use App\Model\Pile\PileApplicationMeasurement;
use App\Model\Pile\PileApplicationLocation;
use App\Model\Pile\PileApplicationSite;
use App\Model\Pile\PileApplication;
use App\Model\ApplicationInstance;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use App\Http\Resources\PileApplicationTargetCollection;
use App\Http\Resources\PileApplicationTarget as PileApplicationTargetResource;

class PileApplicationTargetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,
                        PileApplication $pile_application,
                        PileApplicationSite $pile_application_site, 
                        PileApplicationLocation $pile_application_location,
                        PileApplicationMeasurement $pile_application_measurement)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }


        return new PileApplicationTargetCollection($pile_application_measurement->targets()->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Pile\PileApplicationTarget  $pileApplicationTarget
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance,
                    PileApplication $pile_application,
                    PileApplicationSite $pile_application_site, 
                    PileApplicationLocation $pile_application_location,
                    PileApplicationMeasurement $pile_application_measurement,
                    PileApplicationTarget $pile_application_target)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement,$pile_application_target)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new PileApplicationTargetResource($pile_application_target);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \App\Model\Pile\PileApplicationSite  $pileApplicationSite
     * @param  \App\Model\Pile\PileApplicationLocation  $pile_application_location
     * @param  \App\Model\Pile\PileApplicationMeasurement  $pile_application_measurement
     * @param  \App\Model\Pile\PileApplicationTarget  $pile_application_target
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,
                    PileApplication $pile_application,
                    PileApplicationSite $pile_application_site, 
                    PileApplicationLocation $pile_application_location,
                    PileApplicationMeasurement $pile_application_measurement,
                    PileApplicationTarget $pile_application_target,
                    Request $request)
    {

        $validator = Validator::make($request->all(), [
            'type' => ['string','required'],
            'latitude' => ['numeric', 'required'],
            'ns' => ['string','size:1','required'],
            'longitude' => ['numeric', 'required'],
            'ew' => ['string','size:1','required'],
            'altitude' => ['numeric', 'required'],
            'hdop' => ['string','required'],
            'seq' => ['Integer','required'],
            'sd' => ['numeric', 'required'],
            'hd' => ['numeric', 'required'],
            'h' => ['numeric', 'required'],
            'pitch' => ['numeric', 'required'],
            'az' => ['numeric', 'required'],
            'x' => ['numeric', 'required'],
            'y' => ['numeric', 'required'],
            'z' => ['numeric', 'required'],
            'utm_zone' => ['string']
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement,$pile_application_target)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        //$pile_application_target->update($request->all());

        $pile_application_target->type=$request->type;

        $pile_application_target->save();

        return new PileApplicationTargetResource($pile_application_target);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Pile\PileApplicationTarget  $pileApplicationTarget
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,
                        PileApplication $pile_application,
                        PileApplicationSite $pile_application_site, 
                        PileApplicationLocation $pile_application_location,
                        PileApplicationMeasurement $pile_application_measurement,
                        PileApplicationTarget $pile_application_target)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement,$pile_application_target)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $pile_application_target->delete();

        return response('Destroyed.', 200);
    }

    /**
     * Check the paths relationships
     *
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \App\Model\Pile\PileApplicationSite  $pileApplicationSite
     * @param  \App\Model\Pile\PileApplicationLocation  $pile_application_location
     * @param  \App\Model\Pile\PileApplicationMeasurement  $pile_application_measurement
     * @param  \App\Model\Pile\PileApplicationTarget  $pile_application_target
     * @return Boolean
     */
    private function checkPath(ApplicationInstance $application_instance,
                                PileApplication $pile_application,
                                PileApplicationSite $pile_application_site, 
                                PileApplicationLocation $pile_application_location,
                                PileApplicationMeasurement $pile_application_measurement,
                                PileApplicationTarget $pile_application_target=null) 
                                {
        if($pile_application->application_instance_id==$application_instance->id&&
            $pile_application_site->pile_application_id==$pile_application->id&&
            $pile_application_location->pile_application_site_id==$pile_application_site->id&&
            $pile_application_measurement->pile_application_location_id==$pile_application_location->id) {
            if($pile_application_target!=null) {
                return $pile_application_measurement->id==$pile_application_target->pile_application_measurement_id;
            }
            return true;
        }
        return false;
    }
}