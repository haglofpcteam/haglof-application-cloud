<?php

namespace App\Http\Controllers\Api;

use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\Rule;

use App\Http\Resources\UserCollection;
use App\Http\Resources\User as UserResource;

class UserController extends Controller
{

    /**
     * Display current user.
     *
     * @return \Illuminate\Http\Response
     */
    public function current_user()
    {
        return new UserResource(auth()->user());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        if($user->role<1) {
            return response('Unauthorized.', 401);
        }
        else {
            return new UserCollection(User::where('role','<',$user->role)->get());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            'recaptchaToken' => 'required|captcha',
            'terms_and_conditions' => 'accepted'
        ]);

        $user = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'date_format' => 'YYYY-MM-DD',
            'time_format' => 'HH:mm:ss'
        ]);

        $user->sendApiEmailVerificationNotification();

        return new UserResource($user);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        $cuser = auth()->user();

        if($cuser->role<1||!($cuser->role>$user->role)) {
            return response('Unauthorized.', 401);
        }
        else {
            return new UserResource($user);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        $current_user=auth()->user();
        if(!$current_user->id==$user->id&&$current_user->role<=$user->role) {
            return response('Unauthorized.', 401);
        }

        if($current_user->id==$user->id) {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['string', 'email', 'max:255', 'same:email_confirmation', Rule::unique('users')->ignore($user->id)],
                'password' => ['nullable','string', 'min:6', 'confirmed'],
                'timezone' => ['string'],
                'measurement_system' => ['string'],
                'date_format' => ['string'],
                'time_format' => ['string']
            ]);
    
            $user->name=$request['name'];
            $user->timezone=$request['timezone'];
            $user->measurement_system=$request['measurement_system'];
            $user->time_format=$request['time_format'];
            $user->date_format=$request['date_format'];
    
            if($user->email!=''&&$user->email!=$request['email']) {
                $user->email=$request['email'];
                $user->email_verified_at=null;
                $user->sendApiEmailVerificationNotification();
            }
    
    
            if($request['password']!="") {
                if(Hash::check($request['old_password'],$user->password)||$user->password=="") {
                    $user->password=Hash::make($request['password']);
                }
                else {
                    return response()->json([
                        'message' => 'Wrong password did not update'], 422);
                }
            }
    
            $user=$this->updateActivated($user);
    
            $user->save();
        }
        elseif($current_user->role>$user->role) {
            $request->validate([
                'name' => ['required', 'string', 'max:255'],
                'email' => ['string', 'email', 'max:255',  Rule::unique('users')->ignore($user->id)],
                'timezone' => ['string'],
                'measurement_system' => ['string'],
                'date_format' => ['string'],
                'time_format' => ['string']
            ]);

            $user->update($request->all());
        }


        return new UserResource($user);
    }

    public function updateActivated($user) {
        $user->activated=true;
        return $user;
    }

    public function resetPassword(Request $request) {

        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
        ]);

        $email  = $request['email'];

        $user = User::where('email', $email)->first();

        if($user) {
            $user->activated=false;
            $user->password="";
            $user->save();
            
            $user->sendResetPassowordNotification();
            return response()->json('Check your email to complete resetting your password');
        }

        return response()->json(['message' => 'User does not exist'],422);

     }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        if($user->role<Auth::user()->role) {
            $user->delete();
            return response('Destroyed.', 200);
        }

        return response('Unauthorized.', 401);
    }

}
