<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\FileData;
use App\Model\ApplicationInstance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\FileDataCollection;
use App\Http\Resources\FileData as FileDataResource;

class FileDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance)
    {
        if(!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new FileDataCollection($application_instance->file_datas()->get());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationInstance $application_instance,Request $request)
    {
        if(!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'mime_type' => 'required|string',
            'data' => ['required'],
            'name' => ['string']
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        $data="";
        $name="";

        if($request->has('name')) {
            $name=$request['name'];
        }

        if ($request->hasFile('data')) {
            $data=$request->file('data')->get();
        }
        else {
            $data=$request['data'];
        }

        $fileData = new FileData();
        $fileData->mime_type = $request['mime_type'];
        $fileData->data = $data;
        $fileData->name = $name;
        $fileData->user_id = auth()->user()->id;
        $fileData->application_instance_id=$application_instance->id;
        $fileData->save();

        if ($request->hasFile('data')) {
            return response('File uploaded.', 200);
        }

        return new FileDataResource($fileData);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\FileData  $fileData
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance,FileData $fileData)
    {
        if(!$application_instance->user||$application_instance->id!=$fileData->application_instance_id) {
            return response('Unauthorized.', 401);
        }

        return new FileDataResource($fileData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\FileData  $fileData
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance, FileData $fileData, Request $request)
    {
        if(!$application_instance->user||$application_instance->id!=$fileData->application_instance_id) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'mime_type' => 'required|string',
            'data' => ['required'],
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        $fileData->mime_type=$request->mime_type;
        $fileData->data=$request->data;
        $fileData->save();

        return new FileDataResource($fileData);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\FileData  $fileData
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,FileData $fileData)
    {
        if(!$application_instance->user||$application_instance->id!=$fileData->application_instance_id) {
            return response('Unauthorized.', 401);
        }

        $fileData->delete();

        return response('Destroyed.', 200);
    }
}
