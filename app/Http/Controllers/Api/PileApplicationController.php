<?php

namespace App\Http\Controllers\Api;

use App\Model\ApplicationInstance;
use App\Model\Pile\PileApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\PileApplicationCollection;
use App\Http\Resources\PileApplication as PileApplicationResource;

class PileApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance)
    {

        $pileApplication = PileApplication::where('application_instance_id',$application_instance->id)->first();

        return new PileApplicationResource($pileApplication);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Pile\PileApplication  $pileApplication
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance)
    {
        if(!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $pileApplication = PileApplication::where('application_instance_id',$application_instance->id)->first();

        return new PileApplicationResource($pileApplication);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Pile\PileApplication  $pileApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(PileApplication $pileApplication)
    {
        if(!$pileApplication->admin) {
            return response('Unauthorized.', 401);
        }

        $pileApplication->delete();

        return response('Destroyed.', 200);
    }
}
