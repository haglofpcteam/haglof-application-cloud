<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\ApplicationInstance;
use App\Model\Application;
use App\Model\Organization;
use Illuminate\Http\Request;

use App\Http\Resources\ApplicationInstanceCollection;
use App\Http\Resources\ApplicationInstance as ApplicationInstanceResource;

class OrganizationApplicationInstanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Organization $organization)
    {
        if(!$organization->user&&auth()->user()->role==0) {
            return response('Unauthorized.', 401);
        }

        if(auth()->user()->role==0) {
            return new ApplicationInstanceCollection($organization->applicationInstances()->get());
        }
        else {
            return new ApplicationInstanceCollection($organization->applicationInstances()->withTrashed()->get());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Organization $organization,Request $request)
    {
        $user = auth()->user();

        $application = Application::find($request->application);

        if($organization->admin&&$application->public)
        {
            $applicationInstance = ApplicationInstance::create([
                'organization_id' => $organization->id,
                'application_id' => $application->id,
                'number_of_users' => 0,
                'paid_until' => null
            ]);

            $user->applicationInstances()->save($applicationInstance, ['role' => 'admin']);

        }
        elseif($user->role>0) {
            $applicationInstance = ApplicationInstance::create([
                'organization_id' => $organization->id,
                'application_id' => $application->id,
                'number_of_users' => 0,
                'paid_until' => null
            ]);

            $user->applicationInstances()->save($applicationInstance, ['role' => 'admin']);
        }
        return new ApplicationInstanceCollection($organization->applicationInstances()->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ApplicationInstance  $applicationInstance
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $applicationInstance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\ApplicationInstance  $applicationInstance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ApplicationInstance $applicationInstance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ApplicationInstance  $applicationInstance
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $applicationInstance)
    {
        //
    }
}
