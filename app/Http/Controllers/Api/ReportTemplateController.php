<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\ReportTemplate;
use App\Model\ApplicationInstance;
use Illuminate\Http\Request;

use App\Http\Resources\ReportTemplateCollection;
use App\Http\Resources\ReportTemplate as ReportTemplateResource;

class ReportTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  $key
     * 
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance=null,$key=null)
    {
        if(auth()->user()->role==0) {
            if($application_instance!=null&&$application_instance->user) {
                return new ReportTemplateCollection($application_instance->reportTemplates()->get()->where('key',$key));
            }
            else {
                return response('Unauthorized.', 401);
                
            }
        }
        else {
            if($key!=null) {
                return new ReportTemplateCollection(ReportTemplate::all()->where('key',$key));
            }

            return new ReportTemplateCollection(ReportTemplate::all());
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(auth()->user()->role<1) {
            return response('Unauthorized.', 401);
        }

        $data = $request->all();
        $data['test_data']=json_encode($data['test_data']);
        $parameters = $data['parameters'];


        $template = ReportTemplate::create($data);

        if(count($parameters)) {
            $template->parameters()->createMany($parameters);
        }

        return new ReportTemplateResource($template);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\ReportTemplate  $reportTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance=null,ReportTemplate $reportTemplate)
    {
        if($application_instance==null) {
            if(auth()->user()->role>0) {
                $reportTemplate->test_data=json_decode($reportTemplate->test_data);
                return new ReportTemplateResource($reportTemplate);
            }
            else {
                return response('Unauthorized.', 401);
            }
        }

        if(!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $reportTemplate->test_data=null;

        if(!$reportTemplate->standard) {
            if($reportTemplate->premium) {
                if(!$application_instance->paid) {
                    return response('Unauthorized.', 401);
                }
            }
            else if($reportTemplate->applicationInstances()->where('id',$application_instance->id)->exists()) {
                return response('Unauthorized.', 401);
            }
            else {
                return response('Unauthorized.', 401);
            }
        }

        return new ReportTemplateResource($reportTemplate);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\ReportTemplate  $reportTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ReportTemplate $reportTemplate)
    {
        if(auth()->user()->role<1) {
            return response('Unauthorized.', 401);
        }

        $data = $request->all();

        $reportTemplate->update($data);

        $reportTemplate->parameters()->delete();

        $parameters = $data['parameters'];

        if(count($parameters)) {
            $reportTemplate->parameters()->createMany($parameters);
        }

        return new ReportTemplateResource($reportTemplate);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ReportTemplate  $reportTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportTemplate $reportTemplate)
    {
        if(auth()->user()->role<1) {
            return response('Unauthorized.', 401);
        }

        $reportTemplate->delete();

        return response('Destroyed.', 200);
    }
}
