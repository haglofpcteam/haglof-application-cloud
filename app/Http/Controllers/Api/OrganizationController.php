<?php

namespace App\Http\Controllers\Api;

use App\Model\Organization;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\OrganizationCollection;
use App\Http\Resources\Organization as OrganizationResource;

use Illuminate\Support\Facades\Validator;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {  
        if($request->all_organizations==true&&auth()->user()->role>0) {
            return new OrganizationCollection(Organization::all());
        }

        $user = auth()->user();

        $applicationInstances = $user->applicationInstances()->with('organization')->get();
        $organizations = $user->organizations()->get();

        foreach($applicationInstances as $instance) {
            $found=false;
            foreach($organizations as $organization) {
                if($instance->organization->id==$organization->id) {
                    $found=true;
                    break;
                }
            }
            if(!$found) {
                $organizations->push($instance->organization);
            }
        }

        return new OrganizationCollection($organizations);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        $organization_name = $request['name'];
        $user = auth()->user();

        $organization = Organization::create([
            'name' => $organization_name
        ]);

        $user->organizations()->save($organization, ['role' => 'admin']);

        $organization->createApplicationInstances();

        return new OrganizationResource($organization);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {
        if(!$organization->user&&auth()->user()->role==0) {
            return response('Unauthorized.', 401);
        }

        return new OrganizationResource($organization);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {
        if(!$organization->admin&&auth()->user()->role==0) {
            return response('Unauthorized.', 401);
        }
        
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }
        
        $organization->name = $request['name'];
        $organization->save();

        return new OrganizationResource($organization);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organization $organization)
    {
        if(!$organization->admin&&auth()->user()->role==0) {
            return response('Unauthorized.', 401);
        }

        $organization->delete();

        return response('Destroyed.', 200);
    }

    private function validator($data)
    {
        return Validator::make($data, [
          'name' => 'required|string|max:50'
        ]);
    }

    public function invite(Request $request,Organization $organization) {

        $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'usermessage' => ['nullable','string', 'max:1000'],
        ]);

        $email  = $request['email'];

        $message = $request['usermessage'];


        if($organization->admin||auth()->user()->role>1) {

            $inviteduser = User::where('email', $email)->first();

            if (!$inviteduser) {

                $inviteduser=User::create([
                    'email' => $email,
                    'activated' => false,
                ]);

                $inviteduser->sendAccountActivationNotification($message);

            }

            if($organization->users()->where('id',$inviteduser->id)->exists()){
                return response()->json('User is already added to the organization', 422);
            }

            $inviteduser->organizations()->save($organization, ['role' => 'admin']);
            
            return response()->json('User added to the organization');
            
        }

        return response('Unauthorized.', 401);
     }
}
