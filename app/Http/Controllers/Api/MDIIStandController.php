<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\MDII\MDIIFile;
use App\Model\MDII\MDIISpecie;
use App\Model\MDII\MDIIApplication;
use App\Model\ApplicationInstance;
use App\Model\MDII\MDIIStand;
use Illuminate\Http\Request;

use App\Http\Resources\MDIIStandCollection;
use App\Http\Resources\MDIIStand as MDIIStandResource;
use App\Http\Resources\MDIIStandFull as MDIIStandFullResource;
use Illuminate\Support\Facades\Log;


class MDIIStandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

	Log::channel('slack')->info('Something happened!');
	Log::channel('debug')->info('Something happened!');

        return new MDIIStandCollection($file->stands()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MDII\MDIIStand  $mDIIStand
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new MDIIStandResource($stand);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MDII\MDIIStand  $mDIIStand
     * @return \Illuminate\Http\Response
     */
    public function show_full(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new MDIIStandFullResource($stand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MDII\MDIIStand  $mDIIStand
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand,Request $request)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $old_file=MDIIFile::find($stand->m_d_i_i_file_id);

        $stand->update($request->all());
        $stand->save();

        if($old_file->stands()->count()==0) {
            $old_file->delete();
        }

        if($request['set_default_settings']) {
            $dc_application->stand_default_settings=$request['settings'];
            $dc_application->save();
        }

        return new MDIIStandResource($stand);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MDII\MDIIStand  $mDIIStand
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $stand->delete();

        if(!$file->stands()->first()) {
            $file->delete();
        }

        return response('Destroyed.', 200);
    }

    /**
     * merge 2 Stands
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\MDII\MDIIApplication $mdii_application
     * @param  \App\Model\MDII\MDIIFile $file
     * @param  \App\Model\MDII\MDIIStand $stand
     * @param  \App\Model\MDII\MDIIStand $stand2
     * @return Boolean
     */
    public function merge(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand,MDIIStand $stand2) {
        
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand)||!$application_instance->user||$stand2->m_d_i_i_file_id!=$file->id) {
            return response('Unauthorized.', 401);
        }
        $stand2->plots()->update(['m_d_i_i_stand_id'=>$stand->id]);
        
        $stand2file=MDIIFile::find($stand2->m_d_i_i_file_id);
    
        $stand2->delete();

        if($stand2file->stands()->count()==0) {
            $stand2file->delete();
        }

        $species = MDIISpecie::whereIn('id',$stand->trees()->pluck('m_d_i_i_specie_id')->groupBy('m_d_i_i_specie_id')->toArray())->get();

        foreach($species as $s1) {
            if(!$s1->trashed()) {
                foreach($species as $s2) {
                    if((!$s2->trashed())&&$s1->id!=$s2->id) {
                        if($s1['specie_text']==$s2['specie_text']&&$s1['specie_nr']==$s2['specie_nr']) {
                            if($s2->m_d_i_i_application_id!=null) {
                                continue;
                            }
                            $s2->trees()->update(['m_d_i_i_specie_id'=>$s1->id]);
                            $s2->delete();
                        }
                    }
                }
            }
        }

        return new MDIIStandResource($stand);
    }

        /**
     * Check the paths relationships
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\MDII\MDIIApplication $mdii_application
     * @param  \App\Model\MDII\MDIIFile $file
     * @param  \App\Model\MDII\MDIIStand $stand
     * @return Boolean
     */
    private function checkPath(ApplicationInstance $application_instance,MDIIApplication $mdii_application,MDIIFile $file,MDIIStand $stand=null) {
        if($application_instance->id==$mdii_application->application_instance_id&&$file->m_d_i_i_application_id==$mdii_application->id) {
            if($stand!=null) {
                return $stand->m_d_i_i_file_id==$file->id;
            }
            return true;
        }
        return false;
    }
}
