<?php

namespace App\Http\Controllers\Api;

use App\Model\MDII\MDIIFolder;
use App\Model\MDII\MDIIApplication;
use App\Model\ApplicationInstance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\MDIIFolderCollection;
use App\Http\Resources\MDIIFolder as MDIIFolderResource;
use App\Http\Resources\MDIIStandCollection;
use App\Http\Resources\MDIIStand as MDIIStandResource;

class MDIIFolderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,MDIIApplication $dc_application)
    {
        if(!$this->checkPath($application_instance,$dc_application)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }
        
        return new MDIIFolderCollection($dc_application->folders()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationInstance $application_instance,MDIIApplication $dc_application,Request $request)
    {
        if(!$this->checkPath($application_instance,$dc_application)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'm_d_i_i_folder_id' => 'nullable|integer',
            'name' => 'required|string|max:50',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }


        $parent_folder = MDIIFolder::find($request['m_d_i_i_folder_id']);

        $folder = $parent_folder->folders()->create([
            'name' => $request->name,
            'm_d_i_i_application_id' => $dc_application->id
        ]);
        
        return new MDIIFolderResource($folder);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MDII\MDIIFolder  $mDIIFolder
     * @return \Illuminate\Http\Response
     */
    public function show(MDIIFolder $mdiiFolder)
    {
        if(!$mdiiFolder->user) {
            return response('Unauthorized.', 401);
        }

        return new MDIIFolderResource($mdiiFolder);
    }

    /**
     * Display the folders stands
     *
     * @param  \App\Model\MDII\MDIIFolder  $folder
     * @return \Illuminate\Http\Response
     */
    public function show_stands(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFolder $folder)
    {
        if(!$this->checkPath($application_instance,$dc_application,$folder)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new MDIIStandCollection($folder->stands()->get());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MDII\MDIIFolder  $mDIIFolder
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFolder $folder,Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|max:50',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        if(!$this->checkPath($application_instance,$dc_application,$folder)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $folder->update($request->all());

        $folder->save();

        return new MDIIFolderResource($folder);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MDII\MDIIFolder  $mDIIFolder
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFolder $folder)
    {
        if(!$this->checkPath($application_instance,$dc_application,$folder)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $folder->delete();

        return response('Destroyed.', 200);
    }

     /**
     * Check the paths relationships
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\MDII\MDIIApplication $mdii_application
     * @param  \App\Model\MDII\MDIIFolder $folder
     * @return Boolean
     */
    private function checkPath(ApplicationInstance $application_instance,MDIIApplication $mdii_application,MDIIFolder $folder=null) {
        //return true;
        if($application_instance->id==$mdii_application->application_instance_id) {
            if($folder!=null) {
                return $folder->m_d_i_i_application_id==$mdii_application->id;
            }
            return true;
        }
        return false;
    }
}
