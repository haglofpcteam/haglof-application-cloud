<?php

namespace App\Http\Controllers\Api;

use App\Model\ApplicationInstance;
use App\Model\MDII\MDIIApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\MDIIApplicationCollection;
use App\Http\Resources\MDIIApplication as MDIIApplicationResource;

class MDIIApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dcApplications = MDIIApplication::whereIn('application_instance_id',auth()->user()->applicationInstances()->pluck('id')->toArray())->get();

        return new MDIIApplicationCollection($dcApplications);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MDII\MDIIApplication  $mDIIApplication
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance,MDIIApplication $dc_application)
    {
        if(!$application_instance->user||$dc_application->application_instance_id!=$application_instance->id) {
            return response('Unauthorized.', 401);
        }
    
        return new MDIIApplicationResource($dc_application);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Pile\PileApplicationLocation  $pileApplicationLocation
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,MDIIApplication $dc_application,Request $request)
    {
        if(!$this->checkPath($dc_application->application_instance_id!=$application_instance->id)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'application_instance_id' => 'numeric|required',
            'stand_default_settings' => 'string|required'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        $dc_application->update($request->all());

        $dc_application->save();

        return new MDIIApplicationResource($dc_application);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Pile\PileApplication  $pileApplication
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,MDIIApplication $dc_application)
    {
        if(!$application_instance->user||$dc_application->application_instance_id!=$application_instance->id) {
            return response('Unauthorized.', 401);
        }

        $dc_application->delete();

        return response('Destroyed.', 200);
    }
}
