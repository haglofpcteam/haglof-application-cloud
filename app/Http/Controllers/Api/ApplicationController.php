<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\Application;
use Illuminate\Http\Request;

use App\Http\Resources\ApplicationCollection;
use App\Http\Resources\Application as ApplicationResource;

class ApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role>0) {
            return new ApplicationCollection(Application::all());
        }
        else {
            return new ApplicationCollection(Application::where('public',true)->get());
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Application  $application
     * @return \Illuminate\Http\Response
     */
    public function show(Application $application)
    {
        return new ApplicationResource($application);
    }
}
