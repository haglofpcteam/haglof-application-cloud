<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\MDII\MDIIQuality;
use Illuminate\Http\Request;

use App\Model\MDII\MDIIFile;
use App\Model\MDII\MDIISpecie;
use App\Model\MDII\MDIIApplication;
use App\Model\ApplicationInstance;
use App\Model\MDII\MDIIStand;

use App\Http\Resources\MDIIQualityCollection;
use App\Http\Resources\MDIIQuality as MDIIQualityResource;

class MDIIQualityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,MDIIStand $stand)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file,$stand)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new MDIIQualityCollection($stand->qualities()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * merge 2 Qualities
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\MDII\MDIIApplication $dc_application
     * @param  \App\Model\MDII\MDIIQuality  $quality1
     * @param  \App\Model\MDII\MDIIQuality  $quality2
     * @return Boolean
     */
    public function merge(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIQuality $quality1,MDIIQuality $quality2) {
        
        if(!$application_instance->user
        ||$application_instance->id!=$dc_application->application_instance_id
        ||!$this->checkQualityPermissions($application_instance,$dc_application,$quality1)
        ||!$this->checkQualityPermissions($application_instance,$dc_application,$quality2)) {
            return response('Unauthorized.', 401);
        }

        if($quality1->m_d_i_i_application_id!=null) {
            return response('Cant merge default qualities',405);
        }

        $quality1->trees()->update(['m_d_i_i_quality_id'=>$quality2->id]);
        $quality1->delete();

        return response('OK', 200);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MDII\MDIIQuality  $mDIIQuality
     * @return \Illuminate\Http\Response
     */
    public function show(MDIIQuality $mDIIQuality)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MDII\MDIIQuality  $mDIIQuality
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIQuality $quality,Request $request)
    {
        if(!$application_instance->user
        ||$application_instance->id!=$dc_application->application_instance_id
        ||!$this->checkQualityPermissions($application_instance,$dc_application,$quality)) {
            return response('Unauthorized.', 401);
        }

        $quality->update($request->all());
        $quality->save();


        return new MDIIQualityResource($quality);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MDII\MDIIQuality  $mDIIQuality
     * @return \Illuminate\Http\Response
     */
    public function destroy(MDIIQuality $mDIIQuality)
    {
        //
    }

     /**
     * Check quality permissions
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\MDII\MDIIApplication $mdii_application
     * @param  \App\Model\MDII\MDIIQuality $quality
     * @return Boolean
     */
    private function checkQualityPermissions(ApplicationInstance $application_instance,MDIIApplication $mdii_application,MDIIQuality $quality) {
        
        if($mdii_application->qualities()->where('id',$quality->id)->exists()) {
            return true;
        }

        $trees = $quality->trees()->first();
        $plot = $trees->plot()->first();
        $stand = $plot->stand()->first();
        

        if($stand!=null) {
            $file = $stand->file()->first();
            return $this->checkPath($application_instance,$mdii_application,$file,$stand);
        }
        return true; 

    }

    /**
     * Check the paths relationships
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\MDII\MDIIApplication $mdii_application
     * @param  \App\Model\MDII\MDIIFile $file
     * @param  \App\Model\MDII\MDIIStand $stand
     * @return Boolean
     */
    private function checkPath(ApplicationInstance $application_instance,MDIIApplication $mdii_application,MDIIFile $file,MDIIStand $stand,MDIIQuality $quality=null) {
        if($application_instance->id==$mdii_application->application_instance_id
            &&$file->m_d_i_i_application_id==$mdii_application->id
            &&$stand->m_d_i_i_file_id==$file->id) {
            if($quality!=null) {
                return $stand->qualities()->where('id',$quality->id)->exists();
            }
            return true;
        }
        return false;
    }
}
