<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\SocialiteProvider;
use App\Model\User;
use Illuminate\Http\Request;
use App\Http\Resources\SocialiteProviderCollection;
use Illuminate\Support\Facades\Log;

use Socialite;

class SocialiteProviderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(User $user)
    {
        if(auth()->user()->id!=$user->id) {
            return response('Unauthorized.', 401);
        }

        return new SocialiteProviderCollection($user->socialiteProviders()->get());

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(User $user,Request $request)
    {
        if(auth()->user()->id!=$user->id) {
            return response('Unauthorized.', 401);
        }

        Log::debug(json_encode($request->all()));

        $provider = $request->provider;

        $userData = Socialite::driver($provider)->stateless()->user();

        $socialite_provider = SocialiteProvider::where('provider_id',$userData->id)->where('provider',$provider)->first();

        if($socialite_provider) {
            abort(422, 'Social provider is already registered to another user');
        }

        $socialite_provider = new SocialiteProvider();
        $socialite_provider->provider = $provider;
        $socialite_provider->provider_id = $userData->id;
        $socialite_provider->user_id = $user->id;

        $socialite_provider->save();

        return new SocialiteProviderCollection($user->socialiteProviders()->get());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\SocialiteProvider  $socialiteProvider
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user,SocialiteProvider $social_provider)
    {
        $suser=$social_provider->user()->first();

        if(auth()->user()->id!=$suser->id) {
            return response('Unauthorized.', 401);
        }

        $social_provider->delete();

        return new SocialiteProviderCollection($user->socialiteProviders()->get());
    }
}
