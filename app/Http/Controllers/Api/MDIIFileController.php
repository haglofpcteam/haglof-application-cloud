<?php

namespace App\Http\Controllers\Api;

use App\Model\MDII\MDIIQuality;
use App\Model\MDII\MDIISpecie;
use App\Model\MDII\MDIITree;
use App\Model\MDII\MDIIFile;
use App\Model\MDII\MDIIFolder;
use App\Model\MDII\MDIIApplication;
use App\Model\ApplicationInstance;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\MDIIFileCollection;
use App\Http\Resources\MDIIFile as MDIIFileResource;

use App\Logic\MDIIFileParser;

class MDIIFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,MDIIApplication $dc_application)
    {
        if(!$this->checkPath($application_instance,$dc_application)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new MDIIFileCollection($dc_application->files()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationInstance $application_instance,MDIIApplication $dc_application,Request $request)
    {
        if(!$this->checkPath($application_instance,$dc_application)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'folder_id' => 'required|integer',
            'file_data' => ['required'],
        ]);

        $tree_keys=[
            'm_d_i_i_plot_id',
            'm_d_i_i_specie_id',
            'm_d_i_i_quality_id',
            'number',
            'diameter',
            'height1',
            'height2',
            'height3',
            'item_id',
            'unique_id',
            'is_log',
            'on_bark',
            'metric'
        ];

        $default_settings=(object) [
            'inventory_type' => 'total'
        ];

        $species = $dc_application->species()->get();
        $qualities = $dc_application->qualities()->get();

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        $folder=MDIIFolder::find($request->folder_id);

        if($folder->m_d_i_i_application_id!=$dc_application->id) {
            return response('Unauthorized.', 401);
        }

        $file = $folder->files()->create([
            'user_id' => auth()->user()->id,
            'name' => $request->name,
            'm_d_i_i_application_id' => $folder->m_d_i_i_application_id
        ]);

        $file_data=json_decode($request->file_data);

        if(isset($file_data->inventories)&&is_array($file_data->inventories)) {
            foreach($file_data->inventories as $inventory) {
                
                $stand=$file->stands()->create([
                    'log_list' => $inventory->log_list
                ]);

                if($stand->settings==null) {
                    $stand->settings=json_encode($default_settings);
                }

                $species = $dc_application->species()->get();

                unset($metric);

                if(isset($inventory->plots)&&is_array($inventory->plots)) {
                    foreach($inventory->plots as $p) {
                        $plot=$stand->plots()->create([]);

                        if(isset($p->trees)&&is_array($p->trees)) {

                            $trees = array();

                            foreach($p->trees as $treedata) {

                                if(!isset($metric)&&isset($treedata->metric)) {
                                    $metric=$treedata->metric;
                                }
                                $tree_species = null;

                                foreach($species as $s) {
                                    if($s->specie_text==$treedata->species->text) {
                                        $tree_species=$s;
                                        break;
                                    }
                                }

                                if($tree_species==null) {
                                    $tree_species=MDIISpecie::create([
                                        'specie_nr' => $treedata->species->number,
                                        'specie_text' => $treedata->species->text
                                    ]);

                                    $species->push($tree_species);
                                }

                                $treedata->m_d_i_i_specie_id=$tree_species->id;
                                
                                $treedata->m_d_i_i_quality_id=null;

                                if(isset($treedata->quality)&&$treedata->quality->number!=null&&$treedata->quality->text!=null) {
                                    $quality = null;
                                    foreach($qualities as $q) {
                                        if($q->quality_text==$treedata->quality->text) {
                                            $quality=$q;
                                            break;
                                        }
                                    }

                                    if($quality==null) {
                                        $quality=MDIIQuality::create([
                                            'quality' => $treedata->quality->number,
                                            'quality_text' => $treedata->quality->text
                                        ]);

                                        $qualities->push($quality);
                                    }

                                    $treedata->m_d_i_i_quality_id=$quality->id;
                                }
                                

                                $treedata->m_d_i_i_plot_id=$plot->id;

                                $treedata=$this->unsetExcept((array)$treedata,$tree_keys);
                                array_push($trees,$treedata);
                            }

                            foreach (array_chunk($trees,1000) as $t)  
                            {
                                MDIITree::insert($t);
                            }

                        }
                    }

                    $settings=json_decode($stand->settings);

                    $settings->measurement_system=$metric==1 ? "metric" : "imperial";

                    if(count($inventory->plots)>1) {
                        $settings->inventory_type='plot';
                    }
                    
                    $stand->settings=json_encode($settings);
                    $stand->save();

                }
            }
        }

        return new MDIIFileResource($file);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MDII\MDIIFile  $mDIIFile
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new MDIIFileResource($file);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MDII\MDIIFile  $mDIIFile
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file,Request $request)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'string|max:50',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }


        $file->update($request->all());

        $file->save();

        return new MDIIFileResource($file);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MDII\MDIIFile  $mDIIFile
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIIFile $file)
    {
        if(!$this->checkPath($application_instance,$dc_application,$file)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $file->delete();

        return response('Destroyed.', 200);
    }

    /**
     * Check the paths relationships
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\MDII\MDIIApplication $mdii_application
     * @param  \App\Model\MDII\MDIIFile $file
     * @return Boolean
     */
    private function checkPath(ApplicationInstance $application_instance,MDIIApplication $mdii_application,MDIIFile $file=null) {
        if($application_instance->id==$mdii_application->application_instance_id) {
            if($file!=null) {
                return $file->m_d_i_i_application_id==$mdii_application->id;
            }
            return true;
        }
        return false;
    }

    private function unsetExcept($object,$keys) {
        foreach ($object as $key => $value) {
            if (!in_array($key, $keys)) {
                unset($object[$key]);
            }
        }
        return $object;
    }
}
