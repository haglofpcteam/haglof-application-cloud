<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\MDII\MDIISpecie as MDIISpecies;
use Illuminate\Http\Request;

use App\Model\MDII\MDIIFile;
use App\Model\MDII\MDIIApplication;
use App\Model\ApplicationInstance;
use App\Model\MDII\MDIIStand;


use App\Http\Resources\MDIISpeciesCollection;
use App\Http\Resources\MDIISpecies as MDIISpeciesResource;


class MDIISpeciesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,MDIIApplication $dc_application)
    {
        if($application_instance->id!=$dc_application->application_instance_id||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new MDIISpeciesCollection($dc_application->species()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * merge 2 Species
     *
     * @param  \App\Model\ApplicationInstance  $application_instance
     * @param  \App\Model\MDII\MDIIApplication $dc_application
     * @param  \App\Model\MDII\MDIISpecies  $species1
     * @param  \App\Model\MDII\MDIISpecies  $species2
     * @return Boolean
     */
    public function merge(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIISpecies $species1,MDIISpecies $species2) {
        
        if(!$application_instance->user
        ||$application_instance->id!=$dc_application->application_instance_id
        ||!$this->checkSpeciesPermissions($application_instance,$dc_application,$species1)
        ||!$this->checkSpeciesPermissions($application_instance,$dc_application,$species2)) {
            return response('Unauthorized.', 401);
        }

        if($species1->m_d_i_i_application_id!=null) {
            return response('Cant merge default species',405);
        }

        $species1->trees()->update(['m_d_i_i_specie_id'=>$species2->id]);
        $species1->delete();

        return response('OK', 200);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MDII\MDIISpecie  $mDIISpecie
     * @return \Illuminate\Http\Response
     */
    public function show(MDIISpecie $mDIISpecie)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MDII\MDIISpecie  $mDIISpecie
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,MDIIApplication $dc_application,MDIISpecies $species,Request $request)
    {
        if(!$application_instance->user
        ||$application_instance->id!=$dc_application->application_instance_id
        ||!$this->checkSpeciesPermissions($application_instance,$dc_application,$species)) {
            return response('Unauthorized.', 401);
        }

        $species->update($request->all());
        $species->save();


        return new MDIISpeciesResource($species);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MDII\MDIISpecie  $mDIISpecie
     * @return \Illuminate\Http\Response
     */
    public function destroy(MDIISpecie $mDIISpecie)
    {
        //
    }

    /*
    * Check species permissions
    *
    * @param  \App\Model\ApplicationInstance  $application_instance
    * @param  \App\Model\MDII\MDIIApplication $mdii_application
    * @param  \App\Model\MDII\MDIISpecies $species
    * @return Boolean
    */
   private function checkSpeciesPermissions(ApplicationInstance $application_instance,MDIIApplication $mdii_application,MDIISpecies $species) {
       
       if($mdii_application->species()->where('id',$species->id)->exists()) {
           return true;
       }

       $stand=$species->trees()->first()->plot()->first()->stand()->first();
       if($stand==null) {
           return true;
       }
       $file=$stand->file()->first();

       return $this->checkPath($application_instance,$mdii_application,$file,$stand);

   }

   /**
    * Check the paths relationships
    *
    * @param  \App\Model\ApplicationInstance  $application_instance
    * @param  \App\Model\MDII\MDIIApplication $mdii_application
    * @param  \App\Model\MDII\MDIIFile $file
    * @param  \App\Model\MDII\MDIIStand $stand
    * @return Boolean
    */
   private function checkPath(ApplicationInstance $application_instance,MDIIApplication $mdii_application,MDIIFile $file,MDIIStand $stand,MDIIQuality $quality=null) {
       if($application_instance->id==$mdii_application->application_instance_id
           &&$file->m_d_i_i_application_id==$mdii_application->id
           &&$stand->m_d_i_i_file_id==$file->id) {
           if($quality!=null) {
               return $stand->qualities()->where('id',$quality->id)->exists();
           }
           return true;
       }
       return false;
   }
}
