<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\ApplicationInstance;
use App\Model\ReportTemplate;
use Illuminate\Http\Request;

class ReportTemplateApplicationInstanceController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ReportTemplate $template,Request $request)
    {
        if(auth()->user()->role<1) {
            return response('Unauthorized.', 401);
        }
        $template->applicationInstances()->attach($request->application_instance_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\ApplicationInstance  $applicationInstance
     * @return \Illuminate\Http\Response
     */
    public function destroy(ReportTemplate $template,ApplicationInstance $applicationInstance)
    {
        if(auth()->user()->role<1) {
            return response('Unauthorized.', 401);
        }
        $template->applicationInstances()->detach($applicationInstance->id);
    }
}
