<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\MDII\MDIIPlot;
use Illuminate\Http\Request;

class MDIIPlotController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\MDII\MDIIPlot  $mDIIPlot
     * @return \Illuminate\Http\Response
     */
    public function show(MDIIPlot $mDIIPlot)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\MDII\MDIIPlot  $mDIIPlot
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MDIIPlot $mDIIPlot)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\MDII\MDIIPlot  $mDIIPlot
     * @return \Illuminate\Http\Response
     */
    public function destroy(MDIIPlot $mDIIPlot)
    {
        //
    }
}
