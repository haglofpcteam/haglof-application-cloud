<?php

namespace App\Http\Controllers\Api;

use App\Model\ApplicationInstance;
use App\Model\Pile\PileApplicationLocation;
use App\Model\Pile\PileApplicationSite;
use App\Model\Pile\PileApplication;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\PileApplicationLocationCollection;
use App\Http\Resources\PileApplicationLocation as PileApplicationLocationResource;

use Illuminate\Support\Facades\Validator;
use stdClass;

class PileApplicationLocationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $pile_application_locations = $pile_application_site->locations()->get();

        return new PileApplicationLocationCollection($pile_application_locations);
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \App\Model\Pile\PileApplicationSite $pile_application_site
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site,Request $request)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'volume_adjustment_factor' => 'numeric',
            'density' => 'numeric',
            'description' => 'string|nullable',
            'measurement_system' => 'string'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        //Save Settings

        $user=auth()->user();

        $settings=json_decode($user->settings);

        if($settings==null) {
            $settings=new stdClass;
        }

        $settings->measurement_system=$request->measurement_system;

        if(!isset($settings->pile)) {
            $settings->pile = new stdClass;
        }

        $settings->pile->volume_adjustment_factor=$request->volume_adjustment_factor;
        $settings->pile->density=$request->density;

        $user->settings=json_encode($settings);
        $user->save();

        //Create location

        $data = $request->all();
        $data['pile_application_site_id']=$pile_application_site->id;

        $pile_application_location = PileApplicationLocation::create($data);
    
        return new PileApplicationLocationResource($pile_application_location);
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Pile\PileApplicationLocation  $pileApplicationLocation
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site,PileApplicationLocation $pile_application_location)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new PileApplicationLocationResource($pile_application_location);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Pile\PileApplicationLocation  $pileApplicationLocation
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site,PileApplicationLocation $pile_application_location,Request $request)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:50',
            'volume_adjustment_factor' => 'numeric|required',
            'description' => 'string|nullable',
            'measurement_system' => 'string|required'
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        $pile_application_location->update($request->all());

        $pile_application_location->save();

        return new PileApplicationLocationResource($pile_application_location);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Pile\PileApplicationLocation  $pileApplicationLocation
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site,PileApplicationLocation $pile_application_location)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $pile_application_location->delete();

        return response('Destroyed.', 200);
    }

    /**
     * Check the paths relationships
     *
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \App\Model\Pile\PileApplicationSite  $pileApplicationSite
     * @param  \App\Model\Pile\PileApplicationLocation  $pileApplicationLocation
     * @return Boolean
     */
    private function checkPath(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site, PileApplicationLocation $pile_application_location=null) {
        if($pile_application->application_instance_id==$application_instance->id&&$pile_application_site->pile_application_id==$pile_application->id) {
            if($pile_application_location!=null) {
                return $pile_application_site->id==$pile_application_location->pile_application_site_id;
            }
            return true;
        }
        return false;
    }
}
