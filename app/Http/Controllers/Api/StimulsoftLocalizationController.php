<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Model\FileData;
use App\Model\ApplicationInstance;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

use App\Http\Resources\FileDataCollection;
use App\Http\Resources\FileData as FileDataResource;

class StimulsoftLocalizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new FileDataCollection(FileData::where('tag','localization')->get());
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\FileData  $fileData
     * @return \Illuminate\Http\Response
     */
    public function show(FileData $fileData)
    {
        if($fileData->tag=='localization') {
            return $fileData->data;
        }
        
    }
}
