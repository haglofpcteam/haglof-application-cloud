<?php

namespace App\Http\Controllers\Api;

use App\Model\Pile\PileApplicationTarget;
use App\Model\Pile\PileApplicationMeasurement;
use App\Model\Pile\PileApplicationLocation;
use App\Model\Pile\PileApplicationSite;
use App\Model\Pile\PileApplication;
use App\Model\ApplicationInstance;

use App\Model\FileData;

use App\Libraries\PileModel;
use App\Libraries\target;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;

use App\Http\Resources\PileApplicationMeasurementCollection;
use App\Http\Resources\PileApplicationMeasurement as PileApplicationMeasurementResource;

use App\Http\Controllers\PileMeasurementController;

class PileApplicationMeasurementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,
                        PileApplication $pile_application,
                        PileApplicationSite $pile_application_site,
                        PileApplicationLocation $pile_application_location)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new PileApplicationMeasurementCollection($pile_application_location->measurements()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationInstance $application_instance,
                            PileApplication $pile_application,
                            PileApplicationSite $pile_application_site,
                            PileApplicationLocation $pile_application_location,
                            Request $request)
    {
        $validator = Validator::make($request->all(), [
            'description' => ['string','nullable'],
            'csvfile' => ['required','file'],
        ]);

        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }
    
        if($request->hasFile('csvfile')) {
            $csvfile = $request->csvfile;
            if ($csvfile->isValid()) {
                $filecontent=$csvfile->get();

                $pile_application_measurement = $this->parse_file_content($pile_application_location,$filecontent);

                $pile_application_measurement->description=$request->description;
                $pile_application_measurement->volume_adjustment_factor=$request->volume_adjustment_factor;
                $pile_application_measurement->density=$request->density;
                $pile_application_measurement->measurement_system=
                $pile_application_measurement->save();

                $fileData = new FileData();
                $fileData->mime_type = "text/csv";
                $fileData->data = $filecontent;
                $fileData->name = $csvfile->getClientOriginalName();
                $fileData->user_id = auth()->user()->id;
                $fileData->application_instance_id=$application_instance->id;
                $fileData->save();

            }
            else {
                response('Invalid CSV-file',422);
            }
        }
        else {
            response('CSV file did not upload',422);
        }

        return new PileApplicationMeasurementResource($pile_application_measurement);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Pile\PileApplicationMeasurement  $pileApplicationMeasurement
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance,
                        PileApplication $pile_application,
                        PileApplicationSite $pile_application_site, 
                        PileApplicationLocation $pile_application_location,
                        PileApplicationMeasurement $pile_application_measurement)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new PileApplicationMeasurementResource($pile_application_measurement);
    }

    public function show_snapshot(ApplicationInstance $application_instance,
                                PileApplication $pile_application,
                                PileApplicationSite $pile_application_site, 
                                PileApplicationLocation $pile_application_location,
                                PileApplicationMeasurement $pile_application_measurement) {

        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return json_encode($pileApplicationMeasurement->image()->first());
    }


    public function show_data(ApplicationInstance $application_instance,
                            PileApplication $pile_application,
                            PileApplicationSite $pile_application_site, 
                            PileApplicationLocation $pile_application_location,
                            PileApplicationMeasurement $pile_application_measurement) {

        
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }
        
        $model = new PileModel($pile_application_measurement->volume_adjustment_factor,$pile_application_measurement->density);

        foreach($pile_application_measurement->targets()->orderBy('seq')->get() as $target) {
            $model->addTarget($target);
        }

        $model->type=$pile_application_measurement->type;
        $model->calculate();

        return json_encode($model);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Pile\PileApplicationMeasurement  $pileApplicationMeasurement
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,
                    PileApplication $pile_application,
                    PileApplicationSite $pile_application_site, 
                    PileApplicationLocation $pile_application_location,
                    PileApplicationMeasurement $pile_application_measurement,
                    Request $request)
    {

        $validator = Validator::make($request->all(), [
            'description' => ['string','nullable'],
            'volume_adjustment_factor' => ['numeric'],
            'density' => ['numeric'],
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $pile_application_measurement->update($request->all());

        $pile_application_measurement->save();

        return new PileApplicationMeasurementResource($pile_application_measurement);
    }

        /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Pile\PileApplicationMeasurement  $pileApplicationMeasurement
     * @return \Illuminate\Http\Response
     */
    public function update_snapshot(ApplicationInstance $application_instance,
                    PileApplication $pile_application,
                    PileApplicationSite $pile_application_site, 
                    PileApplicationLocation $pile_application_location,
                    PileApplicationMeasurement $pile_application_measurement,
                    Request $request)
    {

        $validator = Validator::make($request->all(), [
            'image_data' => ['string','required'],
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $fileData = new FileData();
        $fileData->mime_type = $request['mime_type'];
        $fileData->data = $request['image_data'];
        $fileData->save();

        $pileApplicationMeasurement->file_data_id=$fileData->id;
        $pileApplicationMeasurement->save();

        return response('Snapshot updated',200);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Pile\PileApplicationMeasurement  $pileApplicationMeasurement
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,
                        PileApplication $pile_application,
                        PileApplicationSite $pile_application_site, 
                        PileApplicationLocation $pile_application_location,
                        PileApplicationMeasurement $pile_application_measurement)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site,$pile_application_location,$pile_application_measurement)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $pile_application_measurement->delete();

        return response('Destroyed.', 200);
    }

    /**
     * Check the paths relationships
     *
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \App\Model\Pile\PileApplicationSite  $pileApplicationSite
     * @param  \App\Model\Pile\PileApplicationLocation  $pile_application_location
     * @param  \App\Model\Pile\PileApplicationMeasurement  $pile_application_measurement
     * @return Boolean
     */
    private function checkPath(ApplicationInstance $application_instance,
                                PileApplication $pile_application,
                                PileApplicationSite $pile_application_site, 
                                PileApplicationLocation $pile_application_location,
                                PileApplicationMeasurement $pile_application_measurement=null) 
                                {
        if($pile_application->application_instance_id==$application_instance->id&&
            $pile_application_site->pile_application_id==$pile_application->id&&
            $pile_application_location->pile_application_site_id==$pile_application_site->id) {
            if($pile_application_measurement!=null) {
                return $pile_application_location->id==$pile_application_measurement->pile_application_location_id;
            }
            return true;
        }
        return false;
    }

    private function parse_file_content(PileApplicationLocation $pile_application_location,$filecontent) {
        date_default_timezone_set('UTC');

        $lines=preg_split("/\r\n|\n|\r/", $filecontent);

        $headlines=preg_split("/;/",$lines[0]);

        $latest_time=0;

        $records = array();

        $neg_z=0;

        $unit="M";

        $measurement=PileApplicationMeasurement::create([
            'pile_application_location_id' => $pile_application_location->id,
            'user_id' => auth()->user()->id
        ]);

        try {
            foreach($lines as $line) {
                $a = preg_split("/;/",$line);

                if(count($a)<3)
                    continue;

                $type=$this->getValueOfArray($a,$headlines,"TYPE");

                if($type=='TARGET'|$type=='BASE') {

                    $timestamp=$latest_time;

                    $date=$this->getValueOfArray($a,$headlines,"DATE");
                    $utc=$this->getValueOfArray($a,$headlines,"UTC");

                    if((strlen($date)+strlen($utc))>9) {
                        if(strlen($date)==5) {
                            $year=substr($date,3,2);
                            $month=substr($date,1,2);
                            $day=substr($date,0,1);
                        }
                        elseif(strlen($date)==6) {
                            $year=substr($date,4,2);
                            $month=substr($date,2,2);
                            $day=substr($date,0,2);
                        }

                        if(strlen($utc)==5) {
                            $hour=substr($utc,0,1);
                            $minute=substr($utc,1,2);
                            $second=substr($utc,3,2);
                        }
                        elseif(strlen($utc)==6) {
                            $hour=substr($utc,0,2);
                            $minute=substr($utc,2,2);
                            $second=substr($utc,4,2);
                        }


                        $timestamp=mktime($hour,$minute,$second,$month,$day,$year);
                    }
                    elseif($timestamp==0) {
                        $timestamp=time();
                    }

                    $measurement->file_id = $this->getValueOfArray($a,$headlines,"ID");
                    $latest_time=max($timestamp,$latest_time);

                    $z=$this->getValueOfArray($a,$headlines,"Z(m)");

                    if($z<0) {
                        $neg_z++;
                    }

                    $altitude = floatval($this->getValueOfArray($a,$headlines,"ALTITUDE"));
                    $sd = floatval($this->getValueOfArray($a,$headlines,"SD"));
                    $hd = floatval($this->getValueOfArray($a,$headlines,"HD"));
                    $h = floatval($this->getValueOfArray($a,$headlines,"H"));

                    if($unit=="I") {
                        $altitude = $this->feetToMeter($altitude);
                        $sd = $this->feetToMeter($sd);
                        $hd = $this->feetToMeter($hd);
                        $h = $this->feetToMeter($h);
                    }

                    array_push($records,[
                        'pile_application_measurement_id' => $measurement->id,
                        'type' => $type,
                        'latitude' => floatval($this->getValueOfArray($a,$headlines,"LAT")),
                        'ns' => $this->getValueOfArray($a,$headlines,"N/S"),
                        'longitude' => floatval($this->getValueOfArray($a,$headlines,"LON")),
                        'ew' => $this->getValueOfArray($a,$headlines,"E/W"),
                        'altitude' => $altitude,
                        'hdop' => $this->getValueOfArray($a,$headlines,"HDOP"),
                        'time' => date("Y-m-d H:i:s",$timestamp),
                        'seq' => $this->getValueOfArray($a,$headlines,"SEQ"),
                        'sd' => $sd,
                        'hd' => $hd,
                        'h' => $h,
                        'pitch' => floatval($this->getValueOfArray($a,$headlines,"PITCH")),
                        'az' => floatval($this->getValueOfArray($a,$headlines,"AZ")),
                        'x' => $this->getValueOfArray($a,$headlines,"X(m)"),
                        'y' => $this->getValueOfArray($a,$headlines,"Y(m)"),
                        'z' => $z,
                        'utm_zone' => $this->getValueOfArray($a,$headlines,"UTM ZONE"),
                        'created_at'=>date('Y-m-d H:i:s'),
                        'updated_at'=> date('Y-m-d H:i:s')
                    ]);

                }
                elseif($type=="SET") {
                    $measurement->unit_production_id = intval($this->getValueOfArray($a,$headlines,"PROD"));
                    $measurement->unit_version_id = intval($this->getValueOfArray($a,$headlines,"VER"));
                    $measurement->unit_serial_number = intval($this->getValueOfArray($a,$headlines,"SNR"));
                    $unit = $this->getValueOfArray($a,$headlines,"UNIT");

                    $measurement->transponder_height = floatval($this->getValueOfArray($a,$headlines,"TRPH"));
                    $measurement->eye_height = floatval($this->getValueOfArray($a,$headlines,"REFH"));
                    $measurement->pivot_offset = floatval($this->getValueOfArray($a,$headlines,"P.OFF"));
                    $measurement->magnetic_declination = floatval($this->getValueOfArray($a,$headlines,"DECL"));

                    if($unit=="I") {
                        $trph = $this->feetToMeter($trph);
                        $refh = $this->feetToMeter($refh);
                        $poff = $this->feetToMeter($poff);
                    }
                }
            }

            if(count($records)>0) {
                PileApplicationTarget::insert($records);

                if(($neg_z/count($records))>0.7) {
                    $measurement->type='pit';
                }
                else {
                    $measurement->type='pile';
                }

                $measurement->measurement_time=date("Y-m-d H:i:s",$latest_time);
            }
          
            $measurement->save();

            return $measurement;
        }
        catch(\Exception $e) {
            $this->logToFile($e->getLine() . " - " . $e->getMessage());
            $measurement->targets()->delete();
            $measurement->delete();
            return null;
        }
    }

    private function feetToMeter($feet) {
        return $feet==0 ? 0 : floatval($feet) / 3.2808399;
    }

    function getValueOfArray($array,$keys,$key) {

        $length=count($keys);
        $index=-1;
        for($i=0;$i<$length;$i++) {
            if($keys[$i]==$key) {
                $index=$i;
                break;
            }
        }
        if($index==-1) {
            return null;
        }
        else {
            return $array[$index];
        }

    }

    
}
