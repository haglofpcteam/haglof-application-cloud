<?php

namespace App\Http\Controllers\Api;

use App\Model\ApplicationInstance;
use App\Model\Pile\PileApplicationSite;
use App\Model\Pile\PileApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Resources\PileApplicationSiteCollection;
use App\Http\Resources\PileApplicationSite as PileApplicationSiteResource;

use Illuminate\Support\Facades\Validator;

class PileApplicationSiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @return \Illuminate\Http\Response
     */
    public function index(ApplicationInstance $application_instance,PileApplication $pile_application)
    {
        if(!$this->checkPath($application_instance,$pile_application)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }
        return new PileApplicationSiteCollection($pile_application->sites()->get());
    }

    /**
     * Store a newly created resource in storage.
     * 
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ApplicationInstance $application_instance,PileApplication $pile_application,Request $request)
    {
        if(!$this->checkPath($application_instance,$pile_application)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [ 
            'name' => 'required|string|max:50',
            'description' => 'string|nullable',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        $data = $request->all();
        $data['pile_application_id']=$pile_application->id;

        return new PileApplicationSiteResource(PileApplicationSite::create($data));
    }

    /**
     * Display the specified resource.
     * 
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \App\Model\Pile\PileApplicationSite  $pileApplicationSite
     * @return \Illuminate\Http\Response
     */
    public function show(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        return new PileApplicationSiteResource($pile_application_site);
    }

    /**
     * Update the specified resource in storage.
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \App\Model\Pile\PileApplicationSite  $pileApplicationSite
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site,Request $request)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site)||!$application_instance->user) {
            return response('Unauthorized.', 401);
        }

        $validator = Validator::make($request->all(), [
            'name' => 'string|max:50',
            'description' => 'string|nullable',
        ]);

        if ($validator->fails()) {
            return response($validator->errors(),422);
        }

        $pile_application_site->update($request->all());

        $pile_application_site->save();

        return new PileApplicationSiteResource($pile_application_site);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \App\Model\Pile\PileApplicationSite  $pileApplicationSite
     * @return \Illuminate\Http\Response
     */
    public function destroy(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site)
    {
        if(!$this->checkPath($application_instance,$pile_application,$pile_application_site)||!$application_instance->admin) {
            return response('Unauthorized.', 401);
        }

        $pile_application_site->delete();

        return response('Destroyed.', 200);
    }

    /**
     * Check the paths relationships
     *
     * @param  \App\Model\Pile\ApplicationInstance  $application_instance
     * @param  \App\Model\Pile\PileApplication  $pile_application
     * @param  \App\Model\Pile\PileApplicationSite  $pileApplicationSite
     * @return Boolean
     */
    private function checkPath(ApplicationInstance $application_instance,PileApplication $pile_application,PileApplicationSite $pile_application_site=null) {
        if($pile_application->application_instance_id==$application_instance->id) {
            if($pile_application_site!=null) {
                return $pile_application->id==$pile_application_site->pile_application_id;
            }
            return true;
        }
        return false;
    }
}
