<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\User;
use Illuminate\Contracts\Auth\Factory as Auth;

class EnsureUserActivated
{
    /**
     * The authentication factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $redirectToRoute
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (! $request->user($guard)) {

            return response()->json([
                "error" => 'not_logged_in',
                'message' => 'Not logged in',
                'detail' => 'The access token is either missing or incorrect.'
            ], 401);

        } else if
            ($request->user($guard) instanceof User && !$request->user($guard)->activated) {

            return response()->json([
                "error" => 'not_activated',
                'message' => 'User not activated',
                'detail' => 'You have to activate your account before you can use the site'
            ], 403);
        }

        $this->auth->shouldUse($guard);

        return $next($request);
    }
}