<?php

namespace App\Http\Middleware;

use Closure;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (\Auth::user() && \Auth::user()->role > 1) {
            return $next($request);
        }
        if ($request->wantsJson()) {
            return response('Unauthorized.', 401);
        } else {
            return redirect('unauthorized');
        }
    }
}
