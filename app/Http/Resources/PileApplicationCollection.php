<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PileApplicationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
            $this->collection->transform(function($pileApplication){
                return [
                    'id' => $pileApplication->id,
                    'application_instance_id' => $pileApplication->application_instance_id,
                ];
            })
            
        ;
    }
}
