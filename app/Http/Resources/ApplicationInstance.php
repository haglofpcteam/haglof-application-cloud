<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ApplicationInstance extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'organization' => new OrganizationCollection($this->organization()->get()),
            'application' => new Application($this->application()->first()),
            'users' => new UserCollection($this->users()->get()),
            'report_templates' => new ReportTemplateCollection($this->attachedReportTemplates()->get()),
            'paid' => $this->paid,
            'admin' => $this->admin,
            'paid_number_of_users' => $this->paidNumberOfUsers,
            'subscriptions' => $this->subscriptions->transform(function($subscription){
                return [
                    'id' => $subscription->ID,
                    'qty' => $subscription->qty,
                    'active' => $subscription->isActive,
                    'timestamp' => $subscription->post_date
                ];
            }),
            'paid_until' => $this->paid_until,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
