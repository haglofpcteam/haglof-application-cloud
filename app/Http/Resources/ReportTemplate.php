<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ReportTemplate extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'key' => $this->key,
            'standard' => $this->standard,
            'premium' => $this->premium, 
            'test_data' => $this->test_data, 
            'template' => $this->template,
            'javascript' => $this->javascript,
            'data_type' => $this->data_type,
            'parameters' => new ReportParameterCollection($this->parameters()->get()),
            'application_instances' => new ApplicationInstanceCollection($this->applicationInstances()->get()),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
