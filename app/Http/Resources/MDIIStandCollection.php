<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MDIIStandCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
        $this->collection->transform(function($stand){
            return [
                'id' => $stand->id,
                'm_d_i_i_file_id' => $stand->m_d_i_i_file_id,
                'name' => $stand->name,
                'number' => $stand->number,
                'type' => ((int)$stand->log_list)==1 ? 'log_list' : 'stand_view'
            ];
        });
    }
}
