<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ApplicationInstanceCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
            $this->collection->transform(function($applicationInstance){
                $application = $applicationInstance->application()->first();
                return [
                    'id' => $applicationInstance->id,
                    'organization' => $applicationInstance->organization_id,
                    'organization_name' => $applicationInstance->organization()->first()->name,
                    'application' => new Application($applicationInstance->application()->first()),
                    'trashed' => $applicationInstance->trashed(),
                    'deleted_at' => $applicationInstance->deleted_at
                ];
            });
    }
}
