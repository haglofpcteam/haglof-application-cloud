<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MDIIFileCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
        $this->collection->transform(function($mdii_file){
            return [
                'id' => $mdii_file->id,
                'm_d_i_i_application_id' => $mdii_file->m_d_i_i_application_id,
                'm_d_i_i_folder_id' => $mdii_file->m_d_i_i_folder_id,
                'name' => $mdii_file->name,
            ];
        });
    }
}
