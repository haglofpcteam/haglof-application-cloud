<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MDIIApplication extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'application_instance_id' => $this->application_instance_id,
            'stand_default_settings' => $this->stand_default_settings,
            'species' =>new MDIISpeciesCollection($this->species()->get()),
            'qualities' =>new MDIIQualityCollection($this->qualities()->get()),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
