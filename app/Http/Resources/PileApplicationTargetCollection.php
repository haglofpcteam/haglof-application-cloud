<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PileApplicationTargetCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
            $this->collection->transform(function($pileApplicationTarget){
                return [
                    'id' => $pileApplicationTarget->id,        
                    'type' => $pileApplicationTarget->type,
                    'latitude' => $pileApplicationTarget->latitude,
                    'ns' => $pileApplicationTarget->ns,
                    'longitude' => $pileApplicationTarget->longitude,
                    'ew' => $pileApplicationTarget->ew,
                    'altitude' => $pileApplicationTarget->altitude,
                    'hdop' => $pileApplicationTarget->hdop,
                    'time' => $pileApplicationTarget->time,
                    'seq' => $pileApplicationTarget->seq,
                    'sd' => $pileApplicationTarget->sd,
                    'hd' => $pileApplicationTarget->hd,
                    'h' => $pileApplicationTarget->h,
                    'pitch' => $pileApplicationTarget->pitch,
                    'az' => $pileApplicationTarget->az,
                    'x' => $pileApplicationTarget->x,
                    'y' => $pileApplicationTarget->y,
                    'z' => $pileApplicationTarget->z,
                    'utm_zone' => $pileApplicationTarget->utm_zone,
                ];
            })
            
        ;
    }
}
