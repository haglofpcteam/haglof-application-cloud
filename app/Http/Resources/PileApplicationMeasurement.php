<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PileApplicationMeasurement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'volume_adjustment_factor' => $this->volume_adjustment_factor,
            'density' => $this->density,
            'pile_application_location_id' => $this->pile_application_location_id,
            'user' => $this->user()->first()->identifiableName(),
            'measurement_time' => $this->measurement_time,
            'file_data_id' => $this->file_data_id,
            'type' => $this->type,
            'unit_production_id' => $this->unit_production_id,
            'unit_version_id' => $this->unit_version_id,
            'unit_serial_number' => $this->unit_serial_number,
            'transponder_height' => $this->transponder_height,
            'eye_height' => $this->eye_height,
            'pivot_offset' => $this->pivot_offset,
            'magnetic_declination' => $this->magnetic_declination,
            'file_id' => $this->file_id,
            'settings' => $this->settings,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
