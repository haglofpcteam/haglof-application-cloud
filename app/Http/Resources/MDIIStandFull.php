<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MDIIStandFull extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'm_d_i_i_file_id' => $this->m_d_i_i_file_id,
            'species' => new MDIISpeciesCollection($this->species()->get()),
            'qualities' => new MDIIQualityCollection($this->qualities()->get()),
            'plots' => new MDIIPlotCollection($this->plots()->get()),
            'trees' => new MDIITreeCollection($this->trees()->get()),
            'sample_trees' => new MDIITreeCollection($this->trees()->where('height1','>','0')->get()),
            'settings' => json_decode($this->settings),
            'name' => $this->name,
            'log_list' => $this->log_list,
            'number' => $this->number,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
