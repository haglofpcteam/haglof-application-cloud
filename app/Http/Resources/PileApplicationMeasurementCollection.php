<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PileApplicationMeasurementCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
            $this->collection->transform(function($pileApplicationMeasurement) {
                $user = $pileApplicationMeasurement->user()->first();

                if($user) {
                    $username = $user->identifiableName();
                }
                else {
                    $username = "Unknown";
                }

                return [
                    'id' => $pileApplicationMeasurement->id,
                    'pile_application_location_id' => $pileApplicationMeasurement->pile_application_location_id,
                    'volume_adjustment_factor' => $pileApplicationMeasurement->volume_adjustment_factor,
                    'density' => $pileApplicationMeasurement->density,
                    'user' => $username,
                    'measurement_time' => $pileApplicationMeasurement->measurement_time,
                    'file_data_id' => $pileApplicationMeasurement->file_data_id,
                    'type' => $pileApplicationMeasurement->type,
                    'unit_production_id' => $pileApplicationMeasurement->unit_production_id,
                    'unit_version_id' => $pileApplicationMeasurement->unit_version_id,
                    'unit_serial_number' => $pileApplicationMeasurement->unit_serial_number,
                    'transponder_height' => $pileApplicationMeasurement->transponder_height,
                    'eye_height' => $pileApplicationMeasurement->eye_height,
                    'pivot_offset' => $pileApplicationMeasurement->pivot_offset,
                    'magnetic_declination' => $pileApplicationMeasurement->magnetic_declination,
                    'file_id' => $pileApplicationMeasurement->file_id,
                    'settings' => $pileApplicationMeasurement->settings,
                    'description' => $pileApplicationMeasurement->description,
                ];
            })
            
        ;
    }
}
