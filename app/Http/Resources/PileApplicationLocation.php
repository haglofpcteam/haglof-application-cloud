<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PileApplicationLocation extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'pile_application_site_id' => $this->pile_application_site_id,
            'name' => $this->name,
            'measurement_system' => $this->measurement_system,
            'volume_adjustment_factor' => $this->volume_adjustment_factor,
            'density' => $this->density,
            'description' => $this->description,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
