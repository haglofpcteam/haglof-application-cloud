<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MDIIFolderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
        $this->collection->transform(function($mdiiFolder){
            return [
                'id' => $mdiiFolder->id,
                'm_d_i_i_application_id' => $mdiiFolder->m_d_i_i_application_id,
                'm_d_i_i_folder_id' => $mdiiFolder->m_d_i_i_folder_id,
                'name' => $mdiiFolder->name,
                'created_at' => $mdiiFolder->created_at,
            ];
        });
    }
}