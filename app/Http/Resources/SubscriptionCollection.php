<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SubscriptionCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($subscription){
            return [
                'id' => $subscription->id,
                'qty' => $subscription->qty,
                'active' => $subscription->isActive,
                'application_instance' => new ApplicationInstance($subscription->applicationInstance),
                'application' => new Application($subscription->application)
            ];
        });
    }
}
