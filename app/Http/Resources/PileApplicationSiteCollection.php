<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PileApplicationSiteCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
            $this->collection->transform(function($pileApplicationSite){
                return [
                    'id' => $pileApplicationSite->id,
                    'pile_application_id' => $pileApplicationSite->pile_application_id,
                    'name' => $pileApplicationSite->name,
                    'description' => $pileApplicationSite->description,
                ];
            })
            
        ;
    }
}
