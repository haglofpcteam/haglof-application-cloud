<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class SocialiteProviderCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($provider){
            return [
                'id' => $provider->id,
                'user_id' => $provider->user_id,
                'provider' => $provider->provider,
            ];
        });
    }
}
