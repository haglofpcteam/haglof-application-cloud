<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = Auth::user();
        $organizations = [];

        if($user!=null&&$user->role>0) {
            $organizations = $this->organizations()->get();
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'identifiable_name' => $this->identifiableName(),
            'role' => $this->role,
            'empty_password' => $this->password=="",
            'email' => $this->email,
            'timezone' => $this->timezone,
            'measurement_system' => $this->measurement_system,
            'date_format' => $this->date_format,
            'time_format' => $this->time_format,
            'settings' => $this->settings,
            'organizations' => $organizations,
            'email_verified_at' => $this->email_verified_at,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
