<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PileApplicationTarget extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'pile_application_measurement_id' => $this->pile_application_measurement_id,
            'type' => $this->type,
            'latitude' => $this->latitude,
            'ns' => $this->ns,
            'longitude' => $this->longitude,
            'ew' => $this->ew,
            'altitude' => $this->altitude,
            'hdop' => $this->hdop,
            'time' => $this->time,
            'seq' => $this->seq,
            'sd' => $this->sd,
            'hd' => $this->hd,
            'h' => $this->h,
            'pitch' => $this->pitch,
            'az' => $this->az,
            'x' => $this->x,
            'y' => $this->y,
            'z' => $this->z,
            'utm_zone' => $this->utm_zone,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
