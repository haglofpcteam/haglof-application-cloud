<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

use App\Http\Resources\ApplicationInstanceCollection;
use App\Http\Resources\UserCollection;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $subscriptions = $this->subscriptions()->get();

        $connected = false;

        foreach($subscriptions as $subscription) {
            if($subscription->application_instance!=null) {
                $connected = true;
            }
        }
        
        return [
            'id' => $this->ID,
            'connected' => $connected,
            'subscriptions' => new SubscriptionCollection($subscriptions)
        ];
    }
}
