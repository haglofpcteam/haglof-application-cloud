<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PileApplicationLocationCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
            $this->collection->transform(function($pileApplicationLocation){
                return [
                    'id' => $pileApplicationLocation->id,
                    'site' => $pileApplicationLocation->pile_application_site_id,
                    'name' => $pileApplicationLocation->name,
                    'description' => $pileApplicationLocation->description,
                    'measurement_system' => $pileApplicationLocation->measurement_system,
                    'volume_adjustment_factor' => $pileApplicationLocation->volume_adjustment_factor,
                    'density' => $pileApplicationLocation->density,
                ];
            })
            
        ;
    }
}
