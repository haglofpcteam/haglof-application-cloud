<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReportParameterCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return 
        $this->collection->transform(function($parameter){
            return [
                'name' => $parameter->name,
                'data_type' => $parameter->data_type,
                'default_value' => $parameter->default_value
            ];
        })
        
    ;
    }
}
