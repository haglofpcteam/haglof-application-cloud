<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class FileDataCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($fileData){
            return [
                'id' => $fileData->id,
                'mime_type' => $fileData->mime_type,
                'application_instance_id' => $fileData->application_instance_id,
                'user_id' => $fileData->user_id,
                'name' => $fileData->name,
                'tag' => $fileData->tag
            ];
        });
    }
}
