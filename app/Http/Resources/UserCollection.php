<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($user){
            return [
                'id' => $user->id,
                'name' => $user->name,
                'identifiable_name' => $user->identifiableName(),
                'role' => $user->role,
                'email' => $user->email,
            ];
        });
    }
}
