<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class ReportTemplateCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->collection->transform(function($template){
            return [
                'id' => $template->id,
                'name' => $template->name,
                'key' => $template->key,
                'standard' => $template->standard,
                'premium' => $template->premium,  
                'data_type' => $template->data_type,
                'parameters' => new ReportParameterCollection($template->parameters()->get()),
            ];
        })
        
    ;
    }
}
