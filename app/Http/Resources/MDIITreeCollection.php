<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MDIITreeCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
        $this->collection->transform(function($tree){
            return [
                'id' => $tree->id,
                'diameter' => $tree->diameter,
                'height1' => $tree->height1,
                'height2' => $tree->height2,
                'height3' => $tree->height3,
                'is_log' => $tree->is_log,
                'item_id' => $tree->item_id,
                'laravel_through_key' => $tree->laravel_through_key,
                'm_d_i_i_plot_id' => $tree->m_d_i_i_plot_id,
                'm_d_i_i_quality_id' => $tree->m_d_i_i_quality_id,
                'm_d_i_i_specie_id' => $tree->m_d_i_i_specie_id,
                'metric' => $tree->metric,
                'number' => $tree->number,
                'on_bark' => $tree->on_bark,
                'unique_id' => $tree->unique_id,
                'unit_serial_number' => $tree->unit_serial_number
            ];
        });
    }
}
