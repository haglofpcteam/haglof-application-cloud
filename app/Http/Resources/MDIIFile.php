<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MDIIFile extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'm_d_i_i_application_id' => $this->m_d_i_i_application_id,
            'm_d_i_i_folder_id' => $this->m_d_i_i_folder_id,
            'name' => $this->name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
