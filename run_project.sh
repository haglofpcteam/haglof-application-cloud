#!/bin/bash
cd docker
docker-compose up -d nginx workspace
docker-compose exec workspace bash -c "php artisan config:cache && composer dump-autoload && yarn run watch && bash"