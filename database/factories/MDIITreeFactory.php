<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Model\MDII\MDIITree::class,function (Faker $faker) {

    static $number = 1;

    return [
        'number' => $number++,
        'diameter' => $faker->numberBetween(25,700),
        'height1' => $faker->numberBetween(25,450),
        'height2' => $faker->numberBetween(25,450),
        'height3' => $faker->numberBetween(25,450),
        'is_log' => true,
        'on_bark' => true,
        'metric' => true
    ];
});
