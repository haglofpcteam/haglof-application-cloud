<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePileApplicationTargetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pile_application_targets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pile_application_measurement_id');

            $table->string('type');
            $table->double('latitude',13,9);
            $table->char('ns',1);
            $table->double('longitude',13,9);
            $table->char('ew',1);
            $table->float('altitude');
            $table->string('hdop');
            $table->timestamp('time');
            $table->integer('seq');
            $table->float('sd');
            $table->float('hd');
            $table->float('h');
            $table->float('pitch');
            $table->double('az');
            $table->string('x');
            $table->string('y');
            $table->string('z');
            $table->string('utm_zone')->nullable();
            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pile_application_targets');
    }
}
