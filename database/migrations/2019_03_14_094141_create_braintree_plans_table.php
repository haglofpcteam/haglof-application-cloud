<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBraintreePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('braintree_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('application_id')->nullable();
            $table->integer('number_of_users')->nullable();

            $table->string('braintree_id');
            $table->string('merchantId');
            $table->integer('billingDayOfMonth')->nullable();
            $table->integer('billingFrequency');
            $table->string('currencyIsoCode');
            $table->string('description')->nullable();
            $table->string('name');
            $table->integer('numberOfBillingCycles')->nullable();
            $table->string('price');
            $table->integer('trialDuration')->nullable();
            $table->string('trialDurationUnit')->nullable();
            $table->boolean('trialPeriod')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('braintree_plans');
    }
}
