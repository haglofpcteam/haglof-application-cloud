<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMDIITreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_d_i_i_trees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('m_d_i_i_plot_id');
            $table->integer('m_d_i_i_specie_id');
            $table->integer('m_d_i_i_quality_id')->nullable();
            $table->integer('number')->nullable();
            $table->integer('diameter')->nullable();
            $table->integer('height1')->nullable();
            $table->integer('height2')->nullable();
            $table->integer('height3')->nullable();
            $table->text('item_id')->nullable();
            $table->integer('unique_id')->nullable();
            $table->boolean('is_log')->nullable();
            $table->boolean('on_bark')->nullable();
            $table->boolean('metric')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_d_i_i_trees');
    }
}
