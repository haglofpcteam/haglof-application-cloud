<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('application_instance_id')->nullable();
            $table->date('paid_from')->nullable();
            $table->date('paid_until')->nullable();
            $table->integer('number_of_users')->nullable();
            $table->float('price_per_year')->nullable();
            $table->string('currency')->default("SEK");
            $table->string('status')->nullable();
            $table->integer('discount_id')->nullable();
            $table->integer('user_id')->nullable();

            $table->string('name')->nullable();
            $table->string('braintree_id')->nullable();
            $table->string('braintree_plan')->nullable();
            $table->integer('quantity')->nullable();
            $table->timestamp('trial_ends_at')->nullable();
            $table->timestamp('ends_at')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
