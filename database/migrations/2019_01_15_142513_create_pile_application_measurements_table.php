<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePileApplicationMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pile_application_measurements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->timestamp('measurement_time')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('pile_application_location_id');
            $table->integer('file_data_id')->nullable();
            $table->enum('type', ['pile', 'pit']);
            $table->integer('unit_production_id')->nullable();
            $table->integer('unit_version_id')->nullable();
            $table->integer('unit_serial_number')->nullable();
            $table->float('transponder_height')->nullable();
            $table->float('eye_height')->nullable();
            $table->float('pivot_offset')->nullable();
            $table->float('magnetic_declination')->nullable();
            $table->string('file_id')->nullable();
            $table->string('settings')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pile_application_measurements');
    }
}
