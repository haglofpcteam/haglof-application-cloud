<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePileApplicationLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pile_application_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('pile_application_site_id');
            $table->float('volume_adjustment_factor')->default(1);
            $table->enum('measurement_system', ['metric', 'imperial'])->default('metric');
            $table->string('description')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pile_application_locations');
    }
}
