<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVolumeAdjustmentFactorToPileApplicationMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pile_application_measurements', function (Blueprint $table) {
            $table->float('volume_adjustment_factor')->default(1);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pile_application_measurements', function (Blueprint $table) {
            $table->dropColumn('volume_adjustment_factor');
        });
    }
}
