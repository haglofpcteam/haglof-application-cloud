<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reports', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('report_template_id');
            $table->integer('application_instance_id');
            $table->integer('user_id');
            $table->softDeletes();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE reports ADD data MEDIUMBLOB");
        DB::statement("ALTER TABLE reports ADD report MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reports');
    }
}
