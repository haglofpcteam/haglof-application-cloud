<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMDIIFoldersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_d_i_i_folders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('m_d_i_i_application_id');
            $table->integer('m_d_i_i_folder_id')->nullable();
            $table->boolean('root')->default(false);
            $table->text('name');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_d_i_i_folders');
    }
}
