<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mime_type');

            $table->softDeletes();
            $table->timestamps();
        });

        DB::statement("ALTER TABLE file_datas ADD data MEDIUMBLOB");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_datas');
    }
}
