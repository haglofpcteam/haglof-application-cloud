<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMDIIQualitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_d_i_i_qualities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('m_d_i_i_application_id')->nullable();
            $table->integer('quality');
            $table->text('quality_text');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_d_i_i_qualities');
    }
}
