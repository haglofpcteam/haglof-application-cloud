<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStandDefaultSettingsToMDIIApplications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_d_i_i_applications', function (Blueprint $table) {
            $table->string('stand_default_settings');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_d_i_i_applications', function (Blueprint $table) {
            $table->dropColumn('stand_default_settings');
        });
    }
}
