<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUnitSerialNumberToMDIITreesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_d_i_i_trees', function (Blueprint $table) {
            $table->string('unit_serial_number')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_d_i_i_trees', function (Blueprint $table) {
            $table->dropColumn('unit_serial_number');
        });
    }
}
