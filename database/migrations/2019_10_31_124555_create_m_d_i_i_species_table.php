<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMDIISpeciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_d_i_i_species', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('m_d_i_i_application_id')->nullable();
            $table->text('specie_text');
            $table->integer('specie_nr');
            $table->float('form_height')->default(8.0);
            $table->float('form_factor')->default(0.4);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_d_i_i_species');
    }
}
