<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMDIIFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_d_i_i_files', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('m_d_i_i_application_id');
            $table->integer('m_d_i_i_folder_id');
            $table->timestamp('file_datetime')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('user_id');
            $table->text('name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_d_i_i_files');
    }
}
