<?php

use Illuminate\Database\Seeder;
use App\Model\MDII\MDIIApplication;
use App\Model\MDII\MDIIFolder;
use App\Model\MDII\MDIIFile;
use App\Model\MDII\MDIIStand;
use App\Model\MDII\MDIIPlot;
use App\Model\MDII\MDIITree;
use App\Model\MDII\MDIISpecie;
use App\Model\MDII\MDIIQuality;
use App\Model\ApplicationInstance;
use App\Model\Application;
use App\Model\Organization;

class MDIITestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();

        $folder=$this->getFolder();

        $species=$this->getSpecies();

        $qualies=array();

        for($i=0;$i<50;$i++) {
            array_push($qualies,MDIIQuality::create([
                'quality'=>$i,
                'quality_text'=>"Quality " . $i
            ]));
        }

        $name = "100.s.500.t.no.plots";
        $file = $folder->files()->where('name',$name)->first();
        if(!$file) {
            $file = MDIIFile::create([
                'm_d_i_i_application_id' => $folder->m_d_i_i_application_id,
                'm_d_i_i_folder_id' => $folder->id,
                'name' => $name,
                'user_id' => 1
            ]);
            
            $stand = MDIIStand::create([
                'm_d_i_i_file_id' => $file->id,
                'name' => $name . ".stand",
            ]);

            $plot = MDIIPlot::create([
                'm_d_i_i_stand_id' => $stand->id
            ]);

            for($i=0;$i<100;$i++) {
                MDIITree::create([
                    'm_d_i_i_plot_id' => $plot->id,
                    'm_d_i_i_specie_id' => $species[$faker->numberBetween(0,3)]->id,
                    'diameter' => $faker->numberBetween(25,700),
                    'height1' => $faker->numberBetween(25,450),
                    'height2' => $faker->numberBetween(25,450),
                    'height3' => $faker->numberBetween(25,450),
                    'is_log' => false,
                    'on_bark' => true,
                    'metric' => true
                ]);
            }

            for($i=0;$i<400;$i++) {
                MDIITree::create([
                    'm_d_i_i_plot_id' => $plot->id,
                    'm_d_i_i_specie_id' => $species[$faker->numberBetween(0,3)]->id,
                    'diameter' => $faker->numberBetween(25,700),
                    'is_log' => false,
                    'on_bark' => true,
                    'metric' => true
                ]);
            }
            
        } 
        else {
            echo "File exists";
        }

        $name = "Many species and Qualities";
        $file = $folder->files()->where('name',$name)->first();
        if(!$file) {
            $file = MDIIFile::create([
                'm_d_i_i_application_id' => $folder->m_d_i_i_application_id,
                'm_d_i_i_folder_id' => $folder->id,
                'name' => $name,
                'user_id' => 1
            ]);
            
            $stand = MDIIStand::create([
                'm_d_i_i_file_id' => $file->id,
                'name' => $name . ".stand",
            ]);

            $plot = MDIIPlot::create([
                'm_d_i_i_stand_id' => $stand->id
            ]);

            for($i=0;$i<500;$i++) {
                MDIITree::create([
                    'm_d_i_i_plot_id' => $plot->id,
                    'm_d_i_i_specie_id' => $species[$faker->numberBetween(0,count($species)-1)]->id,
                    'm_d_i_i_quality_id' => $qualies[$faker->numberBetween(0,count($qualies)-1)]->id,
                    'diameter' => $faker->numberBetween(25,700),
                    'height1' => $faker->numberBetween(25,450),
                    'height2' => $faker->numberBetween(25,450),
                    'height3' => $faker->numberBetween(25,450),
                    'is_log' => false,
                    'on_bark' => true,
                    'metric' => true
                ]);
            }

            for($i=0;$i<2000;$i++) {
                MDIITree::create([
                    'm_d_i_i_plot_id' => $plot->id,
                    'm_d_i_i_specie_id' => $species[$faker->numberBetween(0,count($species)-1)]->id,
                    'm_d_i_i_quality_id' => $qualies[$faker->numberBetween(0,count($qualies)-1)]->id,
                    'diameter' => $faker->numberBetween(25,700),
                    'is_log' => false,
                    'on_bark' => true,
                    'metric' => true
                ]);
            }
            
        } 
        else {
            echo "File exists";
        }

        $name = "Many species,Qualities and Plots";
        $file = $folder->files()->where('name',$name)->first();
        if(!$file) {
            $file = MDIIFile::create([
                'm_d_i_i_application_id' => $folder->m_d_i_i_application_id,
                'm_d_i_i_folder_id' => $folder->id,
                'name' => $name,
                'user_id' => 1
            ]);
            
            $stand = MDIIStand::create([
                'm_d_i_i_file_id' => $file->id,
                'name' => $name . ".stand",
            ]);

            $plots=array();
            for($i=0;$i<100;$i++) {
                array_push($plots,MDIIPlot::create([
                    'm_d_i_i_stand_id' => $stand->id
                ]));
            }

            $trees = array();
            for($i=0;$i<500;$i++) {
                array_push($trees,[
                    'm_d_i_i_plot_id' => $plots[$faker->numberBetween(0,count($plots)-1)]->id,
                    'm_d_i_i_specie_id' => $species[$faker->numberBetween(0,count($species)-1)]->id,
                    'm_d_i_i_quality_id' => $qualies[$faker->numberBetween(0,count($qualies)-1)]->id,
                    'diameter' => $faker->numberBetween(25,700),
                    'height1' => $faker->numberBetween(25,450),
                    'height2' => $faker->numberBetween(25,450),
                    'height3' => $faker->numberBetween(25,450),
                    'is_log' => false,
                    'on_bark' => true,
                    'metric' => true
                ]);
            }

            for($i=0;$i<5000;$i++) {
                array_push($trees,[
                    'm_d_i_i_plot_id' => $plots[$faker->numberBetween(0,count($plots)-1)]->id,
                    'm_d_i_i_specie_id' => $species[$faker->numberBetween(0,count($species)-1)]->id,
                    'm_d_i_i_quality_id' => $qualies[$faker->numberBetween(0,count($qualies)-1)]->id,
                    'diameter' => $faker->numberBetween(25,700),
                    'is_log' => false,
                    'on_bark' => true,
                    'metric' => true
                ]);
            }

            MDIITree::insert($trees);
            
        } 
        else {
            echo "File exists";
        }

        $name = "Many logs with Species and Qualities [Metric]";
        $file = $folder->files()->where('name',$name)->first();
        if(!$file) {
            $file = MDIIFile::create([
                'm_d_i_i_application_id' => $folder->m_d_i_i_application_id,
                'm_d_i_i_folder_id' => $folder->id,
                'name' => $name,
                'user_id' => 1
            ]);
            
            $stand = MDIIStand::create([
                'm_d_i_i_file_id' => $file->id,
                'name' => $name . ".stand",
            ]);

            $plot=MDIIPlot::create([
                    'm_d_i_i_stand_id' => $stand->id
                ]);

            $trees = array();

            for($i=0;$i<5000;$i++) {
                array_push($trees,[
                    'm_d_i_i_plot_id' => $plot->id,
                    'm_d_i_i_specie_id' => $species[$faker->numberBetween(0,count($species)-1)]->id,
                    'm_d_i_i_quality_id' => $qualies[$faker->numberBetween(0,count($qualies)-1)]->id,
                    'diameter' => $faker->numberBetween(25,700),
                    'height1' => $faker->numberBetween(25,450),
                    "item_id" => sprintf('%08d', $i+1),
                    "unique_id" => $i+1,
                    'is_log' => true,
                    'on_bark' => true,
                    'metric' => true
                ]);
            }

            MDIITree::insert($trees);
            
        } 
        else {
            echo "File exists";
        }

        $name = "Many logs with Species and Qualities [Imperial]";
        $file = $folder->files()->where('name',$name)->first();
        if(!$file) {
            $file = MDIIFile::create([
                'm_d_i_i_application_id' => $folder->m_d_i_i_application_id,
                'm_d_i_i_folder_id' => $folder->id,
                'name' => $name,
                'user_id' => 1
            ]);
            
            $stand = MDIIStand::create([
                'm_d_i_i_file_id' => $file->id,
                'name' => $name . ".stand",
            ]);

            $plot=MDIIPlot::create([
                    'm_d_i_i_stand_id' => $stand->id
                ]);

            $trees = array();

            for($i=0;$i<5000;$i++) {
                array_push($trees,[
                    'm_d_i_i_plot_id' => $plot->id,
                    'm_d_i_i_specie_id' => $species[$faker->numberBetween(0,count($species)-1)]->id,
                    'm_d_i_i_quality_id' => $qualies[$faker->numberBetween(0,count($qualies)-1)]->id,
                    'diameter' => $faker->numberBetween(25,700),
                    'height1' => $faker->numberBetween(25,450),
                    "item_id" => sprintf('%08d', $i+1),
                    "unique_id" => $i+1,
                    'is_log' => true,
                    'on_bark' => true,
                    'metric' => false
                ]);
            }

            MDIITree::insert($trees);
            
        } 
        else {
            echo "File exists";
        }
    }

    private function getFolder() {
        $organization = Organization::find(6);
        $application = Application::where('type','MDIICOM')->first();

        $applicationInstance = $organization->applicationInstances()->where('application_id',$application->id)->first();

        if($applicationInstance) {

            $mdii_application = $applicationInstance->mdiiApplication()->first();

            return $mdii_application->folders()->where('name',"TestData")->first();

        }
        else {
            echo "Instance not found";
        }
    }

    private function getSpecies() {
        $species = array();
        $species_text = [
            'Furu',
            'Björk',
            'Gran',
            'Alm',
            'Ask',
            'Asp',
            'Avenbok',
            'Balsaträ',
            'Bok',
            'Cederträ',
            'Douglasgran',
            'Ek',
            'En',
            'Hägg',
            'Klibbal',
            'Lind',
            'Lärkträ',
            'Lönn',
            'Hondurasmahogny',
            'Afrikansk mahogny',
            'Sitkagran',
            'Sötkörsbärsträ',
            'Teak',
            'Valnötsträ',
        ];

        $number=0;
        foreach($species_text as $s) {
            $number++;
            array_push($species,MDIISpecie::create([
                'specie_text' => $s,
                'specie_nr' => $number
            ]));
        }

        return $species;
    }
}
