<?php

use Illuminate\Database\Seeder;
use App\Model\Application;

class MDIIApplicatioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $mdiiapplication = new Application();
        $mdiiapplication->type="MDIICOM";
        $mdiiapplication->name="Digitech Cloud";
        $mdiiapplication->route="show_mdii_application";
        $mdiiapplication->public=false;
        $mdiiapplication->icon_class='digitech_cloud_icon';
        $mdiiapplication->icon="";
        $mdiiapplication->save();
    }
}
