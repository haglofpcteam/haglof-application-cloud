<?php

use Illuminate\Database\Seeder;
use App\Model\MDII\MDIIApplication;
use App\Model\ApplicationInstance;

class MDIIApplicationTestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $applicationInstance = ApplicationInstance::find(26);

        $mdiiapplication = $applicationInstance->mdiiApplication()->first();

        $rootfolder = $mdiiapplication->rootfolder()->first();

        //######## Rootfolder subfolders

        $folder1 = $rootfolder->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 1",
            'root' => false
        ]);

        $folder2 = $rootfolder->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 2",
            'root' => false
        ]);

        $folder3 = $rootfolder->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 3",
            'root' => false
        ]);

        $folder4 = $rootfolder->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 4",
            'root' => false
        ]);

        //######## Folder 1 subfolders

        $folder5 = $folder1->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 5",
            'root' => false
        ]);

        $folder6 = $folder1->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 6",
            'root' => false
        ]);

        $folder7 = $folder1->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 7",
            'root' => false
        ]);

        $folder8 = $folder1->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 8",
            'root' => false
        ]);

        //######## Folder 2 subfolders

        $folder9 = $folder2->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 9",
            'root' => false
        ]);

        $folder10 = $folder2->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 10",
            'root' => false
        ]);

        $folder11 = $folder2->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 11",
            'root' => false
        ]);

        //######## Folder 4 subfolders

        $folder12 = $folder4->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 12",
            'root' => false
        ]);

        $folder13 = $folder4->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 13",
            'root' => false
        ]);

        //######## Folder 10 subfolders

        $folder14 = $folder10->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 14",
            'root' => false
        ]);

        $folder15 = $folder10->folders()->create([
            'm_d_i_i_application_id' => $mdiiapplication->id,
            'name' => "Folder 15",
            'root' => false
        ]);





    }
}
