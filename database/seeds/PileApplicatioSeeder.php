<?php

use Illuminate\Database\Seeder;
use App\Model\Application;

class PileApplicatioSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pileapplication = new Application();
        $pileapplication->type="PILE";
        $pileapplication->name="Pits & Piles 3D";
        $pileapplication->route="show_pile_application";
        $pileapplication->public=true;
        $pileapplication->icon_class='pile_icon';
        $pileapplication->icon="";
        $pileapplication->save();
    }
}
