<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(PileApplicatioSeeder::class);
        //$this->call(MDIIApplicatioSeeder::class);
        //$this->call(UserTableSeeder::class);
        //$this->call(AdminNoticeTableSeeder::class);
        $this->call(LocalizationSeeder::class);
    }
}
