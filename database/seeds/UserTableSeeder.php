<?php

use Illuminate\Database\Seeder;
use App\Model\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => "Peter Höglund",
            'email' => "peter.hoglund@haglofsweden.com",
            'password' => bcrypt("hej"),
            'email_verified_at' => date("Y-m-d"),
            'role' => 100,
        ]);
        User::create([
            'name' => "Joakim Nygren",
            'email' => "joakim.nygren@haglofsweden.com",
            'password' => bcrypt("w1C9lQaicr"),
            'email_verified_at' => date("Y-m-d"),
            'role' => 2,
        ]);
        User::create([
            'name' => "Patrick Lidström",
            'email' => "patrick.lidstrom@haglofsweden.com",
            'password' => bcrypt("w1C9lQaicr"),
            'email_verified_at' => date("Y-m-d"),
            'role' => 2,
        ]);
        User::create([
            'name' => "Jonas Örnehag",
            'email' => "jonas.ornehag@haglofsweden.com",
            'password' => bcrypt("w1C9lQaicr"),
            'email_verified_at' => date("Y-m-d"),
            'role' => 2,
        ]);
        User::create([
            'name' => "Magnus Kling",
            'email' => "magnus.kling@haglofsweden.com",
            'password' => bcrypt("w1C9lQaicr"),
            'email_verified_at' => date("Y-m-d"),
            'role' => 2,
        ]);

    }
}
