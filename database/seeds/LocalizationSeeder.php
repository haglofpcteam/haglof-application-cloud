<?php

use Illuminate\Database\Seeder;
use App\Model\FileData;

class LocalizationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fileDatas = FileData::where('tag','localization')->delete();

        $path = __DIR__.'/Localizations';

        $files = scandir($path);

        foreach($files as $file) {

            $filepath = $path . '/' . $file;
            if(!is_dir($filepath)) {
                $data = file_get_contents($filepath);

                FileData::create([
                    'name' => $file,
                    'mime_type' => 'text/xml', 
                    'data' => $data, 
                    'application_instance_id' => null, 
                    'tag' => 'localization',
                    'user_id' => null,
                ]);
            }


        }


    }
}
