<?php

use Illuminate\Database\Seeder;
use App\Model\AdminNotice;

class AdminNoticeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Model\AdminNotice::class, 50)->create();

    }
}
