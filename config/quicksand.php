<?php

return [
    // Days before deleting soft deleted content
    'days' => 90,

    // Whether to log the number of soft deleted records per model
    'log' => false,

    // List of models to run Quicksand on
    'models' => [
        \App\Model\AdminNotice::class,
        \App\Model\Application::class,
        \App\Model\ApplicationInstance::class,
        \App\Model\BraintreePlan::class,
        \App\Model\FileData::class,
        \App\Model\Organization::class,
        \App\Model\Report::class,
        \App\Model\ReportTemplate::class,
        \App\Model\ReportTemplateParameter::class,
        \App\Model\SocialiteProvider::class,
        \App\Model\User::class,
        \App\Model\Pile\PileApplication::class,
        \App\Model\Pile\PileApplicationLocation::class,
        \App\Model\Pile\PileApplicationMeasurement::class,
        \App\Model\Pile\PileApplicationSite::class,
        \App\Model\Pile\PileApplicationTarget::class,
    ]
];
