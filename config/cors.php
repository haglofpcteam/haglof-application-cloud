<?php
return [
    'paths' => ['api/*', 'oauth/*'],
    'allowed_origins' => [env('APP_URL', '*')],
    'allowed_methods' => ['*'],
    'allowed_headers' => ['*'],
    'exposed_headers' => [],
    'max_age' => 0,
    'supports_credentials' => false,
];
