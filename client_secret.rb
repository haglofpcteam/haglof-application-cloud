require 'jwt'

key_file = 'AuthKey_T3XYGPKVXH.p8'
team_id = '7SDUDH272B'
client_id = 'auth.app.haglof.applications'
key_id = 'T3XYGPKVXH'

ecdsa_key = OpenSSL::PKey::EC.new IO.read key_file

headers = {
'kid' => key_id
}

claims = {
    'iss' => team_id,
    'iat' => Time.now.to_i,
    'exp' => Time.now.to_i + 86400*180,
    'aud' => 'https://appleid.apple.com',
    'sub' => client_id,
}

token = JWT.encode claims, ecdsa_key, 'ES256', headers

puts token
