<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <html>
        <head>
            <script src="/js/reports.js"></script>
            <style>
                body {
                    margin: 0px;
                    background-color: rgb(216, 216, 216);
                }

                #content {
                    position: relative;
                    height:287mm;
                    width:200mm;
                    padding:5mm;
                    background-color: white;
                }

                #header {
                    width: 100%;
                    padding: 0px;
                    margin: 0px;
                }

                #header img {
                    display: inline-block;
                    vertical-align: middle;
                    width: 20%;
                }

                #header h2 {
                    display: inline-block;
                    text-align: right;
                    vertical-align: middle;
                    width: 80%;
                }

                #data_table {
                    width: 100%;
                }

                .label {
                    font-weight:bold;
                }

                #footer {
                    position: absolute;
                    bottom: 0;
                    padding-bottom: 5mm;
                    width: 200mm;
                }

                #footer-left {
                    display: inline-block;
                    text-align: left;
                    vertical-align: top;
                    width: 66mm;
                }

                #footer-middle {
                    display: inline-block;
                    text-align: center;
                    vertical-align: top;
                    width: 65mm;
                }

                #footer-right {
                    display: inline-block;
                    text-align: right;
                    vertical-align: top;
                    width: 66mm;
                }
            </style>
        </head>
        <body>
            <div id="content">
                <div id="header">
                    <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJUAAABBCAYAAADYDeZ2AAAgPHpUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZtpkly3coX/YxVeAqbEsByMEd6Bl+/voJoSSZF06IVJid2sqr4XN4czJEB3/ue/r/svftXms8tWW+mleH7lnnscfNP859fna/D5/fl+dft6L/z4ustf3/jIS4mv6fPXcr4+P3jd/v6Bmr9enz++7ur6uk77utC3C39dMOnOkW/21yK/LpTi5/Xw9XfX4+ebUb57nK//74pdL9n8vPXz33MlGNu4XoounhSS//z5uVNiFamnwVfjz5BK1Cv+fZ/4M6X8z/i5v0L3iwCW++v4+fX1ifR3OD4X+vZY5ac4fb0e7Nfxe1H6fkUhfn0k/v3Gu04KXyH+Rfzubveez9ONXBzhKl8P9e0R33d8kJDm9H6s8Lvyv/F9fb87v5sffpG1zaNO5yd/6SESzRty2GGEG877usJiiTmeWPka44rpvdZSjT2ul5Ss3+HG6sjPTo1cLTJHPlL8ay3h3bfrftyscecd+GQMXCzwEz/8dj+/8J/+/uFC96rMQ1AwSX34JDiqZFmGMqc/+RQJCfcrpvbiG9zni//5lxKbyKC9MDcecPj5ucS08HdtpZfn5M3x0ew//RLq/roAIeLexmJCIgO+hGShBF9jrCEQx0Z+BiuPKcdJBoI5i5tVxpzohBpb1L35mRreZ6PFz8vAC4mwVFIlNTQQycrZcqHfGiU0nCXLZlasWrNuo6SSi5VSahFOjZpqrlZLrbXVXkdLLTdrpdXWWm+jx56AMXO99Npb730Mbjry4FqDzw9emHGmmafNMutss8+xKJ+Vl62y6mqrr7HjThsIcLvsutvue5xwKKWTj51y6mmnn3GptZtuvnbLrbfdfsdfWfvK6o9Z+zlzf85a+MpafInS5+rfWePlWr9dIghOTDkjYzEHMl6VAQo6Kme+hZyjMqec+R6TS8kiqzQlZwdljAzmE6Ld8Ffu/s7cb/PmiO6/zVv8VeacUvf/kTmn1H2XuX/m7RdZ2+PBbXoJUhcSUxAy0X58aMTGf9DJt6+tnHHIVZylshIWHEMDNDsPWR6nZK3i2LyhVbdgk3XzyduPPcrKl9SlDeLkdM+YiaTsM2cv0+iSvq0TJciEKrAXdQK2y3Wr9rg3ix5x17Fv3cvG7vOUXWKH1CO5W4T31tXPtXPr8Fy07BPB0biv5Zn9cTH0lYpPDRBtJY7KJVuymxY5qiuk2k8EHKelzZJqnH73Ve4MFFWdayoM0K9LhNNzozJHuc3sJN9utkn4Xw3HMWwtAQIFUdKx1fLxsaxVQ4+E5AYjKdGNlqO9+510eyBZYcwMfN/N00WidRR8um5dwvm+97/46n73xr/9+nWhdXZNRs1tGKaQ7Dsp0BitbX92qT63veqhtMCME5WKEMnpIQmh0HbFxUsL0B1RJIbGsbw6MZ+zCd33ptnG2ZNKrW2VOG8fSXfbc9Kji2hyo1ALLVLLGtJhsFykE0sdpuKP+ZLm3aE/fmg0m8BwMXBl13r3BTPC7a1SA53cOD5rg08Ekk5j7X2oLKpujksHp3bWrCyhZj3MQtDsBMvWfBs40ejhLnVXk2O91tqhfsLayMm6cr9+8j8vwQU7lxVrR1Tx+DwRsNI3z8ZDkPxzb20HHbqdF3hQPPAxPZ1OaBQZKMYjKabRtxnUZ7UbBcD94+x8urWa6eiUdj+n5l3JGhVHndkcm/W0OG2BaZ7iLOsQiU7Vw3Kpd3+MKyJeZpWuoIT9uH3O5es4bq/b8lwld3Ail9IOYTKocy9PYcwEXa4x/KEyWBEFzwOWeFqdEbwq6xa/qBx3C83D889mIdPQayCA9S09lvcNva5U/dox51v26OVkdSSgXM1XMsrFQYrhKs/lTVxMLHmgjhDLaRCJRbxv2zWvGypPELhFRcfeYqDDBXvM1jkLHdvQR3cKVtFYtY0BhkYrxPKUEcLmrzWueSw2WAOVdXw/NWYqLdasn6N4d0575OpIx7mwSudhPM9cl5F6IG2ThHG4P78HxDZOyda6nytz1YCaWHftHSfVymWdcgcyDrHTKoUuh90e3BJp/w/8BY64FZy8d0K/JyLM87F8sgZtrFPAsvhZfkrr5DaWIVNOHIeK3yyQSPPfCvVMsCUdio4EDBa3CijrQtuFOEKT3KeAmokeLh4004/XSv6yupougtq4xGqh0ZJhZkCVAqI6OsDgiHw7DSKAdEBtwLhNWju3co078RPGnUtjJWY0GT7iLEJRoGPiDsKXBgESoxRKB3whNVVRPshweZ6KVOZaFYhK8ECYJ/DIexoLwHrZJwQBhIeEd+ZCMCXYRdZ905v4wKnipFHAOcpirjSB7FFBubwK1Mk391zIV7Q8+uqAWFewe1ADQ3ukgXz7pTWtO1VffMczJX1tdCMcANWBg8AitJF7mqAKMDPcA90FWlDL5wZhxwD6hCjnoJEXncfNoLizAae9aVtUw4Qn+rKawRJfyjyOdmmslKWAIlU38QRiU9DD9gQ1QM6LqKkHTZvDGdRiLjXY5klHOKpbKnK7dYAywlrgJPJkSLqpHMFOmBNWglQAPyFmYfcidQmtA2Qp2VSAGhU1Amar+1Eii+QVOD5KPFujsFZPECYeJEW0Cn2N2liEPl/UJt8KuZYnCpfS3e4CmsBEaqXsDu7mSv/QQJkWRntdagMk74GLDsr50EHzq3Um6xlpESBu5Vo1YYYyjekVLaAwDU9F8NYM9DtENf2B5XZDZAgGIbbOj+0IoCRa+tgwNwktintJ9U6ZMZ9nrWnOhPTY8LaemoWhMAf9gaPA57VEWdLVBabnIQJKxQXBrg00E7WLvFnDSDQ36brGVGBRsvnMdg4FgvZEs5mEg4VZYqS6CFpkRchbK/TZ4NfTZjevHSzfPHcGo009QkapPBQV/jWBCBJ9h99IQiwqtwXY1qb9lgjjnACWJMDxAikRiqSex6HQF5QncfD7d4frHREWJWhoIyUY4Ql0R0MobnBuQB8DW0tJ4gAILSUeKqpwpkFFg/hw2zXckZqhLDTcQXn2DltiujK5gxwL0iwLNBuYJoTYJwvPSDJrAbjp6UTK1p4IrdLF/SAbKnSOTvGD+DCOIYMvWEqLidXLgMpg0tv7q4qrp0D8X4K/2nV6UgSn3UxdIzBRaRfCg+sJB1asoe/AIVQuPiI0OCodCg9Qu56LcAksBurE1ZMfWGAs6dh527ATgJC5NmU5N2WBzJ9wFAQrbTkAtrTk/VuYTRoWNxSn24OEhy5Ug7ipdepoXy9bGPEqD96gW7B4Sa/fR+pc806uOqRtF2gH06KiCcKO/AhXiPUko4cR9KCoabawuemEic1U/1wa+VeQFRstD0yiUkrrqTj00q6h+mQ0KrdYTWMIQkWx90YHUGj4BigHawK6goEem3QRgUYNEKSFKuLRQC3kTwVVeb3UXWgXCfsEs9IFMBLPy6OBqhRWLcYVEAC+tnQOQBrQ2Mi36eqAOCRVQNmljFNtAb5J6D6ytK1KA5BLfxNLagAfXScGoHiFy8BDpRDcMzwHIwi9joHhyxHDB70SduAC6pu4GBjTnwHLCJroxMntTG3HRVFIxNjNb9FFG3TkDwIAFWwNk5EGKwP5qbxmCxHbxGUE2DQa6jVDnShRChlYcOjkAgeJdug9P4EzYmIXO8OLoNqWzplgOl72cDEIFswSfNOEswVkC9pwuYSbvVjd0PMMdMLAlibAjoyII7g1qx9pgtIdvYe+7Xjlfgy7g/cFoskgLsZt6SMUldbUw2uY8gEI/XlnFHkkZebCBkS40w9U9bw3o1lOFBVArQ6qx4VdqXl+EKG/E2IV1uhRQIi1lhFGiiFw6Z+HcwnA405ZakUozMuNFomrEdF9UXwQ4sBcNqIlyrbRD14y4MkKdkQGHFVKLxdw+637zJAxG71kR7oJEJ9X2FRZIH8NEW8KZQM/RAM0CrndNUR/HdezCsUHl3kAHLFB12NFMb8obCgbgEYPQxTYVdRCBs3AvBTLwZkXfwE/xQyXxW1paDrFIrhHGwBR5ow6Gwg7QwxMQb4IlWKWo/3APyKJ+qZnh0AU8xYNBQnL0ZlicWQetOdIxe2GeqApYU2aDv02ZP9o3IuJ3nPBLjzJRzwAN3wZAaxaUrhBzgyZ6WDBQ/AScI1LvwVo4jE9BgIMAf24kkd907YnSjsZzZ3ULQRk0CSSOwev4JZHVk7JZ5EDqp/18nyABw+caJmLfukj2EDQJORhAztoKIpcVIAM5P82EwQZ8TxINpp4PtcpBezbTgIDMkvBcEX6CqdCVevPkiEaxKf6XoMwsH46kSLQkjVUCCgDwBrniLabORfNNjGk6Wn8moA4BAuWmAdDsMMprUhuVXX/IBsEf4BL8kRknV9AcygzwlcLvysdDxqkRP6NSg0H1Zc1Y6ERoYtKpcP9Nj1kAMfzBZXGQxHim1pVtJFdfF9xgbSUKpxaLtIDV64QKzA1PVzy/TdiGluWgUOzJBlHGSVAYcIcHXlSCtJoroW+wmpE2COD62hUAjUayepoiuhAaWIF/PSD0Z7rdXMrhCNanq1ePCuZ7NR2BJjyFbN40lfRyRPH76+iFp3UwzIUrSEiqkTEaYiaGL3040dDbq9BrEwlujeHF0ztBJzXuIFkxOSo3EDAdzmh08eYBXJ82kGmFx7riKdRZBO7B4mREY0P4ZKhnQhNXNEQcbQjfUTqg0VPxdHK/aQJUVyUqEbI4ZlA+Hnhu6jE4Cd1L2qAjAhzL5HUUvCuzgD4HGQOMv8YgBzTkss+oPaQhElVTq1kPv5ZCkxPnHigirPiATUqwmUHv+UbqL9Z6WfKOwB15DqTRXjoSicM0In+lDywhOvhsTVc6pQOXmTC+O5Aj3RXDxioh9u+I5CAXiJ/ZDLUN1lkSin529HeQDsNBar2gsYrmlLIZeMBNL0DmSPKDgdDGYEjPEKPcu6qXdqoak5HynSzCW8bckN+Cu0tEyV9BPxvj3vCysBtsAY4W7CGAQew7nn8Ulg2yX7SGwmgmGJU2sFKmpowIf3oWSq3UFwbMU9OgoCdEuZ52+rUZqAw6LgNmmBGczZqYs8KQTUfAos36tbByflqsCBmJWGInQt2ieJwPxFrQQPqEW6XwkOW6C8aWW5qBADzUOrU2MfQ+dqETLRuWKAU1TKipcl9iNBzIGMbWQU4DJvReWCpfpOIxW0TB8SmazjR1TSKBKBMUyt+nKYeyDCsxSpvv+gK6gv4JWdAxVFF9sQQFudWnim7jsILnXrGpn6MCl3nK8ClyxdaNBk6DImaDXmj6ffWIE7z0RqDzzn4uxcuG7pMV+mB2LN2feKgR6giUvkyRn0AYXdhPq80Lk0S5jaNeJF7glZQBgsBuMhZFK+Nx1utcQ3IIyFbcXc8SMxPLRYUIhfC+WDp+BueaFbJJQg042krNECHHAQoSlp7OGBOhpmrYAK+Q7xhcAOym+KifPBKCFYwCFaM8eSG/UQXO69hF/oRZ4jG3D5SRJhmqADjuS6CEuGJscO8kULPNQEWwJDy3PXQt1y9WukObrQKrs/eJlZWJVe15ZAh8Vk1s5IhQxaZIRphOupRzrDK0kDxu29UXAH8B05GoI7H61hdnnSjnCcdGdTxjcr1UiM+e1Z1NE4vGhw88ckySNBZJaBGaGDS1uiXKsLqmxuwQirvLmnXmyVgSAO+hFLCzgOnJTfN3DBOsDAg1FxrqrjI69Q1ekdiGvStq/UJr1KtIAYRQuwVLQhqoVUg5iG/TeZ4VhWJG5AwFgJExRXSRzvoXRSipkRzA2i0IfihjRKE0R2GYkMv0BlUPTJy9YpYy24MNDSMkmsJcFk+MCfMBP5Bmbihs6ag0tZS2cGVNB1GR2OrJV5JsGZHCDhlZ0tU0tSEA8w2QINfTeqr3MVSASi4L/SNQ6TvodoFVOE6EdhkeQJC0bGMSk1IdkqDJiytpj9bss/ed5p8TqqSpkoVyw7GaTKdApYY1sZR0J7boR9C4/E1SIcj6SOoAKkdjzYmOuiM7pAZjUkiofMErWv+4jfZ6UhP1gTbO1QamDGDRsFoAP6WwXN+nM5KuEoaf6FWyqR+R19Le1e2sFjFqEyxt6ZsMzjEkca6WGhMNG2NcyDK2ZBek0dAQQdyqv33ML2gNErXgFx4B4qKmkd91lMdACopf4LGJpEajTzE6h2cvhKks7AcGoyrStcO1aVnYYUVYpO7RsW54tc2XATHs2qUg5eFtpzwdpQd/DdnoTQNBUrm4kMnZT9oErGG9s9ZfKEil/s9jFXtAhbUG6uAbYsqEdoD2QAbNCYycpu2memHWLR5UCJCEvGL4ETA4L14Ugp2drnIK5JdCKB6weFGPOlUsAHuJqDyC6h7aMnB4hd5UhMKD13MTegnHCuNGaVEGqISXdaIN740TDST6D9oQJLBFhobHoYg92yQwCJj2saCHo9WurTFhe/kOehptA5u+iITQEgMg+YH8aJW4S2YUJ1VncgAwVCIVEX4Iag6wnXA9HARRePf7DanjtmH9aENymVi0wkUK1UrAI1WHQ4Mf7jRmCL/dMBauegMgESEdNI2gbbtpb8C8Z59IstQeKi0rC2zwAOCqA7j/pkFhmQ/D9Tx/he8OmHnr50SxBt2YMeDxKT6ILYGrCNJO+lPmicCFEIjry1YSif1g1Kcim9GUhU0CZ45gLALlUKhkffA+osEETbMb/yaOkDKhKyjlY/mXJNOB4T2Qh1IMWhboiXt98qmA0M0euoq4u17WevE5TANC+eDsdwaJanTztuR2BpZLI2uI+XFdascDYiJ7iOtA2Tooi9cDcVRXZRbpZNAkTjw2PuZ46nt64wPpkioqfzZImw82AXF8CjAE/LxTpPThmhFkNgqPl7gHuoRL5u0+4FJp7Eb7nvlSj+NpbzKbLCqGO3tiOJqbfAnXYHOvmPuXqXa0HVzwDD35qaZu3qiexQxRaGG2NzQQ2vapDyRuof+jJViTBJ1JL4umFiNgW8ZBWUcNKNo9AgZ+0zoynOVJcQQRasIToLVRLWIWNR4mU4eBJFxi3Qk5vgEgCBecT89nJE/CXFNmFlglwDDUyLscUhn+GeItvY6u4MqqHJE84jUZ4CA0eEUMmalp+U/O8MxHLl4WItckrJzwNJOJ2hvXYw8i8bQhBlWOwBTHBqloZR2QubhmbSRHbUbALloHgKeLBrMwibc+CCddaG/l0YaSRaQRAxMuKga5CP+ec07kKY05a4ZZhQeY9/x+BRWptkAEflo/FiEraOLYO+e/UlwL8sH6RAPSqJhp8EpmwfRasSmdvoCLsoTh3NMWlZbpCINgg35Ns2eBnQGNAN5k/QunHHjZ8tWiaKCuQs6mdrK2tDXJA3FDCcLWdFvlhx9PNEK2jbCQ69VOumHxBKrbDJkSzNOyC1iHY8B4cDigJN77xq+bm2yUQ8O7QHSDm33lPC13TMlBnhdm+P3jUk1CYZzwHlkzETtkVzgsUsomdxycWsfMrSUb+SWD/tSwZCdeFP7v0myHGpGPfBY1OwkVvBvVq1gCUFukmbdaXBeb9JIbUEaixhzBVpo1aItyfZGxmg1vtfkalYwPooXqTad5QNDEC7N4YCjxi4i+BaHtUZ4yhgq98PShSKoCij4xqpxzLamjXfRZG6dEqTDJg5SJkozCB74EiTML4pq9H3FoKxpIoQqnajNOHgQ/0ybd/OAJ7YQ9z1lFRFauLg95IQhWm5aEGUdauqUGgu+cNTSuQ36pAVevp7w4Rgp6iTBoYlI0wTVTU1WYdqxxfoT3UXMZ8XqVxyDV0ogCvr+jbV5CjiqvTFEk1hFLKCA6Amn2RRS/2jyhPlRzeOTjZRA2pChNJV2gkCKN4iFSxClALGGnn5/hIVPy71jTiS8GiGGOyVikb3Tk51QpNIG3+DbK2IUu30g9/bSadQjCmm+LmxOG53ac6aYCEMFlpLUBsaFYMgtetqh6oCr7SPrgISg25ZNBahH7cHALQGdjTuXUdMmOKClvdYTtROiTdfRWoeNkK9I6OUt0pbvuEKl/3WyJ6EUtclrbuMjGiLgWNIusAgPCKygHdaogQQEAcGqlKM7hZ8Ya0o3a06Jgwh43TV7cHDXyFc6SNO/tqXmtWekBhl1FbR/JcfQJVTEX1ET6yCS0BxhjwFno8KOwf2gDQ+LNsZA6zDrI7WLTn2txWNmicqkSRsB2YqiJtqGSC+p4IPpQgSPw61RT1fnOqBP6poIpsXFYFF5d00SWTPidiGdtI0sJsJzL5G+7EBgyaU6P3GoOukC7esATauIQv7vOm3QYa56dBIOkfO14fRm4u94ooFMmLSp42HZaVsEDz62pgKaHENKgwfa3BWAsXhkTC+5Yamkns4Y1TddRVtFVO6coguHIqEXDv1EiLVtLgMDcJ2EBW4eVY8dhPt02AlLF3S2SivyIDbgeDF4lOiF++EGUDD86s2v914NloJbw8SjEnFHaMcOdwWCdlCK8Iw7BS56HpAmMp2EuuMDuEfHEdRAprtMnVKpn+h0bSSU9v7W2pBLuk4XyCApH8bF8JH7Drnx0dI+l//tuwMPh0iAgmkMN+Y7nXRv0BGJt9OPRtRMFBDREqL2Za52M+/9a9HqfJb9tWh+JjvKaItCKG8pYbTieveFHq/mJXz2aXXcVIUB6firDTlcD32lzTQkS06pO82NZgJQQCXoHe9kOnPTK/EN93OGjp8NeaS1geOj6VFnbVPH2zRSrZpow2tj0x+aiKFFyUiSQuQD0CaEmCSyz9CRPo0xonD2rU5kQR+iAXXMDe50UzZmkiJPRgNkoRMN7ZNHhMi+modpEo1FyQPZLqMwNQZEghAiHRQA9TrAVnQwPWlyrrOfPGEYqCLNbE8sNVAz2EWApGgcCgEQfanmt/ERZDuAkhPh/sNVCdvBAFtDWYKOXudsevv4EPhO2Zgj/qkc3P9RLYjLxQrb7u804jVNG9d5J0MQNLJhBTt5pWpBwEFqrqwAYgkwjxJJhB/NTLR8ohnoEM3cELtew+dgu31q7lvZuJ/qJv+pprRJiRTWUQE9Msqf3m46c7SHS9pPiOilMdfFWCA3C+CGeMNhTZYkXY6Wwp4jQLKmatr4vltnNBIZhPhWwItQeJRLHBdbj8AlFzp98ce4gY7f/B3ljRzLZ0Y8LcyofwcBg2NhGviKvMZxjARsgZGlDPMGiei8h0WkU+hze82NtA0Ai0GWn+5fOix2V8IvLsm5UJA+cP8oeXhVYBjaS6V5TGd0b6e7FthYkzaOub8OLzkEh9gxFamaN+LXGT0kONy4so7WWih7+LI3Arpq5Je2Nou0sda2TkwOWgK7vgFQvyCFqMOi6G0UBW/yHF6Dx4J87XBqCF7nyS/uYXdgYwXNmRDDFDIWOevMKOIDGd8uZI7evU3HwXR+4oHvGtwKW4JV0RYnsa18VLPd4mEBDZVkNpf7tjekwxMFH5eQ4/ygDqvp0CMERZrQZKx5pILW0A4z+QcQdDI0jaXBRCJrU3aAgtepwlwDNnIpzPAH7ETDF833prYVCjanyjcS8PCcFIqwH+6ncyMUH+ry6IBB0dYvXaV9TWyZJwINM4jE14mqvK7O3enkkHW0MXzYmtH7uGWi5GTkqpKAhipapube2jYsnjxqWNNn0KiwAkaUHqpUANumTm+n949IMEKRFoH7qnbgeyO9U8NXMFZnkSKaY8DGVeADwqEhEb35TYdR8HIbmhagXaVf8WuaCZEHBHyFYjUYrQeOwQcNXLRGwbhViXOdjXm6CAqmIEkDNHOOrMKo7wBKQYiRbQz4/BCSr/mT/BrFX7Z0VBvhuLXN2IWlOoDoK1egrPC12zt8Bbe5GkZm7L0ij15Erp+s7UYkUoMHdTBF5xOgCTwzohYTF6pdnReTaBbUsjCvbT9Z3UT2CuoihR11hFmj8sCi9gDNgYSZFP7SdE5WihWEFrKg948j5Tp/tWib3EelM+ChuNbCa2t6fHTmxHQ0dVQqg75S4Sa8jS1N3poqh6s5ylwHYiERBBc/AxRXJVAj23L0kBqZNJw/TyhDHasOfFzA8ZjXBmnSv/zQ2AcPN3p7/0ZL0zaK41xTYIqUeQVeRlCx11siuC5/DtjsogNcctYHh7Cr03HUcj2xz6iJRpH1kYAaON3oxhuT3MmEh7sED60vb5kkBCwDVZ/feJGAPtsSsQA+6BKzzjVqAL0Htba1uaOx6GccMpMkl0YZ2mCyNQBR2gYkJf1aGeWA6PCTn+bCU4f8aObFGnGryAUNgdJe1HXQkATy7jpKxlW0U6WyPS5b6ka9GtDFZzQXBaFNypuaBQyX+QP6S1p4ApyOZkpA3UROaz67tT85t2vvwC924GizU7ZQBp3mbkY34pai19ElQncR/NgQ6GYORL8O50CXxEXjtotg585PSusAykdMi/w+R0yCHGHF2ANidIVpnw2H80qakklFRUjFY7NYkAxIwz8GOUUz/aMv9A2SRnvT76y+1wlHJK/0D72m/dClI4xiPx6ShV/HXc/87HXq31XodOHnnadf31t6Y+Lm+HOD/gvYvilpu3SCZZdS3jm97vd4MrScNq5Y3Dv5cz+i+Kc39LIGrHpz9+61k8t3A33q8GlnhO8X+eMayRq6UTZVB1hBb8qW3JNTvrZsIyoxAFtMEQpud7/ZI2b/zT0DN6F6xCDYKTMZlE3gdNrnHVJN5aELWYbGdUzFqYql347OuB7tP7fGSqX/irZcxsSFVw1PdaYFIy+/nmfUITauTzKKZToHYGunUsxFp3Qilw0lCqkHclNqx3QQHMIqnwj8OhVfIuLXsfmkSf8qgofHGa/z5ua0rhSg/+i1agIJmvk6exJu43mfWPt6Rxbh6i29YVdnmni7FAxDB3JukYQCrIZkZEjjeje1AaFxjxQlgPb1+k8vX/17o5H+Wl36voRembh/1En4fWX9poReBbk/BfDP8Wt/FTlX3S680clX/H4do++D9/XyJ6rfB899Hz2e6Pv4/avwud/G7192oPtD/P7Qgf9ECfcDTPwYv98W369qz/0nxfer4Ln/pPh+VXvu3xcfS8cpuv8FrMi3mmPHGucAAAGEaUNDUElDQyBQUk9GSUxFAAB4nH2RPUjDQBzFX1tLS6ko2EHEIUN1siAq0lGqWAQLpa3QqoPJpV/QpCFJcXEUXAsOfixWHVycdXVwFQTBDxAXVydFFynxf0mhRYwHx/14d+9x9w7wtmpMMfomAUU19UwyIeQLq0LgFX4MIogQ4iIztFR2MQfX8XUPD1/vYjzL/dyfo18uGgzwCMRzTNNN4g3i2U1T47xPHGEVUSY+J57Q6YLEj1yXHH7jXLbZyzMjei4zTxwhFso9LPUwq+gK8QxxVFZUyvfmHZY5b3FWag3WuSd/YbiormS5TnMUSSwhhTQESGigihpMxGhVSTGQof2Ei3/E9qfJJZGrCkaOBdShQLT94H/wu1ujND3lJIUTgP/Fsj7GgMAu0G5a1vexZbVPAN8zcKV2/fUWEP8kvdnVokfAwDZwcd3VpD3gcgcYftJEXbQlH01vqQS8n9E3FYChWyC05vTW2cfpA5CjrpZvgINDYLxM2esu7w729vbvmU5/P3TIcqguGE2jAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH4wUQCA0mrU2xawAAD/ZJREFUeNrtnXuUFcW1xn/VM7wGEAOIiiJoEI0RUaNJJLmaXL1yxUKJ4is+QHRwOgmiYvARL3gjiiI+8FUDUXDFZAnyFJp4Ua+aEC8mAZWAoIKgguAgCMLAMI/Tdf+oaqenp8/MHM45MJj+1pq1TlV3Vz/qq7137b2rRmit2R8QUg0H/kd77moSfKPg7JebStUFGAs8XFlZnfRCQqrsoeFe4CBAtr746fOSbvhmQexr9SekOgVYCghb9aEDvVOeW5V0RyKpMsaPB08BeCxEKIBePtyYdEVCqr3Cm1srLwN+bIuVwHL7e7SQ6rCkOxJSZWqctwEeCFU9Cgw1JhbtgfuT7khIlalxfivQ3RY/FzBWe+4SYKqtGyyk+kHSJQmpmmqcHwHcFqq63ffccoxxdQeww9Y/cdAFpSLploRUTcEDQFv7+x+dCsTvgwO+524G/tsWT9vp62uTbklI1ZiUOgO4MlQ1fMuLJXX8GAXwOPC+LY5zpDoo6ZqEVLHoeGGpsC6EAM9pz/1b9Lwaz60GbrLFLhruTromIVUstqX0YOA0W9wF3J7u3PIXhi4E5gXSTEj1naR7ElJFXQjtgHGhqvu0525Md37bolYAtwB7gELrckiQkKqOC+FOIHBornPg4Uav8dyPgEds8Vwh1cCkixJSBcb5McDNoapfpzx3T5OuhfuAQKI9VCBV66SbElIBPAgEZHh9WJ9Os5p6ofVfjbLFY3wYmXTTgYWcZykIqX4KvBZwBDhZe+7yTNroNWgyq/ek3gT6WgP/eO25G3Jo73XWtd798AhbkfLcyph3Otl4PurgC+25nzYrCRH/XrHPefTFk6j0ze9vtXB474Vieg6azO6U4UNRgWDNzGEN3aujhnOBw4GWQDmwG/ikMJcv1WqAcoCJoapJmRIK4MOZwxBS/QqTItMWGA/8PIf23gXAM/WkJBwLrIm55DXgW5E6BfyiOZFKw4XA05HqJ4FfRc/9uNKfizmfTdX+cOCJj/akFFACQHX8dXaQDQR+j4nZRrEmp+qvSjMM6G2L2wX8115/IM99J/SBrhBS/VuiWPY/CqRqBTybhlAAf8+ZpHKkOhiTIhxgtO+5W7NSpXCnhkuBDsDjrQaoUyvnu37StTnBG9TGXINoxt+oDaf9PdbmhT62P+qoWGCYVYFbckYq6wXvZIsrCwQq2zZ9z90ipBqD8Vn1qdLcYNVOs0ahVIUp6GU7yAFSQJWAdR+XXrLzqCM7N3z9AFWY0nwXaAWUFwlW7Zrv6reWrub9NWV1zj3zh8dyTPdD90YTPPrZpi/pVjy9s4buNkPknwLmpOaXfCVE3bj+ho1beXXRKoBDYppbMPXq3nMdR3DNJT8iJ6QSUh0P/DJUdVPNfLcmJ+IWnkyZUXACMNaRaprvuduaG5EeVAsZtWDtJcAI4Iy4mbUGupfMWAs8VSh4tHq+m4qqFt8MTjcsDXZryoRUvzHCm0l1Gn1u+fe15y7NUKsUaZNtOxRjR9Z5RmdA6XvAMwKUb11B3Ya9cDzwXpomB1/73PJrgIprLvlRu6xJterDDWAclkFb87TnvpKrzqrx3Boh1QjgFaCjNir2l3nixhwhVWVMfaMB7lEL1t4D3NWEexwDTKjRHGdVBgDtLygVPrwI9Iu55lBrXy7P1g0kpPo28FKUTBF8F3hYw3VCqvO0565vxAUl7F9BTvxUJ9wyvz/wn7ZYSR78StpzXwXm2GKJkKp3nkh1IvC9mL+CRjqqKyYvLIwKTDbrUCt5Po4cv15I1SMolPv68jSE2glssb+zem9HqvZ2cEYJVQa8BWyPIdeCAqlaZnSfLGcCLagbfpmoPXdNnjp8JCYu6ACPn33d1Obj7DP+mUuBGzDqaxJwrfbcO7TnTu1SKEqBF2JG909C5Utimn7egc5HtXQOyYV0tirv6Ej1PAd6aM89Q5hjb0eO9/bhSgfWAScTv0hllj32A0Iqa+8MaePHOM4WPxd1Z3+5llbrhFQTrIo567WyPZfGdNR+ge+524HZAJff9Eemr9nRFegtpLrNSrozrQqLIiwBjo45fn9o6dpTQqqSLKXVgJi6O4MQmu+524VUY4N3CaF/ynOnAsuEVN1i2tiqPXfZ1xONLERpZ2BM+OF8z92ZZ4lwv4bBQDdggiOV53vu7hze4thDCsVH0covavRW6js/oyrwOEwevqQ2kB61gUUmWkPApkjV2ixJdVS9mab42p0Q4JOY67pnNPvNQpTeG5qhLOngiGf3gUTYJaQaBTwPdNMmP2t0Lu+xeW6JjiFMY8bvScBioChyaBHwKvAXS4bHGrp1zDc+3vqAgoTHbG3JLzFhldqJkKaztakCHNyUZ8u5TSWk6gNcH6oavn1eyT5Z6nxSu8JptrMAbhVSHd0MNOCIGEIVa889U3vub7XnvoHxWzWEuBnzE0Kqnwip+mxL6d/ZmWM2eDOmbkjwY+Pn2wCuijlnUV4lVb/iZ8HE9wJC/kF77lv7qveWTSsOdox5G2gDTAAu3s+k6hJTV1Rdk6LNwMmFKaOyr29EtU/SxkYN2ywnAa/n8DkfAq623y3AOOtnXGJn8TIqpQRMzqukenlTxSDgLFtsMEU4j0b7Mmpf9CIh1dn7mVQLY+omthw4eXvKfKOnMd7xtIPa99yvgP7A+jT3WAn8M8vv9qGVRJWRWegQ4IkYQu0ALsrU2ZwRqRyTMDc+zHLtuZ/tp2n8XUDwso8VSlW4vxjV3hFPYkJJ0bhkBzvDW2yn29GJzNmRTl8hjK/sVmABJhY3Cyh2zCyyLNs+1J472z5LQw7qlJ1Zn6I9981Mv0dGHaFNDnlgw6wTTUgRzqPRvlVINRqzvOuElPHjTGwiIefp2gUZ4d5JJyX+nZh8qq+Hs7EnbxZSPQT8FBMf09YwfuvhC3t+cHPxfwQ73oQNYb1+41a6de2EkOosTLB2uyXTQgdWh/O7hAnaN/ReL2p4N91zRiT9udZp29e6O4owAeH1AhanSwYQsCjm222pc05Tk/TsA3wAtLNVg7TnzmI/osUAVVCjeRczur8ScKzvuV9wAMKGoqKLPaYJKO7ZpqB8dUWqPzA/Ipl2Cjgsx26VrJGJ6BwXItQb5xzWZtb+fngbkA08vB20yW8/ULHQqp0wLtewY3VFqsqqw2h/jW9uhGoyqYRU37ezBqzdMOKVp4c0ixfQnvs6MMMWrxNSnXogMkp77vt2hlhRX+PQIsbmGd/BEfc2S6nbmPo7fGApn9fot6xxB1CqPddtZqqjO7DKTpUXd2vp9P109g0HpLhypDrEpgWfChwJdLSDvxzj7V4OzAllDhx4pBJSXY3JRwaTItwz24zOPBHrbmrDRtdoz32OBM1P/TlStaXuZmRjmiOhrI54gNq41QN2hXSC5kYqbXKEutriygJ4qrm+iO+5FcCvA62ts1h0kSBP6s8mkK2k1qXfT3vuy835ZfoVP8vLmypex+QpVQEn5mvzfxv/vNDaPRqTQTBXe+4H9vgxwM+AWdpzP45cWwK0FTAl7K12pGqjzbKvP/dqU7Dkw4pUYwmPC7XnrhBSHUr9mF01xpv/1+CZQvc/B+MTS4fNgfkgpLoK6OiASpndecKa7GAN1wEvac9d2RRJNT5EqPllz/785eY+Qhb+bgjWxZDCeLIfWbpsbT4INRp4B5OLvhuTPCiBVUKqkVYd77Df8OrItb0wizfGahgY0QznYGKZLXZU+9jfw+wkKe4vWITQzZ47KHTsbExi4/tCqul2z9UAF9vz07V7Yujcm4CJPjz66YYtUU3WybZTxxlamOajnUltJmIVMLJL5w4HytR8uZCqFONhP/+03yzsrz33TzkkVFdMdudt2nMfDOp37a6k3aVTJgDjhVTTteduEFK9hlnFe0/Y94RZEvU+cBm1e55iibnh8EKxOKQ/PO25TU3RfkR77teJixV7qiga9Ex/YJY2pkudXQqPaOFcumFOk2bJXwK/6F4y4x3tuU9nbFO1rr/KeOKB9v9j7CLWL4MPnWmOdRO+mQAObR/an7RtUSuEkUw/E7Xr6eYCfR2pOgLU1KSwRJoJTAfOcaQ6BKDHRZMCUk3fOLckJw/apnVL7IB6DLNR7xF72dT/2TaeEFL1bezkepKqUnM9Jt8YoEzUHWUHBHzP3SakugszOnv5RoSPz5Ek3CCkuhO4p9zXVwipFmHScJY48Fffc+eFTp8NPK6NKprRYuDkEzFLzS4XsE5DtTaqqPSTKv9UOymKRipOElLFbWpQoz13ShMf+xXMpic/DLf/WbU/LE0C4iLtuavCFQUwMmWSBGcKqU5vKJHAiRheHaibZ573FOF8oaVgEhDkTd8lpDo8hyp2HNAD48ZIYTzh/+vDZiHVmGCHZe25mzCJcefbSy8DPnh73HnL7e42L4XMDAmsP7qVszhyu+9YIzz6d3kGjxz0YdSGuSrNX896DPbcGmEWd1QBMxva4qkwYniNDhl/S9s5YioHKCrnu76Q6kbgz9Ru/j8423YPG1jKFzW6ld2F5rGQrXUoZinW3Tt9vZnaldRzgVu7G/V2BfCHU3r3CC6bBjxv1eP5wIy1s+rZONMzsKnSIVicsjpiU53ZRJsq0ABb7OYcb/pGC9zbIKnsrGR46NiScl9f31h+9l5ihfbcxfvAaP+LkGqaHdXXCKlUtlmqZTX6YaDYkeow33N3he5V9tG6z+/uOXyOG5muzwQmfFrlXwl829pSge23QEOl3S/idGo3080ZrK02BNhcYNb2ZftN3xVSXYdZJ/BVWlKt/2wLmFTTcOAyn8GziZjEtSZj1H1zKdtWsTf3GoXZOqgIs/n/6Tuyy6efArgaZgupbhlxepf3Hh1zMY5UbbUZlF0wKSpBJ3wipHobs1f8e7teGLoqNPJ3CalewmTPbuje0on7JoVOGlUjIBXxHbUMndtCm8WgI63f7rKaiJ9pY7Xf2kkjNNo5ojLdd9KeO83mho1KS6qjbpgRl5u8b2dsxg1QnKfmA9vxezt9PZSYvakyGKkrhFT97MBYMfEfm8snSrUDsyyrDBiiPXdB5LLZ1lYdU1RUL6t4GnARMOHj+CD4jTrNfxnT5j3Cue/PaQjHPGswixb6xW1FoOtnRNQaYb7uQfxyrcBmvaNK04eYVdWFNg13X2dwFsYPvH3yHyjGOVLNtDnhe61WgVOsz+pI+9w72ztiZdzodmCCD1OcWjdHeFY1JwVdRUSV9O3Wltnryrs24jrZbdtf5teG04JjVa0E2ypitl4ScLuG3zbYQcIsyxJmMYQfZ7MWSHWBD51EZLm8EOc/VaTNDCOfKI14XeulzwipJlnv8QjilxLl0o+1tjnuHPNNQaHNHFya106UKhO3xJpMt8ZJ0LzgJJ8gQUKqBAmpEiSkSpAgIVWCf11SJeT9F0Kh3bws34jmM7WOuW8QXjhoHz1TgjxBcP5TNTSyUWqCBIlaSpCQKsE3zKbCRLITJMgZ/h+dtoYCTqvx1gAAAABJRU5ErkJggg=="/>
                    <h2>
                        Pits and Piles - Measurement Report
                    </h2>
                </div>
                <br/>
                <table id="data_table">
                    <tr>
                        <td>
                            <span class="label">
                                Site:
                            </span>
                            <xsl:value-of select="Measurement/@site"/>
                        </td>
                        <td>
                            <span class="label">
                                Generated basepoints:
                            </span>
                            <xsl:value-of select="Measurement/@generated_base_points"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="label">
                                Location:
                            </span>
                            <xsl:value-of select="Measurement/@location"/>
                        </td>
                        <td>
                            <span class="label">
                                Average height:
                            </span>
                            <span id="average_height_data">

                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="label">
                                Time:
                            </span>
                            <xsl:value-of select="Measurement/@measurement_time"/>
                        </td>
                        <td>
                            <span class="label">
                                Slope Angle:
                            </span>
                            <xsl:value-of select="format-number(Measurement/@tilt, '0.00')"/>° (<xsl:value-of select="format-number(Measurement/@tiltpercent, '0.00')"/>%)
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="label">
                                Volume:
                            </span>
                            <span id="volume_data">

                            </span>
                        </td>
                        <td>
                            <span class="label">
                                Periphery:
                            </span>
                            <span id="periphery_data">

                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="label">
                                Weight:
                            </span>
                            <xsl:value-of select="format-number(Measurement/@weight,'0.0')"/> ton
                        </td>
                        <td>
                            <span class="label">
                                Density:
                            </span>
                            <xsl:value-of select="Measurement/@density"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="label">
                                Area:
                            </span>
                            <span id="area_data">

                            </span>
                        </td>
                        <td>
                            <span class="label">
                                Volume adjustment factor:
                            </span>
                            <xsl:value-of select="Measurement/@volume_adjustment_factor"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span class="label">
                                Measurement duration:
                            </span>
                            <span id="duration_data">

                            </span>
                        </td>
                        <td>
                            <span class="label">
                                User:
                            </span>
                            <xsl:value-of select="Measurement/@user"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="label">
                                Site Description:
                            </span>
                            <xsl:value-of select="Measurement/@site_description"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <span class="label">
                                Location Description:
                            </span>
                            <xsl:value-of select="Measurement/@location_description"/>
                        </td>
                    </tr>
                                        <tr>
                        <td colspan="2">
                            <span class="label">
                                Measurement Description:
                            </span>
                            <xsl:value-of select="Measurement/@description"/>
                        </td>
                    </tr>

                    <script>
                        document.getElementById("volume_data").innerHTML = window.Conversion.VolumePrint('<xsl:value-of select="Measurement/@volume"/>','<xsl:value-of select="Measurement/@measurement_system"/>');
                        document.getElementById("periphery_data").innerHTML = window.Conversion.LengthPrint('<xsl:value-of select="Measurement/@periphery"/>','<xsl:value-of select="Measurement/@measurement_system"/>');
                        document.getElementById("average_height_data").innerHTML = window.Conversion.LengthPrint('<xsl:value-of select="Measurement/@average_height"/>','<xsl:value-of select="Measurement/@measurement_system"/>');
                        document.getElementById("area_data").innerHTML = window.Conversion.AreaPrint('<xsl:value-of select="Measurement/@area"/>','<xsl:value-of select="Measurement/@measurement_system"/>');    
                        document.getElementById("duration_data").innerHTML = window.Conversion.DurationPrint('<xsl:value-of select="Measurement/@elapsed_measurement_time"/>');       
                    </script>
                </table>
                <hr/>

                <img src="{Measurement/Snapshot/@data}" alt="snapshot" width="100%"/>

                <div id="footer">
                    <hr/>
                    <div id="footer-left">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEUAAAAeCAYAAAB6xNMdAAAZ9npUWHRSYXcgcHJvZmlsZSB0eXBlIGV4aWYAAHjarZpnchyxdoX/YxVeAnJYDmKVd+Dl+zvoYZIoic9lUmIYzqCBG064PWb/z38f81985OysianU3HK2fMQWm+/8UO3z8Xx3Nt6v9yOm19/c18eN268/eB4KfA/Pr/n1uOs8nj5eUOLr8fH1cVPma536Wuj1h7cFg67s+WG9NvlaKPjncff63TT//NDzp+O8/p/pmx5K4/nTr7/HQjBWYr3gjd/BBft8fa4U2EVoofM98dWF7PWIvT/H+9X/Hj/zHrpvApjP9/Gz8/WM8BGOZ6G3Y+Vf4vR63KXv43ej9HlHzr+e4j/+oI/R3Fsh/B6/s+o5+zldj9kQrvw61NsR7088kZDGcF+W+Sz8T/xc7mfjs9puJ1lbHHUYO/ilOU80j4tuue6O2/f7dJMtRr994bv304f7WA3FNz9vUqI+3fHFkJ8VKrmaZC7wsH/fi7vXbboeF6tceTme6R2Lkbmvn+bXB/6vn18WOkdl7pyCSerdk2CvqmEbypy+8iwS4s4rpunG15nnm/31Q4kNZDDdMFcO2O14lhjJfdRWuHkONhmeGu3TL66s1wKEiGsnNuMCGbDZheTAiuJ9cY44VvLT2bkP0Q8y4JJJfrFLHwOdUHz1ujavKe4+1yf/PAy8kIgUciikhgYiWTGmmOm3Sgl1k0KKKaWcSqqppZ5DjjnlnEsWTvUSSiyp5FJKLa30GmqsqeZaaq2t9uZbAMaSabmVVltrvXPRHjtrdZ7feWD4EUYcaeRRRh1t9En5zDjTzLPMOtvsy6+wgACz8iqrrrb6dptS2nGnnXfZdbfdD7V2woknnXzKqaed/p61V1a/Zu3XzP09a+6VNX8TpeeVj6zxcClvSzjBSVLOyJiPjowXZYCC9sqZrS5Gr8wpZ7b5YEJInl0mJWc5ZYwMxu18Ou49dx+Z+2PeDNH9T/Pmv8ucUer+PzJnlLpPmfs9b99kbfULt+EmSF1ITEHIQPvxpO4r/6CTt+/bjlkAJ+fnmQQm9RnyomN6nX0v8Gk2t1ZNGYTbBogBIUO2bbY96q55cJK1op8jrhrjKqe0fPJwdNG0J8zOodNogWOFVWo4LZVpxtq+jxbHOC7pJDPuBixyWGKTdy3e8ddGkonRJOjs1O7e5z5+NTdbHX1vshbWWnOWfuI6NYcdSlrOh1qiGM2nUAaJOvvUeUbJGdBvp5TcJlxIEP2Y7HGa3VZpPhNo/qUTAY3Z8go+zupICzkc1oewF7sZKZFrHigul+ZiJscEz8eyDMVcw+L/UdZ3LAD/qa3vMRMCIh6VDjFys7cSKdkkkUDT1FqIE3mYmbN1c5rOP33c/kAgUfXeYmkNXIsJakzPD3TRX7+bfz3hD99LOJtiSLksV+CpYlavFMjacR57WhmZnNOJrbkUjxB3WeqbdClanrj3uCIdN4+PhCCR80YHFjMocLv2IYuctYbt/QGGyc8+SYVVXJsl9RNqH4Grrtmo3rpoiEked2ybwANsx1USUqmdjRzbI5UTwQOWZdF0Riwnx9Toi7JVAHOdRdIzVcMVqMIQTomH7j/sGeyCpOnw7gNdRrWR6E73U7mRBgHfWAdKc54FVupkg3Jko2Rl0PHR5BkGCNTowXnWcquE5TlRTp7lY/Zj9zzssXRP6Tm1tam3hhRp5PNQLhzRn2Zov7H0iuA5n5vB7VY54Pb0TGAzISfKotKRFHsd8XgPYrQ+6w5z+nUCuGCjoaAIV7Wp+E22au9g3BibAGdUIJQQV0hU7ODPh76CRUeaCiHgRL+FWGiNiBglUmBeTnVmgByUT2Qv7UahsHWr7qkLpFuZWtiLDt55jTl0vBH7Zjs1TEO5H0KpNNYdc20zoIyCFZ6szTbAUxq6tUIwO9hD07BGIkarVXqx0M9Ah1l75sVP81CXI9gZDkA3x+TlhIpwAIRJjymqjSiseEu7WWKvHxB3zh5Da5ak428KZIS0y+JQywG2myzaspbAI4irz1hpgzZUGTWRi5aPrkF3FGS9v9ZKu3doplMbYS7R1UqDnu0RVA1bpDQmL7cg1iafcAExXMLzBckEcwJRzCeEMsuOMYMFMc8ZWkrsHR4sbORWMHgdBBlw1KE6+RcXMA5TzHqmqXXRolXR84lCoCeDWime3PbcgC9rh0PPqkNuLfJQWAOOAIIIiWPhOs0NYw03xM1TwuIUhNM+YbtOrEeCZs5ugNgKdEZBUbWZ4BD6OviJ9MthWkPhhUVbkWgqKsHNw8GsqGXApdc23JMk35Bz90f7+XtBhHd6IGKzoooISA+cbsODNxe5IpLdSoESaB0c0PYp49AnUodnADHvuwdBoCMYAHjaQ3+HPHaqxNGVTJRwBgQCgLovmVBcOTjCXk8fmQhR60KVwVWGCeWsEBzgQ5fz4IImoWaXZrWnEmFw48w9wpGSL32mZ9EUZ5GIIXObDjhm85CiskDO0Q46hDqGZDB5pM+pMqjc0ATD4vX3hcoInu7hWULpYjKlNoSLbSey2wGLQlsmR2cpUgVjEgW3DquB4KIlWvKCtM7+2cFRXhqYXTCFibLd8Ps9S/BA17xlEbtaj8sTgFe9/KlcDMIw5/QWbVQKLmkvsISWQ8xAsVCrBMqAogc9hGA4yjfZBI7UXhEozmaJjZWOHM661+vIi742JNBREQCDWGSXLipJCQpy6JOQ8flgw86DdkijmLbQKRQ8tQqXJwRBtRuAjsIrUoz7AgY5N4pjCxKgllEbpUMMG1A5gmyhHCTVKLt+EsHpgbrvamkaMQx3QQqABpqQNx1MOwhPemcMWpj+4XhDhLLMRrcjckvrzrnmE87U0cyroewK7MjRKZPZARC6KEvUr8yBANXhMmQ9cm683FTkzdxoqYzacXAf8LouzbSULeAKFKFxLfB72BqHBrgp/L1VK5krCYAbC60S6SV4mAUR3IDUyRVoT/CGhz42JAJ7DliMwEK8oBMB9r6ylY7q7w4GzNDRqh6JETdlykWiv2augqHEHOgF7JA8e9eDXkICt3Z4IeqILRbYF0iqAJdBVMA94GCNmG8UZJ14BsyEuoLGQaSHxBJ+jwGBg+zB03AURcUfsHE1eizd0IfjTJDAIlhOpobRKn637FF3liIgMjVy8RNkw8k2uHsOSA4ZEwZalYLtiIjBL7lURAOln4DzlZT1XGkeDwPSlX1PJxc6Migzx5EKahydFVsXs8Ck5saBYhBEANcd9ruFsS2VCqyvjSuJafUloe7b1kiC0spDcQBRY88bSW9yFuMlD+9UYRjS4Jxgb98NGES6SJeEK+iwSfNmvAL9/TwDQTXB2jXMtKCeD6AORQhn0+dq8Q5ln4K+Jqz5WFI68DApwW3gKceYc81BcgFUyLehjzSrgRkCNb8p+ozMmVHAqMLMc0FS1RfwMfc5LT5wZPaH2LA4EQJ1Ug3AnKHA4OJwtEn8xhRdH7gEdENADbyVjuDoLBpEruIs3zmqRdbMUa5mbJyDOpoURndECQTG74CFwOyo8khAQ4WUkE64TzR/TUfyfdz4WGR8hTsJgl0X2PBGEVym6eE28IGDLFRJB6O3ZW0JpUrBhENlbXReblR6Ka5nyMwR6bys8URDStijMAklhqH4uDbOalGpHtghzNjWIPaRWKEP2VdH5NC+BV1HL+xg2RHcbjNxykQT29c172hQE57Sk3U0ClKAlIIg79t1n7cbRTkm0eMcrDsKM2vus9oFdsQkBULtQ/QgnEPclDUL2lTysstK0oKO+s0ksqKPwBU2VzP6EneO68JGXWoHSAgzr6teUlEVRfXRsxg/MBO3B1RXLUyXNgMsoJrqxOz6ookPupjf0Ve0Q58utpn3Y5U07Ok5tqMsASieJsWZx7TpORPphzQiXpXiQ+VB+jQQ1nCxNXCWmLH5bHvvmEGMLPqhyMRGdBsmYIOzjvIw4IWfWdi3cIo8FdHZ1OwIfRsD4tGqgOGT2kNEzjYMUsDKo52SEA/C9qxpSqbkaWPAhQAkoTOlRsXsjaxDcqqlcUnskc/UHNDp8UVTk0C04MCrabRhwIJTdtUEgqNu+rsWkTdONGA9VuQ6hb1skE3DjSEPI6uHtKS9EZeQTmdHwji8J2ACm4G4wclqxYM37AVcBjoRYhwT+YK1S0LJJZz3gzTGwMYwDT6g/LkYV8UppoKN1uTNTyc37EWloJWn+/tGyyBWUQNARd7EMGNDbfQCWirEqDc1gBFsuSwgG6fbNVGfnIZyLltO70mpeJE/YKDY9QlSJ3IWdJ010AdyV3mG1SbKDZgIEwWCIMDZny7U2YjxQu7StIgckBB9Rde7jgagnrFHxbiyz06ZQzt8RMmuWc11HWrYemi5gIEYtY19oRs5EXXRO7bxpFiuerHosjioI6486/W/iq4Db4D27i6OYSDR+lIGBHR2Sx94UqOpScmWhME8JIjtGrvg6js1BkRgSzSGAo3Phr3RfBVPKp3j7YmgSJeuLamg3aPQtAtRyccyGg5JQBPsESMLUIjoe3UYqrrKZmzgnILxiC30E3SnGXnvtHzcQmqvA6L8OST0wlWd9OtXmsFHCrER+Cj1MHHtgClLAdYoF9oMf6KxUC60CKkuyIIzpBPptoPuH7avmqRBMLZ36YC9ecl2eNnNSA491pOY51ZnN0joeDTAIM5AAnt7cRgGLzy6jaM/hVTlwBaIhFJe1Jz6j3KpIi8De4UnjbysnXve0oCkXYIANfJgQgts/B5kVdEDmvoluXj2nCoJRsF5BLsFbtA0ftM1GD7qZSxP3eGuEDxZjgbIQGjBcZYEFRR/A8Jw0A31Umn9C/5bAwpMJOauSpnIYhbMJ6h2ssuaPwL350rt3lCK9nShON+C6IIXIkvSNX53ggVS91OTBnzoMApl84Ls74go+syhOVjQuARJQ9FtUL3dkSFKbESDAeZYi2cSk8QRO3qHSpK5WbbKVg0JK8uSIlPn3i49YLVthd26x4XLvvi5iqJLNwGXEBZp5/W4aAFJlkFzs7I0Lwb8BHdoAOJ9BxIBJVCNV/VOUNaC83Uir1EFZSwxBVjeISKYFEsUHX3+mNDk5EIxA+2SvPzvNNQFdqqnoyktkga1eIsBJ7gS1QoIyGRCLxZ6vKbMx7zAM5CBcBEkkLtNg/OaHAx3rGElspzTkJ4oYYO5QyYB0I5Gi+in3ZHeDXkUEbWUiiY/mpLVhO/fAY2IHwG1vNDDQmyYS8lmNhjJLEzcCt0yNh6hsEX1V0ZWZdRvXHA8HWbIE4kr0qvwrTtRZHInLXADYThJbmVEzcNWB/0a0dugDpH2FJGfUAO22Ug0Hg2rGgpuVECxaO/NF0CNxGGKlmQ5vezwN5nOpNuR0jDJ1jBl+MQuk9nEAwyGk0TqQzHPmV6ZANad6uHdwYMN80h7P3LGj4sEDqRDxCFVpzPEEPfY+F8JAg2I1QZfNbofoRRZ9QC8cMqpNV2tkCkKBFlCWRYNOxuRxEHKgJFmeHdZBFrPDdWNZsKzViwUlIxxw3Dhk5Dz1Nj1pFyFKs4ZVThQwaOZ0chJQtVxMf553Rn1mgtr8se+qWacCuYbsZVFafiyJSSsrSGoNr7zWMrPuFMaVwHLNV2ntm3CpmOJJlSCEAAFpqqIHKFCK9W7FsenRtkffhMfjMXFHOcZ4cUSavK0KtYniusbl6FIoGfNIJFOqIixqkb14EdrHBTSpYUiwk8TSaDW4dYwBZrMTDjei61PF/AFFi7nvo66HqQgCOiL7i4CDB0LfdUmcnhbA0Rg+N7mDuvOHbK0JL6rngb0l5zQwkWjZzczzZ44K200DsIE8h710tF5Flx3dKAVX+vdQcqj8FFziAIwyV1ymsgtfpNmCBTMSWgN/NpS6R1NiziC8nFni0hlGM5y2NPFitQHeglzOjwRGa5LNu+ECwxuWmx+N8gm+C45f/EfJZUDall3lHAw9Zn76yYEnpQE1xSgazRW1sywDkktt1Q+BrPNzvgPHIAwIagurCYaFvtGktg2lQGpUK/7OjDy6DnGNWLOY+kpm2RwcLZV8jDRLFGeXAoK44BzEcoW3UuhKZBMgP32OQIlVH/G0s+jMQOxo2sMagLSKaPrVldOFg4B14C+O9KhIRG2p+AzSAN0B2MmbB0RQjDJcnrMN9c6RsRPoAWHOE31q24g1JV0B4tSPnt2elZKjl2SkCP4lA6Urp32DlyRVgY9EZLTmBhsbBr2Y/CDJNwd0mqEj5cZynXAvBSNBp4d1+yAk6HbhNhaM1JKaJQVBQANu0JH8eQDTePCOdXCxjm9lwNdS+UjN5A7RKZWICKATStScceAj+Tx8gbCk8LAFui2JFwbNVcdtCtUNzTLwpUKuTtR0MCNE4gcfNx7wP1jCn+RoLyGioNzEJQZ/hUVFrSQbGuhmSlrFKi/Y2uH1Aj0YFGN3IGdKahk9E8Jkb/d8RByAfhsW0KSc6PzKaSAJyWvHJQ9969L3smkQeNQHWh9ekl3ROAP3SYi2Sq/xaoFUQaYzE67LPFrDvB0UW637uaE5fDvJmqcCaFuBDzov8k4xILJlsp3mkiRCAvrUtt4OBUjjhEnCjsBbU63Jyx9ZYIkROT68hyyE4MuYW1/KjiIrruFKQ8YaT6oaOFmGz5jW6R+0r3aPPHMBr4g9F4zgdeRQkOnacL4ftOn4aWgojOgcS81quNpuHvXICMjoLNZ+6z4IA0d/89cYb2rsMXiPGzBTsBYs3RDn1aFG4uKvASkFuX0GPaNHX7+SCFJfCbJPGhHNzDAzNY5JdrAU+/YLPSj6DM44RWtmHVD0NF09I7u99CTxAY35TWUdBdQNrp0ou/x5xCb7roVc7K6mODBZAlk2lFjqslDq6E61q4qCvzSQAUjfsBGqxsmumdLJc5zd7+a6Vfkx67Z4NbYQ4eSNnITGQSZ0g8ak6Pksu7U41HQhwSlQdoZilJQNT8i0JNIQhd35eHrQzHP/aIk/wBfXmzS3sN+XosU4C+oWl6/oQIPsPFSt2frBGF9usTrChp/BxlUXeFZPz3u5K7Pd9ZnefN5fVue5SnA9/VxD99f4eMMA+hLRvcXiY3uHyF5Qa97f2LNyLJq7zvdhanD60Zr7IitjLSWVK10XdP0OXmD8J5YEwR9l9TUyAh3qkGUA8OqtFHemteFjrMHiJLmWL8Pi8yXadFIxdZBNzWSAit75edM/3odXOxq0Sv1TgYNmqYMfsW/TPM0RwwW/XKXdHTU0XB6bIuxz+sZzFWEEe0Bf/A8bKTfG7ePQ0FMapZgMKdUHo9qJt6Ai+W1B8CeVnnSjkq7UExJeTVpxZXheaGl/vi7aks0bI7Qod06gNTbftvB+2jQPWbmNRmcvSEOMc6gBj+g2Boaie0ZTDVmnmujakUSGgFrrS5Y6Pm5tdlUlqvFP+/Vm1Sx/V43MNU51QEiaLICZ/RbDAU7hdCS0R7egaXgBEBbKQsU4AClJgVlp6EyE42lm+ul7zL45Q/7SQNC2FR30c04DM4RFYMm9NCaGiBAvQiaqZkwP+IrdX/T1Q4W2k6VtkSeqVML0eJuRfWomSVJrvcOXNNhzas1tWmuhE2jgG+TAFio7F478e92cGq+dgqTfQkDwD1kLGCMAfb2GDgiITqjZvABpU5QCm0WIj2GPmpUWGsTXQapBDCLVEnJ6MY0SgnhGjoGcDmzUAHTCUej7kBj3woyqtl7N9qi1G1CXsD92CQ46WZLMLdpYfKuu8b3BEZRJED4hC5TCbgV9jM1suSkSEscnN7spFkl0rYk+p2uo4DoJt2Iz1E0j12Puh9JwmNuGpciYfHWbiETeAaEGtSkruo+MPwtybmkTBN8Socrun7aYk1HK5GxE9ad1MLyeBm9ReZE3XLgJ90BZZebyCfdKU1Sj5wBUtt62wx0rPdoUUGoV4cWjMNpRLorHglslsyDJrYU0Y6oHI3oOg6/AmRYSvq/WRit5KibhEZ3+uxLokv8ghFL7g0WB6aG/iA5manqAfWVcxEUMMGWNgyMxPY8yRosKjGYp5UcMDwaAQaW9qKtIwNxPmFzfBFDvu3sP9e90YSl6OYm167oB8m2rLmyQ5/EHpv8O50sgW3vvRVLFoEtTZreUJtOMNgKi5cCplGRcBY5IWjszDWNJKqWyD4mzXp1O6JUKboom6zbUXT+ZZpqaAo4ALGNHtPBOgYkUg+9UEfFHyj7jqlp+x5SjTJoqlgK9arH7VqR/NM7ovQOp8s09cK56O4DZ+qWy9H8QhNlzACGBmkMUFQMBPvFmWswj4VA6EZOPJ5mVaVLCOq9LbUjhw7cMit+QfPNsO5ksOseVAxSW7QXRyrJEAaKNS1ABOfGVQs6nf6c46FKxNrBxiIZsY1NE0WUDECMvKAmYZwhfvTOHBwUtak3mkTd7Nkvm2c1upngGyAeQEOSgYbUWylC0ZufNIREXgy9yWwhLlFsUiOI8KA3dvA6WVhNtVesyBGuW65cuyMoPOBTO1u1Uy6GnyQML0Zl99yxwo+Ua7OW4+vzjhZpgPtSvA/BWi+dgCgIvyxuvlv998WJi+5r3jfLHOWV9vq6rnns7/vS8/FpPCfpTVGP8DoZqXZDU1oBiFYnk1D0E+t8I2iwgYgMnC++X+TfEwivAd6mlXMCuFEbsHHIz3wtz4yUeOj80Q10cK7LYDnV70EjH8ygQ+aFt/R4zZpVP2DlpvdfdP5tAMwNL3KQNLGzhkWBo3nechKRiRIGTFg3ludtC8861j4rycKxFisZ94fU3olYum/l+RJkZPN4Iqd2/1jefL/+35f/rjxMeC7wsfz3MXB/ye5d27wWf5ZGT3y/8X/v2/xs4//et/nZxv+9b/Ozjf973+ZnG//3vs3PNv7vfZufV8rf921+Xil/37f5eaX8fd/m55Xy932bn21cSAOc/i/KJVmHcAkBwAAAAYRpQ0NQSUNDIFBST0ZJTEUAAHicfZE9SMNAHMVfW0tLqSjYQcQhQ3WyICrSUapYBAulrdCqg8mlX9CkIUlxcRRcCw5+LFYdXJx1dXAVBMEPEBdXJ0UXKfF/SaFFjAfH/Xh373H3DvC2akwx+iYBRTX1TDIh5AurQuAVfgwiiBDiIjO0VHYxB9fxdQ8PX+9iPMv93J+jXy4aDPAIxHNM003iDeLZTVPjvE8cYRVRJj4nntDpgsSPXJccfuNcttnLMyN6LjNPHCEWyj0s9TCr6ArxDHFUVlTK9+YdljlvcVZqDda5J39huKiuZLlOcxRJLCGFNARIaKCKGkzEaFVJMZCh/YSLf8T2p8klkasKRo4F1KFAtP3gf/C7W6M0PeUkhROA/8WyPsaAwC7QblrW97FltU8A3zNwpXb99RYQ/yS92dWiR8DANnBx3dWkPeByBxh+0kRdtCUfTW+pBLyf0TcVgKFbILTm9NbZx+kDkKOulm+Ag0NgvEzZ6y7vDvb29u+ZTn8/dMhyqC4YTaMAAAAGYktHRAD/AP8A/6C9p5MAAAAJcEhZcwAALiMAAC4jAXilP3YAAAAHdElNRQfjBRAIDg0q3BvoAAAJjElEQVRo3u2ZeXBVRRbGf33flo0sEsMiSFFsI6IjpAQpZwQsMDomQTQJCALjIIEMlOKoWDOWMiiWCoJQYhJWEUW2RIIGKBV1RkUERnHBESjZwQgkEJKX9/KW22f+uB0JEtbiL5j+6+W+vt2vv3PO932no0SECxmVVTVcPXJJJ2CgC2ZGywuiXGbDutAXWoxc4gEeBiba0OvGIfO4okHpmjcXDb2APOAo8MQ2fzThRE3gygVle8BOAJ4EfgJGAu0FBnYesfjKBCUpu1gJZAO9gakxiq+BYuCRI1Fpfbzaf2WB8sLsddRoaQE8Dnyo4INNL/1JK1gKnADy04a/6bqiQJn0/l4LeAhoAUzrkeQJ3ti1HW29VjXwMjAkKtzQf9TrlwUo6lySnDF6ER9UBLsB5cASF0xqLMOuzCKvhkIgRsFoXV4QPNNa454pYc7XR90aPACxivq69wokJqvICgs+E6VIaPWYqMtlXdSBHplcymtbjjTsoVPdKnxg+SjpfP9CgrbQ3GdxNKRp7rXYsTIff109re5fpOq0NAO8QOSsO0ejNusrgj7gUcAPFNeX5Z/iSzKvjQ8DM4B0gQGpA4vVmdYr+a4SG3IF1giUBIRrAEJCB4EygXIb7vphx8GLjvKKrUexIcfs8UplVGJvH72Y/WE99Kgtr24P2DdX2bJoZ9D+A0C7oYuUX0uewDqB9QJL3WfboNv9C9DQB4dgn0y01EG3+1TqWF04Am9W0faI8Abw+DFbvlhSurFy2H29TwdZUMC1QF8Dcqz5qhlwG050VwQCIeKzilS94NOQAKCgzqcIrfrbrXrZ2h9Ysr3aZQvNANulCGjBAiTOUnajPeIAt60BqAIOAzHAQaA2feh8jtuSABQYAVkDLDxjpti2ZmfQTgYmAl8rKN27+IEma+2bl7O0goWAW2DkqEXfnE/uu4EBKrNoENAP+BXtW/+53hMQxmj4N/AZ8JnA5/XC+Lunb/As/rE6KSpME9go8HlUmKXhCw3v+bVc9duNNlWH04DbgVxgAZAB3LG1JpIi8Dxws4M76UDbM/745EFzlUAOcAMw9fp494mU5ISmTV2XNqS41FFgOjAqJHTKLjind4kFZgLLgBdMlgCgIQm4BagF5gNvA78DntbQQWAQMN4Q/6c4hjId6GLAbjy8wBTgMaDerJUKTBEYC+wF6gAB9gEHmwSl1h/Er6WN4ZLVFnz2/fLRZz1h1eqxomAd8D3w6JoDdd5zgBIyoDwNFAHRRpJYC7xrTGIfnOB4TeqnAJ3N4b9VTiYXAnZTmwgkAncYpZ3hUTwHrDDrDQCWAHsMKCt8irImQUkZssgN/NVE85U728SFz4fk+qXFBIBpQD8Nt6UPnX+26WFgXk6HZlOBNxqDouGPOOWYA5SZTGrsDncbENobQ9m/cfn9ZkSASvO5S0RoBbQ3f1coZ98GWtD56Wn6NFB6PbAAG34PDAfmeBU71hSPPC/m/2jhg7gUW4FS4ImtNZGkQDDURPAQ5+ywctYDpz0zgIVNUHKAMY3KwqdglSm7WOAloJN5V/1mD1FQY0A9ADwBfARkAd+ZTI022ltoov7YUh2OMc71Z+D1t/LT9YVI4qT+7exnPtxXjCOxeWmDX5/vf3esADT3WHLMtlebSEcUVBhl2SMwzKT4Vrdif1S4B+gBBIHNhj8Sgd0C7YBdpuzWmBLrYdaMiFN6e4BjFtS5FKvCwjbDPSnmbBuTLHXIr8Vtw1Pm+XfpXVueat5a3FPMkagMBOYB45ItVXLcHOhCRsrAYlVty4PmimGglBfsAwiFIuRMWMrh2jAKRUb3NJ59LIsJz5aycWc1gtA22Ufp7OFkjl3Mjsp63BZkdEvlmz0nCERsUuM9rDsU6I6TLVcbcrzKyO90t2LKXW3i7V9qw/hcFo/kdCMnqyfTij9g8Sf7CNpCktfixTG9GNDnBla8u4lXS38gZGtaJ/ooKxx+EpSS8i3kFv+nObAaOKxghC4vqLsYA2VrjSd7TjOB5cA2F/zjUl5G+RwH3NFIaarJpu8t2PrxU33r+/S+7tLY/FjHLD1srgYG9Uj0bPrq7YcueuH2981hb0j3BeYCIzrHur7csTL/rO9cnzePHwPRWKMywQRLhfxafAmWCvkUVNkS51UE2/pcele9HaNAizP3V/K2IKpPmsJomlsF/RoV0BLXiIy1RxGICDHJLhUcfUtLmb6hwgPIkTeHR5WIMGFyKbO2HOkIrAVWu+DvlyKyrswij3bIrJWC4YHSUXUxPu/ZeqzOhuBrDX8sNXL6tiHBF4EZhoPGAtuM295lltlqVCnX+I8kYKOCzQKTzTPNScl/Hihu7lJfVdmSAYReGdjpEwtg9pYjHmOGbKCwZFzPS5LquR0TI4bhuwhkt8xdeMa+aOexEMYoRowMv2MiGwW6Ght+GOgnjqQ2NHAVwEpghYItQDLwXxfMAl7F6bWuMWVWYuaWGSluCTxUZUtbw0/N/YEw7luGL8CGnsAQ4Dm3Yv/gws0WhZvPnAEg2R0SZdnMYWcFZdnMYazKKtoVFuYCj57Q8q+pRe9XTCzIOG1ueut49u2pXS9OYAaZH7kG2GB8SDNgMZBvgNtgyuwmE3kE1jeUR2uvFf05rI/bcNwAkmLW1cABgc+NCi01CbHfcBPuzcfDCaa/iQPSo8LL55EE376zq+bN2KwiCckpNX2m8Y4BffTTa3dPmVjAaTL//j5/Q1txSMFrArcC/RRMNaVyzIJd2mnmbjMR7w18qaDhIici0BGIORjWSeZzkjlwJbBIOaAL4BXHDmwSR+7Hm8zCLU4z1ssgeO85Ducx4JVqYUnE8QtvmWfnku7mwOCwsHz8pJIdsyfnnPJlz1SffHykvhy4T6A7EAAWJFiqrlbLWuDoa3nXhQtW/LgO+MWnOB4S9gE9DWgAOwxv9BAno/zASwoOC3wLjJaTLncl8IkFUYFVphv/qXVaMxR3F15tUkudR8QzzE3bGhfk2U4kpgLPmEOca4iCivybUmuLp+Q2OSExu1gFtLg8CnvD83fK/kPH+HPRFuVVcLhsjLS9dy41tqgJfa6Rwk8PqVotqlHbLbGWwq9FKaCV19K7S8fQbfA8dgWiVkN6KuAqt9K1Nur2NnEyLq87g2dsUC4Fh1fli7qQf4apzKIc00CtbQTKZAV/0eUFl83NtcX/x+kXPVZmUfwFzG8gVZeGeNOQuYE4K7NILhtQxGm21HnOTzRk21dgo/EJqeJc9OjLBhRzW3WhZZRg3qPRHetlM/4H2MXqmfn9sRkAAAAASUVORK5CYII="/>
                    </div>
                    <div id="footer-middle">
                        Haglof Application Cloud<br/>
                        www.haglof.app
                    </div>
                    <div id="footer-right">
                        Page 1
                    </div>
                    
                </div>

            </div>
        </body>
    </html>
</xsl:template>

</xsl:stylesheet>
