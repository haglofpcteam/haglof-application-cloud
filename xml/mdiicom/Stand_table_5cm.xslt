<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
<html>
    <head>
        <script src="/js/reports.js"></script>
        <script>
            var context = this;

            const inchPerMillimeter=0.0393700787;
            const footPerDecimeter=0.32808399;
            const m2PerAcre=4046.85642;
            const sqFeetPersqMeter=10.7639104;
            const m2PerHa=10000;

            const page_title="Digitech Cloud - Stand table 5 cm";

            var page_counter=0;

            $(document).ready(function() {

                var data = JSON.parse('<xsl:value-of select="/Stands/StandJSON"/>');
      
                data.settings=data.stand.settings;
                data.sample_trees=[];
                data.plots=[];
                data.class_total=[];
                data.plot_per_hektar = 0;
                data.plot_areal = 0;
                data.areal_factor = 0;

                if(data.settings!=null) {
                    if(typeof data.settings.plot_areal != 'undefined') {
                        data.plot_areal = parseFloat(data.settings.plot_areal);
                        data.plot_per_hektar = m2PerHa/data.plot_areal;
                    }
                }

                data.trees = data.trees.filter(function(tree) {
                    return parseInt(tree.is_log)!=1;
                });

                data.trees.forEach(function(item,index) {
                    if(parseInt(item.metric)===0) {
                        item.diameter=item.diameter/inchPerMillimeter;
                        if(item.height1!=null) {
                            item.height1=item.height1/footPerDecimeter;
                        }
                        if(item.height2!=null) {
                            item.height2=item.height2/footPerDecimeter;
                        }
                        if(item.height3!=null) {
                            item.height3=item.height3/footPerDecimeter;
                        }
                    }

                    if(item.height1!=null) {
                        item.height1=item.height1/10;
                    }
                    if(item.height2!=null) {
                        item.height2=item.height2/10;
                    }
                    if(item.height3!=null) {
                        item.height3=item.height3/10;
                    }
                    if(item.calculated_height!=null) {
                        item.calculated_height=item.calculated_height/10;
                    }
                    
                    var species_id = data.species.findIndex(function(element,index,arr) {
                        return parseInt(element.id)===parseInt(item.m_d_i_i_specie_id);
                    });

                    if(typeof data.species[species_id].classes === 'undefined') {
                        data.species[species_id].classes=[];
                        data.species[species_id].dia=0;
                        data.species[species_id].dia2=0;
                        data.species[species_id].dia3=0;
                        data.species[species_id].count=0;
                        data.species[species_id].heightsum=0;
                        data.species[species_id].heightcount=0;
                        data.species[species_id].form_factor_volume=0;
                    }
                    
                    item.ba=(Math.PI*Math.pow(item.diameter/2,2))/1000000;

                    var height;

                    if(item.height1!=null) {
                        height=item.height1;
                        data.sample_trees.push(item);
                        data.species[species_id].heightsum+=item.height1;
                        data.species[species_id].heightcount++;
                    }
                    else {
                        height=item.calculated_height;
                    }

                    if(height>13) {
                        item.form_factor_volume=Math.PI*Math.pow(item.diameter/1000,2)*data.species[species_id].form_factor*height/4.0;
                        data.species[species_id].form_factor_volume+=item.form_factor_volume;
                    }
                    else {
                        item.form_factor_volume=0;
                    }

                    var plot_id = item.m_d_i_i_plot_id;
                    if(typeof data.plots[plot_id] === 'undefined') {
                        data.plots[plot_id]=[];
                    }
                    data.plots[plot_id].push(item);

                    var class_index = Math.floor(item.diameter/50);
                    
                    data.species[species_id].dia+=item.diameter;
                    data.species[species_id].dia2+=Math.pow(item.diameter,2);
                    data.species[species_id].dia3+=Math.pow(item.diameter,3);
                    data.species[species_id].count++;

                    if(typeof data.species[species_id].classes[class_index] === 'undefined') {
                        data.species[species_id].classes[class_index]=0;
                    }

                    data.species[species_id].classes[class_index]++;

                    if(typeof data.class_total[class_index] === 'undefined') {
                        data.class_total[class_index]=0;
                    }

                    data.class_total[class_index]++; 

                });

                data.plot_count=data.plots.filter(function(value) { return value !== undefined }).length;

                if(typeof data.plot_areal != 'undefined') {
                    data.areal_factor = (data.plot_count*data.plot_areal)/m2PerHa;
                }

                var content = $('<div/>');

                content.append(context.createHeaderTable(data));

                if(16>data.plot_count) {
                    if(16>data.sample_trees.length) {
                        content.append(context.createSampleTreesTable(data,0,100));
                        content.append('<br/>');
                    }
                }
                
                content.append(context.createPlotsTable(data,0,34));

                $("body").append(context.createPage(page_title,content));

                var plot_pages=Math.ceil((data.plot_count-33)/40);

                for(i=0;plot_pages>i;i++) {
                    content = $('<div/>');
                    var start = (i*40)+34;
                    var stop = ((i+1)*40)+34;

                    content.append(context.createPlotsTable(data,start,stop));

                    $("body").append(context.createPage(page_title,content));
                }
                
                if(data.sample_trees.length>15|data.plot_count>15) {
                    var sample_trees_pages=Math.ceil(data.sample_trees.length/40);
                    page=0;
                    while(sample_trees_pages>page) {
                        console.log("Page " + page);
                        content = $('<div/>');

                        content.append(context.createSampleTreesTable(data,page*40,(page+1)*40));
        
                        $("body").append(context.createPage(page_title,content));

                        page++;
                    }
                }

                var class_rows=data.class_total.filter(function(value) { return value !== undefined }).length;

                class_rows+=7;
                var class_pages=Math.ceil(class_rows/39);
                var species_count=data.species.filter(function(s) {
                    return (typeof s.classes != 'undefined');
                }).length;

                var species_pages=Math.ceil((species_count+1)/5);

                for(j=0;class_pages>j;j++) {

                    for(p=0;species_pages>p;p++) {
                        content = $('<div/>');

                        content.append(context.createClassTable(data,j*40,(j+1)*40,p*5,(p+1)*5,j===class_pages-1));
        
                        $("body").append(context.createPage(page_title,content));
                    }

                }


            });

            function createPlotsTable(data,start,stop) {
                var table = $('<table class="plots_table"/>');
                
                var row = $('<tr/>');
                row.append('<th>Plot</th>>');
                row.append('<th>Ba m²/ha</th>>');
                row.append('<th>Pcs/ha</th>>');
                table.append(row);

                var current=0;
                data.plots
                    .filter(function(value) { return value !== undefined })
                    .forEach(function(item,index) {
                        current++;
                        if(current>=start) {
                            if(stop>current) { 
                                row = $('<tr/>');
                                row.append($('<td/>').append(current));
                                row.append($('<td/>').append((item.reduce(function(total,item) { return total+item.ba },0)*data.plot_per_hektar).toFixed(1)));
                                row.append($('<td/>').append((item.length*data.plot_per_hektar).toFixed(0)));
                                table.append(row);
                            }
                        }
                    });

                return table;
            }

            function createSampleTreesTable(data,start,stop) {
                var table = $('<table class="sample_trees_table"/>');
                
                var row = $('<tr/>');
                row.append('<th>#</th>>');
                row.append('<th>Sample Tree</th>>');
                row.append('<th>Diameter [mm]</th>>');
                row.append('<th>Height 1 [m]</th>>');
                row.append('<th>Height 2 [m]</th>>');
                row.append('<th>Height 3 [m]</th>>');
                table.append(row);

                for(i=start;i!=stop;i++) {
                    if(typeof data.sample_trees[i]!='undefined') {
                        var sample_tree = data.sample_trees[i];
                        var species_id = data.species.findIndex(function(element,index,arr) {
                            return parseInt(element.id)===parseInt(sample_tree.m_d_i_i_specie_id);
                        });

                        row = $('<tr/>');
                        row.append($('<td/>').append(i+1));
                        row.append($('<td/>').append(data.species[species_id].specie_text));
                        row.append($('<td/>').append(sample_tree.diameter.toFixed(0)));
                        row.append($('<td/>').append(sample_tree.height1!=null?sample_tree.height1.toFixed(1):null));
                        row.append($('<td/>').append(sample_tree.height2!=null?sample_tree.height2.toFixed(1):null));
                        row.append($('<td/>').append(sample_tree.height3!=null?sample_tree.height3.toFixed(1):null));
                        table.append(row);
                    }
                }
                return table;
            }

            function createClassTable(data,start,stop,species_start,species_stop,print_total) {

                if(typeof data.total_dia === 'undefined') {
                    data.total_dia=0;
                    data.total_dia2=0;
                    data.total_dia3=0;
                    data.total_heightsum=0;
                    data.total_heightcount=0;
                    data.total_count=data.trees.length;
                }

                var table = $('<table class="class_table"/>');

                var print_total_column=true;
                
                var row = $('<tr/>');
                row.append($('<th>Stand table</th>>').attr('colspan',2));
                table.append(row);

                row = $('<tr/>');
                row.append('<th>Class</th>>');

                var species = data.species.filter(function(s) {
                    return (typeof s.classes != 'undefined');
                });

                var sstart=species_start;
                var sstop=Math.min(species.length,species_stop);
                var current=0;

                species.forEach(function(item,index) {
                    if(current>=sstart) {
                        if(sstop>current) {
                            var column = $('<th/>');
                            column.append(item.specie_text);
                            row.append(column);
                        }

                        if(current>=sstop) {
                            print_total_column=false;
                        }
                    }
                    current++;
                });

                if(print_total_column) {
                    row.append('<th>Total</th>');
                }

                table.append(row);

                current=0;
                

                for(i=0;i!=data.class_total.length-1;i++) {
                    if(typeof data.class_total[i] != 'undefined') {
                        if(current>=start) {
                            row = $('<tr/>');
                        
                            var column = $('<th/>');
                            column.append(i*5).append("-").append((i+1)*5);
                            row.append(column);
    
                            var c=0;
                            species.forEach(function(item,index) {
                                if(c>=sstart) {
                                    if(sstop>c) {
                                        column = $('<td/>');
                                        if(typeof item.classes[i] != 'undefined') {
                                            column.append(item.classes[i]);
                                        }
                                        else {
                                            column.append(0);
                                        }
                                        row.append(column);
                                    }
                                }
                                c++;
                            });

                            if(print_total_column) {
                                column = $('<td/>');
                                column.append(data.class_total[i]);
                                row.append(column);
                            }
    
                            table.append(row);
                        }

                        if(current===stop-1) {
                            break;
                        }

                        current++;
                    }
                }

                if(print_total) {
                    row = $('<tr/>');
                    row.append('<th>Sum</th>');

                    current=0;
                    species.forEach(function(item,index) {
                        if(current>=sstart) {
                            if(sstop>current) {
                                row.append($('<td/>').append(item.count));
                            }
                        }
                        current++;
                    });

                    if(print_total_column) {
                        row.append($('<td/>').append(data.total_count));
                    }

                    table.append(row);

                    row = $('<tr/>');
                    row.append('<th> </th>');
                    table.append(row);

                    row = $('<tr/>');
                    row.append('<th>Aver.Dia mm</th>');

                    current=0;
                    species.forEach(function(item,index) {
                        if(current>=sstart) {
                            if(sstop>current) {
                                data.total_dia+=item.dia;
                                row.append($('<td/>').append((item.dia/item.count).toFixed(1)));
                            }
                        }
                        current++;
                    });

                    if(print_total_column) {
                        row.append($('<td/>').append((data.total_dia/data.total_count).toFixed(1)));
                    }

                    table.append(row);

                    row = $('<tr/>');
                    row.append('<th>Aver2Dia mm</th>');


                    current=0;
                    species.forEach(function(item,index) {
                        if(current>=sstart) {
                            if(sstop>current) {
                                data.total_dia2+=item.dia2;
                                row.append($('<td/>').append(Math.sqrt(item.dia2/item.count).toFixed(1)));
                            }
                        }
                        current++;
                    });

                    if(print_total_column) {
                        row.append($('<td/>').append(Math.sqrt(data.total_dia2/data.total_count).toFixed(1)));
                    }

                    table.append(row);

                    row = $('<tr/>');
                    row.append('<th>Aver3Dia mm</th>');

                    current=0;
                    species.forEach(function(item,index) {
                        if(current>=sstart) {
                            if(sstop>current) {
                                data.total_dia3+=item.dia3;
                                row.append($('<td/>').append((item.dia3/item.dia2).toFixed(1)));
                            }
                        }
                        current++;
                    });

                    if(print_total_column) {
                        row.append($('<td/>').append((data.total_dia3/data.total_dia2).toFixed(1)));
                    }

                    table.append(row);

                    row = $('<tr/>');
                    row.append('<th>A.Height m</th>');

                    current=0;
                    species.forEach(function(item,index) {
                        if(current>=sstart) {
                            if(sstop>current) {
                                data.total_heightsum+=item.heightsum;
                                data.total_heightcount+=item.heightcount;
                                row.append($('<td/>').append((item.heightsum/item.heightcount).toFixed(1)));
                            }
                        }
                        current++;
                    });

                    if(print_total_column) {
                        row.append($('<td/>').append((data.total_heightsum/data.total_heightcount).toFixed(1)));
                    }

                    table.append(row);

                    row = $('<tr/>');
                    row.append('<th>Form Factor</th>');

                    current=0;
                    species.forEach(function(item,index) {
                        if(current>=sstart) {
                            if(sstop>current) {
                                row.append($('<td/>').append((item.form_factor)));
                            }
                        }
                        current++;
                    });

                    table.append(row);

                    row = $('<tr/>');
                    row.append('<th>Volume m³/ha</th>');
                    
                    current=0;
                    species.forEach(function(item,index) {
                        if(current>=sstart) {
                            if(sstop>current) {
                                row.append($('<td/>').append((item.form_factor_volume/data.areal_factor).toFixed(0)));
                            }
                        }
                        current++;
                    });

                    table.append(row);
                }

                return table;
            }

            function createHeaderTable(data) {
                var header_table = $('<table class="header_table"/>');
                header_table.append(context.headerTableRow('Stand',data.stand.name));
                header_table.append(context.headerTableRow('Inventory type',data.plot_count>1?'Plot inventory':'Total inventory'));
                header_table.append(context.headerTableRow('Number of plots',data.plot_count));
                header_table.append(context.headerTableRow('Number of trees',data.trees.length));
                header_table.append(context.headerTableRow('Number of sample trees',data.sample_trees.length));
                header_table.append(context.headerTableRow('Plot area m²',data.plot_areal.toFixed(1)));
                header_table.append(context.headerTableRow('Area factor m²/ha',data.areal_factor.toFixed(3)));
                header_table.append('<br/>');

                return header_table;
            }

            function headerTableRow(title,value) {
                var row = $('<tr/>');
                var titleColumn = $('<th/>');
                var valueColumn = $('<td/>');

                titleColumn.append(title);
                valueColumn.append(value);

                row.append(titleColumn);
                row.append(valueColumn);

                return row;
            }
            
            function createPage(title,content) {
                page_counter++;
                var page = $('<div class="content"/>');

                var header = $('<div class="header"/>');

                header.append($('<div class="header_image_div"/>').append(window.reportLayout.getStandardHeaderImage()));
                header.append($('<h2/>').append(title));
                header.append('<hr/>');

                var footer = $('<div class="footer"/>');
                
                footer.append('<hr/>');
                footer.append($('<div class="footer-left"></div>').append(window.reportLayout.getStandardFooterImage()));
                footer.append('<div class="footer-middle">Haglof Application Cloud<br/>www.haglof.app</div>');
                footer.append($('<div class="footer-right"/>').append(page_counter));
    
                page.append(header);
                page.append(content);
                page.append(footer);

                return page;
            }
        </script>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <style>
            body {
                margin: 0px;
                background-color: rgb(216, 216, 216);
            }

            .content {
                position: relative;
                height:287mm;
                width:200mm;
                padding:5mm;
                background-color: white;
            }

            .header {
                width: 100%;
                padding: 0px;
                margin: 0px;
            }

            .header_image_div {
                display: inline-block;
                vertical-align: middle;
                width: 20%;
            }

            .header h2 {
                display: inline-block;
                text-align: right;
                vertical-align: middle;
                width: 80%;
            }

            .label {
                font-weight:bold;
            }

            .footer {
                position: absolute;
                bottom: 0;
                padding-bottom: 5mm;
                width: 200mm;
            }

            .footer-left {
                display: inline-block;
                text-align: left;
                vertical-align: top;
                width: 66mm;
            }

            .footer-middle {
                display: inline-block;
                text-align: center;
                vertical-align: top;
                width: 65mm;
            }

            .footer-right {
                display: inline-block;
                text-align: right;
                vertical-align: top;
                width: 66mm;      
            }
            

            table.header_table th {
                text-align: right;
                
            }

            table.sample_trees_table {
                width:80%;
            }

            table.sample_trees_table td {
                text-align: right;
            }

            table.sample_trees_table th {
                text-align: right;
            }

            table.plots_table {
                width:60%;
            }

            table.plots_table tr td:first-child {
                width: 5%;
            }

            table.plots_table tr th:first-child {
                width: 5%;
            }

            table.plots_table td {
                text-align: right;
            }

            table.plots_table th {
                text-align: right;
            }

            table.class_table {
                width:80%;
            }

            table.class_table tr th:first-child {
                width: 20%;
                text-align: left;
            }


            table.class_table td {
                text-align: right;
                width: 16%;
            }

            table.class_table th {
                text-align: right;
            }

            @media print {
                @page { margin: 0; }
                body { margin: 0; }
                div.background {
                    opacity: 0;
                    visibility: hidden;
                }
            }
        
        </style>
    </head>
    <body>

    </body>
    </html>
</xsl:template>

</xsl:stylesheet>
