<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <html>
        <body>

        <table class="logs_table">
            <tr>
                <th>#</th>
                <th>Id</th>
                <th>Species</th>
                <th>Quality</th>
                <th>Diameter [mm]</th>
                <th>Length [dm]</th>
                <th>On Bark</th>
            </tr>

            <xsl:for-each select="/Stands/Stand/Species/Specie/Trees/Tree[@IsLog=1]">

                <tr>
                    <td><xsl:value-of select="position()"/></td>
                    <td><xsl:value-of select="@Id"/></td>
                    <td><xsl:value-of select="@Species"/></td>
                    <td><xsl:value-of select="@Quality"/></td>
                    <td><xsl:value-of select="@Diameter"/></td>
                    <td><xsl:value-of select="@Length"/></td>
                    <td><xsl:value-of select="@OnBark"/></td>
                </tr>

            </xsl:for-each>

    </table>

        </body>
    </html>
</xsl:template>

</xsl:stylesheet>
