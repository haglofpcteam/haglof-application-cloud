<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <html>
        <body>

        <table class="trees_table">
            <tr>
                <th>#</th>
                <th>Plot</th>
                <th>Plot area [m²]</th>
                <th>Species</th>
                <th>Diameter [mm]</th>
                <th>Height 1 [dm]</th>
                <th>Height 2 [dm]</th>
                <th>Height 3 [dm]</th>
            </tr>

            <xsl:for-each select="/Stands/Stand/Species/Specie/Trees/Tree[@IsLog=0]">

                <tr>
                    <td><xsl:value-of select="position()"/></td>
                    <td><xsl:value-of select="@PlotNumber"/></td>
                    <td><xsl:value-of select="@PlotAreal"/></td>
                    <td><xsl:value-of select="@Species"/></td>
                    <td><xsl:value-of select="@Diameter"/></td>
                    <td><xsl:value-of select="@Height1"/></td>
                    <td><xsl:value-of select="@Height2"/></td>
                    <td><xsl:value-of select="@Height3"/></td>
                </tr>

            </xsl:for-each>

    </table>

        </body>
    </html>
</xsl:template>

</xsl:stylesheet>
