class Preprocessor {

	constructor(data) {
		this.data = data;
	}
	getData()
	{
        let species = [];
        
        for (const s of this.data.species) {
            species[s.id] = {
                avgDia: 0,
                avgDia2: 0,
                avgDia3: 0,
                nTotDia: 0.0,
                nTotDia2: 0.0,
                nNumDia: 0
            } 
        }

        for (const tree of this.data.trees) {
            let s = species[tree.m_d_i_i_specie_id];
            s.nTotDia += tree.diameter;
            s.nTotDia2 += Math.pow(tree.diameter,2);
            s.nNumDia += 1;
        }


        for (const s of species) {
            if(typeof(s)==='undefined') {
                continue;
            }
            if(s.nNumDia>0) {
                s.avgDia = s.nTotDia / s.nNumDia;
                s.avgDia2 = Math.sqrt((s.nTotDia2 / s.nNumDia));
            }
        }

        for (const s of this.data.species) {
            s.result=species[s.id];
        }
				
		return this.data;
	}

};

const json = require('./testdata.json');

let proc = new Preprocessor(json);

console.log(JSON.stringify(proc.getData(),undefined,2));