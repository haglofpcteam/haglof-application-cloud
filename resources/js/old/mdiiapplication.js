export default class {
    constructor(mdiicom_area_id,application_id,is_mobile)
    {
        this.mdiicom_area_id=mdiicom_area_id;
        this.application_id=application_id;
        this.is_mobile=is_mobile;
        this.collapse_panel = {};

        (function ($) {
            $.each(['show', 'hide'], function (i, ev) {
              var el = $.fn[ev];
              $.fn[ev] = function () {
                this.trigger(ev);
                return el.apply(this, arguments);
              };
            });
          })(jQuery);
    }

    load() {
        var context = this;
        jQuery(document).ready(function($) {
            if(context.is_mobile) {
                context.mobileCreateLayout();
            }
            else {
                context.createLayout();
            }
            
            context.loadReports();
        });
    }

    createLayout() {
        var context = this;
        var area=document.getElementById(context.mdiicom_area_id);
        area = context.application_area = $(area);
        
        var body = context.menu = $('<div></div').addClass('card-body');
        var header = context.menu_header = $('<div></div').addClass('card-header').addClass('text-white').addClass('bg-dark');

        var menu = $('<div></div').addClass('card').append(header).append(body);
        menu = context.menu_area =$('<div></div').addClass('col-4').append(menu);

        var file_area = context.file_area = $('<div></div').addClass('col-8');

        area.append(menu);
        area.append(file_area);

        context.loadMenu();
    }

    mobileCreateLayout() {
        var context = this;
        var area=document.getElementById(context.mdiicom_area_id);
        area = context.application_area = $(area);
        
        var body = context.menu = $('<div></div').addClass('card-body');
        var header = context.menu_header = $('<div></div').addClass('card-header').addClass('text-white').addClass('bg-dark').html("Files");

        var menu = $('<div></div').addClass('card').append(header).append(body);
        menu = context.menu_area =$('<div></div').addClass('col-12').append(menu);

        var file_area = context.file_area = $('<div></div').addClass('col-12').hide();

        context.overlayStack = [];
        context.currentOverlay = context.file_area;
        
        area.append(menu);
        area.append(file_area);

        context.loadMenu();
    }

    mobileToggleMenu() {
        var context = this;
        context.menu_area.toggle();

        if(context.menu_area.is(":hidden")){
            context.file_area.show();
        }
        else {
            context.file_area.hide();
        }
    }

    reload() {
        if(this.file_id!=null) {
            this.loadfile(this.file_id);
        }

        if(this.folder_id!=null) {
            this.loadfolder(this.folder_id);
        }

        this.loadMenu();
    }

    loadMenu() {
        var context = this;
        $.get('/mdiicom/' + this.application_id + '/menu').done(function(data) {

            var rootfolder = jQuery.parseJSON(data);
            var menu = $('<div class="list-group"></div>');
            for (var i=0;i<rootfolder.subfolders.length;i++) {
                menu.append(context.loadMenuFolder(rootfolder.subfolders[i]));
            }
            context.menu.html(menu);

            var create_folder_link = $('<a class="clickable-element" data-toggle="modal"><i class="fas fa-plus"></i></a>');

            create_folder_link.click(function() {
                context.showCreateFolderModal(rootfolder);
            });
            
            var header_body = $('<div class="row"/>');
            header_body.append('<div class="col-10">Folders</div>');
            header_body.append($('<div class="col-2"/>').append(create_folder_link));

            context.menu_header.html(header_body);
        });
    }

    loadMenuFolder(folder) {

        var context = this;

        var panel_body = $('<div class="panel-collapse collapse"></div').data("id",folder.folder.id);
        var panel = $('<div class="panel panel-default"></div');

        if (typeof context.collapse_panel["menu_panel" + folder.folder.id] == 'undefined') {
            context.collapse_panel["menu_panel" + folder.folder.id]=true;
        }

        if(!context.collapse_panel["menu_panel" + folder.folder.id]) {
            panel_body.collapse("show");
        }

        for (var i=0;i<folder.subfolders.length;i++) {
            panel_body.append(context.loadMenuFolder(folder.subfolders[i]));
        }

        var folder_link_edit_icon = $('<i class="fa fa-cog col-1 d-none my-auto"/>')

        folder_link_edit_icon.click(function() {
            context.showEditFolderModal(folder);
        });

        var upload_link = $('<a class="clickable-element d-none my-auto col-1" data-toggle="modal"><i class="fas fa-file-upload"></i></a>');

        var folder_link_body = $('<div class="row h-100"/>')
                                .html('<i class="far fa-folder pr-1 col-1 my-auto"></i>')
                                .append($('<div class="col-8 h-100"/>').append(folder.folder.name))
                                .append(folder_link_edit_icon)
                                .append(upload_link);

        var folder_link = $('<a class="list-group-item list-group-item-action mdiifolder clickable-element"></a>')
                        .data("toggle","collapse")
                        .data("id",folder.folder.id)
                        .html(folder_link_body)
                        .click(function() {
                            panel_body.collapse("toggle");
                            panel.toggleClass("mdii_menu_panel_highlight");
                            context.collapse_panel["menu_panel" + folder.folder.id]=!context.collapse_panel["menu_panel" + folder.folder.id];
                            context.loadfolder(folder.folder.id);
                        })
                        .mouseover(function() {
                            folder_link_edit_icon.removeClass('d-none');
                            upload_link.removeClass('d-none');
                        })
                        .mouseout(function() {
                            folder_link_edit_icon.addClass('d-none');
                            upload_link.addClass('d-none');
                        });

        upload_link.click(function() {
            context.showUploadModal(folder.folder);
        });

        //panel_body.append(context.createCreateFolderLink(folder));

        var panel_heading = $('<div></div').addClass('panel-heading').append(folder_link);

        panel.append(panel_heading).append(panel_body);

        return $('<div></div').addClass('panel-group').append(panel);

    }

    showDownloadModal(stand) {
        var context = this;
        var content = $('<div class="form-group w-50 mx-auto"/>');

        var dTreesButton = $('<a class="btn btn-primary btn-block" target="_blank" href="#" role="button">Download Tree list</a>');
        var dLogsButton = $('<a class="btn btn-primary btn-block" target="_blank" href="#" role="button">Download Logs list</a>');

        dTreesButton.attr("href","/mdiicom/folder/file/stand/" + stand.data('id') + "/csv/trees/download");
        dLogsButton.attr("href","/mdiicom/folder/file/stand/" + stand.data('id') + "/csv/logs/download");

        content.append(dTreesButton);
        content.append(dLogsButton);

        var modal = this.createModal("Download data",content);

        modal.modal('show');
    }

    showUploadModal(folder) {
        var context = this;

        var form =  $('<form enctype="multipart/form-data"></form>');
        var file_form_group = $('<div class="form-group"></div>');
        var custom_file_div = $('<div class="custom-file"></div>');
        var file_name_form_group = $('<div class="form-group"></div>');

        var file_label = $('<label for="mdiifile">Digitech File</label>');
        var file_input = $('<input type="file" class="custom-file-input" name="mdiifile" accept=".mdii.xml,.dbt.xml" required>');
        var custom_file_label = $('<label class="custom-file-label">Digitech File</label>');

        var file_name_label = $('<label for="file_name">File name</label>');
        var file_name_input = $('<input type="text" class="form-control file-name" name="file_name" autofocus>');

        var submit_button = $('<button type="button" class="btn btn-default">Upload</button>');

        custom_file_div.append(file_input);
        custom_file_div.append(custom_file_label);

        file_form_group.append(file_label);
        file_form_group.append(custom_file_div);

        file_name_form_group.append(file_name_label);
        file_name_form_group.append(file_name_input);

        form.append(file_form_group);
        form.append(file_name_form_group);

        file_input.on('change',function(){
            var fileName = $(this).val().replace(/^.*[\\\/]/, '');

            custom_file_label.html(fileName);
            file_name_input.val(fileName);
        })

        submit_button.click(function() {
            form.submit();
        });

        var modal = this.createModal("Upload file",form,submit_button);

        form.validate(
        {
            rules:
            {
                file_name:{ required: true },
            },
            messages:
            {
                file_name:
                {
                    required:"File name cannot be empty"
                }
            },
            errorPlacement: function(error, element) {
                $(error).addClass("text-danger").addClass("pt-2");
                var p = element.parent();
                error.appendTo( p );
                $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
            },
            submitHandler: function() { 
                modal.modal('hide');

                var f = file_input.prop('files')[0];

                var reader = new FileReader();
                var xmlDoc;

                reader.readAsText(f, "UTF-8");

                reader.onload = function(evt) {

                    var start = Date.now();
                    var text = evt.target.result;

                    if (window.DOMParser) {
                        var parser = new DOMParser();
                        xmlDoc = parser.parseFromString(text,"text/xml");
                    } 
                    else {
                        xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                        xmlDoc.async = false;
                        xmlDoc.loadXML(text);
                    } 

                    var file_json=context.xml2json(xmlDoc).replace("undefined","");

                    var formData = new FormData();
                    formData.append("name", file_name_input.val());
                    formData.append("mdiifile_data", file_json);

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "/mdiicom/folder/" + folder.id + "/file/create",
                        type: "POST",
                        data:  formData,
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function(data)
                        {
                            context.showResponseMessage(data);
                            context.reload();
                        },
                        error: function(e) 
                        {
                            context.showMessage(e,"danger");
                        }          
                   });
                }
            }
        });

        modal.modal('show');
    }

    showConfirmModal(title,message,yesfunc,nofunc) {
        var context=this;

        var footer = $('<div></div>');

        var yes_button = $('<button type="button" class="btn btn-default">Yes</button>');
        var no_button = $('<button type="button" class="btn btn-default">No</button>');

        footer.append(yes_button).append(no_button);
        
        var modal = context.createModal(title,message,footer,false);

        no_button.click(function() {
            if(context.isFunction(nofunc)) {
                nofunc();
            }
            modal.modal('hide');
        });

        yes_button.click(function() {
            if(context.isFunction(yesfunc)) {
                yesfunc();
            }
            modal.modal('hide');
        });

        modal.modal('show');
    }

    showResponseMessage(json) {

        this.showMessage(json.message,json.code);
    }

    showMessage(message,type) {
        var session_message = $('<div class="session-message"></div>');
        var alert_div = $('<div class="alert"></div>');

        alert_div.addClass("alert-" + type);

        alert_div.html(message);
        session_message.append(alert_div);

        var box = $('<div class="alert-message-box"></div>');
        box.append(session_message);
        box.appendTo('body');

        setTimeout(function() {
            session_message.animate({height: 0, opacity: 0}, 'slow', function() {
                session_message.remove();
            });
        }, 2000);
    }

    showEditFileModal(file) {
        var context = this;

        var form =  $('<form></form>');
        var file_name_form_group = $('<div class="form-group"></div>');
        var file_name_label = $('<label for="file_name">File name</label>');
        var file_name_input = $('<input id="file_name" type="text" class="form-control" name="name" autofocus>');

        file_name_input.val(file.name)

        file_name_form_group.append(file_name_label);
        file_name_form_group.append(file_name_input);

        form.append(file_name_form_group);

        var submit_button = $('<button type="button" class="btn btn-default">Update file</button>');
        var delete_button = $('<button type="button" class="btn btn-danger">Delete file</button>');

        submit_button.click(function() {
            form.submit();
        });

        var footer = $('<div/>');

        footer.append(delete_button);
        footer.append(submit_button);

        var modal = this.createModal("Edit File",form,footer);

        form.validate(
        {
            rules:
            {
                name:{ required: true },
            },
            messages:
            {
                name:
                {
                    required:"Filename cannot be empty"
                }
            },
            errorPlacement: function(error, element) {
                $(error).addClass("text-danger").addClass("pt-2");
                var p = element.parent();
                error.appendTo( p );
                $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
            },
            submitHandler: function() { 
                modal.modal('hide');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/mdiicom/folder/file/" + file.id + "/update",
                    type: "POST",
                    data:  new FormData(form[0]),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
                        context.reload();
                    },
                    error: function(e) 
                    {
                        context.showMessage(e,"danger");
                    }          
               });
            }

        });

        modal.modal('show');

        delete_button.click(function() {
            context.showDeleteFileModal(file);
            modal.modal('hide');
        });
    }

    showDeleteFileModal(file) {
        var context = this;

        var delete_button = $('<button type="button" class="btn btn-danger">Delete file</button>');

        var modal = context.createModal('Edit File','Do you want to delete file "' + file.name + '"?',delete_button,true);

        delete_button.click(function() {
            var url = "/mdiicom/folder/file/" + file.id + "/destroy";

            $.get(url).done(function(data) {
                modal.modal('hide');
                context.reload();
            });
        });

        modal.modal('show');
    }

    showEditFolderModal(folder) {
        var context = this;

        var form =  $('<form></form>');
        var folder_name_form_group = $('<div class="form-group"></div>');
        var folder_name_label = $('<label for="folder_name">Folder name</label>');
        var folder_name_input = $('<input id="folder_name" type="text" class="form-control" name="name" autofocus>');

        folder_name_input.val(folder.folder.name)

        folder_name_form_group.append(folder_name_label);
        folder_name_form_group.append(folder_name_input);

        form.append(folder_name_form_group);

        var submit_button = $('<button type="button" class="btn btn-default">Update folder</button>');
        var delete_button = $('<button type="button" class="btn btn-danger">Delete folder</button>');

        submit_button.click(function() {
            form.submit();
        });

        var footer = $('<div/>');

        footer.append(delete_button);
        footer.append(submit_button);

        var modal = this.createModal("Edit Folder",form,footer);

        form.validate(
        {
            rules:
            {
                name:{ required: true },
            },
            messages:
            {
                name:
                {
                    required:"Folder name cannot be empty"
                }
            },
            errorPlacement: function(error, element) {
                $(error).addClass("text-danger").addClass("pt-2");
                var p = element.parent();
                error.appendTo( p );
                $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
            },
            submitHandler: function() { 
                modal.modal('hide');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/mdiicom/folder/" + folder.folder.id + "/update",
                    type: "POST",
                    data:  new FormData(form[0]),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
                        context.reload();
                    },
                    error: function(e) 
                    {
                        context.showMessage(e,"danger");
                    }          
               });
            }

        });

        modal.modal('show');

        delete_button.click(function() {
            context.showDeleteFolderModal(folder);
            modal.modal('hide');
        });
    }

    showDeleteFolderModal(folder) {
        var context = this;

        var delete_button = $('<button type="button" class="btn btn-danger">Delete folder</button>');

        var modal = context.createModal('Edit Folder','Do you want to delete folder "' + folder.folder.name + '"?',delete_button,true);

        delete_button.click(function() {
            var url = "/mdiicom/folder/" + folder.folder.id + "/destroy";

            $.get(url).done(function(data) {
                modal.modal('hide');
                context.reload();
            });
        });

        modal.modal('show');
    }

    showCreateFolderModal(folder) {
        var context = this;

        var form =  $('<form></form>');
        var folder_name_form_group = $('<div class="form-group"></div>');
        var folder_name_label = $('<label for="folder_name">Folder name</label>');
        var folder_name_input = $('<input id="folder_name" type="text" class="form-control" name="folder_name" autofocus>');

        folder_name_form_group.append(folder_name_label);
        folder_name_form_group.append(folder_name_input);

        form.append(folder_name_form_group);

        var submit_button = $('<button type="button" class="btn btn-default">Create folder</button>');

        submit_button.click(function() {
            form.submit();
        });

        var modal = this.createModal("Create Folder",form,submit_button);

        form.validate(
        {
            rules:
            {
                folder_name:{ required: true },
            },
            messages:
            {
                folder_name:
                {
                    required:"Folder name cannot be empty"
                }
            },
            errorPlacement: function(error, element) {
                $(error).addClass("text-danger").addClass("pt-2");
                var p = element.parent();
                error.appendTo( p );
                $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
            },
            submitHandler: function() { 
                modal.modal('hide');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/mdiicom/folder/" + folder.folder.id + "/createfolder",
                    type: "POST",
                    data:  new FormData(form[0]),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: function(data)
                    {
                        context.showResponseMessage(data);
                        context.reload();
                    },
                    error: function(e) 
                    {
                        context.showMessage(e,"danger");
                    }          
               });
            }

        });

        modal.modal('show');
    }

    createModal(title,body,footer,show_dismiss_button=true) {
        var modal = $('<div class="modal fade" role="dialog"></div>');
        var dialog = $('<div class="modal-dialog"></div>');
        var content = $('<div class="modal-content"></div>');
        var header = $('<div class="modal-header"></div>');
        var title_div = $('<h4 class="modal-title"></h4>');
        var dismiss_button = $('<button type="button" class="close">&times;</button>');
        var body_div = $('<div class="modal-body"></div>');
        var footer_div = $('<div class="modal-footer"></footer>');
        var close_button = $('<button type="button" class="btn btn-default">Close</button>');

        title_div.append(title);
        header.append(title);
        header.append(dismiss_button);
        body_div.append(body);
        footer_div.append(footer);

        if(show_dismiss_button===true) {
            footer_div.append(close_button);
        }
        
        content.append(header);
        content.append(body_div);
        content.append(footer_div);

        dialog.append(content);

        modal.append(dialog);

        var destroy_modal = function() {
            modal.modal('hide');
        };

        modal.on('hidden.bs.modal', function (e) {
            modal.remove();
        })

        close_button.click(destroy_modal);

        dismiss_button.click(destroy_modal);

        modal.appendTo('body');

        return modal;
    }

    createOverlay(title,body) {
        var context = this;
        if(context.is_mobile) {
            return context.mobileCreateOverlay(title,body);
        }
        else {
            return context.fullCreateOverlay(title,body);
        }
    }

    fullCreateOverlay(title,body) {
        var overlay = $('<div id="overlay">');
        var box = $('<div class="col-md-8 mx-auto mt-5">').draggable();
        var card = $('<div class="card border-3">');
        var card_body = $('<div class="card-body">');
        var card_header = $('<div class="card-header text-white bg-dark"></div>');
        var dismiss_button = $('<button type="button" class="text-white close">&times;</button>');

        overlay.append(box);
        box.append(card);
        card.append(card_header);
        card.append(card_body);
        card_header.append(title);
        card_header.append(dismiss_button);
        card_body.append(body);

        overlay.on('click', function(e) { 
            if( e.target == this )  {
                overlay.fadeOut('fast',function() {
                    overlay.remove();
                });
            }
         });


        dismiss_button.click(function() {
            overlay.fadeOut('fast',function() {
                overlay.remove();
            });
        });

        overlay.appendTo('body');

        return overlay;
    }

    mobileCreateOverlay(title,body) {
        var context = this;
        var card = $('<div class="card">');
        var card_body = $('<div class="card-body">');
        var card_header = $('<div class="card-header text-white bg-dark"></div>');
        var dismiss_button = $('<button type="button" class="text-white close">&times;</button>');

        card.append(card_header);
        card.append(card_body);
        card_header.append(title);
        card_header.append(dismiss_button);
        card_body.append(body);
        card = $('<div></div').addClass('col-12').append(card);


        card.on('remove',function() {
            context.currentOverlay = context.overlayStack.pop();
            context.currentOverlay.show();
        });

        dismiss_button.click(function() {
            card.hide();
            card.remove();
        });

        context.overlayStack.push(context.currentOverlay);
        context.currentOverlay.hide();
        context.currentOverlay=card;
        
        context.application_area.append(card);

        return card;
    }

    loadfile(file_id) {
        this.file_id=file_id;
        this.folder_id=null;
        var context = this;
        $.get('/mdiicom/folder/file/' + file_id + '/data').done(function(data) {
            context.parseData(jQuery.parseJSON(data));
        });
    }

    loadfolder(folder_id) {
        this.file_id=null;
        this.folder_id=folder_id;
        var context = this;

        $.get('/mdiicom/folder/' + folder_id + '/data').done(function(data) {
            context.parseData(jQuery.parseJSON(data)[0].files);
        });
        
    }

    parseData(data) {
        var context = this;
        $(context.file_area).html("");

        var files = $('<div></div>');

        for (var i = 0; i < data.length; i++) {
            var card = context.createCardFromFile(data[i]);
            files.append(card);
        }

        $(context.file_area).append(files);

        if(context.is_mobile) {
            context.mobileToggleMenu();
        }
        
    }

    createCardFromFile(fileData) {
        var dismiss_button = $('<button type="button" class="col-1 close text-white">&times;</button>');
        
        var title_row = $('<div class="row"/>');
        title_row.append($('<div class="col-10"/>').append(fileData.name));
        var settings_button = $('<i class="fa fa-cog text-white col-1 my-auto"/>');
        title_row.append(settings_button);

        settings_button.click(function() {
            context.showEditFileModal(fileData);
        });

        var header = $('<div></div').addClass('card-header').addClass('text-white').addClass('bg-dark').html(title_row);
        var body = $('<div></div').addClass('card-body').addClass('stand-container').data("id",fileData.id).data("name",fileData.name);
        var context = this;

        if(context.is_mobile) {
            title_row.append(dismiss_button);

            dismiss_button.click(function() {
                context.mobileToggleMenu();
            });
        }

        var standList = $('<div class="list-group"></div>');

        body.append(standList);

        for (var i=0;i<fileData.stands.length;i++) {
            var standIcon = $('<i class="far fa-file-alt pr-1"></i>');

            var stand = $('<div class="list-group-item stand-list-item draggable-list-item"></div>');

            var standName=fileData.stands[i].name;

            if(standName==null||standName.lenght===0) {
                standName="Inventory " + (i+1);
            }

            stand.append(standIcon).append(standName);
    
            stand.data("id",fileData.stands[i].id);
            stand.data("name",standName);

            stand.draggable({
                opacity: .4,
                revert : 'invalid',
                stack : '.stand-list-item'
            });

            stand.click(function() {
                context.showStand($(this));
            });

            stand.addClass("owner-file-" + fileData.id);

            stand.droppable({
                activeClass: "stand-icon-div-highlight",
                accept: ".owner-file-" + fileData.id,
                drop: function( event, ui ) {
                    var $this = $(this);
                    ui.draggable.position({
                        my: "center",
                        at: "center",
                        of: $this,
                        using: function(pos) {
                            $(this).animate(pos, 200, "linear");
                        }
                    });
                    context.mergeStands($(this),ui.draggable);
                }
              }); 

              standList.append(stand);

        }



        body.droppable({
            accept: ".stand-list-item:not(.owner-file-" + fileData.id + ")",
            activeClass: "stand-container-hightlight",
            drop: function( event, ui ) {
                var $this = $(this);
                context.moveStandToContainer(ui.draggable,$(this));
            }
        });

        
        var output = $('<div></div').addClass('card').append(header).append(body);
        output = $('<div></div').addClass('col-12').append(output);
        output = $('<div></div').addClass('row mb-3').append(output);
        return output;
    }

    moveStandToContainer(stand,container) {
        var context = this;

        context.showConfirmModal(
            "Move Stand",
            "Do you want to move  " + stand.data('name') + " to " + container.data('name') + "?",
            function() {
                var url = '/mdiicom/folder/file/' + container.data("id") + '/movestand/' + stand.data("id");

                $.get(url).done(function(data) {
                    context.reload();
                });
            },
            function() {
                context.reload();
            });
    }

    mergeStands(stand1,stand2) {
        var context = this;

        context.showConfirmModal(
            "Merge Stands",
            "Do you want to merge " + stand1.data('name') + " and " + stand2.data('name') + "?",
            function() {
                var url = "/mdiicom/folder/file/stand/merge/" + stand1.data("id") + "/" + stand2.data("id");

                $.get(url).done(function(data) {
                    context.reload();
                });
            },
            function() {
                context.reload();
            });
    }

    showStand(stand) {
        var context = this;

        $.get('/mdiicom/folder/file/stand/' + stand.data('id') + '/data').done(function(data) {
            var data_object =jQuery.parseJSON(data);

            var stand_data = stand.stand_data = data_object.stand[0];
            var species = data_object.species;
            var qualities = data_object.qualities;

            var reports_div = $('<div class="col border border-1 p-3 m-1"></div>');
            var summary = $('<div class="col border border-1 p-3 m-1"></div>');
            var species_div = $('<div class="col border border-1 p-3 m-1 overflow-auto"></div>');
            var qualities_div = $('<div class="col border border-1 p-3 m-1 overflow-auto"></div>');

            summary.append('<h5 class="mb-3">Summary</h5>');
            summary.append("Number of trees: ");
            summary.append('<span class="text-right">' + stand_data.trees_count + '</span>');
            summary.append("<br>");
            summary.append("Number of sample trees: " + stand_data.sample_trees_count);
            summary.append("<br>");
            summary.append("Number of plots: " + stand_data.plots_count);

            var specie_list = $('<div class="list-group"></div>');

            for(var i=0;i<Math.min(species.length,4);i++) {
                var specie = $('<div class="list-group-item specie-item specie-shortlist-item draggable-list-item"></div>');
                var standard = species[i].m_d_i_i_application_id !== null;

                specie.append(species[i].specie_text);
                specie.append(' (' + species[i].trees_count + ')');

                specie.data("id",species[i].id);
                specie.data("name",species[i].specie_text);
                specie.data("number",species[i].specie_nr);
                specie.data("standard",standard);
                specie.data('form_factor',species[i].form_factor)
                specie.data('form_height',species[i].form_height)
                
                if(standard===true) {
                    specie.addClass('font-weight-bold');
                }
                else {
                    specie.draggable({
                        opacity: .4,
                        revert : 'invalid',
                        stack : '.specie-shortlist-item'
                    });
                }

                specie.droppable({
                    activeClass: "specie-list-item-highlight",
                    accept: '.specie-shortlist-item',
                    drop: function( event, ui ) {
                        var $this = $(this);
                        ui.draggable.position({
                            my: "center",
                            at: "center",
                            of: $this,
                            using: function(pos) {
                                $(this).animate(pos, 200, "linear");
                            }
                        });
                        
                        var specie1 = $(this);
                        var specie2 = ui.draggable;

                        context.showConfirmModal(
                            "Merge Species",
                            "Do you want to merge " + specie1.data('name') + " and " + specie2.data('name') + "?",
                            function() {
                                var url = "/mdiicom/species/merge/" + specie1.data("id") + "/" + specie2.data("id");
                
                                $.get(url).done(function(data) {
                                    context.showStand(stand);
                                });
                            },
                            function() {
                                context.showStand(stand);
                            });
                    }
                });

                specie.click({s: specie},function(eventObject) {
                    context.editSpecie(eventObject.data.s,function(data)
                    {
                        context.showResponseMessage(data);
                        context.showStand(stand);
                    });
                });

                specie_list.append(specie);
            }

            species_div.append('<h5 class="mb-3">Species</h5>');
            species_div.append(specie_list);
            

            if(species.length>4) {
                var show_all_species_button= $('<button type="button" class="btn btn-light btn-sm mt-2">Show all</button>');
                species_div.append(show_all_species_button);

                show_all_species_button.click(function() {
                    context.showSpecies(stand,species);
                });
            }


            var qualities_list = $('<div class="list-group"></div>');

            for(var i=0;i<Math.min(qualities.length,4);i++) {
                var quality = $('<div class="list-group-item quality-item quality-shortlist-item draggable-list-item"></div>');
                var standard = qualities[i].m_d_i_i_application_id !== null;

                quality.append(qualities[i].quality_text);
                quality.append(' (' + qualities[i].trees_count + ')');

                quality.data("id",qualities[i].id);
                quality.data("name",qualities[i].quality_text);
                quality.data("number",qualities[i].quality_nr);
                quality.data("standard",standard);
                
                if(standard===true) {
                    quality.addClass('font-weight-bold');
                }
                else {
                    quality.draggable({
                        opacity: .4,
                        revert : 'invalid',
                        stack : '.quality-shortlist-item'
                    });
                }

                quality.droppable({
                    activeClass: "quality-list-item-highlight",
                    accept: '.quality-shortlist-item',
                    drop: function( event, ui ) {
                        var $this = $(this);
                        ui.draggable.position({
                            my: "center",
                            at: "center",
                            of: $this,
                            using: function(pos) {
                                $(this).animate(pos, 200, "linear");
                            }
                        });

                        var quality1=$(this);
                        var quality2=ui.draggable;

                        context.showConfirmModal(
                            "Merge Qualities",
                            "Do you want to merge " + quality1.data('name') + " and " + quality2.data('name') + "?",
                            function() {
                                var url = "/mdiicom/qualities/merge/" + quality1.data("id") + "/" + quality2.data("id");
                
                                $.get(url).done(function(data) {
                                    context.showStand(stand);
                                });
                            },
                            function() {
                                context.showStand(stand);
                            });
                    }
                });

                quality.click({q: quality},function(eventObject) {
                    context.editQuality(eventObject.data.q,function(data)
                    {
                        context.showResponseMessage(data);
                        context.showStand(stand);
                    });
                });

                qualities_list.append(quality);
            }

            qualities_div.append('<h5 class="mb-3">Qualities</h5>');
            qualities_div.append(qualities_list);

            if(qualities.length>4) {
                var show_all_qualities_button= $('<button type="button" class="btn btn-light btn-sm mt-2">Show all</button>');
                qualities_div.append(show_all_qualities_button);

                show_all_qualities_button.click(function() {
                    context.showQualities(stand,qualities);
                });
            }            

            reports_div.append('<h5 class="mb-3">Report templates</h5>');
            reports_div.append(context.reports_single)
    
            var title=$('<span></span>').html(stand.data('name'));

            title.click(function() {
                var element = $(this);

                if(element.html().includes('<input')) {
                    return;
                }
                
                var element_text = element.text();
                var input = $('<input type="text" />');
                input.val(element_text);

                element.html(input);
                input.focus();
                
                input.blur(function() {
                    input.replaceWith(element_text);
                })
                .keydown(function(evt){
                    if(evt.keyCode == 13) {
                        var new_text=$(this).val();
                        if(new_text.length>0) {
                            element.html(new_text);
                            context.updateStand(stand,new_text);
                        }
                        else {
                            element.html(element_text);
                        }
                        
                    }
                });
            });

            var generate_button = $('<button type="button" class="btn btn-primary">Generate report</button>');
            var download_button = $('<button type="button" class="btn btn-primary"><i class="fas fa-download"></i></button>');
            var delete_button = $('<button type="button" class="btn btn-secondary">Delete stand</button>');
            var close_button = $('<button type="button" class="btn btn-secondary">Close</button>');
            var edit_data_button = $('<button type="button" class="btn btn-secondary">Edit data</button>');
            var settings_button = $('<button type="button" class="btn btn-secondary">Settings</button>');


            var page = $('<div></div>')

            if(context.is_mobile) {
                var footer = $('<div class="col-12"></div>');
                var body = $('<div class="col-12"></div>');

                reports_div.addClass('mt-2');

                body.append(summary);
                body.append(reports_div);

                var show_qualities_button= $('<button type="button" class="btn btn-secondary col-12 mt-2">Qualities</button>');

                show_qualities_button.click(function() {
                    context.showQualities(stand,qualities);
                });

                var show_species_button= $('<button type="button" class="btn btn-secondary col-12 mt-2">Species</button>');

                show_species_button.click(function() {
                    context.showSpecies(stand,species);
                });

                delete_button.addClass('col-12 mt-2');
                download_button.addClass('col-12 mt-2');
                generate_button.addClass('col-12 mt-2');
                close_button.addClass('col-12 mt-2');
                settings_button.addClass('col-12 mt-2')

                footer.append(generate_button);
                footer.append(delete_button);
                footer.append(settings_button);

                if(species.length>0) {
                    footer.append(show_species_button);
                }

                if(qualities.length>0) {
                    footer.append(show_qualities_button);
                }
            
                footer.append(close_button);
    
                page.append(body).append(footer);
            }
            else {
                var footer = $('<div class="col-11 ml-1 mr-2 mt-2 pt-2"></div>');
                var body = $('<div class="row"></div>');

                var left_column=$('<div class="col-6"></div>');

                left_column.append($('<div class="row"></div>').append(summary));
                left_column.append($('<div class="row"></div>').append(species_div).append(qualities_div));
    
                var right_column=$('<div class="col-6"></div>');
                right_column.append($('<div class="row"></div>').append(reports_div));
    
                body.append(left_column);
                body.append(right_column);

                generate_button.addClass('float-right mr-2');
                download_button.addClass('float-right mr-2');
                delete_button.addClass('float-right mr-2');
                close_button.addClass('float-right mr-2');
                edit_data_button.addClass('float-right mr-2');
                settings_button.addClass('float-right mr-2')

                footer.append(close_button);
                footer.append(edit_data_button);
                footer.append(settings_button);
                footer.append(delete_button);
                footer.append(download_button); 
                footer.append(generate_button);  
    
                footer = $('<div class="row"></div>').append(footer);
    
                page.append(body).append(footer);
                
            }

            var overlay = context.createOverlay(title,page);

            close_button.click(function() {
                overlay.remove();
            });

            download_button.click(function() {
                context.showDownloadModal(stand);
            });
            
            edit_data_button.click(function() {
                context.showDataTable(stand,1,species,qualities);
            });

            settings_button.click(function() {
                context.editStandSettings(stand);
            });

            delete_button.click(function() {
                context.showDeleteStandModal(stand);
            });

            generate_button.click(function() {
                context.reports_single.validate(
                {
                    rules:
                    {
                        template_id:{ required: true },
                    },
                    messages:
                    {
                        template_id:
                        {
                            required:"you have to select a template first"
                        }
                    },
                    errorPlacement: function(error, element) {
                        $(error).addClass("text-danger").addClass("pt-2");
                        var p = element.parent();
                        error.appendTo( p );
                        $(p).addClass("border").addClass("border-danger").addClass("p-2");
                    },
        
                });

                var url='/mdiicom/folder/file/stand/' + stand_data.id + '/report/generate';

                context.reports_single.attr("action",url);

                context.reports_single.submit();

            });

            if (typeof context.stand_overlay != 'undefined') {
                context.stand_overlay.remove();
            }

            context.stand_overlay=overlay;
    
        });
    }

    showDeleteStandModal(stand) {
        var context = this;

        var delete_button = $('<button type="button" class="btn btn-danger">Delete stand</button>');

        var modal = context.createModal('Delete stand','Do you want to delete stand "' + stand.data('name') + '"?',delete_button,true);

        delete_button.click(function() {
            var url = "/mdiicom/folder/file/stand/" + stand.data('id') + "/destroy";

            $.get(url).done(function(data) {
                modal.modal('hide');
                context.stand_overlay.remove();
                context.reload();
            });
        });

        modal.modal('show');
    }

    updateStand(stand,name) {
        var context = this;

        var formData = new FormData();
        formData.append("name", name);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/mdiicom/folder/file/stand/" + stand.data('id') + "/update",
            type: "POST",
            data:  formData,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                context.showResponseMessage(data);
                context.showStand(stand);
                context.reload();
            },
            error: function(e) 
            {
                context.showMessage(e,"danger");
            }          
        });
    }

    editQuality(quality,finishfunction) {
        var context = this;

        var form =  $('<form></form>');

        var quality_name_form_group = $('<div class="form-group"></div>');
        var quality_name_label = $('<label for="quality_name">Quality name</label>');
        var quality_name_input = $('<input id="quality_name" type="text" class="form-control" name="quality_name" autofocus>');

        quality_name_input.val(quality.data('name'));
        quality_name_form_group.append(quality_name_label);
        quality_name_form_group.append(quality_name_input);

        var quality_standard_form_group = $('<div class="form-check"></div>');
        var quality_standard_input = $('<input id="quality_standard" type="checkbox" class="form-check-input" name="quality_standard">');
        var quality_standard_label = $('<label for="quality_standard">Standard</label>');

        quality_standard_input.prop('checked', quality.data('standard'));
        quality_standard_input.val(context.application_id);
        quality_standard_form_group.append(quality_standard_input);
        quality_standard_form_group.append(quality_standard_label);
        
        form.append(quality_name_form_group);
        form.append(quality_standard_form_group);

        var submit_button = $('<button type="button" class="btn btn-default">Save</button>');

        submit_button.click(function() {
            form.submit();
        });

        var modal = this.createModal("Edit Quality",form,submit_button);

        form.validate(
        {
            rules:
            {
                quality_name:{ required: true },
            },
            messages:
            {
                quality_name:
                {
                    required:"quality name cannot be empty"
                }
            },
            errorPlacement: function(error, element) {
                $(error).addClass("text-danger").addClass("pt-2");
                var p = element.parent();
                error.appendTo( p );
                $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
            },
            submitHandler: function() { 
                modal.modal('hide');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/mdiicom/qualities/" + quality.data('id') + "/update",
                    type: "POST",
                    data:  new FormData(form[0]),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: finishfunction,
                    error: function(e) 
                    {
                        context.showMessage(e,"danger");
                    }          
                });
            }

        });

        modal.modal('show');
    }

    editStandSettings(stand) {
        var context = this;

        const footPerMeter=3.2808399;

        var form =  $('<form></form>');

        var measurement_system_form_group = $('<div class="radio"/>');
        measurement_system_form_group.append($('<label for="optradio">Measurement System: </label>'));
        var measurement_system_metric_radio_button = $('<input type="radio" name="m-system"/>');
        var measurement_system_imperial_radio_button = $('<input type="radio" name="m-system"/>');

        measurement_system_form_group.append($('<div class="radio"/>').append($('<label/>').append(measurement_system_metric_radio_button).append(" Metric")));
        measurement_system_form_group.append($('<div class="radio"/>').append($('<label/>').append(measurement_system_imperial_radio_button).append(" Imperial")));

        var inventory_type_form_group  = $('<div class="radio"/>');
        inventory_type_form_group.append($('<label for="optradio">Inventory type: </label>'));
        var inventory_type_total_radio_button = $('<input type="radio" name="i-type"/>');
        var inventory_type_plot_radio_button = $('<input type="radio" name="i-type"/>');

        inventory_type_form_group.append($('<div class="radio"/>').append($('<label/>').append(inventory_type_total_radio_button).append(" Total inventory")));
        inventory_type_form_group.append($('<div class="radio"/>').append($('<label/>').append(inventory_type_plot_radio_button).append(" Plot inventory")));

        var volume_calculation_form_group  = $('<div class="radio"/>');
        volume_calculation_form_group.append($('<label for="optradio">Volume calculation: </label>'));
        var volume_calculation_form_factor_radio_button = $('<input type="radio" name="volume-type"/>');
        var volume_calculation_form_height_radio_button = $('<input type="radio" name="volume-type"/>');

        volume_calculation_form_group.append($('<div class="radio"/>').append($('<label/>').append(volume_calculation_form_factor_radio_button).append(" Form factor")));
        volume_calculation_form_group.append($('<div class="radio"/>').append($('<label/>').append(volume_calculation_form_height_radio_button).append(" Form height")));

        var plot_radius_form_group = $('<div class="form-group"></div>');
        var plot_radius_label = $('<label for="plot_radius">Plot Radius </label>');
        var plot_radius_input = $('<input id="plot_radius" type="text" class="form-control" name="plot_radius" autofocus>');

        plot_radius_form_group.append(plot_radius_label);
        plot_radius_form_group.append(plot_radius_input);

        var plot_areal_form_group = $('<div class="form-group"></div>');
        var plot_areal_label = $('<label for="plot_areal">Plot Areal </label>');
        var plot_areal_input = $('<input id="plot_areal" type="text" class="form-control" name="plot_areal" autofocus>');

        plot_areal_form_group.append(plot_areal_label);
        plot_areal_form_group.append(plot_areal_input);

        var total_areal_form_group = $('<div class="form-group"></div>');
        var total_areal_label = $('<label for="total_areal">Total Areal </label>');
        var total_areal_input = $('<input id="total_areal" type="text" class="form-control" name="total_areal" autofocus>');

        total_areal_form_group.append(total_areal_label);
        total_areal_form_group.append(total_areal_input);

        var use_as_default_form_group = $('<div class="checkbox"/>');
        var use_as_default_input = $('<input type="checkbox" name="use_as_default">');
        var use_as_default_label = $('<label class="ml-2">Use as default</label>');

        use_as_default_form_group.append(use_as_default_input);
        use_as_default_form_group.append(use_as_default_label);

        use_as_default_form_group.wrap('<div class="form-group"></div>');

        var settings = jQuery.parseJSON(stand.stand_data.settings);

        if(settings!=null) {
            if(settings.plot_areal!=null) {
                plot_areal_input.val(settings.plot_areal);
            }
            if(settings.total_areal!=null) {
                total_areal_input.val(settings.total_areal);
            }
            if(settings.plot_diameter!=null) {
                plot_radius_input.val(settings.plot_diameter/2);
            }

            if(settings.volume_calculation!=null) {
                if(settings.volume_calculation==='form_factor') {
                    volume_calculation_form_factor_radio_button.prop('checked',true);
                }
                else {
                    volume_calculation_form_height_radio_button.prop('checked',true);
                }
            }
            else {
                volume_calculation_form_factor_radio_button.prop('checked',true);
            }

            if(settings.inventory_type!=null) {
                if(settings.inventory_type==='total') {
                    inventory_type_total_radio_button.prop('checked',true);
                }
                else {
                    inventory_type_plot_radio_button.prop('checked',true);
                }
            }
            else {
                inventory_type_total_radio_button.prop('checked',true);
            }            

            if(settings.measurement_system!=null) {
                if(settings.measurement_system==='metric') {
                    measurement_system_metric_radio_button.prop("checked", true);
                }
                else {
                    measurement_system_imperial_radio_button.prop("checked", true);
                }
            }
            else {
                measurement_system_metric_radio_button.prop("checked", true);
            }
        }
        else {
            volume_calculation_form_factor_radio_button.prop('checked',true);
            measurement_system_metric_radio_button.prop("checked", true);
            inventory_type_total_radio_button.prop('checked',true);
        }

        var plot_areal_label_unit = $('<span/>');
        var total_areal_label_unit = $('<span/>');
        var plot_radius_label_unit = $('<span/>');

        plot_areal_label.append(plot_areal_label_unit);
        total_areal_label.append(total_areal_label_unit);
        plot_radius_label.append(plot_radius_label_unit);

        var msystem = 'metric';

        var measurement_system_clicked = function() {
            if(measurement_system_metric_radio_button.prop("checked")) {
                plot_areal_label_unit.html(" [m²]");
                total_areal_label_unit.html(" [m²]");
                plot_radius_label_unit.html(" [m]");
                if(msystem!=='metric') {
                    plot_radius_input.val(parseFloat(plot_radius_input.val())/footPerMeter);
                    plot_areal_input.val(parseFloat(plot_areal_input.val())/Math.pow(footPerMeter,2));
                    total_areal_input.val(parseFloat(total_areal_input.val())/Math.pow(footPerMeter,2));
                    msystem='metric';
                }
            }
            else {
                plot_areal_label_unit.html(" [ft²]");
                total_areal_label_unit.html(" [ft²]");
                plot_radius_label_unit.html(" [ft]");
                if(msystem!=='imperial') {
                    plot_radius_input.val(parseFloat(plot_radius_input.val())*footPerMeter);
                    plot_areal_input.val(parseFloat(plot_areal_input.val())*Math.pow(footPerMeter,2));
                    total_areal_input.val(parseFloat(total_areal_input.val())*Math.pow(footPerMeter,2));
                    msystem='imperial';
                }
            }
        }

        var inventory_type_clicked = function() {
            if(inventory_type_total_radio_button.prop("checked")) {
                total_areal_form_group.removeClass('d-none');
                plot_radius_form_group.addClass('d-none');
                plot_areal_form_group.addClass('d-none');
            }
            else {
                total_areal_form_group.addClass('d-none');
                plot_radius_form_group.removeClass('d-none');
                plot_areal_form_group.removeClass('d-none');
            }
        }

        measurement_system_clicked();
        inventory_type_clicked();

        measurement_system_imperial_radio_button.click(measurement_system_clicked);
        measurement_system_metric_radio_button.click(measurement_system_clicked);

        inventory_type_plot_radio_button.click(inventory_type_clicked);
        inventory_type_total_radio_button.click(inventory_type_clicked);

        plot_radius_input.change(function() {
            if($.isNumeric(plot_radius_input.val())===true) {
                var radius = parseFloat(plot_radius_input.val());
                var areal = Math.pow(radius,2)*Math.PI;
                plot_areal_input.val(areal.toFixed(2));
            }
        });

        plot_areal_input.change(function() {
            if($.isNumeric(plot_areal_input.val())===true) {
                var areal = parseFloat(plot_areal_input.val());
                var radius = Math.sqrt(areal/Math.PI);
                plot_radius_input.val(radius.toFixed(2));
            }
        });

        form.append(measurement_system_form_group);
        form.append(inventory_type_form_group);
        form.append(volume_calculation_form_group);
        form.append(plot_radius_form_group);
        form.append(plot_areal_form_group);
        form.append(total_areal_form_group);
        form.append(use_as_default_form_group);


        var submit_button = $('<button type="button" class="btn btn-default">Save</button>');

        submit_button.click(function() {
            form.submit();
        });

        var modal = this.createModal("Settings",form,submit_button);

        form.validate(
            {
                rules:
                {
                    plot_radius:{ number: true },
                    plot_areal:{ number: true },
                    total_areal:{ number: true },
                },
                messages:
                {
                    plot_radius:
                    {
                        number:"Plot radius needs to be a number"
                    },
                    plot_areal:
                    {
                        number:"Plot areal needs to be a number"
                    },
                    total_areal:
                    {
                        number:"Total areal needs to be a number"
                    }

                },
                errorPlacement: function(error, element) {
                    $(error).addClass("text-danger").addClass("pt-2");
                    var p = element.parent();
                    error.appendTo( p );
                    $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
                },
                submitHandler: function() { 
                    modal.modal('hide');
                    var formData = new FormData();
                    var keyValues = [];

                    var plot_areal = parseFloat(plot_areal_input.val());
                    var plot_radius = parseFloat(plot_radius_input.val());
                    var total_areal = parseFloat(total_areal_input.val());

                    if(msystem!='metric') {
                        plot_areal = plot_areal/Math.pow(footPerMeter,2);
                        total_areal = total_areal/Math.pow(footPerMeter,2);
                        plot_radius = plot_radius/footPerMeter;
                    }
                    
                    keyValues[0]={'key' : 'plot_areal','value' : plot_areal};
                    keyValues[1]={'key' : 'total_areal','value' : total_areal};
                    keyValues[2]={'key' : 'plot_diameter','value' : plot_radius*2};
                    keyValues[3]={'key' : 'measurement_system','value' : measurement_system_metric_radio_button.prop("checked") ? 'metric' : 'imperial'};
                    keyValues[4]={'key' : 'inventory_type','value' : inventory_type_total_radio_button.prop("checked") ? 'total' : 'plot'};
                    keyValues[5]={'key' : 'volume_calculation','value' : volume_calculation_form_factor_radio_button.prop("checked") ? 'form_factor' : 'form_height'};

                    formData.append('json',JSON.stringify(keyValues));
                    formData.append('use_as_default',use_as_default_input.prop('checked') ? 1 : 0);

                    $.ajax({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        url: "/mdiicom/folder/file/stand/" + stand.data('id') + "/settings/update",                    
                        type: "POST",
                        data:  formData,
                        contentType: false,
                        cache: false,
                        processData:false,
                        success: function(data)
                        {
                            stand.stand_data.settings = data;
                        },
                        error: function(e) 
                        {
                            context.showMessage(e,"danger");
                        }          
                    });
                }
    
            });
    
            modal.modal('show');
    }
    
    editSpecie(specie,finishfunction) {
        var context = this;

        var form =  $('<form></form>');

        var specie_name_form_group = $('<div class="form-group"></div>');
        var specie_name_label = $('<label for="specie_name">Specie name</label>');
        var specie_name_input = $('<input id="specie_name" type="text" class="form-control" name="specie_name" autofocus>');

        specie_name_input.val(specie.data('name'));
        specie_name_form_group.append(specie_name_label);
        specie_name_form_group.append(specie_name_input);

        var form_factor_form_group = $('<div class="form-group"></div>');
        var form_factor_label = $('<label for="form_factor">Form factor</label>');
        var form_factor_input = $('<input id="form_factor" type="text" class="form-control" name="form_factor">');

        form_factor_input.val(specie.data('form_factor'));
        form_factor_form_group.append(form_factor_label);
        form_factor_form_group.append(form_factor_input);

        var form_height_form_group = $('<div class="form-group"></div>');
        var form_height_label = $('<label for="form_height">Form height</label>');
        var form_height_input = $('<input id="form_height" type="text" class="form-control" name="form_height">');

        form_height_input.val(specie.data('form_height'));
        form_height_form_group.append(form_height_label);
        form_height_form_group.append(form_height_input);

        var specie_standard_form_group = $('<div class="form-check"></div>');
        var specie_standard_input = $('<input id="specie_standard" type="checkbox" class="form-check-input" name="specie_standard">');
        var specie_standard_label = $('<label for="specie_standard">Standard</label>');

        specie_standard_input.prop('checked', specie.data('standard'));
        specie_standard_input.val(context.application_id);
        specie_standard_form_group.append(specie_standard_input);
        specie_standard_form_group.append(specie_standard_label);
        
        form.append(specie_name_form_group);
        form.append(form_factor_form_group);
        form.append(form_height_form_group);
        form.append(specie_standard_form_group);

        var submit_button = $('<button type="button" class="btn btn-default">Save</button>');

        submit_button.click(function() {
            form.submit();
        });

        var modal = this.createModal("Edit Specie",form,submit_button);

        form.validate(
        {
            rules:
            {
                specie_name:{ required: true },
                form_factor:{ required: true, number: true },
                form_height: { required: true, number: true },
            },
            messages:
            {
                specie_name:
                {
                    required:"Specie name cannot be empty"
                },
                form_factor:
                {
                    required:"Form factor cannot be empty",
                    number:"Form factor needs to be a number"
                },
                form_height:
                {
                    required:"Form height cannot be empty",
                    number:"Form height needs to be a number"
                }
            },
            errorPlacement: function(error, element) {
                $(error).addClass("text-danger").addClass("pt-2");
                var p = element.parent();
                error.appendTo( p );
                $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
            },
            submitHandler: function() { 
                modal.modal('hide');
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    url: "/mdiicom/specie/" + specie.data('id') + "/update",
                    type: "POST",
                    data:  new FormData(form[0]),
                    contentType: false,
                    cache: false,
                    processData:false,
                    success: finishfunction,
                    error: function(e) 
                    {
                        context.showMessage(e,"danger");
                    }          
                });
            }

        });

        modal.modal('show');
    }
    

    loadReports() {
        var context = this;
        context.reports_single = $('<form></form>');

        context.reports_single.attr("target","_blank");
            
        context.reports_single.attr("method","POST");

        var token = $("meta[name='csrf-token']").attr("content");

        context.reports_single.append($('<input>').attr('type', 'hidden').attr('name', '_token').attr('value', token));

        $.get('/mdiicom/' + context.application_id + '/single').done(function(data) {
            var reports = jQuery.parseJSON(data);

            var pages = [];

            for(var i=0;i<reports.length;i++) {
                var pagenumber=Math.floor(i/10);
                if(typeof pages[pagenumber] === 'undefined') {
                    pages[pagenumber]=$('<div class="d-none"/>');
                    context.reports_single.append(pages[pagenumber]);
                }

                var report = reports[i];
                var form_check = $('<div class="form-check">');
                var input = $('<input class="form-check-input" type="radio" name="template_id"></input>');
                var label = $('<label class="form-check-label"></label>');

                input.val(report.id);
                label.html(report.name);

                form_check.append(input);
                form_check.append(label);

                if(report.parameters.length>0) {
                    var template_parameters = $('<div class="d-none template_parameters">');

                    input.change(function() {
                        $('.template_parameters').addClass('d-none');
                        template_parameters.removeClass('d-none');
                    });

                    for(var j=0;j<report.parameters.length;j++) {
                        var form_group = $('<div class="form-group mt-4 row"></div>');
                        var parameter_label = $('<label class="col-md-4 col-form-label text-md-right"></label>');
                        var input_div = $('<div class="col-md-8"></div>');
                        var parameter_input = $('<input type="text" class="form-control"></input>');
    
                        parameter_input.attr('name', 'template_' + report.id + '_parameter_' + report.parameters[j].name);
                        parameter_label.html(report.parameters[j].name);
    
                        input_div.append(parameter_input);
                        form_group.append(parameter_label);
                        form_group.append(input_div);
                        template_parameters.append(form_group);
                    }
    
                }
                else {
                    input.change(function() {
                        $('.template_parameters').addClass('d-none');
                    });
                }

                pages[pagenumber].append(form_check);
                pages[pagenumber].append(template_parameters); 
            }

            if(pages.length>1) {
                var pagelist = $('<ul class="pagination"/>');
                var navigation = $('<nav aria-label="Reports navigation"/>')
                navigation.append(pagelist);
                context.reports_single.append('<br/>');
                context.reports_single.append(navigation);
                context.reports_single_current_page=0;

                for(var i=0;i<pages.length;i++) {
                    var li = $('<li class="page-item"/>');
                    var a = $('<a class="page-link" href="#"/>');

                    li.append(a);

                    a.html(i+1);

                    a.click({pageslist: pages,index: i},function(eventObject) {
                        eventObject.data.pageslist[context.reports_single_current_page].addClass('d-none');
                        eventObject.data.pageslist[eventObject.data.index].removeClass('d-none');

                        context.reports_single_current_page=eventObject.data.index;
                    });

                    pagelist.append(li);
                }

                pages[context.reports_single_current_page].removeClass("d-none");
            }
            else {
                pages[0].removeClass("d-none");
            }

        });
    }

    isFunction(functionToCheck) {
        return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
    }

    showQualities(stand,qualities) {

        var context = this;

        var column_count=1;

        if(!context.is_mobile) {
            column_count=Math.min(Math.floor(qualities.length/10),4)+1;
        }

        var columns = []; 
        
        var qualities_list = $('<div class="row"></div>');

        for(var i=0;i<column_count;i++) {
            columns[i]=$('<div class="col list-group"></div>');
            qualities_list.append(columns[i]);
        }

        for(var i=0;i<qualities.length;i++) {
            var quality = $('<div class="list-group-item quality-item quality-fullist-item draggable-list-item"></div>');
            var standard = qualities[i].m_d_i_i_application_id !== null;

            quality.append(qualities[i].quality_text);
            quality.append(' (' + qualities[i].trees_count + ')');

            quality.data("id",qualities[i].id);
            quality.data("name",qualities[i].quality_text);
            quality.data("number",qualities[i].quality_nr);
            quality.data("standard",standard);
            
            if(standard===true) {
                quality.addClass('font-weight-bold');
            }
            else {
                quality.draggable({
                    opacity: .4,
                    revert : 'invalid',
                    stack : '.quality-fullist-item'
                });
            }

            quality.droppable({
                activeClass: "quality-list-item-highlight",
                accept: '.quality-fullist-item',
                drop: function( event, ui ) {
                    var $this = $(this);
                    ui.draggable.position({
                        my: "center",
                        at: "center",
                        of: $this,
                        using: function(pos) {
                            $(this).animate(pos, 200, "linear");
                        }
                    });
                    var quality1=$(this);
                    var quality2=ui.draggable;

                    context.showConfirmModal(
                        "Merge Qualities",
                        "Do you want to merge " + quality1.data('name') + " and " + quality2.data('name') + "?",
                        function() {
                            var url = "/mdiicom/qualities/merge/" + quality1.data("id") + "/" + quality2.data("id");

                            $.get(url).done(function(data) {
                                if(data.code==='success') {
                                    quality2.remove();
                                }
                            });
                        },
                        function() {
                            context.showQualities(stand,qualities);
                        });
                }
            });

            quality.click({q: quality,qa: qualities,st: stand},function(eventObject) {
                context.editQuality(eventObject.data.q,function(data)
                {
                    context.showResponseMessage(data);
                    $.get('/mdiicom/folder/file/stand/' + stand.data('id') + '/data').done(function(data) {
                        var data_object=jQuery.parseJSON(data);
                        qualities = data_object.qualities;
                        context.showQualities(stand,qualities);
                    });
                });
            });

            columns[i%column_count].append(quality);
        }

        var footer =$('<div class="row mt-3"></div>');

        var body=$('<div class="container"></div>');

        if (typeof context.qualities_data_body != 'undefined') {
            body=context.qualities_data_body;
            var overlay = context.qualities_overlay;
        }
        else {
            context.qualities_data_body=body;
            var overlay = context.createOverlay("Qualities list - " + stand.data('name'),body);
            context.qualities_overlay=overlay;

            overlay.on("remove", function () {
                context.qualities_data_body=undefined;
            });
        }
        body.empty();
        body.append(qualities_list);
        body.append(footer);
    }

    showSpecies(stand,species) {
        var context = this;

        var column_count=1;

        if(!context.is_mobile) {
            column_count=Math.min(Math.floor(species.length/10),4)+1;
        }

        var columns = []; 
        
        var specie_list = $('<div class="row"></div>');

        for(var i=0;i<column_count;i++) {
            columns[i]=$('<div class="col list-group"></div>');
            specie_list.append(columns[i]);
        }

        for(var i=0;i<species.length;i++) {
            var specie = $('<div class="list-group-item specie-item specie-fullist-item draggable-list-item"></div>');
            var standard = species[i].m_d_i_i_application_id !== null;

            specie.append(species[i].specie_text);
            specie.append(' (' + species[i].trees_count + ')');

            specie.data("id",species[i].id);
            specie.data("name",species[i].specie_text);
            specie.data("number",species[i].specie_nr);
            specie.data("standard",standard);
            specie.data('form_factor',species[i].form_factor)
            specie.data('form_height',species[i].form_height)
            
            if(standard===true) {
                specie.addClass('font-weight-bold');
            }
            else {
                specie.draggable({
                    opacity: .4,
                    revert : 'invalid',
                    stack : '.specie-fullist-item'
                });
            }

            specie.droppable({
                activeClass: "specie-list-item-highlight",
                accept: '.specie-fullist-item',
                drop: function( event, ui ) {
                    var $this = $(this);
                    ui.draggable.position({
                        my: "center",
                        at: "center",
                        of: $this,
                        using: function(pos) {
                            $(this).animate(pos, 200, "linear");
                        }
                    });
                    var specie2 = $(this);
                    var specie1 = ui.draggable;

                    context.showConfirmModal(
                        "Merge Species",
                        "Do you want to merge " + specie1.data('name') + " and " + specie2.data('name') + "?",
                        function() {
                            var url = "/mdiicom/species/merge/" + specie1.data("id") + "/" + specie2.data("id");
            
                            $.get(url).done(function(data) {
                                if(data.code==='success') {
                                    specie1.remove();
                                }
                            });
                        },
                        function() { context.showSpecies(stand,species); }
                        );
                }
            });

            specie.click({s: specie},function(eventObject) {
                context.editSpecie(eventObject.data.s,function(data)
                {
                    context.showResponseMessage(data);
                    $.get('/mdiicom/folder/file/stand/' + stand.data('id') + '/data').done(function(data) {
                        var data_object=jQuery.parseJSON(data);
                        species = data_object.species;
                        context.showSpecies(stand,species);
                    });
                });
            });

            columns[i%column_count].append(specie);
        }

        var footer =$('<div class="row mt-3"></div>');

        var body=$('<div class="container"></div>');

        if(typeof context.species_overlay != 'undefined') {
            context.species_overlay.remove();
        }

        context.species_overlay=context.createOverlay("Species list - " + stand.data('name'),body);

        body.empty();
        body.append(specie_list);
        body.append(footer);

    }

    showDataTable(stand,page,species,qualities) {
        var context = this;
        $.get('/mdiicom/folder/file/stand/' + stand.data('id') + '/all_data?page=' + page).done(function(data) {

            var page_data = jQuery.parseJSON(data);

            var table = $('<table class="table stand_data_table"></table>');
            var thead = $('<thead> \
                                <tr> \
                                    <th>#</th> \
                                    <th>Species</th> \
                                    <th>Diameter</th> \
                                    <th>Height 1</th> \
                                    <th>Height 2</th> \
                                    <th>Height 3</th> \
                                    <th>Quality</th> \
                                    <th>Log</th> \
                                    <th>On Bark</th> \
                                </tr> \
                            </thead>');
            var tbody = $('<tbody></tbody>');

            table.append(thead);
            table.append(tbody);

            var trees=page_data.data;

            var edit_func = function(eventObject) {
                var element = $(eventObject.target);
                var field = eventObject.data.fieldName;
                var tree = eventObject.data.tree;
                var nullable = eventObject.data.nullable;
                var type = eventObject.data.type;
                var unit = eventObject.data.unit;

                if(element.html().includes('<input')) {
                    return;
                }
                
                var element_text = element.text();
                var input = $('<input type="text" class="form-control"/>');

                if(tree[field]>0) {
                    input.val(tree[field]);
                }
                else {
                    input.val("");
                }
                

                element.html(input);
                input.focus();
                
                input.blur(function() {
                    input.replaceWith(element_text);
                })
                .keydown(function(evt){
                    if(evt.keyCode == 13) {
                        var new_text=$(this).val();
                        if(new_text.length>0||nullable===true) {
                            
                            if(type==='number') {
                                tree[field]=0;
                                if(new_text.length!=0) {
                                    tree[field]=parseInt(new_text);
                                    element.html(tree[field] + unit);
                                }
                                else {
                                    element.html("");
                                }
                            }
                            else if(type==='bigint') {
                                tree[field]=0;
                                if(new_text.length!=0) {
                                    tree[field]=parseFloat(new_text);
                                    element.html(tree[field] + unit);
                                }
                                else {
                                    element.html("");
                                }
                            }
                            else {
                                tree[field]=new_text;
                                element.html(tree[field]);
                            }
                            
                            context.updateTree(tree);
                        }
                        else {
                            element.html(element_text);
                        }
                        
                    }
                });
            };

            for(var i=0;i<trees.length;i++) {
                var row = $('<tr></tr>');

                trees[i].metric=parseInt(trees[i].metric);

                var checkbox_func = function(eventObject) {
                    var field = eventObject.data.fieldName;
                    var tree = eventObject.data.tree;

                    tree[field]=eventObject.target.checked===true ? 1 : 0;

                    context.updateTree(tree);
                }

                var specie_dropdown = $('<select class="form-control center-block"></select>');

                for(var j=0;j<species.length;j++) {
                    var option = $('<option></option>');
                    option.val(species[j].id);
                    option.html(species[j].specie_text);
                    
                    if(parseInt(trees[i].m_d_i_i_specie_id)===parseInt(species[j].id)){
                        option.prop('selected', true);
                    }
    
                    specie_dropdown.append(option);
                }

                specie_dropdown.change({tree: trees[i]},function (eventObject) {
                    var tree = eventObject.data.tree;
                    tree.m_d_i_i_specie_id=this.value;
                    context.updateTree(tree);
                });

                var quality_dropdown = $('<select class="form-control center-block"></select>');

                for(var j=0;j<qualities.length;j++) {
                    var option = $('<option></option>');
                    option.val(qualities[j].id);
                    option.html(qualities[j].quality_text);
                    
                    if(parseInt(trees[i].m_d_i_i_quality_id)===parseInt(qualities[j].id)){
                        option.prop('selected', true);
                    }
    
                    quality_dropdown.append(option);
                }

                var option = $('<option></option>');
                option.val(null);
                option.html("");
                
                if(trees[i].m_d_i_i_quality_id===null){
                    option.prop('selected', true);
                }

                quality_dropdown.append(option);

                quality_dropdown.change({tree: trees[i]},function (eventObject) {
                    var tree = eventObject.data.tree;
                    tree.m_d_i_i_quality_id=this.value;
                    context.updateTree(tree);
                });

                row.append($('<td></td>').html(trees[i].id));
                row.append($('<td></td>').html(specie_dropdown));
                var unit=(trees[i].metric===1?"mm":'"');
                var value=trees[i].diameter==null ? "" : trees[i].diameter + unit;
                row.append($('<td></td>').html(value).click({tree: trees[i], fieldName: 'diameter',type: 'bigint',nullable: false, unit: unit}, edit_func));
                var unit=(trees[i].metric===1?"dm":'ft');
                var value=trees[i].height1==null ? "" : trees[i].height1 + unit;
                row.append($('<td></td>').html(value).click({tree: trees[i], fieldName: 'height1',type: 'bigint',nullable: true, unit: unit}, edit_func));
                var value=trees[i].height2==null ? "" : trees[i].height2 + unit;
                row.append($('<td></td>').html(value).click({tree: trees[i], fieldName: 'height2',type: 'bigint',nullable: true, unit: unit}, edit_func));
                var value=trees[i].height3==null ? "" : trees[i].height3 + unit;
                row.append($('<td></td>').html(value).click({tree: trees[i], fieldName: 'height3',type: 'bigint',nullable: true, unit: unit}, edit_func));
                row.append($('<td></td>').html(quality_dropdown));

                var log_checkbox=$('<input type="checkbox"/>').prop('checked', parseInt(trees[i].is_log)===1).click({tree: trees[i], fieldName: 'is_log'},checkbox_func);
                var on_bark_checkbox=$('<input type="checkbox"/>').prop('checked', parseInt(trees[i].on_bark)===1).click({tree: trees[i], fieldName: 'on_bark'},checkbox_func);

                row.append($('<td></td>').html(log_checkbox));
                row.append($('<td></td>').html(on_bark_checkbox));

                tbody.append(row);
            }  

            var footer =$('<div class="row"></div>');
            var prev_button = $('<button type="button" class="btn btn-secondary float-left mr-2">Previous</button>').prop('disabled', true);
            var next_button = $('<button type="button" class="btn btn-secondary float-right mr-2">Next</button>').prop('disabled', true);
            var page_dropdown = $('<select class="form-control center-block"></select>');

            for(var i=0;i<page_data.last_page;i++) {
                var option = $('<option></option>');
                option.val(i+1);
                option.html("Page " + (i+1));
                
                if(i===(page-1)){
                    option.prop('selected', true);
                }

                page_dropdown.append(option);
            }

            page_dropdown.on('change', function (e) {
                context.showDataTable(stand,this.value,species,qualities);
            });

            footer.append($('<div class="col-4"></div>').append(prev_button));
            footer.append($('<div class="col-4"></div>').append(page_dropdown));
            footer.append($('<div class="col-4"></div>').append(next_button));

            if(page>1) {
                prev_button.prop('disabled', false);
                prev_button.click(function() {
                    context.showDataTable(stand,page-1,species,qualities);
                });
            }

            if(page<page_data.last_page) {
                next_button.prop('disabled', false);
                next_button.click(function() {
                    context.showDataTable(stand,page+1,species,qualities);
                });
            }

            var body=$('<div class="container"></div>');

            if (typeof context.stand_data_body != 'undefined') {
                body=context.stand_data_body;
            }
            else {
                context.stand_data_body=body;
                var overlay = context.createOverlay("Data Table - " + stand.data('name'),body);

                overlay.on("remove", function () {
                    context.stand_data_body=undefined;
                })
            }

            body.empty();
            body.append(table),
            body.append(footer);

        });
    }

    updateTree(tree) {
        var context = this;
        var form_data = new FormData();

        for(var key in tree) {
            if(typeof tree[key] !== 'object') {
                form_data.append(key, tree[key]);
            } 
        }
        
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "/mdiicom/folder/file/stand/plot/tree/" + tree.id + "/update",
            type: "POST",
            data:  form_data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(data)
            {
                if(data.code=='danger') {
                    context.showResponseMessage(data);
                }
            },
            error: function(e) 
            {
                context.showMessage(e,"danger");
            }          
        });
        
    }

    xml2json(xml, tab) {
        var X = {
           toObj: function(xml) {
              var o = {};
              if (xml.nodeType==1) {   // element node ..
                 if (xml.attributes.length)   // element with attributes  ..
                    for (var i=0; i<xml.attributes.length; i++)
                       o["@"+xml.attributes[i].nodeName] = (xml.attributes[i].nodeValue||"").toString();
                 if (xml.firstChild) { // element has child nodes ..
                    var textChild=0, cdataChild=0, hasElementChild=false;
                    for (var n=xml.firstChild; n; n=n.nextSibling) {
                       if (n.nodeType==1) hasElementChild = true;
                       else if (n.nodeType==3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) textChild++; // non-whitespace text
                       else if (n.nodeType==4) cdataChild++; // cdata section node
                    }
                    if (hasElementChild) {
                       if (textChild < 2 && cdataChild < 2) { // structured element with evtl. a single text or/and cdata node ..
                          X.removeWhite(xml);
                          for (var n=xml.firstChild; n; n=n.nextSibling) {
                             if (n.nodeType == 3)  // text node
                                o["text"] = X.escape(n.nodeValue);
                             else if (n.nodeType == 4)  // cdata node
                                o["cdata"] = X.escape(n.nodeValue);
                             else if (o[n.nodeName]) {  // multiple occurence of element ..
                                if (o[n.nodeName] instanceof Array)
                                   o[n.nodeName][o[n.nodeName].length] = X.toObj(n);
                                else
                                   o[n.nodeName] = [o[n.nodeName], X.toObj(n)];
                             }
                             else  // first occurence of element..
                                o[n.nodeName] = X.toObj(n);
                          }
                       }
                       else { // mixed content
                          if (!xml.attributes.length)
                             o = X.escape(X.innerXml(xml));
                          else
                             o["text"] = X.escape(X.innerXml(xml));
                       }
                    }
                    else if (textChild) { // pure text
                       if (!xml.attributes.length)
                          o = X.escape(X.innerXml(xml));
                       else
                          o["text"] = X.escape(X.innerXml(xml));
                    }
                    else if (cdataChild) { // cdata
                       if (cdataChild > 1)
                          o = X.escape(X.innerXml(xml));
                       else
                          for (var n=xml.firstChild; n; n=n.nextSibling)
                             o["cdata"] = X.escape(n.nodeValue);
                    }
                 }
                 if (!xml.attributes.length && !xml.firstChild) o = null;
              }
              else if (xml.nodeType==9) { // document.node
                 o = X.toObj(xml.documentElement);
              }
              else
                 alert("unhandled node type: " + xml.nodeType);
              return o;
           },
           toJson: function(o, name, ind) {
              var json = name ? ("\""+name+"\"") : "";
              if (o instanceof Array) {
                 for (var i=0,n=o.length; i<n; i++)
                    o[i] = X.toJson(o[i], "", ind+"\t");
                 json += (name?":[":"[") + (o.length > 1 ? ("\n"+ind+"\t"+o.join(",\n"+ind+"\t")+"\n"+ind) : o.join("")) + "]";
              }
              else if (o == null)
                 json += (name&&":") + "null";
              else if (typeof(o) == "object") {
                 var arr = [];
                 for (var m in o)
                    arr[arr.length] = X.toJson(o[m], m, ind+"\t");
                 json += (name?":{":"{") + (arr.length > 1 ? ("\n"+ind+"\t"+arr.join(",\n"+ind+"\t")+"\n"+ind) : arr.join("")) + "}";
              }
              else if (typeof(o) == "string")
                 json += (name&&":") + "\"" + o.toString() + "\"";
              else
                 json += (name&&":") + o.toString();
              return json;
           },
           innerXml: function(node) {
              var s = ""
              if ("innerHTML" in node)
                 s = node.innerHTML;
              else {
                 var asXml = function(n) {
                    var s = "";
                    if (n.nodeType == 1) {
                       s += "<" + n.nodeName;
                       for (var i=0; i<n.attributes.length;i++)
                          s += " " + n.attributes[i].nodeName + "=\"" + (n.attributes[i].nodeValue||"").toString() + "\"";
                       if (n.firstChild) {
                          s += ">";
                          for (var c=n.firstChild; c; c=c.nextSibling)
                             s += asXml(c);
                          s += "</"+n.nodeName+">";
                       }
                       else
                          s += "/>";
                    }
                    else if (n.nodeType == 3)
                       s += n.nodeValue;
                    else if (n.nodeType == 4)
                       s += "<![CDATA[" + n.nodeValue + "]]>";
                    return s;
                 };
                 for (var c=node.firstChild; c; c=c.nextSibling)
                    s += asXml(c);
              }
              return s;
           },
           escape: function(txt) {
              return txt.replace(/[\\]/g, "\\\\")
                        .replace(/[\"]/g, '\\"')
                        .replace(/[\n]/g, '\\n')
                        .replace(/[\r]/g, '\\r');
           },
           removeWhite: function(e) {
              e.normalize();
              for (var n = e.firstChild; n; ) {
                 if (n.nodeType == 3) {  // text node
                    if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) { // pure whitespace text node
                       var nxt = n.nextSibling;
                       e.removeChild(n);
                       n = nxt;
                    }
                    else
                       n = n.nextSibling;
                 }
                 else if (n.nodeType == 1) {  // element node
                    X.removeWhite(n);
                    n = n.nextSibling;
                 }
                 else                      // any other node
                    n = n.nextSibling;
              }
              return e;
           }
        };
        if (xml.nodeType == 9) // document node
           xml = xml.documentElement;
        var json = X.toJson(X.toObj(X.removeWhite(xml)), xml.nodeName, "\t");
        return "{\n" + tab + (tab ? json.replace(/\t/g, tab) : json.replace(/\t|\n/g, "")) + "\n}";
     }

}
