const CubicFeetPerCubicMeter = 35.3147;
const SquareFeetPerSquareMeter = 10.7639;
const SquareFeetPerAcre = 43560.2;
const FeetPerMeter = 3.28084;

exports.VolumePrint = function(metricValue,measurementSystem) {
    if(measurementSystem=="impirial") {
        return Math.round(metricValue*CubicFeetPerCubicMeter) + " ft\xB3";
    }
    else {
        return Math.round(metricValue) + " m\xB3";
    }
}

exports.WeightPrint = function(metricValue,measurementSystem,decimalPlaces) {
    return roundWithDecimals(metricValue,decimalPlaces) + " t";
}

exports.Volume = function(metricValue,measurementSystem) {
    if(measurementSystem=="impirial") {
        return metricValue*CubicFeetPerCubicMeter;
    }
    else {
        return metricValue;
    }
}

exports.AreaPrint = function(metricValue,measurementSystem) {
    if(measurementSystem=="impirial") {
        var value = Math.round(metricValue*SquareFeetPerSquareMeter);
        if(value>10000) {
            return Math.floor(value/SquareFeetPerAcre) + " ac " + Math.round(value%SquareFeetPerAcre) + " ft\xB2";
        }
        else {
            return value + " ft\xB2";
        }
        
    }
    else {
        var value = Math.round(metricValue);
        if(value>10000) {
            return (value/10000) + " ha";
        }
        else {
            return Math.round(metricValue) + " m\xB2";
        }
    }
}

exports.Area = function(metricValue,measurementSystem) {
    if(measurementSystem=="impirial") {
        return metricValue*SquareFeetPerSquareMeter;
    }
    else {
        return metricValue;
    }
}

exports.LengthPrint = function(metricValue,measurementSystem,decimalPlaces) {
    if(measurementSystem=="impirial") {
        return roundWithDecimals(metricValue*FeetPerMeter,decimalPlaces) + " ft";
    }
    else {
        return roundWithDecimals(metricValue,decimalPlaces) + " m";
    }
}

exports.Length = function(metricValue,measurementSystem) {
    if(measurementSystem=="impirial") {
        return metricValue*FeetPerMeter;
    }
    else {
        return metricValue;
    }
}

exports.DurationPrint = function(duration) {
    var seconds = duration;
    var hour = Math.floor(duration/3600);
    seconds = seconds - (hour * 3600);
    var minutes = Math.floor(seconds/60);
    seconds = seconds - (minutes * 60);

    if(hour>0) {
        return hour + "h " + minutes + "m " + seconds + "s";
    }
    else if(minutes>0) {
        return minutes + "m " + seconds + "s";
    }
    else {
        return seconds + "s";
    }
}

function roundWithDecimals(num,decimalPlaces) {
    return Math.round(num * Math.pow(10,decimalPlaces)) / Math.pow(10,decimalPlaces);
}


