
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

import mdii from './mdiiapplication';

window.mdii = mdii;


//window.Vue = require('vue');


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

//Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//const app = new Vue({
//    el: '#app'
//});

jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
});

jQuery(document).ready(function($) {
    $(".clickable-element").click(function() {
        window.location = $(this).data("href");
    });
});

jQuery(document).ready(function($) {
    $(".clickable-cell ").click(function() {
        window.location = $(this).data("href");
    });
});

jQuery(document).ready(function($) {
    setTimeout(function(){
        $('.alert').animate({height: 0, opacity: 0}, 'slow', function() {
            $('.alert').remove();
        });
    }, 2000);
 });


