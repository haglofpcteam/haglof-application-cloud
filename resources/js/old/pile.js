require('./bootstrap');
const THREE = require('three');
const OrbitControls = require('three-orbitcontrols');
const Conversion = require('./conversion');

var model;
var targetDetail;

var points = [];

var sizeRatio=13;

var lowestZ = 0;
var largestZ = 0;
var lowestX = 0;
var lowestY = 0;

var selectedObject;
var selectedObjectOldColor;
var selectedObjectId=null;

var scene = new THREE.Scene();
var camera = new THREE.PerspectiveCamera( 70, window.innerWidth / window.innerHeight, 1, 10000 );
var raycaster = new THREE.Raycaster();
var mouse = new THREE.Vector2();
var renderer = new THREE.WebGLRenderer({preserveDrawingBuffer: true});
var ambientLight = new THREE.AmbientLight( 0x0f0f0f );
var light = new THREE.SpotLight( 0xffffff, 1.5 );
var background = new THREE.Color( 0xf0f0f0 );
var controls = new OrbitControls(camera,renderer.domElement);

var rotSpeed = .02;

var showPointsAndLines=false;
var showPlane=true;
var showHull=true;

var hullColor=0x808080;

var linesObject;
var hullObject;
var planeObjects = [];
var pointObjects = [];
var meshIndex;

var maxDim;
var fov;
var cameraZ;
var offset;

var pileApplicationDOmElement;

var measurementId;
var mobile;

var measurement_system;

//### Functions


window.initMeasurement = function(id,isMobile,system) {

    mobile=isMobile;
    measurementId=id;
    measurement_system=system;

    $.get('/pileapplication/sites/location/measurements/' + id + '/data').done(function(data) {
        model = data;

        if(isset(model.settings))  {
            if(isset(model.settings.show_points_and_lines)) {
                showPointsAndLines=model.settings.show_points_and_lines;
            }

            if(isset(model.settings.show_map)) {
                showPlane=model.settings.show_map;
            }
            
            if(isset(model.settings.show_hull)) {
                showHull=model.settings.show_hull;
            }

            if(isset(model.settings.hull_color)) {
                hullColor=model.settings.hull_color;
            }
        }

        initPile();
        initEnv();
    });
}

window.deletePoint = function() {

    selectedObject=null;
    targetDetail.style.display="none";

    if(selectedObjectId!=null) {
        $.get('/pileapplication/sites/location/measurements/targets/' + model.targets[selectedObjectId].id + '/remove').done(function(data) {
            model = data;
            initPile();
        });
    }
}

window.takeSnapShot = function() {

    postSnapshot(function(response){
        if(response != 0){
            setupImage(imgData);
            $('#imageModal').modal('show');
        }
    });
}

window.generateReport = function() {
    if(model.snapshot==null) {
        postSnapshot(function(response){
            if(response != 0){
                setupImage(imgData);
                $('#reportModal').modal('show');
            }
        });
    }
    else {
        $('#reportModal').modal('show');
    }
}

window.orientationChange = function() {
    location.reload();
}

window.closeTargetWindow = function() {
    if(selectedObject!=null) {
        selectedObject.material.color.set( selectedObjectOldColor );
    }

    selectedObject=null;

    targetDetail.style.display="none";
}

function initEnv() {

    //### Setup Ajax
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    //### Setup Scene
    scene.background = background;
    renderer.setSize( window.innerWidth, window.innerHeight);
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.shadowMap.enabled = true;
    renderer.shadowMap.type = THREE.BasicShadowMap;
    renderer.setClearColor(0xffffff, 0);

    //### light settings

    light.position.set( 0, 500, 2000 );

    //### Zoom out and set boundries
    if(model.type=='pile') {
        controls.maxPolarAngle = Math.PI/2 -0.1; //Dont go below ground
    }
    controls.addEventListener( 'change', light_update );

    if(mobile) {
        controls.dampingFactor = 0.15; // friction
        controls.rotateSpeed = 1; // mouse sensitivity
        controls.panSpeed = 1;
    }
    else {
        controls.dampingFactor = 0.15; // friction
        controls.rotateSpeed = 0.15; // mouse sensitivity
        controls.panSpeed = 0.1;
    }

    controls.keys = {
        LEFT: 100, //left arrow
        UP: 104, // up arrow
        RIGHT: 102, // right arrow
        BOTTOM: 98 // down arrow
    }

    maxDim = Math.max(model.size_x,model.size_y,model.size_z)*40;
    fov = camera.fov * ( Math.PI / 180 );
    cameraZ = Math.abs(maxDim / 4 * Math.tan( fov * 2 ));
    offset = 1.25;

    cameraZ *= offset;

    camera.position.z = cameraZ;

    document.addEventListener( 'keydown', onDocumentKeyPress, false );


}

function initPile() {

    targetDetail = document.getElementById("target_detail");

    //### Populate Overview
    document.getElementById("model_overview_number_of_measurments").innerHTML=model.targets.length;
    document.getElementById("model_overview_volume").innerHTML=Conversion.VolumePrint(model.volume,model.measurement_system);
    document.getElementById("model_overview_base_area").innerHTML=Conversion.AreaPrint(model.area,model.measurement_system);
    document.getElementById("model_overview_periphery").innerHTML=Conversion.LengthPrint(model.periphery,model.measurement_system,0);
    document.getElementById("model_overview_average_height").innerHTML=Conversion.LengthPrint(model.average_height,model.measurement_system,1);
    document.getElementById("model_overview_slope_angle").innerHTML=round1dec(model.tilt);
    document.getElementById("model_overview_slope_angle_percent").innerHTML=round1dec(model.tiltpercent);
    document.getElementById("model_overview_volume_adjustment_factor").innerHTML=model.volume_adjustment_factor;
    document.getElementById("model_overview_unit_production_id").innerHTML=model.unit_production_id;
    document.getElementById("model_overview_unit_version_id").innerHTML=model.unit_version_id;
    document.getElementById("model_overview_unit_serial_number").innerHTML=model.unit_serial_number;
    document.getElementById("model_overview_transponder_height").innerHTML=model.transponder_height;
    document.getElementById("model_overview_eye_height").innerHTML=model.eye_height;
    document.getElementById("model_overview_pivot_offset").innerHTML=model.pivot_offset;
    document.getElementById("model_overview_magnetic_declination").innerHTML=model.magnetic_declination;
    document.getElementById("model_overview_file_id").innerHTML=model.file_id;
    document.getElementById("model_overview_user").innerHTML=model.user;
    document.getElementById("model_overview_density").innerHTML=model.density;
    document.getElementById("model_overview_weigth").innerHTML=Conversion.WeightPrint(model.weight,model.measurement_system,1);

    //### Setup Snapshot
    if(model.snapshot!=null) {
        setupImage(model.snapshot)
    }

    //### Populate Arrays of points and indexes

    points = [];
    lowestZ = 0;
    lowestX = 0;
    lowestY = 0;

    for (var i = 0; i < model.targets.length; i++) {
        var t = model.targets[i];
        largestZ=Math.max(largestZ,t.point.y*sizeRatio);
        lowestZ=Math.min(lowestZ,t.point.y*sizeRatio);
        lowestX=Math.min(lowestX,t.point.x*sizeRatio);
        lowestY=Math.min(lowestY,t.point.y*sizeRatio);
        points.push(new THREE.Vector3( t.point.x*sizeRatio,  t.point.y*sizeRatio, t.point.z*sizeRatio ));
    }

    meshIndex = model.meshIndex;

    pileApplicationDOmElement=document.getElementById("pile_application");
    pileApplicationDOmElement.appendChild( renderer.domElement );

    //### Initiate
    init_point_cloud();
    init_geometry_lines();
    init_geometry_hull();

    render();
}

window.toggleBrownPile = function() {
    changeHullColor('#baa86f');
}

window.toggleMap = function() {
    showPlane=!showPlane;

    updateSetting("show_map",showPlane);

    render();
}

window.toggleGrayPile = function() {
    changeHullColor('#808080');
}

window.toggleHull = function() {
    showHull=!showHull;

    updateSetting("show_hull",showHull);
}

window.changeHullColor = function(color) {
    if(hullColor==color) {
        showHull=!showHull;
    }
    else {
        var material = new THREE.MeshLambertMaterial( {
            color: color,
            opacity: 1,
            transparent: true
        });

        hullObject.material=material;
        hullColor=color;
    }

    updateSetting("hull_color",hullColor);

    render();
}

window.togglePointsAndLines = function() {
    showPointsAndLines=!showPointsAndLines;

    updateSetting("show_points_and_lines",showPointsAndLines);

    render();
}

function render() {

    for(var i = scene.children.length - 1; i >= 0; i--) {
        var obj = scene.children[i];
        scene.remove(obj);
    }

    scene.add(ambientLight);
    scene.add(light);

    if(showPointsAndLines) {

        renderer.domElement.addEventListener('mousedown', onDocumentMouseDown, false );
        renderer.domElement.addEventListener('touchstart', onDocumentTouchStart, false );

        if(selectedObject!=null) {
            targetDetail.style.display="block";
        }

        for (i = 0; i < pointObjects.length; i++) {
            scene.add(pointObjects[i]);
        }
        scene.add(linesObject);
    }
    else {
        targetDetail.style.display="none";
        renderer.domElement.removeEventListener('mousedown', onDocumentMouseDown, false );
    }

    if(showHull) {
        scene.add(hullObject);
    }


    if(showPlane) {
        if(planeObjects.length==0) {
            init_geometry_plane();
        }
        for (i = 0; i < planeObjects.length; i++) {
            scene.add(planeObjects[i]);
        }
    }

    requestAnimationFrame( render );

    renderer.render(scene, camera);
}

function init_point_cloud() {
    pointObjects = [];

    var geometry = new THREE.BoxGeometry( 3, 3, 3 );

    for (var i = 0; i < model.targets.length; i++) {
        var t = model.targets[i];
        var object;

        if (t.type=="BASE") {
            object = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { color: 0x00ff00 } ) );
        }
        else {
            object = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { color: 0x000000 } ) );
        }

        object.position.x = t.point.x*sizeRatio;
        object.position.y = t.point.y*sizeRatio;
        object.position.z = t.point.z*sizeRatio;

        object.castShadow = false;
        object.receiveShadow = false;

        pointObjects.push( object );
    }

}


function init_geometry_lines() {

    var geometry = new THREE.BufferGeometry().setFromPoints(points);

    geometry.setIndex(meshIndex);
    geometry.computeVertexNormals();

    geometry = new THREE.Geometry().fromBufferGeometry(geometry);

    linesObject = new THREE.Mesh(
        geometry,
        new THREE.MeshLambertMaterial({ color: "purple", wireframe: true })
    );
}

function init_geometry_hull() {

    var geometry = new THREE.BufferGeometry().setFromPoints(points);

    geometry.setIndex(meshIndex);
    geometry.computeVertexNormals();

    var material = new THREE.MeshLambertMaterial( {
            color: hullColor,
            opacity: 1,
            transparent: true
        });

    hullObject = new THREE.Mesh(
        geometry,
        material
    );
}

function init_geometry_plane() {
    if(model.type=='pit') {

    }
    else {
        init_geometry_plane_pile();
    }
}

function init_geometry_plane_pile() {

    var lat=model.center_latitude;
    var lon=model.center_longitude;

    var here_app_id = 'Z0zYFz6suTQ8qdWJ1ndS';
    var here_app_code = 'nASq-XrZOIMRdAHljW2aBA';

    var zoom=19;
    var tileSize=512;
    var displayRowsAndCols=20;

    var latRad = lat * Math.PI / 180;
    var n = Math.pow(2, zoom);
    var xTile = n * ((lon + 180) / 360);
    var yTile = n * (1-(Math.log(Math.tan(latRad) + 1/Math.cos(latRad)) /Math.PI)) / 2;

   var startRow = Math.floor(yTile)-(displayRowsAndCols/2);
   var endRow=startRow+displayRowsAndCols;

   var startCol = Math.floor(xTile)-(displayRowsAndCols/2);
   var endCol=startCol+displayRowsAndCols;

   var startPositionX=-((displayRowsAndCols/2)*tileSize)+(tileSize/2);

   var startPositionY=-((displayRowsAndCols/2)*tileSize)+(tileSize/2);

   var diffX=(xTile%1)*tileSize;
   var diffY=(1-(yTile%1))*tileSize;

   startPositionX-=diffX;
   startPositionY+=diffY;

   var positionY=startPositionY;

    //for(var row=endRow;row>startRow;row--) {
    for(var row=endRow;row>startRow;row--) {
        var positionX=startPositionX;
        for(var col=startCol;col<endCol;col++) {

            var url="https://3.aerial.maps.api.here.com/maptile/2.1/maptile";
            url+="/newest";
            url+="/satellite.day";
            url+="/" + zoom;
            url+="/" + col;
            url+="/" + row;
            url+="/" + tileSize;
            url+="/jpg";
            url+="?app_id=" + here_app_id;
            url+="&app_code=" + here_app_code;

            var texture = new THREE.TextureLoader().load(url);

            texture.wrapS = THREE.RepeatWrapping;
            texture.wrapT = THREE.RepeatWrapping;
            texture.repeat.set(1, 1);

            var geometry = new THREE.PlaneBufferGeometry(tileSize, tileSize, 8, 8);

            var material = new THREE.MeshLambertMaterial({
                color: 0xffffff,
                map: texture
            })
            var mesh = new THREE.Mesh(geometry, material);
            mesh.rotateX( - Math.PI / 2);
            mesh.rotateZ( - Math.PI / 2);
            mesh.position.y=lowestZ-1;

            mesh.position.z=positionX;
            mesh.position.x=positionY;

            mesh.castShadow = true;
            mesh.receiveShadow = true;

            planeObjects.push(mesh);
            positionX+=tileSize;
        }
        positionY+=tileSize;
    }

}

function light_update()
{
    light.position.copy( camera.position );
}

function onDocumentMouseDown( event ) {
    event.preventDefault();
    console.log(event.clientX + " " + event.clientY);
    coordinatesClicked(event.clientX,event.clientY);
}

function onDocumentTouchStart( event ) {
    event.preventDefault();

    var clientX = event.targetTouches[0].clientX;
    var clientY = event.targetTouches[0].clientY;

    console.log(clientX + " " + clientY);
    coordinatesClicked(clientX,clientY);
}

function coordinatesClicked(clientX,clientY) {

    var canvasRect = renderer.domElement.getBoundingClientRect();

    mouse.set( ( (clientX-canvasRect.left) / canvasRect.width ) * 2 - 1, - ( (clientY-canvasRect.top) / canvasRect.height ) * 2 + 1 );

    raycaster.setFromCamera( mouse, camera );
    var intersects = raycaster.intersectObjects( pointObjects, true );

    var newObject;

    for ( var i = 0; i < intersects.length; i++ ) {
        newObject = intersects[ i ].object;
    }

    if(newObject!=null) {
        objectClicked(newObject,true);
    }
}

function objectClicked(object,scroll) {
    if(selectedObject!=null) {
        selectedObject.material.color.set( selectedObjectOldColor );
    }

    if(selectedObject==object) {
        selectedObject=null;
        hideTargetDetail();
        return;
    }

    selectedObject = object;
    selectedObjectOldColor=object.material.color.getHex();
    selectedObject.material.color.set( 0xff0000 );

    var i;
    for (i = 0; i < pointObjects.length; i++) {
        if(selectedObject==pointObjects[i]) {

            showTargetDetail(i);

            selectedObjectId=i;

            break;
        }
    }

    renderer.render( scene, camera );
}


function round1dec(num) {
    return Math.round(num * 10) / 10;
}

function hideTargetDetail() {
    targetDetail.style.display="none";
}

function showTargetDetail(id) {
    targetDetail.style.display="block";

    document.getElementById("target_detail_type").innerHTML=model.targets[id].type;

    var target_type_dropdown = $('#target_detail_type');
    target_type_dropdown.html("");

    target_type_dropdown.append($('<option/>').val('TARGET').text('TARGET'));
    target_type_dropdown.append($('<option/>').val('BASE').text('BASE'));

    target_type_dropdown.children('option').each(function () {
        var option=$(this);
        if(option.val()===model.targets[id].type) {
            option.prop('selected', true);
        }
    });

    target_type_dropdown.unbind();
   
    target_type_dropdown.change({target_id: id},function (eventObject) {
        var target = model.targets[eventObject.data.target_id];

        target.type=this.value;

        updateTarget(eventObject.data.target_id,target);

    });

    document.getElementById("target_detail_latitude").innerHTML=model.targets[id].latitude + model.targets[id].ns;
    document.getElementById("target_detail_longitude").innerHTML=model.targets[id].longitude + model.targets[id].ew;
    document.getElementById("target_detail_altitude").innerHTML=model.targets[id].altitude;
    document.getElementById("target_detail_hdop").innerHTML=model.targets[id].hdop;
    document.getElementById("target_detail_date").innerHTML=model.targets[id].date;
    document.getElementById("target_detail_utc").innerHTML=model.targets[id].utc;
    document.getElementById("target_detail_seq").innerHTML=model.targets[id].seq;
    document.getElementById("target_detail_sd").innerHTML=model.targets[id].sd;
    document.getElementById("target_detail_hd").innerHTML=model.targets[id].hd;
    document.getElementById("target_detail_h").innerHTML=model.targets[id].h;
    document.getElementById("target_detail_pitch").innerHTML=model.targets[id].pitch;
    document.getElementById("target_detail_az").innerHTML=model.targets[id].az;
    document.getElementById("target_detail_x").innerHTML=model.targets[id].x;
    document.getElementById("target_detail_y").innerHTML=model.targets[id].y;
    document.getElementById("target_detail_z").innerHTML=model.targets[id].z;
    document.getElementById("target_detail_utm_zone").innerHTML=model.targets[id].utm_zone;

}

function updateTarget(target_id,target) {
    var fd = new FormData();
    Object.keys(target).forEach(key => fd.append(key, target[key]));
    
    $.ajax({
        url: '/pileapplication/sites/location/measurements/targets/' + target.id + '/update',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: function(response){
            model = response;
            initPile();

            showTargetDetail(target_id);
        },
        error: function(jqXhr, textStatus, errorThrown){
            console.log(errorThrown);
            console.log(textStatus);
            console.log(jqXhr);
        }
    })
}

function onDocumentKeyPress( event ) {
    switch(event.which) {
        case 37: //Left
            rotateLeft();
            break;
        case 39: //Right
            rotateRight();
            break;
        case 38: //Up
            panUp();
            break;
        case 40: //Down
            panDown();
            break;
    }
}

function rotateLeft() { rotate(-rotSpeed); }
function rotateRight() { rotate(rotSpeed); }
function panUp() { pan(10); }
function panDown() { pan(-10); }

function rotate(rotation) {

    camera.position.x = camera.position.x * Math.cos(rotation) + camera.position.z * Math.sin(rotation);
    camera.position.z = camera.position.z * Math.cos(rotation) - camera.position.x * Math.sin(rotation);
    camera.lookAt(scene.position);
    light_update();
}

function pan(value) {

    camera.position.y+=value;
    camera.lookAt(scene.position);
    light_update();
}

function setupImage(imgData) {
    document.getElementById("snapshot_image_container").src=imgData;
    document.getElementById("show_snapshot_button").disabled=false;
}

function postSnapshot(success) {
    var strMime = "image/jpeg";
    imgData = renderer.domElement.toDataURL(strMime);

    var fd = new FormData();
    fd.append('image_data',imgData);
    fd.append('mime_type',strMime);

    $.ajax({
        url: '/pileapplication/sites/location/measurements/' + measurementId + '/snapshot',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: success,
    })
}

function updateSetting(key,value) {
    var fd = new FormData();
    fd.append('key',key);
    fd.append('value',value);

    
    $.ajax({
        url: '/pileapplication/sites/location/measurements/' + measurementId + '/setting',
        type: 'post',
        data: fd,
        contentType: false,
        processData: false,
        success: function(response){
            console.log(response);
        },
        error: function(jqXhr, textStatus, errorThrown){
            console.log(errorThrown);
            console.log(textStatus);
            console.log(jqXhr);
        }
    })
}
 function isset(obj) {
    var i, max_i;
    if(obj === undefined) return false;
    for (i = 1, max_i = arguments.length; i < max_i; i++) {
      if (obj[arguments[i]] === undefined) {
          return false;
      }
      obj = obj[arguments[i]];
    }
    if(obj==null) {
        return false;
    }
    return true;
  };