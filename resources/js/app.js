require('./bootstrap');
window.Vue = require('vue');

import App from './vue/App.vue';
import {routes} from './vue/routes';
import social_providers from './vue/social_providers';
import store from './store';

import 'es6-promise/auto'
import VueRouter from 'vue-router';
import VueAxios from 'vue-axios';
import VueSocialauth from 'vue-social-auth'
import VueCookies from 'vue-cookies'
import axios from 'axios';
import { mixins } from './vue/mixins';


import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import { ModalPlugin } from 'bootstrap-vue'
import { CardPlugin } from 'bootstrap-vue'
import { LayoutPlugin } from 'bootstrap-vue'

import Sortable from './vue/directives/SortableDirective.js';

import moment from 'moment-timezone';
import Vue from 'vue';

Vue.prototype.$localizations =  require('./vue/Localizations/localizations.json');

Vue.prototype.$eventHub = new Vue();

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(LayoutPlugin)
Vue.use(CardPlugin)
Vue.use(ModalPlugin)
Vue.use(VueCookies)
Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueSocialauth, social_providers);

Vue.directive('sortable',Sortable);

var constants = {
    CubicFeetPerCubicMeter : 35.3147,
    SquareFeetPerSquareMeter : 10.7639,
    SquareFeetPerAcre : 43560.2,
    FeetPerMeter : 3.28084,
    FeetPerDecimeter : 0.328084,
    InchPerMm : 0.0393700787
}

Vue.prototype.$constants = constants;

Vue.prototype.$moment = moment;

const round = function (value, decimals) {
    return Math.round(value*Math.pow(10,decimals))/Math.pow(10,decimals);
};

Vue.filter('formatDate', function(value,format,timezone=null) {
    if(value&&format) {
        var t = moment(String(value));
        if(timezone) {
            t=t.tz(timezone)
        }
        return t.format(format)
    }
});

Vue.filter('formatVolume', function(value,measurement_system,decimals=0) {
    if(value) {
        if(measurement_system==="imperial") {
            return round(value*constants.CubicFeetPerCubicMeter,decimals) + " ft\xB3";
        }
        else {
            return round(value,decimals) + " m\xB3";
        }
    }
});

Vue.filter('formatArea', function(metricValue,measurement_system) {
    if(measurement_system=="imperial") {
        var value = Math.round(metricValue*constants.SquareFeetPerSquareMeter);
        if(value>10000) {
            let mod = value%constants.SquareFeetPerAcre;
            return Math.floor(value/constants.SquareFeetPerAcre) + " ac " + Math.round(mod) + " ft\xB2";
        }
        else {
            return value + " ft\xB2";
        }
        
    }
    else {
        var value = Math.round(metricValue);
        if(value>10000) {
            return (value/10000) + " ha";
        }
        else {
            return Math.round(metricValue) + " m\xB2";
        }
    }
});

Vue.filter('formatLength', function(metricValue,measurement_system,decimals=0) {
    if(metricValue) {
        if(measurement_system==="imperial") {
            return round(metricValue*constants.FeetPerMeter,decimals) + " ft";
        }
        else {
            return round(metricValue,decimals) + " m";
        }
    }
});

Vue.filter('formatWeight', function(metricValue,measurement_system,decimals=0) {
    return round(metricValue,decimals) + " t";
});

Vue.filter('formatDegrees', function(value,decimals=0) {
    return round(value,decimals) + "°";
});

Vue.filter('formatPercent', function(value,decimals=0) {
    return round(value,decimals) + "%";
});

store.dispatch('auth/initialiseStore');

axios.defaults.baseURL = `${process.env.MIX_APP_URL}`;

axios.interceptors.request.use(
    (requestConfig) => {
        if (store.getters['auth/isAuthenticated']) {
            requestConfig.headers.Authorization = `Bearer ${store.state.auth.accessToken}`;
        }
        return requestConfig;
    },
    (requestError) => Promise.reject(requestError),
);


axios.interceptors.response.use(
    response => response,
    (error) => {
        console.log(error);
        if (error.response.status === 401) {
            store.commit('auth/setAccessToken', null);
            window.location.replace(`${window.location.origin}/login`);
        }
        else if (error.response.status === 403 && error.response.data.error==='email_not_verified') {
            window.location.replace(`${window.location.origin}/verify`);
        }
        else if (error.response.status === 403 && error.response.data.error==='not_activated') {
            window.location.replace(`${window.location.origin}/activation_notice`);
        }

        return Promise.reject(error);
    },
);



const router = new VueRouter({
    mode: 'history',
    routes: routes
});

Vue.router=router;
Vue.mixin(mixins);

const app = new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App),
});

