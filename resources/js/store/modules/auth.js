import axios from 'axios';

const state = {
    accessToken: null,
    intendedUrl: "/organizations",
    user: null,
    activated: true
};

const mutations = {
    setAccessToken: (state, value) => {
        state.accessToken = value;
    },
    setIntendedUrl: (state, value) => {
        state.intendedUrl = value;
    },
    setUser: (state, value) => {
        state.user = value;
    },
    setActivated: (state, value) => {
        state.activated=value;
    }
};

const getters = {
    isAuthenticated: (state) => {
        return state.accessToken !== null;
    },
    isActivated: (state) => {
        return state.activated;
    },
    getUser: (state) => {
        return state.user;
    },
    getIntendedUrl: (state) => {
        return state.intendedUrl;
    },
};

const actions = {
    initialiseStore(context) {
        if(localStorage.getItem('access_token')) {
            context.commit('setAccessToken', localStorage.getItem('access_token'));
        }
    },
    login(context, credentials) {

        credentials.grant_type='password';
        credentials.client_id=process.env.MIX_PASSPORT_CLIENT_ID;
        credentials.client_secret=process.env.MIX_PASSPORT_CLIENT_SECRET;
        credentials.scope='*';

        return axios.post('/oauth/token', credentials)
            .then((response) => {
                const { access_token: accessToken } = response.data;

                context.commit('setAccessToken', accessToken);
                localStorage.setItem('access_token',accessToken);


                return Promise.resolve();
            })
            .catch((error) => Promise.reject(error.response));
    },
    socialLogin(context, credentials) {

        credentials.grant_type='social_authorization';
        credentials.client_id=process.env.MIX_PASSPORT_CLIENT_ID;
        credentials.client_secret=process.env.MIX_PASSPORT_CLIENT_SECRET;
        credentials.scope='*';

        return axios.post('/oauth/token', credentials)
            .then((response) => {
                const { access_token: accessToken } = response.data;

                context.commit('setAccessToken', accessToken);
                localStorage.setItem('access_token',accessToken);

                return Promise.resolve();
            })
            .catch((error) => Promise.reject(error.response));
    },
    activationLogin(context, credentials) {

        credentials.grant_type='activation_authorization';
        credentials.client_id=process.env.MIX_PASSPORT_CLIENT_ID;
        credentials.client_secret=process.env.MIX_PASSPORT_CLIENT_SECRET;
        credentials.scope='*';

        return axios.post('/oauth/token', credentials)
            .then((response) => {
                const { access_token: accessToken } = response.data;

                context.commit('setAccessToken', accessToken);
                localStorage.setItem('access_token',accessToken);

                return Promise.resolve();
            })
            .catch((error) => Promise.reject(error.response));
    },
    getUser(context) {
        if(context.state.user===null) {
            return axios.get('/api/user').then((response) => {
                response.data.settings=JSON.parse(response.data.settings);
                context.commit('setUser',response.data);
                return response.data;
            });
        }
        else {
            return Promise.resolve(context.state.user);
        }

    },
    logout(context) {
        context.commit('setAccessToken', null);
        localStorage.removeItem('access_token');
    },

};

export default {
    state,
    getters,
    actions,
    mutations,
    namespaced: true
}