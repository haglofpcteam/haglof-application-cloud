class {
	constructor(data) {
		this.data = data;
	}
	getData()
	{
	   let d = this.data;
       d.calculated_values={};
       
       d.calculated_values.first_date=new Date(d.measurements[0].measurement.measurement_time);
       d.calculated_values.last_date=new Date(d.measurements[0].measurement.measurement_time);

	   for(let i=0;i<d.measurements.length;i++) {
            d.measurements[i].measurement.measurement_time=new Date(d.measurements[i].measurement.measurement_time);
            if(d.calculated_values.first_date>d.measurements[i].measurement.measurement_time) {
                d.calculated_values.first_date=d.measurements[i].measurement.measurement_time;
            }
            if(d.calculated_values.last_date<d.measurements[i].measurement.measurement_time) {
                d.calculated_values.last_date=d.measurements[i].measurement.measurement_time;
            }

            let targets=d.measurements[i].model.targets; 
            d.measurements[i].model.first_timestamp=new Date(targets[0].timestamp)
            d.measurements[i].model.last_timestamp=new Date(targets[0].timestamp)

            for(let j=0;j<targets.length;j++) {
                targets[j].timestamp=new Date(targets[j].timestamp);

                if(d.measurements[i].model.first_timestamp>targets[j].timestamp) {
                    d.measurements[i].model.first_timestamp=targets[j].timestamp;
                }

                if(d.measurements[i].model.last_timestamp<targets[j].timestamp) {
                    d.measurements[i].model.last_timestamp=targets[j].timestamp;
                }
            }
	   }
	   return d;
	}
}