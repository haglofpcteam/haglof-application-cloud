import {RoutesValidateNotLoggedIn,RoutesValidateLoggedInAndActivated} from '@/vue/auth_routes.js';
import LayoutDefault from '@/vue/Layouts/LayoutDefault'

import ApplicationsRedirect from './ApplicationsRedirect'

export const routes = [
        {
            path: '/applications/:id',
            name: 'applications_redirect',
            component: ApplicationsRedirect,
            meta: { },
            beforeEnter: RoutesValidateLoggedInAndActivated,
        },
];