import {RoutesValidateNotLoggedIn,RoutesValidateLoggedInAndActivated} from '@/vue/auth_routes.js';
import LayoutDefault from '@/vue/Layouts/LayoutDefault'
import LayoutBlank from '@/vue/Layouts/LayoutBlank'

import ReportDesigner from './ReportDesigner';
import Organizations from './Organizations'
import ReportTemplates from './ReportTemplates'
import ReportTemplate from './ReportTemplate'
import Users from './Users'
import User from './User'

export const routes = [
        {
            path: '/admin/reportdesigner/:id',
            name: 'reportdesigner',
            component: ReportDesigner,
            meta: { layout: LayoutBlank },
            beforeEnter: RoutesValidateLoggedInAndActivated
        },
        {
            path: '/admin/users',
            name: 'admin_users',
            component: Users,
            meta: { layout: LayoutDefault },
            beforeEnter: RoutesValidateLoggedInAndActivated
        },
        {
            path: '/admin/users/:id',
            name: 'admin_user',
            component: User,
            meta: { layout: LayoutDefault },
            beforeEnter: RoutesValidateLoggedInAndActivated,
        },
        {
            path: '/admin/organizations',
            name: 'admin_organizations',
            component: Organizations,
            meta: { layout: LayoutDefault },
            beforeEnter: RoutesValidateLoggedInAndActivated
        },
        {
            path: '/admin/reporttemplates',
            name: 'reporttemplates',
            component: ReportTemplates,
            meta: { layout: LayoutDefault },
            beforeEnter: RoutesValidateLoggedInAndActivated
        },
        {
            path: '/admin/reporttemplates/:id',
            name: 'reporttemplate',
            component: ReportTemplate,
            meta: { layout: LayoutDefault },
            beforeEnter: RoutesValidateLoggedInAndActivated
        },

];