import {RoutesValidateNotLoggedIn,RoutesValidateLoggedIn} from '@/vue/auth_routes.js';
import LayoutDefault from '@/vue/Layouts/LayoutDefault'

import Login from './Login.vue';
import Verify from './Verify.vue';
import Reset from './Reset.vue';
import Register from './Register.vue';
import Activate from './Activate.vue';
import EditUser from './EditUser.vue';
import ActivationNotice from './ActivationNotice';
import ResetNotice from './ResetNotice';

export const routes = [
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: { layout: LayoutDefault },
            beforeEnter: RoutesValidateNotLoggedIn
        },
        {
            path: '/register',
            name: 'register',
            meta: { layout: LayoutDefault },
            component: Register
        },
        {
            path: '/verify',
            name: 'verify',
            meta: { layout: LayoutDefault },
            component: Verify
        },
        {
            path: '/activation_notice',
            name: 'activation_notice',
            meta: { layout: LayoutDefault },
            component: ActivationNotice
        },
        {
            path: '/reset_notice',
            name: 'reset_notice',
            meta: { layout: LayoutDefault },
            component: ResetNotice
        },
        {
            path: '/activate',
            name: 'activate',
            meta: { layout: LayoutDefault },
            component: Activate
        },
        {
            path: '/reset',
            name: 'reset',
            meta: { layout: LayoutDefault },
            component: Activate
        },
        {
            path: '/edit_user',
            name: 'edit_user',
            meta: { layout: LayoutDefault },
            component: EditUser,
            beforeEnter: RoutesValidateLoggedIn
        },
        {
            path: '/reset_password',
            name: 'reset_password',
            meta: { layout: LayoutDefault },
            component: Reset,
            beforeEnter: RoutesValidateNotLoggedIn
        },
        

];