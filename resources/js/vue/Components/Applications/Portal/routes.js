import {RoutesValidateNotLoggedIn,RoutesValidateLoggedInAndActivated} from '@/vue/auth_routes.js';
import LayoutDefault from '@/vue/Layouts/LayoutDefault'
import LayoutBlank from '@/vue/Layouts/LayoutBlank'

import Start from './Start';
import Organizations from './Organizations';
import OrganizationsSubMenu from "./OrganizationsSubmenu"
import Organization from './Organization';
import ApplicationInstance from './ApplicationInstance';
import ReportViewer from './ReportViewer';
import ConnectSubscription from './ConnectSubscription';

export const routes = [
        {
            path: '/',
            name: 'start',
            component: Start,
            meta: { layout: LayoutBlank },
            beforeEnter: RoutesValidateNotLoggedIn
        },
        {
            path: '/organizations',
            name: 'organizations',
            component: Organizations,
            meta: { 
                layout: LayoutDefault,
                submenu: OrganizationsSubMenu
            },
            beforeEnter: RoutesValidateLoggedInAndActivated,
        },
        {
            path: '/organizations/:id',
            name: 'organization',
            component: Organization,
            meta: { layout: LayoutDefault },
            beforeEnter: RoutesValidateLoggedInAndActivated,
        },
        {
            path: '/application_instances/:id',
            name: 'application_instance',
            component: ApplicationInstance,
            meta: { layout: LayoutDefault },
            beforeEnter: RoutesValidateLoggedInAndActivated,
        },
        {
            path: '/reports/:id',
            name: 'reportviewer',
            component: ReportViewer,
            meta: { layout: LayoutBlank },
            beforeEnter: RoutesValidateLoggedInAndActivated
        },
        {
            path: '/subscription/connect/:order',
            name: 'connect_subscription',
            component: ConnectSubscription,
            meta: { layout: LayoutDefault },
            beforeEnter: RoutesValidateLoggedInAndActivated,
        }

];