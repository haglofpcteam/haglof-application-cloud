import {RoutesValidateNotLoggedIn,RoutesValidateLoggedInAndActivated} from '@/vue/auth_routes.js';
import LayoutDefault from '@/vue/Layouts/LayoutDefault'

import Start from './Start'

export const routes = [
        {
            path: '/applications/digitech_cloud/:id',
            name: 'digitech_cloud.start',
            component: Start,
            meta: { 
                layout: LayoutDefault
             },
            beforeEnter: RoutesValidateLoggedInAndActivated,
        },
];