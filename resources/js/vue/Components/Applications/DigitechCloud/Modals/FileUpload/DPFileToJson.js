export function DigitechFileToObject(file) {
   return new Promise((resolve, reject) => {
      try {
         var reader = new FileReader();
         var xmlDoc;

         reader.onload = function(evt) {
      
               var text = evt.target.result;
               var metric=null;
      
               if (window.DOMParser) {
                  var parser = new DOMParser();
                  xmlDoc = parser.parseFromString(text,"text/xml");
               } 
               else {
                  xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
                  xmlDoc.async = false;
                  xmlDoc.loadXML(text);
               } 
      
               var file_obj=xml2obj(xmlDoc);

               var data = {
                  trees: [],
                  header: {
                     'name': "",
                     'metric': metric,
                     'serial_nr': null,
                     'date': null,
                     'program_name': "",
                     'program_ver': 0
                  }
               }

               if(isset(file_obj.Header)) {
                  data.header.name = isset(file_obj.Header.Name) ? file_obj.Header.Name : data.header.name;
                  data.header.metric = isset(file_obj.Header.Metric) ? file_obj.Header.Metric : data.header.metric;
                  data.header.serial_nr = isset(file_obj.Header.SerialNr) ? file_obj.Header.SerialNr : data.header.serial_nr;
                  data.header.date = isset(file_obj.Header.Date) ? file_obj.Header.Date : data.header.date;
                  data.header.program_name = isset(file_obj.Header.ProgramName) ? file_obj.Header.ProgramName : data.header.program_name;
                  data.header.program_ver = isset(file_obj.Header.ProgramVer) ? file_obj.Header.ProgramVer : data.header.program_ver;
                  metric=data.header.metric;
               }
               if(isset(file_obj.Data)&&isset(file_obj.Data.Tree)) {
                  for(let i=0;i<file_obj.Data.Tree.length;i++) {
                     let tree=file_obj.Data.Tree[i];
                     let new_tree_obj= {
                        'number': i+1,
                        'species': {
                           'number': parseInt(tree.Species),
                           'text': tree.SpeciesText
                        },
                        'quality': {
                           'number': 0,
                           'text' : ''
                        },
                        'diameter': 0,
                        'height1': 0,
                        'height2': 0,
                        'height3': 0,
                        'item_id': 0,
                        'unique_id': parseInt(tree.UniqueId),
                        'is_log': 0,
                        'on_bark': 1,
                        'metric': 1,
                        'unit_serial_number': data.header.serial_nr
                     }
   
                     new_tree_obj.ctrl = isset(tree.Ctrl) ? tree.Ctrl : '';
                     new_tree_obj.crc = isset(tree.Crc) ? tree.Crc : '';
   
                     new_tree_obj.on_bark = isset(tree.OnBark) ? tree.OnBark : 1;
                     
                     if(metric==null) {
                        if(isset(tree.Metric)) {
                           metric=tree.Metric;
                        }
                        else {
                           metric=1;
                        }
                     }
                     new_tree_obj.metric = isset(tree.Metric) ? tree.Metric : metric;
                     new_tree_obj.is_log = isset(tree.IsLog) ? tree.IsLog : 0;
                     new_tree_obj.item_id = isset(tree.Id) ? tree.Id : 0;
                     new_tree_obj.quality.number = isset(tree.Quality) ? parseInt(tree.Quality) : 0;
                     new_tree_obj.quality.text = isset(tree.QualityText) ? tree.QualityText : '';
         
                     new_tree_obj.diameter = typeof tree.Diameter === 'object' ? tree.Diameter.text.trim() : tree.Diameter;
                     new_tree_obj.diameter = parseFloat(new_tree_obj.diameter);  
   
                     if(isset(tree.Height1)&&tree.Height1!=null) {
                        new_tree_obj.height1 = parseFloat(typeof tree.Height1 === 'object' ? tree.Height1.text : tree.Height1);
                     }
   
                     if(isset(tree.Height2)&&tree.Height2!=null) {
                        new_tree_obj.height2 = parseFloat(typeof tree.Height2 === 'object' ? tree.Height2.text : tree.Height2);
                     }
   
                     if(isset(tree.Height3)&&tree.Height3!=null) {
                        new_tree_obj.height3 = parseFloat(typeof tree.Height3 === 'object' ? tree.Height3.text : tree.Height3);
                     }

                     if(typeof new_tree_obj.is_log === 'number') {
                        new_tree_obj.is_log = new_tree_obj.is_log === 1;
                    }
                    else if(typeof new_tree_obj.is_log === 'string') {
                        if(new_tree_obj.is_log.length==1) {
                           new_tree_obj.is_log=new_tree_obj.is_log==='1';
                        }
                        else {
                           new_tree_obj.is_log=new_tree_obj.is_log==='true';
                        }
                    }
   
                     data.trees.push(new_tree_obj);
   
                  }
               }

               data.header.metric=metric;
      
               resolve(data);
         }

         reader.readAsText(file, "UTF-8");
      }
      catch(e) {
         reject(e);
      }
   });
}

var isset = function(variable) {
   try {
       if (typeof variable !== 'undefined') {
           return true;
       }
       return false;
   } 
   catch ( e ) {
       return false;
   }
}

var xml2obj = function(xml, tab) {
    var X = {
       toObj: function(xml) {
          var o = {};
          if (xml.nodeType==1) {   // element node ..
             if (xml.attributes.length)   // element with attributes  ..
                for (var i=0; i<xml.attributes.length; i++)
                   o["@"+xml.attributes[i].nodeName] = (xml.attributes[i].nodeValue||"").toString();
             if (xml.firstChild) { // element has child nodes ..
                var textChild=0, cdataChild=0, hasElementChild=false;
                for (var n=xml.firstChild; n; n=n.nextSibling) {
                   if (n.nodeType==1) hasElementChild = true;
                   else if (n.nodeType==3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) textChild++; // non-whitespace text
                   else if (n.nodeType==4) cdataChild++; // cdata section node
                }
                if (hasElementChild) {
                   if (textChild < 2 && cdataChild < 2) { // structured element with evtl. a single text or/and cdata node ..
                      X.removeWhite(xml);
                      for (var n=xml.firstChild; n; n=n.nextSibling) {
                         if (n.nodeType == 3)  // text node
                            o["text"] = X.escape(n.nodeValue);
                         else if (n.nodeType == 4)  // cdata node
                            o["cdata"] = X.escape(n.nodeValue);
                         else if (o[n.nodeName]) {  // multiple occurence of element ..
                            if (o[n.nodeName] instanceof Array)
                               o[n.nodeName][o[n.nodeName].length] = X.toObj(n);
                            else
                               o[n.nodeName] = [o[n.nodeName], X.toObj(n)];
                         }
                         else  // first occurence of element..
                            o[n.nodeName] = X.toObj(n);
                      }
                   }
                   else { // mixed content
                      if (!xml.attributes.length)
                         o = X.escape(X.innerXml(xml));
                      else
                         o["text"] = X.escape(X.innerXml(xml));
                   }
                }
                else if (textChild) { // pure text
                   if (!xml.attributes.length)
                      o = X.escape(X.innerXml(xml));
                   else
                      o["text"] = X.escape(X.innerXml(xml));
                }
                else if (cdataChild) { // cdata
                   if (cdataChild > 1)
                      o = X.escape(X.innerXml(xml));
                   else
                      for (var n=xml.firstChild; n; n=n.nextSibling)
                         o["cdata"] = X.escape(n.nodeValue);
                }
             }
             if (!xml.attributes.length && !xml.firstChild) o = null;
          }
          else if (xml.nodeType==9) { // document.node
             o = X.toObj(xml.documentElement);
          }
          else
             alert("unhandled node type: " + xml.nodeType);
          return o;
       },
       innerXml: function(node) {
          var s = ""
          if ("innerHTML" in node)
             s = node.innerHTML;
          else {
             var asXml = function(n) {
                var s = "";
                if (n.nodeType == 1) {
                   s += "<" + n.nodeName;
                   for (var i=0; i<n.attributes.length;i++)
                      s += " " + n.attributes[i].nodeName + "=\"" + (n.attributes[i].nodeValue||"").toString() + "\"";
                   if (n.firstChild) {
                      s += ">";
                      for (var c=n.firstChild; c; c=c.nextSibling)
                         s += asXml(c);
                      s += "</"+n.nodeName+">";
                   }
                   else
                      s += "/>";
                }
                else if (n.nodeType == 3)
                   s += n.nodeValue;
                else if (n.nodeType == 4)
                   s += "<![CDATA[" + n.nodeValue + "]]>";
                return s;
             };
             for (var c=node.firstChild; c; c=c.nextSibling)
                s += asXml(c);
          }
          return s;
       },
       escape: function(txt) {
          return txt.replace(/[\\]/g, "\\\\")
                    .replace(/[\"]/g, '\\"')
                    .replace(/[\n]/g, '\\n')
                    .replace(/[\r]/g, '\\r');
       },
       removeWhite: function(e) {
          e.normalize();
          for (var n = e.firstChild; n; ) {
             if (n.nodeType == 3) {  // text node
                if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) { // pure whitespace text node
                   var nxt = n.nextSibling;
                   e.removeChild(n);
                   n = nxt;
                }
                else
                   n = n.nextSibling;
             }
             else if (n.nodeType == 1) {  // element node
                X.removeWhite(n);
                n = n.nextSibling;
             }
             else                      // any other node
                n = n.nextSibling;
          }
          return e;
       }
    };
    if (xml.nodeType == 9) // document node
       xml = xml.documentElement;
    var obj = X.toObj(X.removeWhite(xml));
    return obj;
 }