import * as THREE from 'three'
import * as OrbitControls from 'three-orbitcontrols'

export default  {
    data() {
        return {
            model: null,
            targetDetail: null,
            selectedTarget: null,
            points: [],
            
            sizeRatio: 13,

            lowestZ:0,
            largestZ: 0,
            lowestX: 0,
            lowestY: 0,

            selectedObject: null,
            selectedObjectOldColor: null,
            selectedObjectId: null,

            scene: null,
            camera: null,
            raycaster: null,
            mouse: null,
            renderer: null,
            ambientLight: null,
            light: null,
            background: null,
            controls: null,

            rotSpeed: .02,

            showPointsAndLines: true,
            showPlane: false,
            showHull: true,

            hullColor: 0x808080,

            linesObject: null,
            hullObject: null,
            planeObjects: [],
            pointObjects: [],
            meshIndex: null,

            maxDim: null,
            fov: null,
            cameraZ: null,
            offset: null,

            pileApplicationDOmElement: null,

            mobile: null,

            measurement_system: null
        };
    },
    methods: {
        initModel(model) {
            if(this.scene===null) {
                this.scene = new THREE.Scene();
                this.camera = new THREE.PerspectiveCamera(70,window.innerWidth/window.innerHeight,1,10000);
                this.raycaster = new THREE.Raycaster();
                this.mouse = new THREE.Vector2();
                this.renderer = new THREE.WebGLRenderer({preserveDrawingBuffer: true});
                this.ambientLight = new THREE.AmbientLight( 0x0f0f0f );
                this.light = new THREE.SpotLight( 0xffffff, 1.5 );
                this.background = new THREE.Color( 0xf0f0f0 );
                this.controls = new OrbitControls(this.camera,this.renderer.domElement);
            }

            this.model=model;
            this.initPile();
            this.initEnv();
        },
        initPile() {
            //### Populate Arrays of points and indexes
            this.points = [];
            this.lowestZ = 0;
            this.lowestX = 0;
            this.lowestY = 0;

            for (var i = 0; i < this.model.targets.length; i++) {
                var t = this.model.targets[i];
                this.largestZ=Math.max(this.largestZ,t.point.y*this.sizeRatio);
                this.lowestZ=Math.min(this.lowestZ,t.point.y*this.sizeRatio);
                this.lowestX=Math.min(this.lowestX,t.point.x*this.sizeRatio);
                this.lowestY=Math.min(this.lowestY,t.point.y*this.sizeRatio);
                this.points.push(new THREE.Vector3( t.point.x*this.sizeRatio,  t.point.y*this.sizeRatio, t.point.z*this.sizeRatio ));
            }

            this.meshIndex = this.model.meshIndex;

            this.pileApplicationDOmElement=document.getElementById("pile_application");
            this.pileApplicationDOmElement.appendChild(this.renderer.domElement);

            //### Initiate
            this.init_point_cloud();
            this.init_geometry_lines();
            this.init_geometry_hull();

            this.scene.add(this.ambientLight);
            this.scene.add(this.light);

            for (let i = 0; i < this.pointObjects.length; i++) {
                this.scene.add(this.pointObjects[i]);
            }

            this.scene.add(this.linesObject);
            this.scene.add(this.hullObject);

            this.render();
        },
        updateModel(model) {
            for (let i = 0; i < this.pointObjects.length; i++) {
                this.scene.remove(this.pointObjects[i]);
            }

            this.scene.remove(this.linesObject);
            this.scene.remove(this.hullObject);

            this.model=model;
            this.meshIndex = model.meshIndex;

            this.points = [];
            this.lowestZ = 0;
            this.lowestX = 0;
            this.lowestY = 0;

            for (var i = 0; i < this.model.targets.length; i++) {
                var t = this.model.targets[i];
                this.largestZ=Math.max(this.largestZ,t.point.y*this.sizeRatio);
                this.lowestZ=Math.min(this.lowestZ,t.point.y*this.sizeRatio);
                this.lowestX=Math.min(this.lowestX,t.point.x*this.sizeRatio);
                this.lowestY=Math.min(this.lowestY,t.point.y*this.sizeRatio);
                this.points.push(new THREE.Vector3( t.point.x*this.sizeRatio,  t.point.y*this.sizeRatio, t.point.z*this.sizeRatio ));
            }
            
            this.init_point_cloud();
            this.init_geometry_lines();
            this.init_geometry_hull();

            for (let i = 0; i < this.pointObjects.length; i++) {
                this.scene.add(this.pointObjects[i]);
            }

            this.scene.add(this.linesObject);
            this.scene.add(this.hullObject);
            
        },
        initEnv() {
            //### Setup Scene
            this.scene.background = this.background;
            this.renderer.setSize(window.innerWidth,window.innerHeight);
            this.renderer.setPixelRatio(window.devicePixelRatio);
            this.renderer.shadowMap.enabled = true;
            this.renderer.shadowMap.type = THREE.BasicShadowMap;
            this.renderer.setClearColor(0xffffff, 0);

            this.renderer.domElement.addEventListener('mousedown', this.onDocumentMouseDown, false );
            this.renderer.domElement.addEventListener('touchstart', this.onDocumentTouchStart, false );

            //### light settings

            this.light.position.set( 0, 500, 2000 );

            //### Zoom out and set boundries
            if(this.model.type=='pile') {
                this.controls.maxPolarAngle = Math.PI/2 -0.1; //Dont go below ground
            }
            this.controls.addEventListener( 'change', this.light_update );

            if(this.isMobile()) {
                this.controls.dampingFactor = 0.15; // friction
                this.controls.rotateSpeed = 1; // mouse sensitivity
                this.controls.panSpeed = 1;
            }
            else {
                this.controls.dampingFactor = 0.15; // friction
                this.controls.rotateSpeed = 0.15; // mouse sensitivity
                this.controls.panSpeed = 0.1;
            }

            this.controls.keys = {
                LEFT: 100, //left arrow
                UP: 104, // up arrow
                RIGHT: 102, // right arrow
                BOTTOM: 98 // down arrow
            }

            this.maxDim = Math.max(this.model.size_x,this.model.size_y,this.model.size_z)*40;
            this.fov = this.camera.fov * (Math.PI/180);
            this.cameraZ = Math.abs(this.maxDim / 4 * Math.tan(this.fov * 2));
            this.offset = 1.25;

            this.cameraZ *= this.offset;

            this.camera.position.z = this.cameraZ;

            document.addEventListener('keydown', this.onDocumentKeyPress, false );
        },
        render() {
            requestAnimationFrame(this.render);

            this.renderer.render(this.scene, this.camera);
        },
        init_point_cloud() {
            this.pointObjects = [];

            var geometry = new THREE.BoxGeometry( 3, 3, 3 );

            for (var i = 0; i < this.model.targets.length; i++) {
                var t = this.model.targets[i];
                var object;

                if (t.type=="BASE") {
                    object = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { color: 0x00ff00 } ) );
                }
                else {
                    object = new THREE.Mesh( geometry, new THREE.MeshBasicMaterial( { color: 0x000000 } ) );
                }

                object.position.x = t.point.x*this.sizeRatio;
                object.position.y = t.point.y*this.sizeRatio;
                object.position.z = t.point.z*this.sizeRatio;

                object.castShadow = false;
                object.receiveShadow = false;

                //object.visible=this.showPointsAndLines;

                this.pointObjects.push(object);
            }
        },
        init_geometry_lines() {
            var geometry = new THREE.BufferGeometry().setFromPoints(this.points);

            geometry.setIndex(this.meshIndex);
            geometry.computeVertexNormals();
            geometry.visible=this.showPointsAndLines;

            geometry = new THREE.Geometry().fromBufferGeometry(geometry);

            this.linesObject = new THREE.Mesh(
                geometry,
                new THREE.MeshLambertMaterial({ color: "purple", wireframe: true })
            );
        },
        init_geometry_hull() {
            var geometry = new THREE.BufferGeometry().setFromPoints(this.points);

            geometry.setIndex(this.meshIndex);
            geometry.computeVertexNormals();

            var material = new THREE.MeshLambertMaterial( {
                    color: this.hullColor,
                    opacity: 1,
                    transparent: true
                });

            this.hullObject = new THREE.Mesh(
                geometry,
                material
            );
            this.hullObject.visible=this.showHull;
        },
        init_geometry_plane() {
            if(this.model.type=='pit') {

            }
            else {
                this.init_geometry_plane_pile();
            }
        },
        init_geometry_plane_pile() {
            var lat=this.model.center_latitude;
            var lon=this.model.center_longitude;

            var here_app_id = 'Z0zYFz6suTQ8qdWJ1ndS';
            var here_app_code = 'nASq-XrZOIMRdAHljW2aBA';

            var zoom=19;
            var tileSize=512;
            var displayRowsAndCols=20;

            var latRad = lat * Math.PI / 180;
            var n = Math.pow(2, zoom);
            var xTile = n * ((lon + 180) / 360);
            var yTile = n * (1-(Math.log(Math.tan(latRad) + 1/Math.cos(latRad)) /Math.PI)) / 2;

            var startRow = Math.floor(yTile)-(displayRowsAndCols/2);
            var endRow=startRow+displayRowsAndCols;

            var startCol = Math.floor(xTile)-(displayRowsAndCols/2);
            var endCol=startCol+displayRowsAndCols;

            var startPositionX=-((displayRowsAndCols/2)*tileSize)+(tileSize/2);

            var startPositionY=-((displayRowsAndCols/2)*tileSize)+(tileSize/2);

            var diffX=(xTile%1)*tileSize;
            var diffY=(1-(yTile%1))*tileSize;

            startPositionX-=diffX;
            startPositionY+=diffY;

            var positionY=startPositionY;

            //for(var row=endRow;row>startRow;row--) {
            for(var row=endRow;row>startRow;row--) {
                var positionX=startPositionX;
                for(var col=startCol;col<endCol;col++) {

                    var url="https://3.aerial.maps.api.here.com/maptile/2.1/maptile";
                    url+="/newest";
                    url+="/satellite.day";
                    url+="/" + zoom;
                    url+="/" + col;
                    url+="/" + row;
                    url+="/" + tileSize;
                    url+="/jpg";
                    url+="?app_id=" + here_app_id;
                    url+="&app_code=" + here_app_code;

                    var texture = new THREE.TextureLoader().load(url);

                    texture.wrapS = THREE.RepeatWrapping;
                    texture.wrapT = THREE.RepeatWrapping;
                    texture.repeat.set(1, 1);

                    var geometry = new THREE.PlaneBufferGeometry(tileSize, tileSize, 8, 8);

                    var material = new THREE.MeshLambertMaterial({
                        color: 0xffffff,
                        map: texture
                    })
                    var mesh = new THREE.Mesh(geometry, material);
                    mesh.rotateX( - Math.PI / 2);
                    mesh.rotateZ( - Math.PI / 2);
                    mesh.position.y=this.lowestZ-1;

                    mesh.position.z=positionX;
                    mesh.position.x=positionY;

                    mesh.castShadow = true;
                    mesh.receiveShadow = true;

                    this.planeObjects.push(mesh);
                    positionX+=tileSize;
                }
                positionY+=tileSize;
            }
        },
        light_update(){
            this.light.position.copy(this.camera.position);
        },
        onDocumentMouseDown(event) {
            event.preventDefault();
            this.coordinatesClicked(event.clientX,event.clientY);
        },
        onDocumentTouchStart(event) {
            event.preventDefault();

            var clientX = event.targetTouches[0].clientX;
            var clientY = event.targetTouches[0].clientY;

            this.coordinatesClicked(clientX,clientY);
        },
        coordinatesClicked(clientX,clientY) {
            var canvasRect = this.renderer.domElement.getBoundingClientRect();

            this.mouse.set(((clientX-canvasRect.left)/canvasRect.width)*2-1,-((clientY-canvasRect.top)/canvasRect.height)*2+1);

            this.raycaster.setFromCamera( this.mouse, this.camera );
            var intersects = this.raycaster.intersectObjects( this.pointObjects, true );

            var newObject;

            for ( var i = 0; i < intersects.length; i++ ) {
                newObject = intersects[i].object;
            }

            if(newObject!=null) {
                this.objectClicked(newObject,true);
            }
        },
        objectClicked(object,scroll) {
            if(this.selectedObject!=null) {
                this.selectedObject.material.color.set(this.selectedObjectOldColor);
            }

            if(this.selectedObject==object) {
                this.selectedObject=null;
                this.hideTargetDetail();
                return;
            }

            this.selectedObject = object;
            this.selectedObjectOldColor=object.material.color.getHex();
            this.selectedObject.material.color.set( 0xff0000 );

            var i;
            for (i = 0; i < this.pointObjects.length; i++) {
                if(this.selectedObject==this.pointObjects[i]) {

                    this.selectedTarget=this.model.targets[i];

                    this.selectedObjectId=i;

                    break;
                }
            }

            this.renderer.render( this.scene, this.camera );
        },
        round1dec(num) {
            return Math.round(num * 10) / 10;
        },
        onDocumentKeyPress( event ) {
            switch(event.which) {
                case 37: //Left
                    this.rotate(-this.rotSpeed);
                    break;
                case 39: //Right
                    this.rotate(this.rotSpeed);
                    break;
                case 38: //Up
                    this.pan(10);
                    break;
                case 40: //Down
                    this.pan(-10);
                    break;
            }
        },
        rotate(rotation) {
            this.camera.position.x = this.camera.position.x * Math.cos(rotation) + this.camera.position.z * Math.sin(rotation);
            this.camera.position.z = this.camera.position.z * Math.cos(rotation) - this.camera.position.x * Math.sin(rotation);
            this.camera.lookAt(this.scene.position);
            this.light_update();
        },
        pan(value) {
            this.camera.position.y+=value;
            this.camera.lookAt(this.scene.position);
            this.light_update();
        },
        isset(obj) {
            var i, max_i;
            if(obj === undefined) return false;
            for (i = 1, max_i = arguments.length; i < max_i; i++) {
            if (obj[arguments[i]] === undefined) {
                return false;
            }
            obj = obj[arguments[i]];
            }
            if(obj==null) {
                return false;
            }
            return true;
        },
        changeHullColor(color,updateSettings=true) {

            var material = new THREE.MeshLambertMaterial( {
                color: color,
                opacity: 1,
                transparent: true
            });
    
            this.hullObject.material=material;
            this.hullColor=color;
    

            if(updateSettings) {
                this.updateSetting("hull_color",this.hullColor);
            }
            

        },
        toggleHull(showHull=!this.showHull,updateSettings=true) {
            this.showHull=showHull;

            this.hullObject.visible=showHull;
        
            if(updateSettings) {
                this.updateSetting("show_hull",showHull);
            }
            

        },
        togglePointsAndLines(showPointsAndLines=!this.showPointsAndLines,updateSettings=true) {
            this.showPointsAndLines=showPointsAndLines;

            for (let i = 0; i < this.pointObjects.length; i++) {
                this.pointObjects[i].visible=showPointsAndLines;
            }
            this.linesObject.visible=showPointsAndLines;
        
            if(updateSettings) {
                this.updateSetting("show_points_and_lines",showPointsAndLines);
            }
        },
        toggleMap(showPlane=!this.showPlane,updateSettings=true) {
            this.showPlane=showPlane;

            if(this.showPlane) {
                if(this.planeObjects.length==0) {
                    this.init_geometry_plane();
                    for (let i = 0; i < this.planeObjects.length; i++) {
                        this.scene.add(this.planeObjects[i]);
                    }
                }
            }

            for (let i = 0; i < this.planeObjects.length; i++) {
                this.planeObjects[i].visible=showPlane;
            }
        
            if(updateSettings) {
                this.updateSetting("show_map",showPlane);
            }
        
        },
        generateSnapshot() {
            let strMime = "image/jpeg";
            return this.renderer.domElement.toDataURL(strMime);
        }

    },
    mounted() {

    },
}