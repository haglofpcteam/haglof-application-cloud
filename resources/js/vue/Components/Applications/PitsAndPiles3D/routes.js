import {RoutesValidateNotLoggedIn,RoutesValidateLoggedInAndActivated} from '@/vue/auth_routes.js';
import LayoutDefault from '@/vue/Layouts/LayoutDefault'
import PileSubmenu from './PileSubmenu'

import Start from './Start'

export const routes = [
        {
            path: '/applications/pits_and_piles3d/:id',
            name: 'pits_and_piles3d.start',
            component: Start,
            meta: { 
                layout: LayoutDefault,
                submenu: PileSubmenu
             },
            beforeEnter: RoutesValidateLoggedInAndActivated,
        },
];