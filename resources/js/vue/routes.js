var files = require.context('./Components', true, /routes\.js$/)
var routes = [
    { path: '/auth/*' },
    { path: '*', redirect: '/' }
];

files.keys().map(key => {
    files(key).routes.forEach(element => {
        routes.push(element);
    });
});

export var routes;