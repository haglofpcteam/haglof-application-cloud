export default {
    providers: {
        facebook: {
            clientId: process.env.MIX_FACEBOOK_KEY,
            redirectUri: process.env.MIX_FACEBOOK_REDIRECT_URI
        },
        google: {
            clientId: process.env.MIX_GOOGLE_KEY,
            redirectUri: process.env.MIX_GOOGLE_REDIRECT_URI
        },
        linkedin: {
            clientId: process.env.MIX_LINKEDIN_KEY,
            redirectUri: process.env.MIX_LINKEDIN_REDIRECT_URI,
            scope: ['r_emailaddress','r_liteprofile'],
        },
        live: {
            clientId: process.env.MIX_MICROSOFT_KEY,
            redirectUri: process.env.MIX_MICROSOFT_REDIRECT_URI,
            scope: ['User.Read']
        },
    }
};