fs = require('fs');

var new_localizations = null;
let strings = [];
let languages = [];


try {
    let new_file = 'new_localizations.json';
    if (fs.existsSync(new_file)) {
        let newRawData = fs.readFileSync(new_file,{encoding: 'utf-8'});

        new_localizations=newRawData;

        new_localizations = JSON.parse(newRawData);

        console.log(new_localizations);

        if(typeof new_localizations.localizations[0].Name != 'undefined') {
            for (let index = 0; index < new_localizations.localizations.length; index++) {
                const element = new_localizations.localizations[index];

                switch(element.Name) {
                    case 'languages':
                        languages.push({
                            language: element.Value.language,
                            locale: element.Value.locale,
                            flag: element.Value.flag
                        });
                        break;
                    case 'strings':
                        let str = {};
                        for (const property in element) {
                            if(property.includes('Value.')) {
                                str[property.replace('Value.')] = element[property];
                            }
                        }
                        break;
                }

                if(element.Name==='languages') {

                }
                
            }
        }
    }
} catch(err) {
    console.log(err);
}

console.log(strings);
console.log(languages);