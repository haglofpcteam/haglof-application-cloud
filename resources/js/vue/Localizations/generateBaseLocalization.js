fs = require('fs');

var getFilesAndFolders = function(dir,object={files: [],folders :[]}) {
    var paths = fs.readdirSync(dir);
    var files = [];

    for (let pathsIndex = 0; pathsIndex < paths.length; pathsIndex++) {
        var fullPath = dir + '/' + paths[pathsIndex];
        files.push(fullPath);
    }

    for (let fileIndex = 0; fileIndex < files.length; fileIndex++) {
        let tempFile = files[fileIndex];
        let check = fs.statSync(tempFile);
        
        if (check.isDirectory()) {
            object.folders.push(tempFile);
            object=getFilesAndFolders(tempFile,object);
        } 
        else {
            object.files.push(tempFile);
        }
    }

    return object;
}

var getExtension = function (file) {
    var re = /(?:\.([^.]+))?$/;
    return re.exec(file)[1];
}

var getRootFolder = function (file) {
    var temp = file.split('/');
    temp.pop();
    return temp.join('/');
}

var startDir=getRootFolder(process.cwd());

var obj = getFilesAndFolders(startDir);

let rawdata = fs.readFileSync('localizations.json');
let localizations = JSON.parse(rawdata);

var count=0;

for (let index = 0; index < localizations.strings.length; index++) {
    localizations.strings[index].count=0;
}

for (let fileIndex = 0; fileIndex < obj.files.length; fileIndex++) {
    let file=obj.files[fileIndex];
    if(getExtension(file)==='vue') {
        let data = fs.readFileSync(file);

        let pattern = /localization[(](.+?)[)]/g;

        while ((matches = pattern.exec(data)) !== null) {
            let str = matches[1];
            let index = -1;

            if(str[0]==="'"&&str[str.length-1]==="'") {
                str = str.substring(1,str.length-1);
            }
            
            for (let i = 0; i < localizations.strings.length; i++) {
                if(localizations.strings[i].en===str) {
                    index=i;
                    localizations.strings[i].count++;
                }
            }

            if(index===-1) {
                index=localizations.strings.length;
                localizations.strings.push({file: file, count: 1, en: str});
            }

            for (let langIndex = 0; langIndex < localizations.languages.length; langIndex++) {
                let lang = localizations.languages[langIndex];

                if(typeof localizations.strings[index][lang.locale] === 'undefined') {
                    localizations.strings[index][lang.locale]="";
                }

            }       
        }  
    }
}

let newStrings = [];

for (let index = 0; index < localizations.strings.length; index++) {
    if(localizations.strings[index].count>0) {
        newStrings.push(localizations.strings[index]);
    }
}

localizations.strings=newStrings;

fs.writeFileSync('localizations.json', JSON.stringify(localizations));

console.log("Found " + localizations.strings.length + " strings");








