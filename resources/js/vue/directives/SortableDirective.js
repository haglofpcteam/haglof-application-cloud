import Sortable from 'sortablejs/modular/sortable.core.esm.js';
const SortableDirective = {
    inserted( el, binding, vnode ) {
        let options = binding.value;
        options.onUpdate = (e) => vnode.data.on.sorted( e );
        const sortable = Sortable.create( el, binding.value );
    }
};
export default SortableDirective;