import store from '@/store';

export function RoutesValidateNotLoggedIn(to, from, next) {
  const isAuthenticated = store.getters['auth/isAuthenticated'];

  if (isAuthenticated) {
      return next({
          name: 'organizations',
      });
  }
  return next();
}

export function RoutesValidateLoggedIn(to, from, next) {
  const isAuthenticated = store.getters['auth/isAuthenticated'];


  if (!isAuthenticated) {
      store.commit('setIntendedUrl',to.path);
      return next({
          name: 'login',
      });
  }
  return next();
}

export function RoutesValidateLoggedInAndActivated(to, from, next) {
  const isAuthenticated = store.getters['auth/isAuthenticated'];
  const isActivated = store.getters['auth/isActivated'];

  if (!isAuthenticated) {
    store.commit('setIntendedUrl',to.path);
    return next({
        name: 'login',
    });
  }

  if(!isActivated) {
      return next({
        name: 'edit_user',
    });
  }

  return next();
}