<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" value="{{ csrf_token() }}"/>
        <title>Haglof Cloud Services</title>


        <link rel="stylesheet" type="text/css" href="{{ asset('css/stimulsoft.designer.office2013.whiteblue.css') }}">
        <link rel="stylesheet" type="text/css" href="/css/stimulsoft.viewer.office2013.whiteblue.css">

        <script src="{{ mix('js/manifest.js') }}" type="text/javascript" defer></script>
        <script src="{{ mix('js/vendor.js') }}" type="text/javascript" defer></script>
        <script src="{{ mix('js/app.js') }}" type="text/javascript" defer></script>
        <script src="{{ mix('js/vanilla.js') }}" type="text/javascript"></script>
        <script type="text/javascript" src="https://appleid.cdn-apple.com/appleauth/static/jsapi/appleid/1/en_US/appleid.auth.js"></script>

        <script>
            var openAndClose = function(url) {
                window.location.href = url;
            }
        </script>

        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.ico') }}"/>

    </head>
    <body onload="openAndClose('{{ $back_to_link_url }}')">
        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header text-white bg-dark">Finish authenticating</div>
                            <div class="card-body">
                                Close this window to return to application.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>
</html>