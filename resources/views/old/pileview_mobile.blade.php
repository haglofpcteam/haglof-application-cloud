<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

    <!-- Icon -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.ico') }}"/>

    <!-- Styles -->
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link href="{{ mix('/css/pile_mobile.css') }}" rel="stylesheet">

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{ mix('/js/pile.js') }}" type="text/javascript"></script>

    <script type="text/javascript" defer>

        window.initMeasurement('{{ $pileApplicationMeasurement->id }}',true,'{{ $pileApplicationMeasurement->location()->first()->measurement_system }}');

        window.addEventListener("orientationchange", function() {
            window.orientationChange();
        });

        function toggleModelOverview(elementId) {
            var elem = document.getElementById(elementId);
            if(elem.style.display == "block") {
                elem.style.display="none";
            }
            else {
                elem.style.display="block";
            }
        }

    </script>

    <style>

        body { margin: 0;}
        canvas { width: 100%; height: 100% }

    </style>
</head>
<body>
    <div id="app">

        <div id="pile_application">

        </div>

        <div id="right-menu">
            <a href='#' class="btn btn-default bg-light border" onclick="toggleModelOverview('controls_pane')"><i class="fa fa-fw  fa-bars"></i></a>
        </div>

        <div id="left-menu">
            <a href="{{route('show_application',$applicationInstance)}})" class="btn btn-default bg-light border"><i class="fa fa-fw fa-chevron-left"></i></a><br>
            <a href='#' class="btn btn-default bg-light border mt-2" onclick="toggleModelOverview('model_overview')"><i class="fa fa-fw fa-info-circle"></i></a><br>
            <a href='#' id="target_detail" style="display: none;" class="btn btn-default bg-light border mt-2" onclick="toggleModelOverview('target_detail_box')"><i class="fa fa-bullseye"></i></a>
        </div>

        <div id="model_overview" class="overlay" onclick="toggleModelOverview('model_overview')">
            <div class="white_box">
                <h5>{{__('Overview')}}</h5>
                <div class="row">
                        <div class="col">
                            <b class="text-nowrap">{{__('Number of measurments:')}}</b><br>
                            <b class="text-nowrap">{{__('Volume:')}}</b><br>
                            <b class="text-nowrap">{{__('Base area:')}}</b><br>
                            <b class="text-nowrap">{{__('Periphery:')}}</b><br>
                            <b class="text-nowrap">{{__('Average height:')}}</b><br>
                            <b class="text-nowrap">{{__('Weight:')}}</b><br>
                            <b class="text-nowrap">{{__('Density:')}}</b><br>
                            <b class="text-nowrap">{{__('Slope angle:')}}</b><br>
                            <b class="text-nowrap">{{__('Volume adjustment factor:')}}</b><br>
                            <b class="text-nowrap">{{__('Unit production id:')}}</b><br>
                            <b class="text-nowrap">{{__('Unit version id:')}}</b><br>
                            <b class="text-nowrap">{{__('Unit serial number:')}}</b><br>
                            <b class="text-nowrap">{{__('Transponder height:')}}</b><br>
                            <b class="text-nowrap">{{__('Eye height:')}}</b><br>
                            <b class="text-nowrap">{{__('Pivot offset:')}}</b><br>
                            <b class="text-nowrap">{{__('Magnetic declination:')}}</b><br>
                            <b class="text-nowrap">{{__('File id:')}}</b><br>
                            <b class="text-nowrap">{{__('User:')}}</b><br>
                            

                        </div>
                        <div class="col">
                            <span id="model_overview_number_of_measurments"></span><br>
                            <span id="model_overview_volume"></span><br>
                            <span id="model_overview_base_area"></span><br>
                            <span id="model_overview_periphery"></span><br>
                            <span id="model_overview_average_height"></span><br>
                            <span id="model_overview_weigth"></span><br>
                            <span id="model_overview_density"></span><br>
                            <span id="model_overview_slope_angle"></span>° (<span id="model_overview_slope_angle_percent"></span> %)<br>
                            <span id="model_overview_volume_adjustment_factor"></span><br>
                            <span id="model_overview_unit_production_id"></span><br>
                            <span id="model_overview_unit_version_id"></span><br>
                            <span id="model_overview_unit_serial_number"></span><br>
                            <span id="model_overview_transponder_height"></span><br>
                            <span id="model_overview_eye_height"></span><br>
                            <span id="model_overview_pivot_offset"></span><br>
                            <span id="model_overview_magnetic_declination"></span><br>
                            <span id="model_overview_file_id"></span><br>
                            <span id="model_overview_user"></span><br>
                        
                        </div>
                    </div>
            </div>
        </div>

        <div id="controls_pane" class="overlay" onclick="toggleModelOverview('controls_pane')">
            <div class="white_box">
                <h5>{{__('Menu')}}</h5>
                <button type="button" class="menu-button" onclick="window.togglePointsAndLines()">{{__('Show Points/Lines')}}</button>
                <button type="button" class="menu-button" onclick="window.toggleMap()">{{__('Show Map')}}</button>
                <button type="button" class="menu-button"  onclick="window.toggleGrayPile()">{{__('Show Gray Pile')}}</button>
                <button type="button" class="menu-button"  onclick="window.toggleBrownPile()">{{__('Show Brown Pile')}}</button>

                @if($applicationInstance->paid)
                <button type="button" class="menu-button"  onclick="window.generateReport()">{{__('Generate Report')}}</button>
                <button type="button" class="menu-button"  onclick="window.takeSnapShot()">{{__('Take Snapshot')}}</button>
                <button type="button" class="menu-button"  id="show_snapshot_button" data-toggle="modal" data-target="#imageModal" disabled>{{__('View Image')}}</button>
                @endif
            </div>
        </div>

        <div id="target_detail_box" class="overlay" onclick="toggleModelOverview('target_detail_box')">
            <div class="white_box">
                <h5>{{__('Target detail')}}</h5>
                <table>
                    <tr>
                        <td><b>{{__('Type:')}}</b></td>
                        <td><span id="target_detail_type"></span></td>
                    </tr>
                    <tr class="d-none">
                        <td><b>{{__('Latitude:')}}</b></td>
                        <td><span id="target_detail_latitude"></span></td>
                    </tr>
                    <tr class="d-none">
                        <td><b>{{__('Longitude:')}}</b></td>
                        <td><span id="target_detail_longitude"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('Altitude:')}}</b></td>
                        <td><span id="target_detail_altitude"></span></td>
                    </tr>
                    <tr class="d-none">
                        <td><b>{{__('HDOP:')}}</b></td>
                        <td><span id="target_detail_hdop"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('Date:')}}</b></td>
                        <td><span id="target_detail_date"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('UTC:')}}</b></td>
                        <td><span id="target_detail_utc"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('Seq:')}}</b></td>
                        <td><span id="target_detail_seq"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('SD:')}}</b></td>
                        <td><span id="target_detail_sd"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('HD:')}}</b></td>
                        <td><span id="target_detail_hd"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('H:')}}</b></td>
                        <td><span id="target_detail_h"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('Pitch:')}}</b></td>
                        <td><span id="target_detail_pitch"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('Azimut:')}}</b></td>
                        <td><span id="target_detail_az"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('X:')}}</b></td>
                        <td><span id="target_detail_x"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('Y:')}}</b></td>
                        <td><span id="target_detail_y"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('Z:')}}</b></td>
                        <td><span id="target_detail_z"></span></td>
                    </tr>
                    <tr>
                        <td><b>{{__('UTM Zone:')}}</b></td>
                        <td><span id="target_detail_utm_zone"></span></td>
                    </tr>
                </table>

                <button type="button" class="mt-3" onclick="window.deletePoint()">{{__('Delete point')}}</button>

            </div>
        </div>


        <!-- Image Modal -->
        <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">{{__('Current Snapshot')}}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <img id="snapshot_image_container" width="100%" height="100%"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    <!-- Report Modal -->
    <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('Select report template')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="modal-body">
                        <script>
                            $(document).ready(function () {
                                $('#report_form').validate(
                                {
                                    rules:
                                    {
                                        template_id:{ required: true }
                                    },
                                    messages:
                                    {
                                        template_id:
                                        {
                                            required:"Please select a template"
                                        }
                                    },
                                    errorPlacement: function(error, element) {
                                        $(error).addClass("text-danger");
                                        if($(element).attr('name')=="template_id") {
                                            var p = element.parent().parent();
                                            error.appendTo( p );
                                            $(p).addClass("border").addClass("border-danger").addClass("p-2");
                                        }
                                        else {
                                            error.appendTo( element.parent() );
                                        }
                                        
                                    },
                                    submitHandler: function() { 
                                        $('#reportModal').modal('hide');
                                        window.open('about:blank','print_popup','width=630,height=891');
                                        form.submit();
                                    }

                                });

                            });

                        </script>
                        <form 
                            action="{{route('pileapplication_generate_report',$pileApplicationMeasurement)}}"
                            method="POST"
                            id="report_form"
                            target="print_popup">
                            @csrf
                        <div>
                        @foreach($report_templates as $template)                        
                            <div class="form-check">
                                <input 
                                    class="form-check-input" 
                                    type="radio" 
                                    name="template_id" 
                                    id="template_{{$template->id}}" 
                                    value="{{$template->id}}"
                                    onchange="$('.template_parameters').addClass('d-none');$('#template_{{$template->id}}_parameters').removeClass('d-none');"
                                    >
    
                                <label class="form-check-label" for="template_{{$template->id}}">
                                        {{$template->name}}
                                </label>
                            </div>
    
                            <div class="d-none template_parameters" id="template_{{$template->id}}_parameters">
                                @foreach($template->parameters()->get() as $parameter)
                                <div class="form-group mt-4 row">
                                    <label for="template_{{$template->id}}_parameter_{{$parameter->name}}" class="col-md-4 col-form-label text-md-right">{{$parameter->name}}: </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="template_{{$template->id}}_parameter_{{$parameter->name}}" id="template_{{$template->id}}_parameter_{{$parameter->name}}">
                                    </div>
                                </div>
                                @endforeach
                            </div>
    
                        @endforeach
                        </div>
                        <div class="TemplateValidation text-danger"></div>    
    
                        
                    </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Generate"/>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                </div>
                </form>
            </div>
            </div>
        </div>
</body>
</html>


