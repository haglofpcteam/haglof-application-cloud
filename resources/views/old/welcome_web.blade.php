<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Haglöf Application Cloud') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <!-- Icon -->
        <link rel="shortcut icon" type="image/png" href="{{ asset('img/favicon.ico') }}"/>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #004798;
                color: #fff;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #fff;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links > a:hover {
                text-decoration: underline;
            }

            .steps {
                margin-left: auto;
                margin-right: auto;
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .steps li {
                white-space: nowrap;
                text-align: left;
            }

            .steps li:before {
                content: "";
                margin-left: 0.5rem;
            }

            .information {
                margin-top: 30px;
            }

            .information h4 {
                font-weight: 900;
            }

            .applications {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .application {
                padding-left: 10px;
                padding-right: 10px;
                text-align: left;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .display-block {
                display: block;
            }

            .display-inline-block {
                display: inline-block;
                vertical-align: middle;
            }

        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">{{ __('Home')}}</a>
                    @else
                        <a href="{{ route('login') }}">{{ __('Login')}}</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">{{ __('Register')}}</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <h3>Haglof Cloud Services</h3>
                </div>

                <hr>

                <div class="links">
                    @guest
                    <a href="{{ route('login') }}">{{ __('Login') }}</a>
                    @if (Route::has('register'))
                    <a href="{{ route('register') }}">{{ __('Register') }}</a>
                    @endif
                    @endguest
                    <a href="https://haglof.app/privacy-policy/">{{ __('Privacy policy')}}</a>
                    <a href="https://haglof.app/terms-and-conditions/">{{ __('Terms and conditions')}}</a>
                </div>

                <hr>

                <div class="information">
                    {{__('This is a platform where Haglöf Sweden AB provides different application services.')}}<br>
                    {{__('Read more about the services at')}}<br>
                    <div class="links">
                        <a href="https://www.haglof.app">{{ __('Haglof Application Cloud')}}</a>
                    </div>
                    {{__('or')}}
                    <h3>{{__('Get Started with our new service!')}}</h3>

                    {{__('Haglof Application Cloud is a platform where we will provide different application services.')}}<br>
                    
                    {{__('Create your free account , Get started in three easy steps')}}<br>
                    
                    <div class="steps">
                        <ol>
                            <li>{{__('Register and create a free Account')}}</li>
                            <li>{{__('Verify your information')}}</li>
                            <li>{{__('Start using our services')}}</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>