@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">Overview User</div>

            <div class="card-body">


                @if(Auth::user()->role<2)
                <label>{{__('Name:')}} </label> {{ $user->name}}<br>
                <label>{{__('E-mail:')}} </label> {{ $user->email }}<br>

                @else

                <form method="POST" action="{{ route('update_user',$user) }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$user->name) }}"
                                    required autofocus> @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span> @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email',$user->email) }}"
                                    required>
                                    @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span> @endif
                            </div>
                        </div>

                        <div class="form-group row">
                        <label for="verified" class="col-md-4 col-form-label text-md-right">{{ __('Verified at:') }}</label>
                        <div class="col-md-6">
                        <input id="verified" type="date" class="form-control{{ $errors->has('verified') ? ' is-invalid' : '' }}" name="verified" value="{{ old('verified',date('Y-m-d',strtotime($user->email_verified_at))) ? '' : old('verified',date('Y-m-d',strtotime($user->email_verified_at))) }}">
                        @if ($errors->has('verified'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('verified') }}</strong>
                                </span> @endif
                            </div>

                            </div>
                        <div class="form-group row">
                        <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Role:') }}</label>
                        <div class="col-md-6">
                        <input id="role" type="number" class="form-control{{ $errors->has('role') ? ' is-invalid' : '' }}" name="role" min="0" max="{{Auth::user()->role-1}}" value="{{ old('role',$user->role) }}">
                        @if ($errors->has('role'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('role') }}</strong>
                        </span> @endif
                    </div>
                </div>
                    <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Timezone') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" name="timezone">
                                    @foreach(DateTimeZone::listIdentifiers(DateTimeZone::ALL) as $key => $value)
                                    @if($value==$user->timezone)
                                    <option value="{{$value}}" selected>{{$value}}</option>
                                    @else
                                    <option value="{{$value}}">{{$value}}</option>
                                    @endif
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                        <div class="form-group row justify-content-center">
                            <div class="col-xs-2 col-sm-2">
                                <button type="submit" class="btn btn-primary btn-block">
                                {{ __('Save') }}
                            </button>
                            </div>
                        </div>


                    </form>
                @endif

                @if($user->organizations()->first())

                <br><br>

                <table class="table">
                    <thead>
                        <tr>
                            <th colspan=2 scope="col">{{__('Organizations')}}</th>
                        </tr>
                        <tr>
                            <th scope="col">{{__('#')}}</th>
                            <th scope="col">{{__('Organization')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($user->organizations()->get() as $organization)
                            <tr>
                                <th scope="row">{{ $organization->id }}</th>
                                <td class='clickable-cell' data-href='{{route("admin_show_organization",$organization) }}'> {{$organization->name}} </td>
                                @if(Auth::user()->role>1)
                                <td><a data-toggle="modal" href="#removeOrganization{{ $organization->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                @endif
                                <!-- Remove User Modal -->
                                <div id="removeOrganization{{ $organization->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{ __('Remove user from organization?')}}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                {{ __('Are you sure you want to remove user :user_name from :organization_name',['user_name'=>$user->name,'organization_name'=>$organization->name])}}

                                            </div>
                                            <div class="modal-footer">
                                                    <button type="submit" class="btn btn-default" onclick="window.location.href = '{{ route('organization_remove_admin',[$organization,$user]) }}';">{{ __('Yes')}}</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('No')}}</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @endif

                @if($user->applicationInstances()->first())

                <br><br>

                <table class="table">
                    <thead>
                        <tr>
                            <th colspan=3 scope="col">{{__('Application Instances')}}</th>
                        </tr>
                        <tr>
                            <th scope="col">{{__('#')}}</th>
                            <th scope="col">{{__('Application')}}</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($user->applicationInstances()->get() as $applicationInstance)
                            <tr>
                                <th scope="row">{{ $applicationInstance->id }}</th>
                                <td class='clickable-cell' data-href='{{route("admin_show_application_instance",$applicationInstance)}}'>
                                    {{$applicationInstance->application()->first()->name}}<br>
                                    <em>{{$applicationInstance->organization()->first()->name }}</em>
                                </td>
                                @if(Auth::user()->role>1)
                                <td><a data-toggle="modal" href="#removeApplicationInstance{{ $applicationInstance->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                @endif
                                <!-- Remove User Modal -->
                                <div id="removeApplicationInstance{{ $applicationInstance->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{ __('Remove user from application instance?')}}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                    {{ __('Are you sure you want to remove user :user_name from :application_name',['user_name'=> $user->name,'application_name'=>$applicationInstance->name])}}

                                            </div>
                                            <div class="modal-footer">
                                                    <button type="submit" class="btn btn-default" onclick="window.location.href = '{{ route('application_instance_remove_user',[$applicationInstance,$user]) }}';">{{ __('Yes')}}</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('No')}}</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @endif

                <br><br>
                @if(Auth::user()->role>9)
                <a href="{{route('admin_show_revision_history_user',$user)}}">{{__('Show revision history')}}</a>
                @endif

            </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
