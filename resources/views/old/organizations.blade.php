@extends('layouts.app')
@section('script')

@if ($show_create_dialog==true)
<script type="text/javascript">
    $(document).ready(function(){
        $('#createOrganizationModal').modal('show');
    });

</script>
@endif
@endsection

@section('parent-url', route('start'))

@section('menu')

    <div class="col-md-auto p-4 text-right">
        <h4 class="text-white">{{ __('Organizations')}}</h4>
        <ul class="list-unstyled">
            <li>
                <a data-toggle="modal" href="#createOrganizationModal" class="text-white">
                        {{ __('Add Organization')}}
                </a>
            </li>
        </ul>
    </div>

@endsection


@section('content')

@if(!Agent::isMobile())
<section class="jumbotron text-center" style="background-image:url('{{asset('img/jumbotron/' . $random_jumbotron)}}');">
    <div class="jumbotron-container">
        <div class="jumbotron-box">
            <h1 class="jumbotron-heading text-white">{{env("APP_NAME","Haglof Cloud Services")}}</h1>
            <p class="lead text-white">Get to know your forest</p>
        </div>
    </div>
</section>
@endif

<main class="py-4">
    <div class="container">

        @if(!$organizations->count())
        <div class="col d-flex justify-content-center">
                <a data-toggle="modal" href="#createOrganizationModal">
                    <i class="fas fa-plus fa-7x text-dark"></i>
                </a>
        </div>
        @else

        <div class="col-md-12">
            @foreach($organizations->get() as $organization)
            <div class="card mb-5">
                <div class="card-header text-white bg-dark">
                    <div class="row">
                        <div class="col-10">
                            {{ $organization->name }}
                        </div>
                        @if($organization->admin)
                        <div class="col text-right">
                            <a href="{{ route('edit_organization',$organization) }}" class="text-white">
                                <i class="fa fa-cog"></i>
                            </a>
                        </div>
                        @endif
                    </div>
                </div>

                <div class="card-body">
                    @foreach(Auth::user()->applicationInstances()->where('organization_id',$organization->id)->get() as $applicationInstance)
                    <div class="clickable-element application-icon" data-href='{{ route('show_application',$applicationInstance) }}'>
                        <div class="application-icon-div">
                            <img src="{{$applicationInstance->application()->first()->icon}}">
                        </div>
                        
                        <br>
                        {{ $applicationInstance->application()->first()->name }}
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach

        </div>
        @endif
    </div>

</main>


<!-- Create Organization Modal -->
<div id="createOrganizationModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form method="POST" action="{{ route('store_organization') }}">
                @csrf
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Create Organization')}}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <label for="organization_name">{{ __('Organization name') }}</label>
                    <input id="organization_name" type="text" class="form-control{{ $errors->has('organization_name') ? ' is-invalid' : '' }}" name="organization_name" required autofocus>

                    @if ($errors->has('organization_name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('organization_name') }}</strong>
                        </span>
                    @endif

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-default">{{ __('Create')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close')}}</button>
                </div>
            </form>
        </div>

    </div>
</div>
@endsection
