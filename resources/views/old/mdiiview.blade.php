@extends('layouts.app')

@section('script')

<script>
    
    var mdiicom = new mdii("mdiicom_area",{{$mdiiApplication->id}},{{Agent::isMobile()?'true':'false'}});
    mdiicom.load();

</script>

@endsection

@section('parent-url', route('start'))

@section('content')

@if(!Agent::isMobile())
<section class="jumbotron text-center" style="background-image:url('{{asset('img/jumbotron/' . $random_jumbotron)}}');">
    <div class="jumbotron-container">
        <div class="jumbotron-box">
            <h1 class="jumbotron-heading text-white">{{__("Digitech Cloud")}}</h1>
            <p class="lead text-white">{{__('A Reporting tool for Digitech Caliper')}}</p>
        </div>
    </div>
</section>
@endif

<main class="py-4">
    <div class="container">
        <div class="row" id="mdiicom_area">

        </div>
    </div>

</main>
@endsection
