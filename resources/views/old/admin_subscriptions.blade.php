@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{ __('Subscriptions')}}</div>

            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('ID')}}</th>
                            <th scope="col">{{ __('Status')}}</th>
                            <th scope="col">{{ __('Modified')}}</th>
                            <th scope="col">{{ __('E-mail')}}</th>
                            <th scope="col">{{ __('Application')}}</th>
                            <th scope="col">{{ __('Organization')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($subscriptions as $subscription)
                            <tr>
                                <td>{{$subscription->ID}}</td>
                                <td>{{$subscription->post_status}}</td>
                                <td>{{$subscription->post_modified}}</td>
                                <td>{{$subscription->email}}</td>
                                @if($subscription->application)
                                <td>{{$subscription->application->name}}</td>
                                @endif
                                @if($subscription->applicationInstance)
                                <td>{{$subscription->applicationInstance->organization()->first()->name}}</td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
