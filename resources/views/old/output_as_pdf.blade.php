<html>
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
    <script src="{{asset('/js/html2canvas.min.js')}}"></script>
    <script id="htmlTemplate" type="text/x-tmpl">
        <html>
            <head>

            </head>
            <body>
                Hej
            </body>
        </html>
    </script>
    <script>
        var doc = new jsPDF();

        var html = document.getElementById('htmlTemplate').innerHTML;

        console.log(html);

        var specialElementHandlers = {
            '#bypassme': function(element, renderer) {
                return true;
            }
        };

        doc.fromHTML(
        html, // HTML string or DOM elem ref.
        0.5, // x coord
        0.5, // y coord
        {
        'width': 7.5, // max width of content on PDF
        'elementHandlers': specialElementHandlers
        });

        doc.output('dataurl');

    </script>
</head>
</html>