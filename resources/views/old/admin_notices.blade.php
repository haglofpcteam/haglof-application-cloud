@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{ __('Notices')}}</div>

            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('Time')}}</th>
                            <th scope="col">{{ __('User')}}</th>
                            <th scope="col">{{ __('Message')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($notices as $notice)
                            <tr>
                                @if($notice->read_at)
                                    <td class="text-nowrap">{{ $notice->created_at }}</td>
                                @else
                                    <th scope="row" class="text-nowrap">{{ $notice->created_at }}</th>
                                @endif


                                @if($notice->user()->first())
                                <td class="text-nowrap"><a href='{{ route('admin_show_user',$notice->user()->first()) }}'>{{ $notice->user()->first()->identifiableName() }}</a></td>
                                @else
                                <td></td>
                                @endif


                                <td>{{ $notice->message }}</td>

                                @if($notice->read_at)
                                <td></td>
                                @else
                                <form action="{{route('admin_notice_mark_read',$notice)}}">
                                    <td><input type="submit" value="X"></td>
                                </form>

                                @endif

                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
