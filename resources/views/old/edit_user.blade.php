@extends('layouts.app')
@section('parent-url', URL::previous())
@section('content')

@if(!Agent::isMobile())
    <section class="jumbotron text-center" style="background-image:url('{{asset('img/jumbotron/' . $random_jumbotron)}}');">
    <div class="jumbotron-container">
        <div class="jumbotron-box">
            <h1 class="jumbotron-heading text-white">{{__('Settings')}}</h1>
            <p class="lead text-white">{{__('User')}}</p>
        </div>
    </div>
    </section>
@endif

<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-white bg-dark">{{ __('Settings')}}</div>

                    <div class="card-body">


                        <form method="POST" action="{{ route('update_user',$user) }}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$user->name) }}"
                                        required autofocus> @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span> @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email',$user->email) }}"
                                        required> @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span> @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="email-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email-confirm" type="email" class="form-control{{ $errors->has('email-confirm') ? ' is-invalid' : '' }}" name="email-confirm"
                                        value="{{ old('email-confirm',$user->email) }}" required>
                                </div>
                            </div>

                            @if($user->password)
                            <div class="form-group row">
                                <label for="old_password" class="col-md-4 col-form-label text-md-right">{{ __('Old Password') }}</label>

                                <div class="col-md-6">
                                    <input id="old_password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="old_password">                                    @if ($errors->has('old_password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('old_password') }}</strong>
                                    </span> @endif
                                </div>
                            </div>
                            @endif

                            <div class="form-group row">
                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">                                    @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span> @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="timezone" class="col-md-4 col-form-label text-md-right">{{ __('Timezone') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="timezone">
                                        @foreach(DateTimeZone::listIdentifiers(DateTimeZone::ALL) as $key => $value)
                                        @if($value==$user->timezone)
                                        <option value="{{$value}}" selected>{{$value}}</option>
                                        @else
                                        <option value="{{$value}}">{{$value}}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="measurement_system" class="col-md-4 col-form-label text-md-right">{{ __('Measurement System') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="measurement_system">
                                        @if($user->measurement_system=="metric")
                                        <option value="metric" selected>{{__('Metric')}}</option>
                                        <option value="imperial">{{__('Imperial')}}</option>
                                        @else
                                        <option value="metric">{{__('Metric')}}</option>
                                        <option value="imperial" selected>{{__('Imperial')}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date_format" class="col-md-4 col-form-label text-md-right">{{ __('Date Format') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="date_format">
                                        @if($user->date_format=="Y-m-d")
                                        <option value="Y-m-d" selected>{{__('YYYY-MM-DD')}}</option>
                                        <option value="m/d/Y">{{__('MM/DD/YYYY')}}</option>
                                        @else
                                        <option value="Y-m-d">{{__('YYYY-MM-DD')}}</option>
                                        <option value="m/d/Y" selected>{{__('MM/DD/YYYY')}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="time_format" class="col-md-4 col-form-label text-md-right">{{ __('Time Format') }}</label>

                                <div class="col-md-6">
                                    <select class="form-control" name="time_format">
                                        @if($user->time_format=="G:i:s")
                                        <option value="G:i:s" selected>{{__('HH:MM:SS')}}</option>
                                        <option value="g:i:s A">{{__('HH:MM:SS AM/PM')}}</option>
                                        @else
                                        <option value="G:i:s">{{__('HH:MM:SS')}}</option>
                                        <option value="g:i:s A" selected>{{__('HH:MM:SS AM/PM')}}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>



                            <div class="form-group row justify-content-center">
                                <div class="col-xs-2 col-sm-2">
                                    <button type="submit" class="btn btn-primary btn-block">
                                    {{ __('Save') }}
                                </button>
                                </div>
                            </div>


                        </form>

                        <hr>

                        <div class="container">

                            <div class="row row-sm-offset-3 socialButtons justify-content-center">

                                <!-- Facebook -->
                                <div class="col-xs-2 col-sm-2">
                                        @if($user->socialiteProviders()->where('provider','facebook')->exists())

                                        <a href="{{route('socialite_remove_provider','facebook')}}" class="btn btn-lg btn-block btn-facebook bg-light border" data-toggle="tooltip" data-placement="top" title="{{ __('Remove Facebook Connection')}}">
                                        <i class="fab fa-facebook text-muted"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @else

                                        <a href="{{route('socialite_redirect_provider','facebook')}}" class="btn btn-lg btn-block btn-facebook border" data-toggle="tooltip" data-placement="top" title="{{ __('Connect with Facebook')}}">
                                        <i class="fab fa-facebook"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @endif
                                    </div>

                                <!-- Twitter -->
                                <div class="col-xs-2 col-sm-2">
                                    @if($user->socialiteProviders()->where('provider','twitter')->exists())

                                    <a href="{{route('socialite_remove_provider','twitter')}}" class="btn btn-lg btn-block btn-twitter bg-light border" data-toggle="tooltip" data-placement="top" title="{{ __('Remove Twitter Connection')}}">
                                    <i class="fab fa-twitter text-muted"></i>
                                    <span class="hidden-xs"></span>
                                    </a> @else

                                    <a href="{{route('socialite_redirect_provider','twitter')}}" class="btn btn-lg btn-block btn-twitter border" data-toggle="tooltip" data-placement="top" title="{{ __('Connect with Twitter')}}">
                                    <i class="fab fa-twitter"></i>
                                    <span class="hidden-xs"></span>
                                    </a> @endif
                                </div>

                                <!-- Google -->
                                <div class="col-xs-2 col-sm-2">
                                        @if($user->socialiteProviders()->where('provider','google')->exists())

                                        <a href="{{route('socialite_remove_provider','google')}}" class="btn btn-lg btn-block btn-google bg-light border" data-toggle="tooltip" data-placement="top" title="{{ __('Remove Google Connection')}}">
                                        <i class="fab fa-google text-muted"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @else

                                        <a href="{{route('socialite_redirect_provider','google')}}" class="btn btn-lg btn-block btn-google border" data-toggle="tooltip" data-placement="top" title="{{ __('Connect with Google')}}">
                                        <i class="fab fa-google"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @endif
                                    </div>

                            </div><br>

                            <div class="row row-sm-offset-3 socialButtons justify-content-center">
                                <!-- Github -->
                                <div class="col-xs-2 col-sm-2">
                                        @if($user->socialiteProviders()->where('provider','github')->exists())

                                        <a href="{{route('socialite_remove_provider','github')}}" class="btn btn-lg btn-block btn-github bg-light border" data-toggle="tooltip" data-placement="top" title="{{ __('Remove Github Connection')}}">
                                        <i class="fab fa-github text-muted"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @else

                                        <a href="{{route('socialite_redirect_provider','github')}}" class="btn btn-lg btn-block btn-github border" data-toggle="tooltip" data-placement="top" title="{{ __('Connect with Github')}}">
                                        <i class="fab fa-github"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @endif
                                    </div>

                                <!-- LinkedIn -->
                                <div class="col-xs-2 col-sm-2">
                                        @if($user->socialiteProviders()->where('provider','linkedin')->exists())

                                        <a href="{{route('socialite_remove_provider','linkedin')}}" class="btn btn-lg btn-block btn-linkedin bg-light border" data-toggle="tooltip" data-placement="top" title="{{ __('Remove LinkedIn Connection')}}">
                                        <i class="fab fa-linkedin text-muted"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @else

                                        <a href="{{route('socialite_redirect_provider','linkedin')}}" class="btn btn-lg btn-block btn-linkedin border" data-toggle="tooltip" data-placement="top" title="{{ __('Connect with LinkedIn')}}">
                                        <i class="fab fa-linkedin"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @endif
                                    </div>

                                <!-- Microsoft -->
                                <div class="col-xs-2 col-sm-2">
                                        @if($user->socialiteProviders()->where('provider','microsoft')->exists())

                                        <a href="{{route('socialite_remove_provider','microsoft')}}" class="btn btn-lg btn-block btn-microsoft bg-light border" data-toggle="tooltip" data-placement="top" title="{{ __('Remove Microsoft Connection')}}">
                                        <i class="fab fa-windows text-muted"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @else

                                        <a href="{{route('socialite_redirect_provider','microsoft')}}" class="btn btn-lg btn-block btn-microsoft border" data-toggle="tooltip" data-placement="top" title="{{ __('Connect with Microsoft')}}">
                                        <i class="fab fa-windows"></i>
                                        <span class="hidden-xs"></span>
                                        </a> @endif
                                    </div>

                            </div><br>

                        </div>


                    </div>



                </div>
            </div>

        </div>
    </div>
</main>
@endsection
