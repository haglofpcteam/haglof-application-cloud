@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{ __('User list')}}</div>

            <div class="card-body">

                    @if(Agent::isMobile())
                    <div class="table-responsive">
                            <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('#')}}</th>
                            <th scope="col">{{ __('User')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($users as $user)
                            <tr class='clickable-row' data-href='{{ route('admin_show_user',$user) }}'>
                                <th scope="row">{{ $user->id }}</th>
                                <td>
                                    {{ $user->identifiableName() }}<br>
                                    {{ $user->email }}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                    @else
                    <table class="table">
                    <thead>
                            <tr>
                                <th scope="col">{{ __('#')}}</th>
                                <th scope="col">{{ __('Name')}}</th>
                                <th scope="col">{{ __('E-mail')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                                <tr>
                                    <th scope="row">{{ $user->id }}</th>
                                    <td><a href='{{ route('admin_show_user',$user) }}'>{{ $user->identifiableName() }}</a></td>
                                    <td>{{ $user->email }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    @endif

            </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
