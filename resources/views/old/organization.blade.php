@extends('layouts.app')
@section('parent-url', route('organizations'))
@section('content')

@if(!Agent::isMobile())
<section class="jumbotron text-center" style="background-image:url('{{asset('img/jumbotron/' . $random_jumbotron)}}');">
    <div class="jumbotron-container">            
        <div class="jumbotron-box">
            <h1 class="jumbotron-heading text-white">{{ __('Settings') }}</h1>
            <p class="lead text-white">{{__('Organization')}}</p>
        </div>
    </div>
</section>
@endif

<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-white bg-dark">{{ $organization->name }}</div>

                    <div class="card-body">

                        <form method="POST" action="{{ route('update_organization',$organization) }}">
                            @csrf

                            <label for="organization_name">{{ __('Name:') }}</label>

                            <input id="organization_name" type="text" class="form-control{{ $errors->has('organization_name') ? ' is-invalid' : '' }}" value="{{ $organization->name }}" name="organization_name" required autofocus>

                            @if ($errors->has('organization_name'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('organization_name') }}</strong>
                                </span>
                            @endif

                            <br>
                            <button type="submit" class="btn btn-default border">{{ __('Save')}}</button>

                        </form>

                        @if($organization->applicationInstances()->first())

                        <br><br>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan=3 scope="col">{{__('Application Instances')}}</th>
                                </tr>
                                <tr>
                                    <th scope="col">{{__('#')}}</th>
                                    <th scope="col">{{__('Application')}}</th>
                                    @if(!Agent::isMobile())
                                    <th scope="col">{{__('Payment Plan')}}</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach($organization->applicationInstances()->get() as $applicationInstance)
                                    <tr>
                                        <th scope="row">{{ $applicationInstance->id }}</th>
                                        <td><a href={{route("application_instance_edit",$applicationInstance)}}> {{$applicationInstance->name}} </a></td>
                                        @if(!Agent::isMobile())
                                        <td>
                                            @if($applicationInstance->application()->first()->payment_link)
                                                <a href={{$applicationInstance->application()->first()->payment_link}}>Handle payment</a>
                                            @endif
                                        </a></td>
                                        <td><a data-toggle="modal" href="#removeApplicationInstance{{ $applicationInstance->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>

                                        <!-- Remove Application Instance -->
                                        <div id="removeApplicationInstance{{ $applicationInstance->id }}" class="modal fade" role="dialog">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">{{__('Remove Application?')}}</h4>
                                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                    </div>
                                                    <div class="modal-body">
                                                        {{__('Are you sure you want to remove :application_name',['application_name' => $applicationInstance->name])}}

                                                    </div>
                                                    <div class="modal-footer">
                                                            <button type="submit" class="btn btn-default" onclick="window.location.href = '{{ route('remove_application_instance',$applicationInstance)}}';">{{__('Yes')}}</button>
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('No')}}</button>
                                                        </form>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @endif


                        @if($applications->count())

                        <br><br>

                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan=3 scope="col">{{__('Create Application Instances')}}</th>
                                </tr>
                                <tr>
                                    <th scope="col">{{__('#')}}</th>
                                    <th scope="col">{{__('Application')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                    @foreach($applications as $application)
                                    <tr>
                                        <th scope="row">{{ $application->id }}</th>
                                        <td>{{$application->name}}</td>

                                        <td><a href={{ route( 'create_application_instance',[$organization,$application])}}>{{__('Create Application Instance')}}</a></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                        @endif

                        @if($organization->users()->first())

                <br><br>

                <table class="table">
                    <thead>
                        <tr>
                            <th colspan=3 scope="col">{{__('Administrators')}}</th>
                        </tr>
                        <tr>
                            <th scope="col">{{__('#')}}</th>
                            <th scope="col">{{__('User')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($organization->users()->get() as $user)
                            <tr>
                                <th scope="row">{{ $user->id }}</th>
                                <td>{{ $user->identifiableName() }}</td>
                                @if(Auth::user()->role>9)
                                <td><a data-toggle="modal" href="#removeUser{{ $user->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                @endif

                                <!-- Remove User Modal -->
                                <div id="removeUser{{ $user->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{__('Remove user?')}}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                    {{__('Are you sure you want to remove user :user_name from :organization_name',['user_name'=>$user->name,'organization_name'=>$organization->name])}}
                                            </div>
                                            <div class="modal-footer">
                                                    <button type="submit" class="btn btn-default" onclick="window.location.href = '{{ route('organization_remove_admin',[$organization,$user]) }}';">{{__('Yes')}}</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('No')}}</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @endif

                <form method="POST" action="{{ route('invite_to_organization',$organization) }}">
                    @csrf

                    <label for="email">{{ __('E-mail:') }}</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <label for="usermessage">{{ __('Message:') }}</label>
                    <textarea id="usermessage" name="usermessage" cols="40" rows="5" class="form-control{{ $errors->has('usermessage') ? ' is-invalid' : '' }}" value="{{ old('usermessage') }}"></textarea>

                    @if ($errors->has('usermessage'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('usermessage') }}</strong>
                        </span>
                    @endif

                    <br>
                    <button type="submit" class="btn btn-default border">{{__('Invite')}}</button>
                </form>

                <br>

                <a data-toggle="modal" href="#deleteOrganizationModal" class="btn btn-danger my-2">
                    {{ __('Delete Organization')}}
                </a>

                </div>

                </div>
            </div>

        </div>
    </div>
</main>

<!-- Delete Organization Modal -->
<div id="deleteOrganizationModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{ __('Delete Organization')}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                    {{ __('Are you shure you want to delete organization')}} {{ $organization->name }}?
            </div>
            <div class="modal-footer">
                <form action="{{ route('destroy_organization',[$organization->id]) }}">
                    <button type="submit" class="btn btn-default">{{ __('Delete')}}</button>
                </form>
                <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close')}}</button>
            </div>
            </form>
        </div>

    </div>
</div>
@endsection
