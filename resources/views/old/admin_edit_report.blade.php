@extends('layouts.app')

@section('parent-url', route('admin_report_templates_index') )

@section('content')

<main class="py-4">
<div class="container">
        <form method="POST">
            @csrf

            @if(Agent::isMobile())

            {{__('Name:')}} <input type="text" name="name" value="{{ isset($template) ? $template->name : "" }}"><br>
            {{__('Key:')}} <input type="text" name="key" value="{{ isset($template) ? $template->key : "" }}"><br>
            
            <input type="checkbox" name="standard" {{ isset($template) ? ($template->standard ? "checked" : "") : "" }}>{{__('Is standard')}}<br>
            <input type="checkbox" name="premium" {{ isset($template) ? ($template->premium ? "checked" : "") : "" }}>{{__('Is premium')}}<br>

            @if(isset($template))
            <input type=submit value="{{__('Save')}}" formaction="{{ route('admin_report_template_update',$template) }}">
            @else
            <input type=submit value="{{__('Create')}}" formaction="{{ route('admin_report_template_store') }}">
            @endif

            @else

            {{__('Name:')}} <input type="text" name="name" value="{{ isset($template) ? $template->name : "" }}">
            {{__('Key:')}} <input type="text" name="key" value="{{ isset($template) ? $template->key : "" }}">

            @if(isset($template))
            <input type=submit value="{{__('Save')}}" formaction="{{ route('admin_report_template_update',$template) }}">
            @else
            <input type=submit value="{{__('Create')}}" formaction="{{ route('admin_report_template_store') }}">
            @endif

            <input type="checkbox" name="standard" {{ isset($template) ? ($template->standard ? "checked" : "") : "" }}>{{__('Is standard')}}
            <input type="checkbox" name="premium" {{ isset($template) ? ($template->premium ? "checked" : "") : "" }}>{{__('Is premium')}}

            @endif

            <br><br>

            <h6>{{__('XSLT:')}}</h6>
            <textarea name="xslt" rows=30 style="width:100%;">{{ isset($template) ? $template->template : "" }}</textarea>

        </form>
        <br><br>
        @if(isset($template))
        <table class="table">
            <thead>
                <tr>
                    <th colspan=3 scope="col">{{__('Applications to connect with')}}</th>
                </tr>
                <tr>
                    <th scope="col">{{ __('Id')}}</th>
                    <th scope="col">{{ __('Name')}}</th>
                </tr>
            </thead>
            <tbody>
                @foreach($applications as $application)
                    <tr>
                        <th scope="row" class="text-nowrap">{{ $application->id }}</th>
                        <td><a href={{route('admin_show_application',$application)}}>{{ $application->name }}</a></td>
                        <td><a href={{route('admin_connect_report_template',[$template,$application])}}>{{__('Connect')}}</a></td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        @endif
    </div>
</main>
@endsection
