@extends('layouts.app')

@section('parent-url', route('admin_superuser'))

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-dark">{{ __('Plan: ') . $plan->name}}</div>

                <div class="card-body">


                    <form method="POST" action="{{ route('admin_update_plan',$plan) }}">
                        @csrf

                        <label for="number_of_users">{{ __('Max number of users:') }}</label>

                        <input id="number_of_users" type="text" class="form-control{{ $errors->has('number_of_users') ? ' is-invalid' : '' }} col-md-1" name="number_of_users" value="{{ old('number_of_users',$plan->number_of_users) }}"
                            required autofocus> @if ($errors->has('number_of_users'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('number_of_users') }}</strong>
                        </span> @endif

                        <button type="submit" class="mt-10">
                            {{ __('Save') }}
                        </button>

                    </form>
                    <br><br>

                    <label>{{__('Application:')}} </label>
                    @if($application)
                    <a href={{route('admin_show_application',$application)}}>{{ $application->name }}</a>
                    <br>
                    <a href={{route('admin_disconnect_plan',[$application,$plan])}} class="btn btn-primary">{{__('Disconnect from application')}}</a>

                    @else
                    {{__('Not connected with any application')}}
                    @endif
                    <br><br>
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan=3 scope="col">{{__('Applications to connect with')}}</th>
                            </tr>
                            <tr>
                                <th scope="col">{{ __('Id')}}</th>
                                <th scope="col">{{ __('Name')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($available_applications as $application)
                                <tr>
                                    <th scope="row" class="text-nowrap">{{ $application->id }}</th>
                                    <td><a href={{route('admin_show_application',$application)}}>{{ $application->name }}</a></td>
                                    <td><a href={{route('admin_connect_plan',[$application,$plan])}}>{{__('Connect')}}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
