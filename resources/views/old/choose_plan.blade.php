@extends('layouts.app')

@section('parent-url', route('application_instance_edit',$applicationInstance))

@section('script')
    <script src="https://js.braintreegateway.com/web/dropin/1.8.1/js/dropin.min.js"></script>
@endsection

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">



        </div>

        <div class="row justify-content-center">

        @foreach($plans as $plan)
            @if($plan->number_of_users>=$number_of_users)
                <div class="col-md-3">
            @else
                <div class="col-md-3 text-muted">
            @endif
                <div class="card mt-2">
                    <div class="card-body">
                        <h4>{{$plan->name}}</h4>
                        {{$plan->description}}
                        <h3 class="mt-4">{{$plan->price . " " . $plan->currencyIsoCode . "/" . $plan->frequency}}</h3>

                        @if($plan->number_of_users>=$number_of_users)
                        <a href="{{route('payment.pay_plan',[$applicationInstance,$plan])}}" class="btn btn-secondary btn-lg active" role="button" aria-pressed="true">{{__('Buy')}}</a>
                        @else
                        <a href="#" class="btn btn-secondary btn-lg disabled" role="button" aria-pressed="true">{{__('Buy')}}</a>
                        @endif

                    </div>
                </div>
            </div>

        @endforeach

        </div>


    </div>
</div>
</main>
@endsection
