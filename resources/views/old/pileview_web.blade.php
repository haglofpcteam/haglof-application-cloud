@extends('layouts.app')

@section('script')

<meta name="csrf-token" content="{{ csrf_token() }}">
<link href="{{ mix('/css/pile_web.css') }}" rel="stylesheet">
<script src="{{ mix('/js/pile.js') }}" type="text/javascript"></script>

<script type="text/javascript" defer>

    $(document).ready(function() {
        $('#navbar-btn').hide();
    });

    window.initMeasurement('{{ $pileApplicationMeasurement->id }}',false,'{{ $pileApplicationMeasurement->location()->first()->measurement_system }}');

</script>

<style>

        body { margin: 0; overflow: hidden; }
        canvas { width: 100%; height: 100% }

        ::-webkit-scrollbar-thumb:hover {
            background: #555;
        }
</style>

@endsection

@section('parent-url', route('show_pile_application',$applicationInstance))

@section('content')

    <div id="pile_application">

    </div>

    <div id="model_overview" class="card">
        <div class="card-header text-white bg-dark">
                {{__('Overview')}}
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col">
                    <b class="text-nowrap">{{__('Number of measurments:')}}</b><br>
                    <b class="text-nowrap">{{__('Volume:')}}</b><br>
                    <b class="text-nowrap">{{__('Base area:')}}</b><br>
                    <b class="text-nowrap">{{__('Volume adjustment factor:')}}</b><br>
                    <b class="text-nowrap">{{__('Average height:')}}</b><br>
                    <b class="text-nowrap">{{__('Weight:')}}</b><br>
                    <div class="collapseOverviewData">
                        <b class="text-nowrap">{{__('Density:')}}</b><br>
                        <b class="text-nowrap">{{__('Periphery:')}}</b><br>
                        <b class="text-nowrap">{{__('Slope angle:')}}</b><br>                     
                        <b class="text-nowrap">{{__('Production id:')}}</b><br>
                        <b class="text-nowrap">{{__('Version id:')}}</b><br>
                        <b class="text-nowrap">{{__('Unit serial number:')}}</b><br>
                        <b class="text-nowrap">{{__('Transponder height:')}}</b><br>
                        <b class="text-nowrap">{{__('Eye height:')}}</b><br>
                        <b class="text-nowrap">{{__('Pivot offset:')}}</b><br>
                        <b class="text-nowrap">{{__('Magnetic declination:')}}</b><br>
                        <b class="text-nowrap">{{__('File id:')}}</b><br>
                        <b class="text-nowrap">{{__('User:')}}</b><br>
                        
                    </div>
                </div>
                <div class="col">
                    <span class="text-nowrap" id="model_overview_number_of_measurments"></span><br>
                    <span class="text-nowrap" id="model_overview_volume"></span><br>
                    <span class="text-nowrap" id="model_overview_base_area"></span><br>
                    <span class="text-nowrap" id="model_overview_volume_adjustment_factor"></span><br>
                    <span class="text-nowrap" id="model_overview_average_height"></span><br>
                    <span class="text-nowrap" id="model_overview_weigth"></span><br>
                    <div class="collapseOverviewData">
                        <span class="text-nowrap" id="model_overview_density"></span><br>
                        <span class="text-nowrap" id="model_overview_periphery"></span><br>
                        <span class="text-nowrap" id="model_overview_slope_angle"></span>° (<span id="model_overview_slope_angle_percent"></span> %)<br>
                        <span class="text-nowrap" id="model_overview_unit_production_id"></span><br>
                        <span class="text-nowrap" id="model_overview_unit_version_id"></span><br>
                        <span class="text-nowrap" id="model_overview_unit_serial_number"></span><br>
                        <span class="text-nowrap" id="model_overview_transponder_height"></span><br>
                        <span class="text-nowrap" id="model_overview_eye_height"></span><br>
                        <span class="text-nowrap" id="model_overview_pivot_offset"></span><br>
                        <span class="text-nowrap" id="model_overview_magnetic_declination"></span><br>
                        <span class="text-nowrap" id="model_overview_file_id"></span><br>
                        <span class="text-nowrap" id="model_overview_user"></span><br>
                        
                    </div>
                </div>
            </div>

            <div class="row mt-2">
                <div class="col">
                    <a href="#" onclick="$('.collapseOverviewData').toggle('slow');$(this).children().toggleClass('fa-plus-square fa-minus-square');">
                        <i class='fa fa-plus-square'></i>
                    </a>
                </div>
            </div>


        </div>
    </div>

    <div id="controls_pane">
        <div class="row justify-content-center align-items-end">

            <div>
                <div class="btn-group mr-2 my-auto">
                    <button type="button" class="btn btn-primary" onclick="window.togglePointsAndLines()">
                        <i class="fa fa-3x fa-fw fa-draw-polygon"></i>
                    </button>

                    <button type="button" class="btn btn-primary" onclick="window.toggleMap()">
                        <i class="fa fa-3x fa-fw fa-map"></i>
                    </button>

                    <button type="button" class="btn btn-primary" onclick="window.toggleHull()">
                        <i class="fa fa-3x fa-fw fa-mountain"></i>
                    </button>

                </div>
            </div>

            <div class="btn-group-vertical" id="color_picker">
                <button type="button" class="btn btn-primary colorButton" onclick="window.changeHullColor('#808080');$('.colorButton').toggle();">
                    <i class="fas fa-3x fa-fw fa-square gravel-pile"></i>
                </button>
                <button type="button" class="btn btn-primary colorButton" onclick="window.changeHullColor('#baa86f');$('.colorButton').toggle();">
                    <i class="fas fa-3x fa-fw fa-square wood-chip-pile"></i>
                </button>
                <button type="button" class="btn btn-primary colorButton" onclick="window.changeHullColor('#9b7653');$('.colorButton').toggle();">
                    <i class="fas fa-3x fa-fw fa-square dirt-pile"></i>
                </button>
                <button type="button" onclick="$('.colorButton').toggle();" class="btn btn-primary">
                    <i class="fas fa-3x fa-fw fa-fill"></i>
                </button>
            </div>

            @if($applicationInstance->paid)
            <div>
                <div class="btn-group ml-2">                  
                    <button type="button" class="btn btn-primary" onclick="window.generateReport()">
                        <i class="fa fa-3x fa-fw fa-print"></i>
                    </button>
                    
                    
                    <button type="button" class="btn btn-primary" onclick="window.takeSnapShot()">
                        <i class="fa fa-3x fa-fw fa-camera"></i>
                    </button>

                    <button type="button" class="btn btn-primary" id="show_snapshot_button" data-toggle="modal" data-target="#imageModal" disabled>
                        <i class="fa fa-3x fa-fw fa-image"></i>
                    </button>
                </div>
            </div>
            @endif

        </div>
    </div>

    <div id="target_detail" class=card>
        <div class="card-header text-white bg-dark">

            <div class="row">
                <div class="col text-nowrap">
                        {{__('Target detail')}}
                </div>
                <div class="col text-right">
                    <a  data-toggle="modal" href="#" onclick="window.closeTargetWindow()" class="text-white">
                        <i class="fa fa-window-close" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body">
            <table>
                <tr>
                    <td><b>{{__('Type:')}}</b></td>
                    <td><select id="target_detail_type"></select></td>
                </tr>
                <tr>
                    <td><b>{{__('Latitude:')}}</b></td>
                    <td><span id="target_detail_latitude"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('Longitude:')}}</b></td>
                    <td><span id="target_detail_longitude"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('Altitude:')}}</b></td>
                    <td><span id="target_detail_altitude"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('HDOP:')}}</b></td>
                    <td><span id="target_detail_hdop"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('Date:')}}</b></td>
                    <td><span id="target_detail_date"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('UTC:')}}</b></td>
                    <td><span id="target_detail_utc"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('Seq:')}}</b></td>
                    <td><span id="target_detail_seq"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('SD:')}}</b></td>
                    <td><span id="target_detail_sd"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('HD:')}}</b></td>
                    <td><span id="target_detail_hd"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('H:')}}</b></td>
                    <td><span id="target_detail_h"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('Pitch:')}}</b></td>
                    <td><span id="target_detail_pitch"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('Azimut:')}}</b></td>
                    <td><span id="target_detail_az"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('X:')}}</b></td>
                    <td><span id="target_detail_x"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('Y:')}}</b></td>
                    <td><span id="target_detail_y"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('Z:')}}</b></td>
                    <td><span id="target_detail_z"></span></td>
                </tr>
                <tr>
                    <td><b>{{__('UTM Zone:')}}</b></td>
                    <td><span id="target_detail_utm_zone"></span></td>
                </tr>
            </table>

            <br>
            <button type="button" class="btn btn-primary" onclick="window.deletePoint()">
                <i class="fa fa-fw fa-trash"></i>
            </button>


        </div>
    </div>


    <!-- Image Modal -->
    <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="imageModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('Current Snapshot')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="snapshot_image_container" width="100%" height="100%"/>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Report Modal -->
    <div class="modal fade" id="reportModal" tabindex="-1" role="dialog" aria-labelledby="reportModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">{{__('Select report template')}}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="modal-body">
                        <script>
                            $(document).ready(function () {
                                $('#report_form').validate(
                                {
                                    rules:
                                    {
                                        template_id:{ required: true }
                                    },
                                    messages:
                                    {
                                        template_id:
                                        {
                                            required:"Please select a template"
                                        }
                                    },
                                    errorPlacement: function(error, element) {
                                        $(error).addClass("text-danger");
                                        if($(element).attr('name')=="template_id") {
                                            var p = element.parent().parent();
                                            error.appendTo( p );
                                            $(p).addClass("border").addClass("border-danger").addClass("p-2");
                                        }
                                        else {
                                            error.appendTo( element.parent() );
                                        }
                                        
                                    },
                                    submitHandler: function() { 
                                        $('#reportModal').modal('hide');
                                        window.open('about:blank','print_popup','width=630,height=891');
                                        form.submit();
                                    }

                                });

                            });

                        </script>
                        <form 
                            action="{{route('pileapplication_generate_report',$pileApplicationMeasurement)}}"
                            method="POST"
                            id="report_form"
                            target="print_popup">
                            @csrf
                        <div>
                        @foreach($report_templates as $template)                        
                            <div class="form-check">
                                <input 
                                    class="form-check-input" 
                                    type="radio" 
                                    name="template_id" 
                                    id="template_{{$template->id}}" 
                                    value="{{$template->id}}"
                                    onchange="$('.template_parameters').addClass('d-none');$('#template_{{$template->id}}_parameters').removeClass('d-none');"
                                    >
    
                                <label class="form-check-label" for="template_{{$template->id}}">
                                        {{$template->name}}
                                </label>
                            </div>
    
                            <div class="d-none template_parameters" id="template_{{$template->id}}_parameters">
                                @foreach($template->parameters()->get() as $parameter)
                                <div class="form-group mt-4 row">
                                    <label for="template_{{$template->id}}_parameter_{{$parameter->name}}" class="col-md-4 col-form-label text-md-right">{{$parameter->name}}: </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="template_{{$template->id}}_parameter_{{$parameter->name}}" id="template_{{$template->id}}_parameter_{{$parameter->name}}">
                                    </div>
                                </div>
                                @endforeach
                            </div>
    
                        @endforeach
                        </div>
                        <div class="TemplateValidation text-danger"></div>    
    
                        
                    </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-primary" value="Generate"/>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">{{__('Close')}}</button>
                </div>
                </form>
            </div>
            </div>
        </div>


@endsection
