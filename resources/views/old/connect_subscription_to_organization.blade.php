@extends('layouts.app')
@section('parent-url', URL::previous())
@section('content')

@if(!Agent::isMobile())
    <section class="jumbotron text-center" style="background-image:url('{{asset('img/jumbotron/' . $random_jumbotron)}}');">
    <div class="jumbotron-container">
        <div class="jumbotron-box">
            <h1 class="jumbotron-heading text-white">{{__('Settings')}}</h1>
            <p class="lead text-white">{{__('Connect subscription')}}</p>
        </div>
    </div>
    </section>
@endif

<main class="py-4">
    <div class="container">
        <div class="row justify-content-center">

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-white bg-dark">{{ __('Pick organization')}}</div>

                    <div class="card-body">

                        @foreach($organizations as $organization)

                        <a href="{{ route('connect_subscription', [ 'order' => $order, 'organization' => $organization]) }}">{{ $organization->name }}</a><br>

                        @endforeach

                    </div>
                </div>
            </div>

        </div>
    </div>
</main>
@endsection
