@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{__('Overview Organization')}}</div>

            <div class="card-body">


                    @if(Auth::user()->role<2)
                    <label>{{__('Name:')}} </label> {{ $organization->name }}<br>

                    @else
                    <form method="POST" action="{{ route('update_organization',$organization) }}">
                        @csrf

                        <label for="organization_name">{{ __('Name:') }}</label>

                        <input id="organization_name" type="text" class="form-control{{ $errors->has('organization_name') ? ' is-invalid' : '' }}" value="{{ $organization->name }}" name="organization_name" required autofocus>

                        @if ($errors->has('organization_name'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('organization_name') }}</strong>
                            </span>
                        @endif

                        <br>
                        <button type="submit" class="btn btn-default border">{{ __('Save')}}</button>


                    </form>
                    @endif


                @if($organization->applicationInstances()->first())

                <br><br>

                <table class="table">
                    <thead>
                        <tr>
                            <th colspan=3 scope="col">{{__('Application Instances')}}</th>
                        </tr>
                        <tr>
                            <th scope="col">{{__('#')}}</th>
                            <th scope="col">{{__('Application')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($organization->applicationInstances()->get() as $applicationInstance)
                            <tr>
                                <th scope="row">{{ $applicationInstance->id }}</th>
                                <td class='clickable-cell' data-href='{{route("admin_show_application_instance",$applicationInstance) }}'>{{$applicationInstance->name}}</td>
                                @if(Auth::user()->role>1)
                                <td><a data-toggle="modal" href="#removeApplicationInstance{{ $applicationInstance->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                @endif
                                <!-- Remove Application Modal -->
                                <div id="removeApplicationInstance{{ $applicationInstance->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{__('Remove Application?')}}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                {{__('Are you sure you want to remove :application_name',['application_name' => $applicationInstance->name])}}

                                            </div>
                                            <div class="modal-footer">
                                                    <button type="submit" class="btn btn-default" onclick="window.location.href = '{{ route('remove_application_instance',$applicationInstance)}}';">{{__('Yes')}}</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('No')}}</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @endif

                <br><br>

                <table class="table">
                    <thead>
                        <tr>
                            <th colspan=3 scope="col">{{__('Create Application Instances')}}</th>
                        </tr>
                        <tr>
                            <th scope="col">{{__('#')}}</th>
                            <th scope="col">{{__('Application')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                            @foreach($applications as $application)
                            <tr>
                                <th scope="row">{{ $application->id }}</th>
                                <td>{{$application->name}}</td>

                                <td><a href={{ route( 'create_application_instance',[$organization,$application])}}>{{__('Create Application Instance')}}</a></td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>


                @if($organization->users()->first())

                <br><br>

                <table class="table">
                    <thead>
                        <tr>
                            <th colspan=3 scope="col">{{__('Administrators')}}</th>
                        </tr>
                        <tr>
                            <th scope="col">{{__('#')}}</th>
                            <th scope="col">{{__('User')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($organization->users()->get() as $user)
                        <tr>
                                <th scope="row">{{ $user->id }}</th>
                                <td class='clickable-cell' data-href='{{route('admin_show_user',$user) }}'>{{ $user->identifiableName() }}</td>
                                @if(Auth::user()->role>9)
                                <td><a data-toggle="modal" href="#removeUser{{ $user->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                @endif

                                <!-- Remove User Modal -->
                                <div id="removeUser{{ $user->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{__('Remove user?')}}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                    {{__('Are you sure you wan to remove user :user_name from :organization_name',['user_name'=>$user->name,'organization_name'=>$organization->name])}}
                                            </div>
                                            <div class="modal-footer">
                                                    <button type="submit" class="btn btn-default" onclick="window.location.href = '{{ route('organization_remove_admin',[$organization,$user]) }}';">{{__('Yes')}}</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('No')}}</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </tr>
                        @endforeach
                    </tbody>
                </table>

                @endif

                @if(Auth::user()->role>1)
                <form method="POST" action="{{ route('invite_to_organization',$organization) }}">
                    @csrf

                    <label for="email">{{ __('E-mail:') }}</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <label for="usermessage">{{ __('Message:') }}</label>
                    <textarea id="usermessage" name="usermessage" cols="40" rows="5" class="form-control{{ $errors->has('usermessage') ? ' is-invalid' : '' }}" value="{{ old('usermessage') }}"></textarea>

                    @if ($errors->has('usermessage'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('usermessage') }}</strong>
                        </span>
                    @endif

                    <br>
                    <button type="submit" class="btn btn-default border">{{__('Invite')}}</button>
                </form>
                @endif


                <br><br>
                @if(Auth::user()->role>9)
                <a href="{{route('admin_show_revision_history_organization',$organization)}}">{{__('Show revision history')}}</a>
                @endif

                @if(Auth::user()->role>1)
                <br><br>
                <a data-toggle="modal" href="#deleteOrganizationModal" class="btn btn-primary my-2">
                    {{__('Delete Organization')}}
                </a>
                @endif



            </div>
            </div>
        </div>

    </div>
</div>

<!-- Delete Organization Modal -->
<div id="deleteOrganizationModal" class="modal fade" role="dialog">
        <div class="modal-dialog">

          <!-- Modal content-->
          <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{ __('Delete Organization')}}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                        {{ __('Are you shure you want to delete organization')}} {{ $organization->name }}?
                </div>
                <div class="modal-footer">
                    <form action="{{ route('destroy_organization',[$organization->id]) }}">
                        <button type="submit" class="btn btn-default">{{ __('Delete')}}</button>
                    </form>
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('Close')}}</button>
                </div>
            </form>
          </div>

        </div>
      </div>

</main>
@endsection
