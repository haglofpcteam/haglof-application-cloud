<div class="panel-group">
    <div class="panel panel-default">
        <div class ="panel-heading">
            <a data-toggle="collapse" class="list-group-item list-group-item-action mdiifolder" data-id="{{$folder->id}}" href="#folder{{$folder->id}}"><i class="far fa-folder pr-1"></i> {{$folder->name}}</a>
        </div>
        <div id="folder{{$folder->id}}" class="panel-collapse collapse border border-dark">
            @foreach($folder->folders()->get() as $subfolder)
            @component('components.mdiifolder',['folder'=>$subfolder])
            @endcomponent
            @endforeach
            @foreach($folder->files()->get() as $file)
            <a class="list-group-item list-group-item-action mdiifile" data-id="{{$file->id}}"><i class="far fa-file-alt"></i> {{$file->identifiableName()}}</a>
            @endforeach
            <a class="list-group-item list-group-item-action" data-toggle="modal" href="#createFolder{{$folder->id}}"><i class="fas fa-plus"></i> {{ __('New folder') }}</a>
            <a class="list-group-item list-group-item-action" data-toggle="modal" href="#uploadFile{{$folder->id}}"><i class="fas fa-file-upload"></i> {{ __('Upload File') }}</a>
        </div>
    </div>
</div>

<!-- Upload MDII.XML file Modal -->
<div id="uploadFile{{$folder->id}}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('Upload File')}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <script>
                $(document).ready(function () {

                    $('#mdiifile{{ $folder->id }}').on('change',function(){
                        //get the file name
                        var fileName = $(this).val().replace(/^.*[\\\/]/, '');
                        //replace the "Choose a file" label
                        $(this).next('.custom-file-label').html(fileName);
                        $('#file_name{{ $folder->id }}').val(fileName);
                    })

                    $("#upload_file_form_{{$folder->id}}").validate(
                    {
                        rules:
                        {
                            file_name:{ required: true },
                        },
                        messages:
                        {
                            file_name:
                            {
                                required:"File name cannot be empty"
                            }
                        },
                        errorPlacement: function(error, element) {
                            $(error).addClass("text-danger").addClass("pt-2");
                            var p = element.parent();
                            error.appendTo( p );
                            $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
                        },
                        submitHandler: function() { 
                            $('#createFolder{{ $folder->id }}').modal('hide');
                            form.submit();
                        }

                    });
                });

            </script>

            <div class="modal-body">
                <form method="POST" id="upload_file_form_{{$folder->id}}" enctype="multipart/form-data" action="{{ route('upload_mdiifile',$folder) }}">
                    @csrf
                    <label for="mdiifile">{{ __('MDII File') }}</label>

                    <div class="input-group form-group">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="mdiifile{{ $folder->id }}" name="mdiifile" accept=".mdii.xml" aria-describedby="inputGroupFileAddon01" required>
                            <label class="custom-file-label" for="mdiifile{{ $folder->id }}">{{ __('MDII File') }}</label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="file_name">{{ __('File name') }}</label>
                        <input id="file_name{{ $folder->id }}" type="text" class="form-control file-name" name="file_name" autofocus>
                    </div>

            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-default">{{__('Upload')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                </form>
            </div>
        </div>

    </div>
</div>

<!-- Create Folder Modal -->
<div id="createFolder{{ $folder->id }}" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">{{__('Create folder')}}</h4>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>

            <script>
                $(document).ready(function () {
                    $("#create_folder_{{$folder->id}}").validate(
                    {
                        rules:
                        {
                            folder_name:{ required: true },
                        },
                        messages:
                        {
                            folder_name:
                            {
                                required:"Folder name cannot be empty"
                            }
                        },
                        errorPlacement: function(error, element) {
                            $(error).addClass("text-danger").addClass("pt-2");
                            var p = element.parent();
                            error.appendTo( p );
                            $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
                        },
                        submitHandler: function() { 
                            $('#createFolder{{ $folder->id }}').modal('hide');
                            form.submit();
                        }

                    });
                });
            </script>

            <div class="modal-body">
                <form method="POST" id="create_folder_{{$folder->id}}" action="{{ route('create_mdii_folder',$folder) }}">
                    @csrf
                    <div class="form-group">
                        <label for="folder_name">{{ __('Folder name') }}</label>
                        <input id="folder_name" type="text" class="form-control" name="folder_name" autofocus>
                    </div>
            </div>
            <div class="modal-footer">
                    <button type="submit" class="btn btn-default">{{__('Create Folder')}}</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>