@extends('layouts.app')

@section('parent-url', route('application_instance_edit',$applicationInstance))

@section('script')
    <script src="https://js.braintreegateway.com/web/dropin/1.8.1/js/dropin.min.js"></script>
@endsection

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{ __('Handle payment for')}} {{ $applicationInstance->application()->first()->name }}</div>

                <div class="card-body">


                        <div class="container">
                                <div class="row">
                                  <div class="col-md-8 col-md-offset-2">
                                        <form method="post" id="payment-form" action="{{ route('payment.subscribe',[$applicationInstance,$plan]) }}">
                                            @csrf
                                            <section>
                                                <div class="bt-drop-in-wrapper">
                                                    <div id="bt-dropin"></div>
                                                </div>
                                            </section>

                                            <input id="nonce" name="payment_method_nonce" type="hidden" />
                                            <button class="button" type="submit"><span>{{__('Subscribe')}}</span></button>
                                        </form>
                                  </div>
                                </div>
                             </div>

                             <script>
                                    var form = document.querySelector('#payment-form');
                                    var client_token = "{{ Braintree_ClientToken::generate() }}";

                                    braintree.dropin.create({
                                      authorization: client_token,
                                      selector: '#bt-dropin',
                                      paypal: {
                                        flow: 'vault'
                                      }
                                    }, function (createErr, instance) {
                                      if (createErr) {
                                        console.log('Create Error', createErr);
                                        return;
                                      }
                                      form.addEventListener('submit', function (event) {
                                        event.preventDefault();
                                        instance.requestPaymentMethod(function (err, payload) {
                                          if (err) {
                                            console.log('Request Payment Method Error', err);
                                            return;
                                          }
                                          // Add the nonce to the form and submit
                                          document.querySelector('#nonce').value = payload.nonce;
                                          form.submit();
                                        });
                                      });
                                    });
                                </script>

                </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
