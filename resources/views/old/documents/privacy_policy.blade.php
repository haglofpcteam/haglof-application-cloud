<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Calibri Light";
	panose-1:2 15 3 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
h1
	{mso-style-link:"Rubrik 1 Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	line-height:107%;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;
	font-weight:normal;}
h2
	{mso-style-link:"Rubrik 2 Char";
	margin-top:2.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	line-height:107%;
	page-break-after:avoid;
	font-size:13.0pt;
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;
	font-weight:normal;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:36.0pt;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:36.0pt;
	margin-bottom:.0001pt;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:36.0pt;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
span.Rubrik1Char
	{mso-style-name:"Rubrik 1 Char";
	mso-style-link:"Rubrik 1";
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;}
span.Rubrik2Char
	{mso-style-name:"Rubrik 2 Char";
	mso-style-link:"Rubrik 2";
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:70.85pt 70.85pt 70.85pt 70.85pt;}
div.WordSection1
	{page:WordSection1;}
 /* List Definitions */
 ol
	{margin-bottom:0cm;}
ul
	{margin-bottom:0cm;}
-->
</style>

</head>

<body lang=SV>

<div class=WordSection1>

<h1><span lang=EN-US>PRIVACY POLICY HAGLÖF SWEDEN AB</span></h1>

<p class=MsoNormal><span lang=EN-US>About Haglöf Sweden Aktiebolag and this
privacy policy</span></p>

<h2><span lang=EN-US>Processing of your personal data</span></h2>

<p class=MsoNormal><span lang=EN-US>Haglöf Sweden Aktiebolag, Reg. No.
556148-8197, Klockargatan 8, 882 30 Långsele (the ”Company” or “we”) takes all
necessary measures to make sure that personal data concerning our customers is
being processed by us in a lawfully, fairly and transparent manner.</span></p>

<p class=MsoNormal><span lang=EN-US>When you use our various services, we
collect your personal data. The Company is consequently the data controller of
your personal data and processes it in accordance with this privacy policy.
This privacy policy explains and clarifies your rights in relation to the
Company regarding the processing of your personal data and how you can exercise
those rights.</span></p>

<p class=MsoNormal><span lang=EN-US>The Company is committed to protecting your
personal data and it is important to the Company to ensure that your personal
data is being processed in a secure way. We comply with all applicable laws and
rules that exist to protect the privacy of individuals, including the Swedish
Personal Data Act (1998:204), the Swedish Act on Electronic Communication
(2003:389) and such other laws or regulation that implements the EU Data
Protection Directive 95/46/EC, the Electronic Communications Directive
2002/58/EC and the EU General Data Protection Regulation 2016/679 (GDPR) and
any changes to, amendments to or regulations that replace such laws and
regulations. We use appropriate technical and organisational measures with
respect to the amount and sensitivity of personal data.</span></p>

<p class=MsoNormal><span lang=EN-US>It is important that you read and
understand this privacy policy before you use any of the Company’s services.
You should not use any of our services if you do not approve of this privacy
policy.</span></p>

<p class=MsoNormal><span lang=EN-US>Some pages on our website contain links to
third party websites. These websites have their own privacy policies and the
Company is not responsible for their operations or their information policies.
Any user who sends information to or through these third party websites should
hence review the privacy policies posted on those websites before any personal
data is transferred to them.</span></p>

<p class=MsoNormal><span lang=EN-US>For information on the collection, handling
and storage of information obtained through cookies, see the “Cookies” section
below.</span></p>

<p class=MsoNormal><span lang=EN-US>What personal data do we collect and where
from?</span></p>

<p class=MsoNormal><span lang=EN-US>If you place an order of goods, or register
on website or otherwise use our services, you may provide information to us
that is considered personal data under applicable data protection laws.</span></p>

<p class=MsoNormal><span lang=EN-US>The types of personal data that we collect
may, depending on the context, include:</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>name and contact information including address,
mobile number and e-mail address;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>personal identity number;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>delivery and billing information, payment
information and other information that you provide in connection with the
purchase or delivery of a product or service;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>different kinds of demographic information;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>information about preferences and interests
based on the use of the Company’s websites;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>user name and password for our services;</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>other information that is relevant for customer
surveys, advertisement/marketing or offers; and/or</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Other user-generated information that you
actively choose to share through our websites.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-US
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>We may collect your personal data from the
following sources:</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>When you order a product, subscribe to any of
our newsletters, or have any other communication with us;</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-US
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>from third parties in the form of group
companies, or companies that the Company engages for the purpose of enriching
its customer database, e.g.</span></p>

<h2><span lang=EN-US>Why do we process your personal data?</span></h2>

<p class=MsoNormal><span lang=EN-US>If you have ordered a product you have
entered into an agreement with the Company. As a result, we process your
personal data for the purpose of managing your purchase or order, and in order
to deliver the service or product that you have ordered.</span></p>

<p class=MsoNormal><span lang=EN-US>In addition to processing your personal data
to complete your purchase of products or services, the Company may use your
personal data for other purposes, based on other legal grounds, as set out
below.</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-US>Performance of a contract</span></b><span
lang=EN-US>: For the handling and administration of purchases, deliveries of
products, to make payments as well as to give you access to the Company’s
websites and the services (inter alia customer support) provided there.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-US>Consent</span></b><span lang=EN-US>: Direct
marketing (by regular mail and e-mail), customer surveys, customer support and
newsletters.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-US>Compliance with a legal obligation</span></b><span
lang=EN-US>: We save invoice documentation in accordance with applicable
accounting laws and regulations.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-US>Legitimate interest</span></b><span
lang=EN-US>: In order to be able to provide, carry out and improve our
commitments, products and services, it is necessary for us to process personal
data in some other cases as well, e.g. by analyzing our customers’ buying
patterns in order to provide them with relevant information and marketing, as
well as for statistical purposes.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-US
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-US>Profiling</span></b></p>

<p class=MsoNormal><span lang=EN-US>In the event that a service that we provide
requires your consent, we will always explicitly ask you to give your consent
to such a service and to the processing of your personal data in such a case.
For example, we will ask for your consent if you would like to subscribe to any
of the Company’s newsletters or if you would like to create an account on any
of the Company’s websites.</span></p>

<p class=MsoNormal><span lang=EN-US>In connection with your giving of consent,
you will have to confirm that you have read this privacy policy and that you
consent to the processing of your personal data as described herein.</span></p>

<h2><span lang=EN-US>Retention of personal data</span></h2>

<p class=MsoNormal><span lang=EN-US>The Company takes all reasonable steps to
ensure that your personal data is processed and stored securely. Your personal
data will never be stored longer than permitted by applicable law or longer
than necessary to fulfil the above stated purposes. Your personal data will be
processed by us during the following time periods.</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Member or customer: If you have registered for
any of the Company’s digital services your personal data is saved until you
cancel your membership or unsubscribe from our services, but no later than
three years after your last purchase. This does not apply if we need to save
your personal data for a longer period of time due to any of the reasons stated
below.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Performance of a contract: Personal data (name,
personal identity number, address, telephone number, e-mail, billing and
delivery information) which is submitted to the Company in connection with the
order of products or services is stored for as long as necessary in order for
the Company’s performance of the agreement with you. This includes, inter alia,
fulfilling delivery or warranty commitments.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Legal obligation: The Company saves any
documentation that constitutes accounting information in accordance with
applicable accounting legislation.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Consent: In cases where we process your personal
data based on your consent, we will only save your personal data for as long as
we still have your consent.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-US
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><span lang=EN-US>Direct marketing: We may process your personal
data for direct marketing purposes for as long as [three years] after the
customer relationship has ended, unless you object to your personal data being
used for direct marketing purposes.</span></p>

<h2><span lang=EN-US> Transfer of personal data</span></h2>

<p class=MsoNormal><span lang=EN-US>Personal data regarding our customers is an
important part of the Company’s operations and we do not sell the personal data
to anyone else. We only transfer personal data as described below. We always
observe great caution when transferring your personal data and your personal
data is only transferred in accordance with this privacy policy and after
taking appropriate security measures.</span></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-US>Partners outside the group of companies that
the Company belongs to</span></b><span lang=EN-US>: Our partners, i.e.
companies outside the group of companies that the Company belongs to and which
are approved by the Company, may get access to your personal data for the
purpose of giving you access to targeted information and offers about products
and services.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-US>Matching with private and public records</span></b><span
lang=EN-US>: The Company may enrich your personal data with further information
through collection of information from other private and public records, for
example, Bisnode Sverige AB. In this way, your personal data may be enriched
and updated.</span></p>

<p class=MsoListParagraphCxSpMiddle style='text-indent:-18.0pt'><span
lang=EN-US style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-US>Business transactions</span></b><span
lang=EN-US>: If all or part of the Company’s operations is sold or integrated
with any other business, operation or company, your personal data may be
disclosed to our advisors, potential buyers and their advisors, and be
transferred to the new owners of the operation.</span></p>

<p class=MsoListParagraphCxSpLast style='text-indent:-18.0pt'><span lang=EN-US
style='font-family:Symbol'>·<span style='font:7.0pt "Times New Roman"'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
</span></span><b><span lang=EN-US>Legal obligations</span></b><span lang=EN-US>:
Your personal data may also be disclosed for the purpose of the Company’s
compliance with certain legal obligations and it may be transferred to the
Police and other relevant public authorities when permitted and required by
law.</span></p>

<p class=MsoNormal><span lang=EN-US>The type of transfers mentioned above may
only be carried out to companies within the EU or EEA (i.e. all EU member
states and Iceland, Norway and Lichtenstein</span></p>

<h2><span lang=EN-US>Withdrawal of consent</span></h2>

<p class=MsoNormal><span lang=EN-US>In the event that we process your personal
data based on your consent, e.g. regarding subscription to newsletters, you may
withdraw your consent at any time by contacting us as stated below. Such
withdrawal may be made in whole or in part. If you do not wish to receive
marketing and special offers from us you may withdraw your consent by
contacting customer service or, if sent by e-mail, through a link in that
e-mail. By clicking the subscriptions link, you will be able to manage all your
subscriptions to newsletters and other information and marketing sent by e-mail
from the Company.</span></p>

<p class=MsoNormal><span lang=EN-US>If you withdraw your consent with respect
to the use or disclosure of your personal data for other purposes stated in
this privacy policy, we may no longer be able to continue to give you access to
our websites or provide customer service or other services being offered to our
users and permitted under this privacy policy.</span></p>

<p class=MsoNormal><span lang=EN-US>&nbsp;</span></p>

<p class=MsoNormal><span lang=EN-US> </span></p>

<h2><span lang=EN-US>Your rights</span></h2>

<p class=MsoNormal><span lang=EN-US>You have the right to request information
about what personal data concerning you that we are processing and how it is
being used by contacting us in writing (see contact details below). You can
change your personal details at any time in your account settings online, if
you have an account on any of our websites, or by contacting customer service
(contact details below). You are also entitled to request correction of
incorrect, incomplete or ambiguous personal data concerning you by contacting
customer service. For the protection of your privacy and your personal data, we
may require that you identify yourself in connection with our assistance.</span></p>

<p class=MsoNormal><span lang=EN-US>In accordance with applicable data
protection laws, you also have the right to request that your personal data be
erased or that the processing of your personal data be restricted. In certain
situations you also have the right to object to the processing of your personal
data and request that your personal data be transmitted in an electronic
format.</span></p>

<p class=MsoNormal><span lang=EN-US>You may file a complaint with the Swedish
Data Protection Authority (Sw. Datainspekionen) if you believe that the
Company’s processing of your personal data is not carried out in accordance
with applicable laws.</span></p>

<h2><span lang=EN-US>COOKIES</span></h2>

<p class=MsoNormal><span lang=EN-US>The Company uses so-called cookies on our
websites. A cookie is a small text file sent from a website to your web
browser. The cookie cannot identify you personally, but only the web browser
that is installed on your computer and the web browser you use when visiting the
webpage. Consequently, different cookies are saved on different computers,
should you use different computers when visiting our websites. Cookies do not
carry viruses and cannot destroy any other information stored on your computer.</span></p>

<p class=MsoNormal><span lang=EN-US>Cookies are usually categorized based on
their origin and based on whether they are stored in your web browser or not.
Cookies can either be sent to you from the website you visit (i.e. first-party
cookie) or from another organization that delivers services to the current website,
such as an analysts and statistical company (i.e. third-party cookies). Cookies
can also be divided into session cookies and permanent cookies. A session
cookie is sent to your computer so that the webpages can function properly
during your visit and is not stored on your computer, but is erased when you
close down your web browser. The function of a session cookie is for example
that it is activated when you return to a previously visited part of the
website and thus facilitates your navigation on the website. A permanent
cookie, on the other hand, is stored in your web browser and thus allows a web
page to recognize your computer’s IP address even if you turn off your computer
or log out between visits. The cookie is stored for six months and is then
deleted.</span></p>

<p class=MsoNormal><span lang=EN-US>The Company uses both session cookies and
permanent cookies on our website. Both types of cookies are needed primarily
for the functionality of our services, to help us improve our delivery of
products and services, to provide you with additional functionality or to help
us provide you with relevant and customized advertisement/marketing.
Furthermore, the Company uses third-party cookies from Google Analytics to
investigate how to adapt and develop the information on the Company’s website
in the best possible way.</span></p>

<p class=MsoNormal><span lang=EN-US>Most web browsers have a default setting
that accepts the use of cookies. You can easily refrain from allowing the
Company’s websites to store cookies on your computer with a setting in your web
browser, including blocking cookies or erasing any cookies stored on your
computer. [Please note that if you choose to not allow cookies, some
functionality on our websites may cease to function securely and properly and
some services may not be provided.] [Adjust the text with additional information
about what functions that are affected by the visitor’s choice to disable
cookie storage.] How you erase or change the settings for cookies is stated in
the instructions to your web browser or in the utility function that usually is
available in the web browser.</span></p>

<h2><span lang=EN-US>Changes to the privacy policy</span></h2>

<p class=MsoNormal><span lang=EN-US>Please note that the terms of the privacy
policy may be changed or amended. Any new version will be published on the
Company’s website. Consequently, you should review these terms on a regular
basis to make sure that you are satisfied with the changes. In case of any
material changes we will, however, e-mail you, if you have given us your e-mail
address, to make you aware of any changes made.</span></p>

<p class=MsoNormal><span lang=EN-US>If the changes concern processing of
personal data that we carry out based on your consent, we will give you the
opportunity to once again give your consent to the processing on the new terms
presented.</span></p>

<h2><span lang=EN-US>Contact us</span></h2>

<p class=MsoNormal><span lang=EN-US>If you have any questions related to this
privacy policy, if you suspect that a breach of this privacy policy has occurred,
or if you would like to contact us for any reason stated in this privacy
policy, please contact our customer service on the contact details provided
below.</span></p>

<p class=MsoNormal>Haglöf Sweden AB</p>

<p class=MsoNormal>Klockargatan 8</p>

<p class=MsoNormal>882 30 Långsele</p>

<p class=MsoNormal>&nbsp;</p>

<p class=MsoNormal>Info@haglofsweden.com</p>

<p class=MsoNormal>Tel.nr +46 620 255 80</p>

</div>

</body>

</html>
