<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Calibri Light";
	panose-1:2 15 3 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
h1
	{mso-style-link:"Rubrik 1 Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	line-height:107%;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;
	font-weight:normal;}
h2
	{mso-style-link:"Rubrik 2 Char";
	margin-top:2.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	line-height:107%;
	page-break-after:avoid;
	font-size:13.0pt;
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;
	font-weight:normal;}
h3
	{mso-style-link:"Rubrik 3 Char";
	margin-top:2.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	line-height:107%;
	page-break-after:avoid;
	font-size:12.0pt;
	font-family:"Calibri Light",sans-serif;
	color:#1F3763;
	font-weight:normal;}
span.Rubrik1Char
	{mso-style-name:"Rubrik 1 Char";
	mso-style-link:"Rubrik 1";
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;}
span.Rubrik2Char
	{mso-style-name:"Rubrik 2 Char";
	mso-style-link:"Rubrik 2";
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;}
span.Rubrik3Char
	{mso-style-name:"Rubrik 3 Char";
	mso-style-link:"Rubrik 3";
	font-family:"Calibri Light",sans-serif;
	color:#1F3763;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:70.85pt 70.85pt 70.85pt 70.85pt;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang=SV>

<div class=WordSection1>

<h1><span lang=EN-US>TERMS AND CONDITIONS</span></h1>

<p class=MsoNormal><span lang=EN-US>TERMS AND CONDITIONS OF USE OF HAGLOF SWEDEN
WEBSITES</span></p>

<h2><span lang=EN-US>1 Acceptance The Use Of Haglöf Sweden AB websites Terms
and Conditions</span></h2>

<p class=MsoNormal><span lang=EN-US>Your access to and use of Haglöf Sweden AB websites
is subject exclusively to these Terms and Conditions. You will not use the
Website for any purpose that is unlawful or prohibited by these Terms and
Conditions. By using the Website you are fully accepting the terms, conditions
and disclaimers contained in this notice. If you do not accept these Terms and
Conditions you must immediately stop using the Website.</span></p>

<h2><span lang=EN-US>2 Advice</span></h2>

<p class=MsoNormal><span lang=EN-US>The contents of Haglöf Sweden AB website do
not constitute advice and should not be relied upon in making or refraining
from making, any decision.</span></p>

<h2><span lang=EN-US>3 Change of Use</span></h2>

<p class=MsoNormal><span lang=EN-US>Haglöf Sweden AB reserves the right to:</span></p>

<p class=MsoNormal><span lang=EN-US>4.1 change or remove (temporarily or
permanently) the Website or any part of it without notice and you confirm that Haglöf
Sweden AB shall not be liable to you for any such change or removal and.</span></p>

<p class=MsoNormal><span lang=EN-US>4.2 change these Terms and Conditions at
any time, and your continued use of the Website following any changes shall be
deemed to be your acceptance of such change.</span></p>

<h2><span lang=EN-US>5 Links to Third Party Websites</span></h2>

<p class=MsoNormal><span lang=EN-US>Haglöf Sweden AB Website may include links
to third party websites that are controlled and maintained by others. Any link
to other websites is not an endorsement of such websites and you acknowledge
and agree that we are not responsible for the content or availability of any
such sites.</span></p>

<h2><span lang=EN-US>6 Copyright</span></h2>

<p class=MsoNormal><span lang=EN-US>6.1 All copyright, trade marks and all
other intellectual property rights in the Website and its content (including
without limitation the Website design, text, graphics and all software and
source codes connected with the Website) are owned by or licensed to Haglöf
Sweden AB or otherwise used by Haglöf Sweden AB as permitted by law.</span></p>

<p class=MsoNormal><span lang=EN-US>6.2 In accessing the Website you agree that
you will access the content solely for your personal, non-commercial use. None
of the content may be downloaded, copied, reproduced, transmitted, stored, sold
or distributed without the prior written consent of the copyright holder. This
excludes the downloading, copying and/or printing of pages of the Website for
personal, non-commercial home use only.</span></p>

<h3><span lang=EN-US>7 Disclaimers and Limitation of Liability</span></h3>

<p class=MsoNormal><span lang=EN-US>7.1 The Website is provided on an AS IS and
AS AVAILABLE basis without any representation or endorsement made and without
warranty of any kind whether express or implied, including but not limited to
the implied warranties of satisfactory quality, fitness for a particular
purpose, non-infringement, compatibility, security and accuracy.</span></p>

<p class=MsoNormal><span lang=EN-US>7.2 To the extent permitted by law, Haglöf
Sweden AB will not be liable for any indirect or consequential loss or damage
whatever (including without limitation loss of business, opportunity, data,
profits) arising out of or in connection with the use of the Website.</span></p>

<p class=MsoNormal><span lang=EN-US>7.3 Haglöf Sweden AB makes no warranty that
the functionality of the Website will be uninterrupted or error free, that
defects will be corrected or that the Website or the server that makes it
available are free of viruses or anything else which may be harmful or
destructive.</span></p>

<p class=MsoNormal><span lang=EN-US>7.4 Nothing in these Terms and Conditions
shall be construed so as to exclude or limit the liability of Haglöf Sweden AB for
death or personal injury as a result of the negligence of Haglöf Sweden AB or
that of its employees or agents.</span></p>

<h2><span lang=EN-US>8 Indemnity</span></h2>

<p class=MsoNormal><span lang=EN-US>You agree to indemnify and hold Haglöf
Sweden AB and its employees and agents harmless from and against all
liabilities, legal fees, damages, losses, costs and other expenses in relation
to any claims or actions brought against Haglöf Sweden AB arising out of any
breach by you of these Terms and Conditions or other liabilities arising out of
your use of this Website.</span></p>

<h2><span lang=EN-US>9 Severance</span></h2>

<p class=MsoNormal><span lang=EN-US>If any of these Terms and Conditions should
be determined to be invalid, illegal or unenforceable for any reason by any
court of competent jurisdiction then such Term or Condition shall be severed
and the remaining Terms and Conditions shall survive and remain in full force
and effect and continue to be binding and enforceable.</span></p>

<h2><span lang=EN-US>10 Governing Law</span></h2>

<p class=MsoNormal><span lang=EN-US>These Terms and Conditions shall be
governed by and construed in accordance with the law of USA and you hereby
submit to the exclusive jurisdiction of the USA courts.</span></p>

<p class=MsoNormal><span lang=EN-US>For any further information send a mail to
Haglöf Sweden AB, </span>Info@haglofsweden.com</p>

<p class=MsoNormal>&nbsp;</p>

</div>

</body>

</html>
