<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=windows-1252">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Calibri Light";
	panose-1:2 15 3 2 2 2 4 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0cm;
	margin-right:0cm;
	margin-bottom:8.0pt;
	margin-left:0cm;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
h1
	{mso-style-link:"Rubrik 1 Char";
	margin-top:12.0pt;
	margin-right:0cm;
	margin-bottom:0cm;
	margin-left:0cm;
	margin-bottom:.0001pt;
	line-height:107%;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;
	font-weight:normal;}
span.Rubrik1Char
	{mso-style-name:"Rubrik 1 Char";
	mso-style-link:"Rubrik 1";
	font-family:"Calibri Light",sans-serif;
	color:#2F5496;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
@page WordSection1
	{size:595.3pt 841.9pt;
	margin:70.85pt 70.85pt 70.85pt 70.85pt;}
div.WordSection1
	{page:WordSection1;}
-->
</style>

</head>

<body lang=SV>

<div class=WordSection1>

<h1><span lang=EN-US>About Haglöf Sweden®</span></h1>

<p class=MsoNormal><span lang=EN-US>Haglöf Sweden® manufactures a full range of
precision measurement solutions used by industry professionals all over the
world. The products combine traditional craftsmanship with modern technology,
tough exteriors with smart interiors. </span></p>

<p class=MsoNormal><span lang=EN-US>Apart from the world’s largest selection of
increment borer models for wood control, Haglöf Sweden offers different
diameter caliper models, hypsometers, rangefinders and other instruments and
tools for forest survey, management, testing and control work. Patented
technology and long manufacturing know-how are keys to why Haglöf Sweden® is
one of the most respected brand names in the business. All products are
developed and manufactured in limited and quality assured series by highly
experienced production teams. </span></p>

<p class=MsoNormal><span lang=EN-US>Since 1943 Haglöf Sweden AB has
manufactured and sold quality measuring instruments and tools. Our experienced,
knowledgeable and tight team and co-workers all take part in the company's
constant strive to improve, develop and increase effectiveness in forest and
field measuring work. </span></p>

<p class=MsoNormal><span lang=EN-US>Haglöf Sweden's main office is located in
the small village of Långsele in northern part of Sweden. Surrounded by vast
forest areas, deep lakes and wild rivers, Haglöf Sweden development team works
in close contact with foresters, scientists, land surveyors and other
professional operators. Our products are always tested to withstand different
types of climates, terrains, temperatures and surroundings.</span></p>

<p class=MsoNormal><span lang=EN-US>Haglöf Sweden products are sold all over
the world through our worldwide network of independent and dedicated
distributors. You will meet Haglöf staff and representatives at most major
international forestry exhibitions, and often on branch seminars, meetings and
conferences. We like to stay ahead and updated. Good ideas have no boundaries,
they are everywhere! Contact us for details on where we will be next and for
your closest Haglöf Sweden representative!</span></p>

</div>

</body>

</html>
