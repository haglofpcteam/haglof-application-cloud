@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{__('Overview ApplicationInstance')}}</div>

            <div class="card-body">


                <label>{{__('Application:')}} </label> {{$applicationInstance->application()->first()->name}}<br>
                <label>{{__('Organization:')}} </label> <a href='{{ route('admin_show_organization',$applicationInstance->organization()->first()) }}'>{{$applicationInstance->organization()->first()->name}}</a><br>
                <label>{{__('Is paid:')}} </label> {{ $applicationInstance->paid ? 'True' : 'False' }}<br>

                <br><br>

                <form method="POST" action="{{ route('admin_application_instance_paid_until',$applicationInstance) }}">
                    @csrf
                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan="2" scope="col">{{__('Manual Payment')}}</th>
                            </tr>
                        </thead>
                    </table>
                    <div class="form-group row">
                        <label for="paid_until" class="col-md-2 col-form-label text-md-left">{{__('Paid until:')}}</label>

                        <div class="col-md-6">
                            <input id="paid_until" type="date" class="form-control{{ $errors->has('paid_until') ? ' is-invalid' : '' }}" name="paid_until" value="{{ old('paid_until',$applicationInstance->paid_until) }}"
                                required autofocus> @if ($errors->has('paid_until'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('paid_until') }}</strong>
                            </span> @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="number_of_users" class="col-md-2 col-form-label text-md-left">{{__('Number of users:')}}</label>
    
                        <div class="col-md-6">
                            <input id="number_of_users" type="text" class="form-control{{ $errors->has('number_of_users') ? ' is-invalid' : '' }}" name="number_of_users" value="{{ old('number_of_users',$applicationInstance->number_of_users) }}"
                                required autofocus> @if ($errors->has('number_of_users'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('number_of_users') }}</strong>
                            </span> @endif
                        </div>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-default border">{{__('Save')}}</button>
                </form>
                
                
                            

                @if($applicationInstance->users()->first())

                <br><br>

                <table class="table">
                    <thead>
                        <tr>
                            <th colspan=3 scope="col">{{__('Users')}}</th>
                        </tr>
                        <tr>
                            <th scope="col">{{__('#')}}</th>
                            <th scope="col">{{__('User')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($applicationInstance->users()->get() as $user)
                            <tr>
                                <th scope="row">{{ $user->id }}</th>
                                <td class='clickable-cell' data-href='{{ route('admin_show_user',$user) }}'>
                                    {{ $user->identifiableName() }}
                                </td>
                                @if(Auth::user()->role>9)
                                <td><a data-toggle="modal" href="#removeUser{{ $user->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a></td>
                                @endif

                                <!-- Remove User Modal -->
                                <div id="removeUser{{ $user->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{ __('Remove user?')}}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                {{ __('Are you sure you wan to remove user :user_name from :application_name',['user' => $user->name, 'application_name' => $applicationInstance->name])}}
                                            </div>
                                            <div class="modal-footer">
                                                    <button type="submit" class="btn btn-default" onclick="window.location.href = '{{ route('application_instance_remove_user',[$applicationInstance,$user]) }}';">{{ __('Yes')}}</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('No')}}</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </tr>
                        @endforeach
                    </tbody>
                </table>


                @endif


            <form method="POST" action="{{ route('invite_to_application_instance',$applicationInstance) }}">
                @csrf

                <label for="email">{{ __('E-mail:') }}</label>
                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif

                <label for="usermessage">{{ __('Message:') }}</label>
                <textarea id="usermessage" name="usermessage" cols="40" rows="5" class="form-control{{ $errors->has('usermessage') ? ' is-invalid' : '' }}" value="{{ old('usermessage') }}"></textarea>

                @if ($errors->has('usermessage'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('usermessage') }}</strong>
                    </span>
                @endif

                <br>
                <button type="submit" class="btn btn-default border">{{__('Invite')}}</button>
            </form>
            <br><br>

            <table class="table">
                    <thead>
                        <tr>
                            <th colspan=3 scope="col">{{__('Report templates')}}</th>
                        </tr>
                        <tr>
                            <th scope="col">{{__('#')}}</th>
                            <th scope="col">{{__('Report')}}</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($report_templates_all as $template)
                            <tr>
                                <th scope="row">{{ $template->id }}</th>
                                <td class='clickable-cell' data-href='{{ route('admin_report_template_edit',$template) }}'>
                                    {{ $template->name }}<br>
                                    <em>{{ $template->key }}</em>
                                </td>
                                @if(!$template->standard)
                                    <td>
                                    @if($report_templates->contains($template))
                                    <a href={{route('admin_remove_report_template',[$applicationInstance,$template])}}>
                                        <i class="fa fa-minus-circle"></i>
                                    </a>
                                    @else
                                    <a href={{route('admin_add_report_template',[$applicationInstance,$template])}}>
                                        <i class="fa fa-plus-circle"></i>
                                    </a>
                                    @endif
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
