@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{ __('Organization list')}}</div>

            <div class="card-body">
                <div class="table-responsive">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('#')}}</th>
                            <th scope="col">{{ __('name')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(Agent::isMobile())
                        @foreach($organizations as $organization)
                            <tr class='clickable-row' data-href='{{ route('admin_show_organization',$organization) }}'>
                                <th scope="row">{{ $organization->id }}</th>
                                <td>{{ $organization->name }}</td>
                            </tr>
                        @endforeach
                        @else
                        @foreach($organizations as $organization)
                        <tr>
                            <th scope="row">{{ $organization->id }}</th>
                            <td><a href='{{ route('admin_show_organization',$organization) }}'>{{ $organization->name }}</a></td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
