@extends('layouts.app')

@section('parent-url', route('organizations'))

@php
    $timezone=Auth::user()->timezone;
@endphp

@section('menu')

    <div class="col-md-auto p-4 text-right">
        <h4 class="text-white">{{ __('Pits and Piles 3D')}}</h4>
        <ul class="list-unstyled">
            <li>
                <a data-toggle="modal" href="#addSite" class="text-white">
                    {{ __('Add Site')}}
                </a>
            </li>
            <li>
                <a href="{{route("application_instance_edit",$pileApplication->applicationInstance()->first())}}" class="text-white">
                    {{ __('Settings')}}
                </a>
            </li>
        </ul>
    </div>

@endsection

@section('content')

@if(!Agent::isMobile())
    <section class="jumbotron text-center" style="background-image:url('{{asset('img/jumbotron/' . $random_jumbotron)}}');">
        <div class="jumbotron-container">
            <div class="jumbotron-box">
                <h1 class="jumbotron-heading text-white">{{ __('Pits and Piles 3D')}}</h1>
                <p class="lead text-white">{{__('A visualization tool for Vertex Laser GEO and Laser GEO measurements')}}</p>
            </div>
        </div>
    </section>
@endif

@if($showStartupMessage&&!Agent::isMobile())
<div id="overlay">
    <div class="col-md-8 mx-auto mt-5">
        <div class="card">
            <div class="card-body">

                    <h3>Pits & Piles 3D</h3>

                    With our new cloud application, Pits & Piles 3D, you can calculate, edit, store and print all your measurements<br>
                    created in your Vertex Laser Geo or Laser Geo measured by using the Map target function.<br>
                    
                    Just upload your CSV file and get the volume of your object.<br>
                    
                    This Application can be used on any platform all you need is access to the Internet and a Web browser.<br>
                    <br>
                    <br> 
                    
                    <h6>Free version</h6>
                    <ul>
                        <li>It’s free to upload a measurement</li>
                        <li>Edit nodes</li>
                        <li>Look and rotate the 3D image and see the volume.</li>
                    </ul><br>
                    
                    <h6>Paid version</h6>
                    
                    <ul>
                        <li>With the paid version you get the opportunity to save all the measurements in a database.</li>
                        <li>Create multiple sites</li>
                        <li>Create multiple locations on every site</li>
                        <li>Upload and create multiple measurements on every locations</li>
                        <li>All changes that are made are saved</li>
                        <li>Add multiple measurements for one location of an object</li>
                        <li>Generate a report for a measurement</li>
                        <li>Generate report for several measurements during a period and comparison between the different measurement occasions.</li>
                        <li>It I also possible to ask for and buy your own customized reports.</li>
                    </ul><br>

                <br><br>
                <button type="button" class="btn btn-default border" onclick="$('#overlay').hide();">
                    Close
                </button>
                @if(!$pileApplication->admin)
                    <button type="button" class="btn btn-success border" data-toggle="modal" data-target="#notAuthorizedModal">
                        Subscribe
                    </button>
                @else
                    @if($pileApplication->applicationInstance()->first()->application()->first()->payment_link)
                        <a href="{{$pileApplication->applicationInstance()->first()->application()->first()->payment_link}}" class="btn btn-success border">
                            Subscribe
                        </a>   
                    @endif
                @endif
            </div>
        </div>
    </div>
</div>
@endif



    <main class="py-4">
    <div class="container">
        <div class="row justify-content-center">

            @foreach($pileApplication->pileApplicationSites()->get() as $site)
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header text-white bg-dark">
                            <div class="row">
                            <div class="col">
                                <img src="{{asset('img/site_trans.png')}}">
                                {{$site->name}}
                            </div>
                            <div class="col text-right my-auto">

                                <a  data-toggle="modal" href="#editSite{{ $site->id }}" class="text-white">
                                    <i class="fa fa-cog"></i>
                                </a>

                            </div>
                        </div>
                    </div>

                    <div class="card-body">

                        @foreach($site->locations()->get() as $location)

                            <div class="panel-group">
                                    <div class="panel panel-default">
                                      <div class="panel-heading location-title mb-2" data-toggle="collapse" data-target="#collapse{{$location->id}}" href="#collapse{{$location->id}}" onclick="void(0)">
                                        <div class="row" onclick="void(0)">
                                            <div class="col text-nowrap" onclick="void(0)">
                                                    <img src="{{asset('img/measurement_trans.png')}}" class="bg-dark rounded">
                                                <h5 class="panel-title d-inline" onclick="void(0)">
                                                    {{$location->name}}
                                                </h5>
                                            </div>
                                            <div class="col text-right my-auto" onclick="void(0)">
                                                <a  data-toggle="modal" href="#editLocation{{ $location->id }}" id="collapse{{$location->id}}" class="panel-collapse collapse">
                                                    <i class="fa fa-cog"></i>
                                                </a>
                                            </div>
                                        </div>
                                      </div>
                                      <div id="collapse{{$location->id}}" class="panel-collapse collapse">
                                        <ul class="list-group">
                                            @foreach($location->measurements()->orderBy('measurement_time')->get() as $measurement)
                                            @if(!Agent::isMobile())
                                            <li class="list-group-item measurement-title">
                                            @else
                                            <li class="list-group-item">
                                            @endif
                                                <div class="row">
                                                    <div class="col clickable-element" data-href='{{ route('show_measurement',$measurement) }}'>                                                  
                                                        {{ $measurement->toLocalTime('measurement_time')["date_time"] }}
                                                    </div>
                                                    <div class="col-1 text-right">
                                                        <a  data-toggle="modal" href="#editMeasurement{{ $measurement->id }}">
                                                            <i class="fa fa-cog"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </li>

                                            <!-- Edit Measurement Modal -->
                                            <div id="editMeasurement{{ $measurement->id }}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">{{__('Edit measurement: ')}} {{ $measurement->toLocalTime('measurement_time')["date_time"] }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <script>
                                                            $(document).ready(function () {
                                                            $('#edit_measurement_form_{{ $measurement->id }}').validate(
                                                            {
                                                                rules:
                                                                {
                                                                    volume_adjustment_factor: { 
                                                                        required: true,
                                                                        number: true
                                                                    },
                                                                    density: { 
                                                                        required: true,
                                                                        number: true
                                                                    }
                                                                },
                                                                messages:
                                                                {
                                                                    volume_adjustment_factor:
                                                                    {
                                                                        required:"Volume adjustment factor cannot be empty",
                                                                        number:"Volume adjustment factor must be a number"
                                                                    },
                                                                    density:
                                                                    {
                                                                        required:"Density cannot be empty",
                                                                        number:"Density must be a number"
                                                                    }
        
                                                                },
                                                                errorPlacement: function(error, element) {
                                                                    $(error).addClass("text-danger").addClass("pt-2");
                                                                    var p = element.parent();
                                                                    error.appendTo( p );
                                                                    $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
                                                                },
                                                                submitHandler: function() { 
                                                                    $('#editLocation{{ $location->id }}').modal('hide');
                                                                    form.submit();
                                                                }
                            
                                                            });
                            
                                                        });
                                
                                                        </script>

                                                        <div class="modal-body">
                                                            <form method="POST" id="edit_measurement_form_{{ $measurement->id }}" action="{{ route('pile_update_measurement',$measurement) }}">
                                                                @csrf

                                                                <div class="form-group">
                                                                    <label for="volume_adjustment_factor">{{ __('Volume adjustment factor') }}</label>
                                                                    <input type="text" class="form-control" name="volume_adjustment_factor" value="{{$measurement->volume_adjustment_factor}}">
                                                                </div>
                
                                                                <div class="form-group">
                                                                    <label for="density">{{ __('Density') }}</label>
                                                                    <input type="text" class="form-control" name="density" value="{{$measurement->density}}">
                                                                </div>
                                                                
                                                                <div class="form-group">
                                                                    <label for="description">{{ __('Description') }}</label>
                                                                    <textarea type="text" rows=5 class="form-control" name="description">{{$measurement->description}}</textarea>
                                                                </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                                <button data-toggle="modal" data-target="#deleteMeasurement{{ $measurement->id }}" data-dismiss="modal" class="btn btn-danger">{{__('Delete')}}</a>
                                                                <button type="submit" class="btn btn-default">{{__('Save')}}</button>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- Delete Measurement Modal -->
                                            <div id="deleteMeasurement{{ $measurement->id }}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h5 class="modal-title">{{__('Delete measurement: ')}} {{ $measurement->toLocalTime('measurement_time')["date_time"] }}</h5>
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                        </div>
                                                        <div class="modal-body">
                                                        {{ __('Delete measurement ') . $measurement->toLocalTime('measurement_time')["date_time"] . "?"}}

                                                        </div>
                                                        <div class="modal-footer">
                                                                <a href="{{ route('pile_destroy_measurement',$measurement) }}" class="btn btn-danger">{{__('Yes')}}</a>
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('No')}}</button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            @endforeach
                                            
                                        </ul>
                                        <div class="panel-footer">
                                            <br>
                                            <a data-toggle="modal" class="btn btn-default border" href="#uploadMeasurement{{ $location->id }}">
                                                    {{__('Upload')}}
                                            </a>
                                            @if($pileApplication->applicationInstance()->first()->paid)
                                            <a data-toggle="modal" class="btn btn-default border" href="#generateReport{{ $location->id }}">
                                                    <i class="fa fa-fw fa-print"></i>
                                            </a>
                                            @endif
                                            <br><hr>
                                        </div>
                                      </div>
                                    </div>
                                  </div>

                            <!-- Upload CSV file Modal -->
                            <div id="uploadMeasurement{{ $location->id }}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">{{__('Upload Measurement')}}</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">
                                            <form method="POST" id="upload_measurement_form_{{ $location->id }}" enctype="multipart/form-data" action="{{ route('upload_pileapplication_measurement',$location) }}">
                                                @csrf
                                                <label for="csvfile">{{ __('CSV File') }}</label>

                                                <div class="input-group form-group">
                                                    <div class="custom-file">
                                                      <input type="file" class="custom-file-input" id="csvfile{{ $location->id }}" name="csvfile" accept=".csv" aria-describedby="inputGroupFileAddon01" required>
                                                      <label class="custom-file-label" for="csvfile{{ $location->id }}">{{ __('CSV File') }}</label>
                                                    </div>
                                                  </div>

                                                  <script>
                                                    $('#csvfile{{ $location->id }}').on('change',function(){
                                                        //get the file name
                                                        var fileName = $(this).val().replace(/^.*[\\\/]/, '');

                                                        //replace the "Choose a file" label
                                                        $(this).next('.custom-file-label').html(fileName);
                                                    })

                                                    $(document).ready(function () {
                                                        $('#upload_measurement_form_{{ $location->id }}').validate(
                                                        {
                                                            rules:
                                                            {
                                                                volume_adjustment_factor: { 
                                                                    required: true,
                                                                    number: true
                                                                },
                                                                density: { 
                                                                    required: true,
                                                                    number: true
                                                                }
                                                            },
                                                            messages:
                                                            {
                                                                volume_adjustment_factor:
                                                                {
                                                                    required:"Volume adjustment factor cannot be empty",
                                                                    number:"Volume adjustment factor must be a number"
                                                                },
                                                                density:
                                                                {
                                                                    required:"Density cannot be empty",
                                                                    number:"Density must be a number"
                                                                }
    
                                                            },
                                                            errorPlacement: function(error, element) {
                                                                $(error).addClass("text-danger").addClass("pt-2");
                                                                var p = element.parent();
                                                                error.appendTo( p );
                                                                $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
                                                            },
                                                            submitHandler: function() { 
                                                                $('#editLocation{{ $location->id }}').modal('hide');
                                                                form.submit();
                                                            }
                        
                                                        });
                        
                                                    });
                            
                                                    </script>

                                                <div class="form-group">
                                                    <label for="volume_adjustment_factor">{{ __('Volume adjustment factor') }}</label>
                                                    <input type="text" class="form-control" name="volume_adjustment_factor" value="{{$location->volume_adjustment_factor}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="density">{{ __('Density') }}</label>
                                                    <input type="text" class="form-control" name="density" value="{{$location->density}}">
                                                </div>

                                                <div class="form-group">
                                                    <label for="description">{{ __('Description') }}</label>
                                                    <textarea type="text" rows=5 class="form-control" name="description"></textarea>
                                                </div>

                                        </div>
                                        <div class="modal-footer">
                                                <button type="submit" class="btn btn-default">{{__('Upload')}}</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Generate Report Modal -->
                            <div id="generateReport{{ $location->id }}" class="modal fade" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">{{__('Generate report')}}</h4>
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        </div>
                                        <div class="modal-body">

                                            <script>
                                                $(document).ready(function () {
                                                    $('#generate_report_form_{{ $location->id }}').validate(
                                                    {
                                                        rules:
                                                        {
                                                            template_id:{ required: true },
                                                            "measurements[]":{ required: true } 
                                                        },
                                                        messages:
                                                        {
                                                            template_id:
                                                            {
                                                                required:"Please select a template"
                                                            },
                                                            "measurements[]":
                                                            {
                                                                required:"Please select a measurement"
                                                            }
                                                        },
                                                        errorPlacement: function(error, element) {
                                                            $(error).addClass("text-danger");
                                                            if($(element).attr('name')=="template_id") {
                                                                var p = element.parent().parent();
                                                                error.appendTo( p );
                                                                $(p).addClass("border").addClass("border-danger").addClass("p-2");
                                                            }
                                                            else if($(element).attr('name')=="measurements[]") {
                                                                var p = element.parent();
                                                                error.appendTo( p );
                                                                $(p).addClass("border").addClass("border-danger").addClass("p-2");
                                                            }
                                                            else {
                                                                error.appendTo( element.parent() );
                                                            }
                                                            
                                                        },
                                                        submitHandler: function() { 
                                                            $('#generateReport{{ $location->id }}').modal('hide');
                                                            window.open('about:blank','print_popup','width=630,height=891');
                                                            form.submit();
                                                        }
                    
                                                    });
                    
                                                });
                    
                                            </script>

                                            <form method="POST" id="generate_report_form_{{ $location->id }}" target="print_popup" action="{{ route('pileapplication_generate_multi_report') }}">
                                                @csrf
                                                <h5 class="mt-1 mb-3">{{__('Select Measurements')}}</h5>
                                                <div>
                                                @foreach($location->measurements()->orderBy('measurement_time')->get() as $measurement)
                                                    <input type="checkbox" class="ml-4" name="measurements[]" value="{{$measurement->id}}">
                                                    <label>
                                                        {{$measurement->toLocalTime('measurement_time')["date_time"]}}
                                                    </label><br>
                                                @endforeach
                                                </div>
                                                <h5 class="mt-2 mb-3">{{__('Select Report Template')}}</h5>
                                                <div>
                                                @foreach($report_templates as $template)

                                                <div class="form-check">
                                                        <input 
                                                            class="form-check-input" 
                                                            type="radio" 
                                                            name="template_id" 
                                                            id="template_{{$template->id}}" 
                                                            value="{{$template->id}}"
                                                            onchange="$('.template_parameters').addClass('d-none');$('#template_{{$template->id}}_parameters').removeClass('d-none');"
                                                            >
                            
                                                        <label class="form-check-label" for="template_{{$template->id}}">
                                                                {{$template->name}}
                                                        </label>
                                                    </div>
                            
                                                    <div class="d-none template_parameters" id="template_{{$template->id}}_parameters">
                                                        @foreach($template->parameters()->get() as $parameter)
                                                        <div class="form-group mt-4 row">
                                                            <label for="template_{{$template->id}}_parameter_{{$parameter->name}}" class="col-md-4 col-form-label text-md-right">{{$parameter->name}}: </label>
                                                            <div class="col-md-8">
                                                                <input type="text" class="form-control" name="template_{{$template->id}}_parameter_{{$parameter->name}}" id="template_{{$template->id}}_parameter_{{$parameter->name}}">
                                                            </div>
                                                        </div>
                                                        @endforeach
                                                    </div>

                                                @endforeach
                                                </div>
                                        </div>
                                        <div class="modal-footer">
                                                <button type="submit" class="btn btn-default">{{__('Generate')}}</button>
                                                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                                            </form>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <!-- Edit Location Modal -->
                            <div id="editLocation{{ $location->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{__('Edit location: ')}} {{ $location->name }}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>

                                            <script>
                                                $(document).ready(function () {
                                                    $('#edit_location_form{{ $location->id }}').validate(
                                                    {
                                                        rules:
                                                        {
                                                            location_name:{ required: true },
                                                            volume_adjustment_factor: { 
                                                                required: true,
                                                                number: true
                                                            },
                                                            density: { 
                                                                required: true,
                                                                number: true
                                                            }
                                                        },
                                                        messages:
                                                        {
                                                            location_name:
                                                            {
                                                                required:"Location name cannot be empty"
                                                            },
                                                            volume_adjustment_factor:
                                                            {
                                                                required:"Volume adjustment factor cannot be empty",
                                                                number:"Volume adjustment factor must be a number"
                                                            },
                                                            density:
                                                            {
                                                                required:"Density cannot be empty",
                                                                number:"Density must be a number"
                                                            }

                                                        },
                                                        errorPlacement: function(error, element) {
                                                            $(error).addClass("text-danger").addClass("pt-2");
                                                            var p = element.parent();
                                                            error.appendTo( p );
                                                            $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
                                                        },
                                                        submitHandler: function() { 
                                                            $('#editLocation{{ $location->id }}').modal('hide');
                                                            form.submit();
                                                        }
                    
                                                    });
                    
                                                });
                    
                                            </script>

                                            <div class="modal-body">
                                                <form method="POST" id="edit_location_form{{ $location->id }}" action="{{ route('pile_update_location',$location) }}">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label for="location_name">{{ __('Location name') }}</label>
                                                        <input type="text" class="form-control" name="location_name" value="{{$location->name}}" required autofocus>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="volume_adjustment_factor">{{ __('Volume adjustment factor') }}</label>
                                                        <input type="text" class="form-control" name="volume_adjustment_factor" value="{{$location->volume_adjustment_factor}}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="density">{{ __('Density') }}</label>
                                                        <input type="text" class="form-control" name="density" value="{{$location->density}}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="description">{{ __('Measurement system') }}</label>
                                                        <select class="form-control" name="measurement_system">
                                                            <option value="metric" {{ $location->measurement_system=='metric' ? 'selected' : ''}}>{{__('Metric')}}</option>
                                                            <option value="imperial" {{ $location->measurement_system=='imperial' ? 'selected' : ''}}>{{__('Imperial')}}</option>
                                                        </select>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="description">{{ __('Description') }}</label>
                                                        <textarea type="text" rows=5 class="form-control" name="description">
                                                                {{$location->description}}
                                                        </textarea>
                                                    </div>

                                            </div>
                                            <div class="modal-footer">
                                                    <button data-toggle="modal" data-target="#deleteLocation{{ $location->id }}" data-dismiss="modal" class="btn btn-danger">{{__('Delete')}}</a>
                                                    <button type="submit" class="btn btn-default">{{__('Save')}}</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                            </div>

                            <!-- Delete Location Modal -->
                            <div id="deleteLocation{{ $location->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{__('Delete location: ')}} {{ $location->name }}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                            {{ __('Delete location ') . $location->name . "?"}}

                                            </div>
                                            <div class="modal-footer">
                                                    <a href="{{ route('pile_destroy_location',$location) }}" class="btn btn-danger">{{__('Yes')}}</a>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{__('No')}}</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                            </div>

                        @endforeach

                        <br>
                        <a data-toggle="modal" class="btn btn-default border" href="#addLocation{{ $site->id }}">
                            {{__('Add location')}}
                        </a>

                        <!-- Add Location Modal -->
                        <div id="addLocation{{ $site->id }}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">{{__('Add Location to')}} {{ $site->name }}</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    
                                    <script>
                                            $(document).ready(function () {
                                                $("#add_location_to_{{$site->id}}").validate(
                                                {
                                                    rules:
                                                    {
                                                        location_name:{ required: true },
                                                        volume_adjustment_factor: { 
                                                            required: true,
                                                            number: true
                                                        },
                                                        density: { 
                                                            required: true,
                                                            number: true
                                                        }
                                                    },
                                                    messages:
                                                    {
                                                        location_name:
                                                        {
                                                            required:"Location name cannot be empty"
                                                        },
                                                        volume_adjustment_factor:
                                                        {
                                                            required:"Volume adjustment factor cannot be empty",
                                                            number:"Volume adjustment factor must be a number"
                                                        },
                                                        density:
                                                        {
                                                            required:"Density cannot be empty",
                                                            number:"Density must be a number"
                                                        }

                                                    },
                                                    errorPlacement: function(error, element) {
                                                        $(error).addClass("text-danger").addClass("pt-2");
                                                        var p = element.parent();
                                                        error.appendTo( p );
                                                        $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
                                                    },
                                                    submitHandler: function() { 
                                                        $('#addLocation{{ $site->id }}').modal('hide');
                                                        form.submit();
                                                    }
                
                                                });
                
                                            });
                
                                        </script>

                                    <div class="modal-body">
                                        <form method="POST" id="add_location_to_{{$site->id}}" action="{{ route('store_pileapplication_location',$site) }}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="location_name">{{ __('Location name') }}</label>
                                                <input type="text" class="form-control" name="location_name" autofocus>
                                            </div>
                                            <div class="form-group">
                                                <label for="volume_adjustment_factor">{{ __('Volume adjustment factor') }}</label>
                                                <input type="text" class="form-control" name="volume_adjustment_factor" value="1.0">
                                            </div>
                                            <div class="form-group">
                                                <label for="density">{{ __('Density') }}</label>
                                                <input type="text" class="form-control" name="density" value="1.0">
                                            </div>
                                            <div class="form-group">
                                                <label for="measurement_system">{{ __('Measurement system') }}</label>
                                                <select class="form-control" name="measurement_system">
                                                    <option value="metric" selected>{{__('Metric')}}</option>
                                                    <option value="imperial">{{__('Imperial')}}</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label for="description">{{ __('Description') }}</label>
                                                <textarea type="text" rows=5 class="form-control" name="description"></textarea>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                            <button type="submit" class="btn btn-default">{{__('Add location')}}</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Edit Site Modal -->
                        <div id="editSite{{ $site->id }}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">{{__('Edit site: ')}} {{ $site->name }}</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>

                                    <script>
                                            $(document).ready(function () {
                                                $('#edit_site_form{{ $site->id }}').validate(
                                                {
                                                    rules:
                                                    {
                                                        site_name:{ required: true },
                                                    },
                                                    messages:
                                                    {
                                                        site_name:
                                                        {
                                                            required:"Site name cannot be empty"
                                                        },

                                                    },
                                                    errorPlacement: function(error, element) {
                                                        $(error).addClass("text-danger").addClass("pt-2");
                                                        var p = element.parent();
                                                        error.appendTo( p );
                                                        $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
                                                    },
                                                    submitHandler: function() { 
                                                        $('#editSite{{ $site->id }}').modal('hide');
                                                        form.submit();
                                                    }
                
                                                });
                
                                            });
                
                                        </script>

                                    <div class="modal-body">
                                        <form method="POST" id="edit_site_form{{ $site->id }}" action="{{ route('pile_update_site',$site) }}">
                                            @csrf
                                            <div class="form-group">
                                                <label for="site_name">{{ __('Site name') }}</label>
                                                <input type="text" class="form-control" name="site_name" value="{{$site->name}}" autofocus>
                                            </div>
                                            <div class="form-group">
                                                <label for="description">{{ __('Description') }}</label>
                                                <textarea type="text" rows=5 class="form-control" name="description">{{$site->description}}</textarea>
                                            </div>
                                    </div>
                                    <div class="modal-footer">
                                            <button data-toggle="modal" data-target="#deleteSite{{ $site->id }}" data-dismiss="modal" class="btn btn-danger">{{__('Delete')}}</a>
                                            <button type="submit" class="btn btn-default">{{__('Save')}}</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- Delete Site Modal -->
                        <div id="deleteSite{{ $site->id }}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">{{__('Delete site: ')}} {{ $site->name }}</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                    {{ __('Delete site ') . $site->name . "?"}}
                                    {{ route('pile_destroy_site',$site) }}

                                    </div>
                                    <div class="modal-footer">
                                            <a href="{{ route('pile_destroy_site',$site) }}" class="btn btn-danger">{{__('Yes')}}</a>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">{{__('No')}}</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                @if(Agent::isMobile())
                <a data-toggle="modal" href="#addSite" class="btn btn-primary my-2">
                        {{ __('Add site')}}
                    </a>
                    @if($pileApplication->admin)
                    <a href="{{route("application_instance_edit",$pileApplication)}}" class="btn btn-secondary my-2"><i class="fa fa-cog"></i></a>
                    @endif
                @endif

            </div>
            @endforeach

            @if(!$pileApplication->pileApplicationSites()->count())
                <a data-toggle="modal" href="#addSite">
                    <i class="fas fa-plus fa-7x text-dark mx-5"></i>
                </a>
            @endif
            
            <br>

            <!-- Add site Modal -->
            <div id="addSite" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">{{__('Add Site')}}</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <script>
                                $(document).ready(function () {
                                    $('#add_site').validate(
                                    {
                                        rules:
                                        {
                                            site_name:{ required: true },
                                        },
                                        messages:
                                        {
                                            site_name:
                                            {
                                                required:"Site name cannot be empty"
                                            },

                                        },
                                        errorPlacement: function(error, element) {
                                            $(error).addClass("text-danger").addClass("pt-2");
                                            var p = element.parent();
                                            error.appendTo( p );
                                            $(p).addClass("border").addClass("border-danger").addClass("p-2");                                                 
                                        },
                                        submitHandler: function() { 
                                            $('#add_site').modal('hide');
                                            form.submit();
                                        }
    
                                    });
    
                                });
    
                            </script>

                        <div class="modal-body">
                            <form method="POST" id="add_site" action="{{ route('store_pileapplication_site',$pileApplication) }}">
                                @csrf
                                <div class="form-group">
                                    <label for="site_name">{{ __('Site name') }}</label>
                                    <input id="site_name" type="text" class="form-control" name="site_name" autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="description">{{ __('Description') }}</label>
                                    <textarea id="description" type="text" rows=5 class="form-control" name="description"></textarea>
                                </div>
                        </div>
                        <div class="modal-footer">
                                <button type="submit" class="btn btn-default">{{__('Add site')}}</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">{{__('Close')}}</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
</main>

@endsection
