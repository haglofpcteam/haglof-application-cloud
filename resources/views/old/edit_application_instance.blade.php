@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

@if(!Agent::isMobile())
    <section class="jumbotron text-center" style="background-image:url('{{asset('img/jumbotron/' . $random_jumbotron)}}');">
    <div class="jumbotron-container">
        <div class="jumbotron-box">
            <h1 class="jumbotron-heading text-white">{{$applicationInstance->application()->first()->name}}</h1>
            <p class="lead text-white">{{__('Settings')}}</p>
        </div>
    </div>
    </section>
@endif

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{__('Instance settings')}}</div>

            <div class="card-body">


                    <label>{{__('Application:')}} </label> {{$applicationInstance->application()->first()->name}}<br>
                    <label>{{__('Organization:')}} </label>

                    @if($applicationInstance->organization()->first()->admin)
                        <a href='{{ route('edit_organization',$applicationInstance->organization()->first()) }}'>{{$applicationInstance->organization()->first()->name}}</a><br>
                    @else
                        {{$applicationInstance->organization()->first()->name}}
                    @endif

                    @if($applicationInstance->application()->first()->payment_link)
                    @if($applicationInstance->paid)
                    <label>Paid number of users:</label>
                    {{$applicationInstance->paidNumberOfUsers}}<br/>
                    @else
                    {{__('Not paid')}}<br/>
                    @endif
                    <label>{{__('Payment:')}}</label>
                    <a href={{$applicationInstance->application()->first()->payment_link}}>Handle payment</a>
                    @endif

                @if($applicationInstance->users()->first())

                <br><br>

                <table class="table">
                    <thead>
                        <tr>
                            <th colspan=3 scope="col">{{__('Users')}}</th>
                        </tr>
                        <tr>
                            <th scope="col">{{__('#')}}</th>
                            <th scope="col">{{__('User')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($applicationInstance->users()->get() as $user)
                            <tr>
                                <th scope="row">{{ $user->id }}</th>
                                <td>{{ $user->identifiableName() }}</td>

                                <td>
                                    <a data-toggle="modal" href="#removeUser{{ $user->id }}"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                                

                                <!-- Remove User Modal -->
                                <div id="removeUser{{ $user->id }}" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h4 class="modal-title">{{ __('Remove user?')}}</h4>
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            </div>
                                            <div class="modal-body">
                                                {{ __('Are you sure you wan to remove user :user_name from :application_name',['user' => $user->name, 'application_name' => $applicationInstance->name])}}
                                            </div>
                                            <div class="modal-footer">
                                                    <button type="submit" class="btn btn-default" onclick="window.location.href = '{{ route('application_instance_remove_user',[$applicationInstance,$user]) }}';">{{ __('Yes')}}</button>
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">{{ __('No')}}</button>
                                                </form>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </tr>
                        @endforeach
                    </tbody>
                </table>


                @endif

                <form method="POST" action="{{ route('invite_to_application_instance',$applicationInstance) }}">
                    @csrf

                    <label for="email">{{ __('E-mail:') }}</label>
                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif

                    <label for="usermessage">{{ __('Message:') }}</label>
                    <textarea id="usermessage" name="usermessage" cols="40" rows="5" class="form-control{{ $errors->has('usermessage') ? ' is-invalid' : '' }}" value="{{ old('usermessage') }}"></textarea>

                    @if ($errors->has('usermessage'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('usermessage') }}</strong>
                        </span>
                    @endif

                    <br>
                    <button type="submit" class="btn btn-default border">{{__('Invite')}}</button>
                </form>

                <br>

                <a data-toggle="modal" href="#removeApplicationInstance" class="btn btn-danger my-2">{{__('Remove Instance')}}</a>

            </div>
            </div>
        </div>

    </div>
</div>
</main>

 <!-- Remove Application Instance -->
 <div id="removeApplicationInstance" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">{{__('Remove Application?')}}</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    {{__('Are you sure you want to remove :application_name',['application_name' => $applicationInstance->name])}}

                </div>
                <div class="modal-footer">
                        <button type="submit" class="btn btn-default" onclick="window.location.href = '{{ route('remove_application_instance',$applicationInstance)}}';">{{__('Yes')}}</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">{{__('No')}}</button>
                    </form>
                </div>
            </div>

        </div>
    </div>
@endsection
