@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-dark">{{$application->name}}</div>

                <div class="card-body">


                        <form method="POST" action="{{ route('admin_update_application',$application) }}">
                                @csrf

                                <div class="form-group row">
                                    <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name',$application->name) }}"
                                            required autofocus> @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span> @endif
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="icon" class="col-md-4 col-form-label text-md-right">{{ __('Image Base64') }}</label>

                                    <div class="col-md-6">
                                        <textarea id="icon" type="email" class="form-control{{ $errors->has('icon') ? ' is-invalid' : '' }}" name="icon">
                                            {{ old('icon',$application->image) }}
                                        </textarea>
                                            @if ($errors->has('icon'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('icon') }}</strong>
                                                </span>
                                            @endif
                                    </div>
                                </div>
                                <div class="form-group row justify-content-center">
                                        <div class="col-xs-2 col-sm-2">
                                            <button type="submit" class="btn btn-primary btn-block">
                                            {{ __('Update') }}
                                        </button>
                                        </div>
                                    </div>

                            </form>

                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan=3 scope="col">{{__('Plans')}}</th>
                            </tr>
                            <tr>
                                <th scope="col">{{ __('Id')}}</th>
                                <th scope="col">{{ __('Name')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($plans as $plan)
                                <tr>
                                    <th scope="row" class="text-nowrap">{{ $plan->id }}</th>
                                    <td><a href={{route('admin_show_plan',$plan)}}>{{ $plan->name }}</a></td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
