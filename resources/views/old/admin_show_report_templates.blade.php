@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{ __('Report Templates')}}</div>

            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('#')}}</th>
                            <th scope="col">{{ __('Report')}}</th>
                            <th scope="col"></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($templates as $template)
                            <tr>
                                <th scope="row">{{ $template->id }}</th>
                                <td class='clickable-cell' data-href='{{ route('admin_report_template_edit',$template) }}'>
                                    {{ $template->name }}
                                    <em>
                                        {{ $template->key }}<br>
                                        @if($template->application()->first())
                                        {{ $template->application()->first()->name }}
                                        @endif
                                    </em>
                                </td>
                                <td>
                                    <a href={{route('admin_report_template_destroy',$template)}}><i class="fa fa-trash" aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

                <br><br>
                <a href={{route('admin_report_template_create')}}>{{__('Create report template')}}</a>
            </div>
            </div>
        </div>


    </div>
</div>
</main>
@endsection
