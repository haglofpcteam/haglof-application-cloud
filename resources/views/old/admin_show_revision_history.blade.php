@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{$title}}</div>

            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">{{ __('User')}}</th>
                            <th scope="col">{{ __('Message')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if($history!="")
                        @foreach($history as $h)
                            <tr>
                                <th scope="row">{{ $h->userResponsible()->name }}</th>

                                @if($h->fieldName() == 'created_at' && !$h->old_value)
                                    <td>{{ __('Created :class_name at :new_value',['class_name' => getClassName($h),'new_value' => $h->newValue()])}}</td>
                                @elseif(!$h->old_value)
                                    <td>{{ __('Set :field_name of :class_name to :new_value',['field_name' => $h->fieldName(),'class_name' => getClassName($h),'new_value'=> $h->newValue()])}}</td>
                                @else
                                    <td>{{ __('Changed :field_name of :class_name from :old_value to :new_value',['field_name' => $h->fieldName(),'class_name' => getClassName($h),'old_value' => $h->oldValue(),'new_value'=> $h->newValue()])}}</td>
                                @endif
                            </tr>
                        @endforeach
                        @endif
                    </tbody>
                </table>
            </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
