@extends('layouts.app')

@section('content')
<main class="py-4">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-white bg-dark">{{ __('Register') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right"></label>
    
                                <div class="col-md-6">
                                    <input type="checkbox" name="terms_and_conditions">
                                    {{ __('I agree to the ') }}
                                    <a href="#" onclick="window.open('https://haglof.app/terms-and-conditions/','_blank','width=630,height=891');">
                                        {{ __('Terms and conditions') }}
                                    </a>
                                    {{ __(' and ') }}
                                    <a href="#" onclick="window.open('https://haglof.app/privacy-policy/','_blank','width=630,height=891');">
                                            {{ __('Privacy policy') }}
                                        </a>
                                </div>
                            </div>


                        <div class="form-group row">
                            <label for="ReCaptcha" class="col-md-4 col-form-label text-md-right"></label>
                            <div class="col-md-6">
                                {!! NoCaptcha::renderJs() !!}
                                {!! NoCaptcha::display() !!}
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>

                        <hr>

                        <div class="container">

                                <div class="row row-sm-offset-2 socialButtons justify-content-center">

                                    <!-- Facebook -->
                                    <div class="col-xs-2 col-sm-2">
                                            <a href="{{route('socialite_redirect_provider','facebook')}}" class="btn btn-lg btn-block btn-facebook border" data-toggle="tooltip" data-placement="top" title="Connect with Facebook">
                                            <i class="fab fa-facebook"></i>
                                            <span class="hidden-xs"></span>
                                            </a>
                                        </div>

                                    <!-- Twitter -->
                                    <div class="col-xs-2 col-sm-2">
                                        <a href="{{route('socialite_redirect_provider','twitter')}}" class="btn btn-lg btn-block btn-twitter border" data-toggle="tooltip" data-placement="top" title="Connect with Twitter">
                                        <i class="fab fa-twitter"></i>
                                        <span class="hidden-xs"></span>
                                        </a>
                                    </div>

                                    <!-- Google -->
                                    <div class="col-xs-2 col-sm-2">
                                            <a href="{{route('socialite_redirect_provider','google')}}" class="btn btn-lg btn-block btn-google border" data-toggle="tooltip" data-placement="top" title="Connect with Google">
                                            <i class="fab fa-google"></i>
                                            <span class="hidden-xs"></span>
                                            </a>
                                        </div>

                                </div><br>

                                <div class="row socialButtons justify-content-center">
                                    <!-- Github -->
                                    <div class="col-xs-2 col-sm-2">
                                            <a href="{{route('socialite_redirect_provider','github')}}" class="btn btn-lg btn-block btn-github border" data-toggle="tooltip" data-placement="top" title="Connect with Github">
                                            <i class="fab fa-github"></i>
                                            <span class="hidden-xs"></span>
                                            </a>
                                        </div>

                                    <!-- LinkedIn -->
                                    <div class="col-xs-2 col-sm-2">
                                           <a href="{{route('socialite_redirect_provider','linkedin')}}" class="btn btn-lg btn-block btn-linkedin border" data-toggle="tooltip" data-placement="top" title="Connect with LinkedIn">
                                            <i class="fab fa-linkedin"></i>
                                            <span class="hidden-xs"></span>
                                            </a>
                                        </div>

                                    <!-- Microsoft -->
                                    <div class="col-xs-2 col-sm-2">
                                            <a href="{{route('socialite_redirect_provider','microsoft')}}" class="btn btn-lg btn-block btn-microsoft border" data-toggle="tooltip" data-placement="top" title="Connect with Microsoft">
                                            <i class="fab fa-windows"></i>
                                            <span class="hidden-xs"></span>
                                            </a>
                                        </div>

                                </div><br>

                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
