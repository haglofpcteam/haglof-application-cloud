@extends('layouts.app')

@section('parent-url', URL::previous())

@section('content')

<main class="py-4">
<div class="container">
    <div class="row justify-content-center">

        <div class="col-md-12">
            <div class="card">
            <div class="card-header text-white bg-dark">{{ __('Superuser')}}</div>

            <div class="card-body">


                <table class="table">
                        <thead>
                            <tr>
                                <th colspan=3 scope="col">{{__('Applications')}}</th>
                            </tr>
                            <tr>
                                <th scope="col">{{ __('Id')}}</th>
                                <th scope="col">{{ __('Name')}}</th>
                                <th scope="col">{{ __('Instances')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($applications as $application)
                                <tr>
                                    <th scope="row" class="text-nowrap">{{ $application->id }}</th>
                                    <td><a href={{route('admin_show_application',$application)}}>{{ $application->name }}</a></td>
                                    <td>{{ $application->applicationInstances()->count() }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>


                    <table class="table">
                        <thead>
                            <tr>
                                <th colspan=3 scope="col">{{__('Plans')}}</th>
                            </tr>
                            <tr>
                                <th scope="col">{{ __('Id')}}</th>
                                <th scope="col">{{ __('Name')}}</th>
                                <th scope="col">{{ __('Application')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($plans as $plan)
                                <tr>
                                    <th scope="row" class="text-nowrap">{{ $plan->id }}</th>
                                    <td><a href={{route('admin_show_plan',$plan)}}>{{ $plan->name }}</a></td>
                                    @if($plan->application()->first())
                                    <td><a href={{route('admin_show_application',$plan->application()->first())}}>{{ $plan->application()->first()->name }}</a></td>
                                    @else
                                    <td></td>
                                    @endif
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <a href="{{route('list_subscriptions')}}">{{__('List subscriptions')}}</a>
            </div>
            </div>
        </div>

    </div>
</div>
</main>
@endsection
