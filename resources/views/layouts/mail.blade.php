<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body style="width: 100% !important;font-family: 'Open Sans', sans-serif;font-weight: 200;font-size: 12pt;">
    <div style="width: 100%;background-color: #608CBF;color: white;text-align: center;margin-bottom: 40px;">
        <div style="border-style: solid;border-color: white;border-width: 3px;display: inline-block;padding-top: 20px;padding-bottom: 20px;padding-left: 50px;padding-right: 50px;margin: 20px;font-family: 'Fjalla One', sans-serif;font-size: 4em;">
            {{ config('app.name') }}
        </div>
    </div>
    <div style="display: block;width: 50%;margin-left: auto;margin-right: auto;">
        @yield('content')
    </div>
    <div style="width: 100%;padding-top: 20px;padding-bottom: 20px;background-color: #608CBF;color: white;text-align: center;margin-top: 40px;">
        {{__('© 2019 Haglöf Sweden AB. All rights reserved.')}}
    </div>
</body>
</html>