@extends('layouts.mail')

@section('content')


<h4>{{ __('New subscription')}}</h4><br>
{{$text}}
<br><br>

<a href="{{$signed_route}}" style="border-radius: 3px;background-color: #608CBF;border-top: 10px solid #608CBF;border-right: 18px solid #608CBF;border-bottom: 10px solid #608CBF;border-left: 18px solid #608CBF;text-decoration: none;color: white;">
    Click here to Connect
</a>
<br>
<br>

<hr>
{{ __('If you’re having trouble clicking the link, copy and paste the URL below into your web browser:')}}<br>
<a href="{{$signed_route}}">{{$signed_route}}</a><br>


@endsection

