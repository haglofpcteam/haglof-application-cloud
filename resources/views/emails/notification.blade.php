@extends('layouts.mail')

@section('content')
<h4>{{ __('Notification')}}</h4>
{{$text}}
<br>

@isset($usermessage)
<h4>{{ __('Message from user:')}}</h4>
<br>
{{$usermessage}}<br>
<br>
@endisset

@endsection

