@extends('layouts.mail')

@section('content')

<h4>{{ __('Invitation')}}</h4><br>
{{$info}}<br>
<br>
<a href='{{route('invite_activate',[$invitee,$invitee->activation_token])}}' style="border-radius: 3px;background-color: #608CBF;border-top: 10px solid #608CBF;border-right: 18px solid #608CBF;border-bottom: 10px solid #608CBF;border-left: 18px solid #608CBF;text-decoration: none;color: white;">Click here to Verify</a><br>
<br>
@if($usermessage!="")
<h4>{{ __('Message from user:')}}</h4>
<br>
{{$usermessage}}<br>
<br>
@endif


<hr>
{{ __('If you’re having trouble clicking the link, copy and paste the URL below into your web browser:')}}<br>
<a href='{{route('invite_activate',[$invitee,$invitee->activation_token])}}'>{{route('invite_activate',[$invitee,$invitee->activation_token])}}</a><br>

@endsection
