const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
    node: { fs: 'empty' },
    resolve: {
      extensions: ['.js'],
      alias: {
        '@': __dirname + '/resources/js'
      },
    },
  })

mix.js('resources/js/app.js', 'public/js')
    .extract([
      'vue',
      'vue-router',
      'vue-axios',
      'vue-social-auth',
      'axios',
      'bootstrap-vue',
      'moment-timezone',
      'three',
      'three-orbitcontrols',
      'lodash',
      'jquery',
      'jquery-ui',
      'popper.js'
    ]).sourceMaps();

mix.scripts([
      'resources/stimulsoft/Scripts/stimulsoft.reports.js',
      'resources/stimulsoft/Scripts/stimulsoft.viewer.js',
      'resources/stimulsoft/Scripts/stimulsoft.designer.js',

  ], 'public/js/vanilla.js');

mix.autoload({  
    'jquery': ['$', 'window.jQuery', 'jQuery'],
});

if (mix.inProduction()) {
    mix.version();
}
